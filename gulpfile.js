var gulp = require('gulp'),
    git = require('gulp-git'),
    rename = require("gulp-rename"),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    paths = {
        js: ['app.js', 'lib/**/*']
    };

module.exports = {
    default: gulp.series(dist, watch),
    dist,
    watch
};

function dist() {
    return gulp.src(paths.js)
        .pipe(concat('sp-services.js'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add())
        .pipe(uglify({
            compress: true
        }))
        .pipe(rename('sp-services.min.js'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add());
}

function watch() {
    gulp.watch(paths.js, dist);
}
