(function(angular) {
    'use strict';

    angular.module('spServices', [
        'spApi',
        'spProductTags'
    ]);
})(angular);
(function (angular) {

    /**
     * Provider instance
     * @property {String, String} [getDataForAnalyticServiceProvider] - ng inject. should return { storeId: {String}, userId: {String} }
     */
    var provider = {};

    function spAIAnalyticService($filter, $controller, $rootScope, Api) {
        var self = this,
            _nameFilter = $filter('name');
        var serviceData = {};

        var locals;
        var _trafficParams;

        var script = document.createElement("script");

        self.init = init;

        function init() {

            locals = _getLocals();
            getAnalyticServiceProviderData().then(function(response){
                serviceData = response;
                if (serviceData){
                    if (serviceData.selectedProviderId == 2){
                        initConnectionToProvider();
                        initEventListeners();
                        _trafficParams = _getTrafficParams();
                    }
                }
            } )
        }

        function initConnectionToProvider(){
            var companyId = serviceData.fields.storeId.value;
            script.type = "text/javascript";
            script.async = true;
            script.src = "https://sf.exposebox.com/widget/predictl.min.js?c=" + companyId; //http(s): protocol depends on your website
            document.getElementsByTagName("head")[0].appendChild(script);
            window.predictlApi = window.predictlApi || function (cb) { var pa = window.predictlApi; pa.or = pa.or || []; pa.or.push(cb); }
        }

        function initEventListeners(){
            $rootScope.$on('loginForAnalyticServiceProvider', _setUserId);
            $rootScope.$on('firstCheckout', _onFirstCheckout);
            $rootScope.$on('finalCheckout', _onFinalCheckout);
            $rootScope.$on('productPage', _onProductPage);
            $rootScope.$on('categoryPage', _onCategoryPage);
            $rootScope.$on('productSearch', _onProductSearch);
            $rootScope.$on('orderCanceled', _onOrderCanceled);
            $rootScope.$on('cartCleared', _onCartCleared);
            $rootScope.$on('contactUs', _onContactUs);
            $rootScope.$on('loyaltyClubRegistration', _onLoyaltyClubRegistration);
            $rootScope.$on('customerRegistration', _onCustomerRegistration);
            $rootScope.$on('cart.lines.added', _onCartLinesAdded);
            $rootScope.$on('cart.lines.removed', _onCartLinesRemoved);
            $rootScope.$on('cart.lines.added.to.mylist', _onLinesAddedToMyList);
        }

        /**
         * Gets the analytic service provider data
         * @private
         *
         * @returns {Object}
         */
        function getAnalyticServiceProviderData(){
            var apiOptions = angular.copy(locals.apiOptions || {});
            apiOptions.id = 'spAnalyticServiceProvider';
            apiOptions.loadingElement = (locals.apiOptions || {}).loadingElement;
            return Api.request({
                url: '/v2/retailers/:rid/analytic-services/getSelectedAnalyticServiceProviderData'
            }, apiOptions).then(function(response) {
                return response;
            }).catch(function(e){
                console.log(e);
            });

        }


        /**
         * On browse product page
         * @private
         * @param {Event} event
         * @param {String} productId
         */
        function _onProductPage(event, productId) {
            window.predictlApi(function () {
                window.predictlApi.setProducts([productId]);
            });
        }

        /**
         * On browse category
         * @private
         * @param {Event} event
         * @param {Array} categories
         */
        function _onCategoryPage(event, categories){
            var categoryNames = [];
            angular.forEach(categories, function(category){
                categoryNames.push(category.names[locals.languageId])
            })
            window.predictlApi(function () {
                window.predictlApi.setCategories(categoryNames);
            });
        }

        function _stopListeners(stopListeners) {
            angular.forEach(stopListeners, function (stopListener) {
                stopListener();
            });
        }

        /**
         * On adding products to MyList
         * @private
         * @param {Event} event
         * @param {Array} products
         */
        function _onLinesAddedToMyList(event, products){
            if(products){
                angular.forEach(products, function(product) {
                    window.predictlApi(function () {
                        window.predictlApi.events.addToWishlist(product);
                     });
                });
            }
        }

        /**
         * On login
         * @private
         */
        function _setUserId() {
            var locals = _getLocals();
            if (locals && locals.userId) {
                window.predictlApi(function () {
                    window.predictlApi.setCustomerData({
                        customerId: locals.userId.toString(),
                        email: locals.username
                    })
                });
            }
        }


        function _getLocals() {
            if (!provider.getLocals || (!angular.isFunction(provider.getLocals) && !angular.isArray(provider.getLocals))) {
                throw new Error('spAnalyticsProvider.getLocals must be implemented');
            }

            return $controller(provider.getLocals);
        }

        /**
         * On cart lines added
         * @private
         *
         * @param {Event} event
         * @param {Object} data
         */
        function _onCartLinesAdded(event, data) {
            window.predictlApi(function () {
                if(data){
                    var unitPrice =  data.unitPrice.toString().split('.');

                    if(unitPrice.length > 1 && unitPrice[1].length > 5) {
                        data.unitPrice = data.unitPrice.toFixed(5);
                    }
                    window.predictlApi.events.sendEvent("addToCart",{productId: data.productId,
                        quantity: data.quantity,unitPrice: data.unitPrice, sourceType: data.sourceType, sourceValue: data.sourceValue, cartId: data.cartId});
                }
            });
        }

        /**
         * On cart lines removed
         * @private
         *
         * @param {Event} event
         * @param {Object} data
         */
        function _onCartLinesRemoved(event, data) {
            window.predictlApi(function () {
                if(data){
                    window.predictlApi.events.sendEvent("removeFromCart",{productId: data.productId,quantity: data.quantity,cartId: data.cartId});
                }
            });
        }

        /**
         * On first checkout
         * @private
         */
        function _onFirstCheckout() {
            window.predictlApi(function () {
                window.predictlApi.events.sendEvent('checkout',{type:'cartPage'});
            });
            _setUserId();
        }

        /**
         * On final checkout
         * @private
         */
        function _onFinalCheckout(event, orderId, totalPrice, products, sourceType, sourceValue) {
            var totalPriceSplitted =  totalPrice.toString().split('.');

            if(totalPriceSplitted && totalPriceSplitted[1] && totalPriceSplitted[1].length > 5) {
                totalPrice = totalPrice.toFixed(5);
            }
            window.predictlApi(function () {
                predictlApi.events.sendEvent("conversion", {
                    cartProducts: products,
                    orderId: orderId,
                    totalPrice: totalPrice,
                    sourceType: sourceType,
                    sourceValue: sourceValue
                });
            });
        }

        /**
         * On order canceled
         * @private
         * @param {Event} event
         * @param {String} orderId
         */
        function _onOrderCanceled(event, orderId) {
            window.predictlApi(function () {
                predictlApi.events.conversion({}, orderId, 0);
            });
        }

        /**
         * On order canceled
         * @private
         * @param {Event} event
         * @param {String} cartId
         */
        function _onCartCleared(event, cartId) {
            var locals = _getLocals();
            if(locals && locals.userId && cartId) {
                window.predictlApi(function () {
                        window.predictlApi.events.sendEvent('clearCart', {customerId: locals.userId, cartId: cartId});
                    }
                )
            }
        }

        /**
         * On applying contact us
         * @private
         */
        function _onContactUs(event, userEmail, userName) {
            window.predictlApi(function () {
                    window.predictlApi.events.sendEvent('contactCS',{flag:"true", userEmail: userEmail, userName: userName });
                }
            )
        }

        /**
         * On Loyalty club registration
         * @private
         * @param {Event} event
         * @param {String} customerId
         */
        function _onLoyaltyClubRegistration(event, customerId) {
            window.predictlApi(function () {
                    window.predictlApi.events.sendEvent('clubMembershipReg', {customerId: customerId});
                }
            )
        }

        /**
         * On customer registration
         * @private
         * @param {Event} event
         * @param {Object} newCustomer
         */
        function _onCustomerRegistration(event, newCustomer) {
            var sourceType = "internal";
            var sourceValue = "Registration";
            if (_trafficParams && _trafficParams.utm_source){
                sourceType = _trafficParams.utm_source;
                sourceValue = _trafficParams.utm_campaign_id;
            }
            window.predictlApi(function () {
                window.predictlApi.events.sendEvent('newUserRegistration',
                    {
                        using: "onsite",
                        customerId: newCustomer.id,
                        email: newCustomer.email,
                        sourceType: sourceType,
                        sourceValue: sourceValue
                    });
                window.predictlApi.setCustomerData({
                    customerId: newCustomer.id.toString(),
                    email: newCustomer.email
                })
            });
        }

        /**
         * On product search
         * @private
         * @param {Event} event
         * @param {String} query
         * @param {Array} products
         * @param {Object} filters
         */
        function _onProductSearch(event, query, products, filters) {
            window.predictlApi(function () {
                window.predictlApi.events.sendEvent("searchAndFilter",{
                    query: query, //The search query
                    products: products, //Top 6 product results
                    categories: filters.categoryFilters,
                    productTags: filters.productTags,
                    brandFilters: filters.brandFilters
                });
            });
        }

        function _getTrafficParams() {
            var trafficParams = {};
            if (document.referrer && document.referrer !== document.location.href) {
                if (document.referrer.indexOf(document.location.hostname) > -1) {
                    return;
                }
                trafficParams.dr = document.referrer;
            }
            if (document.location.search) {
                var searchParams = document.location.search.substring(1).split('&');
                angular.forEach(searchParams, function (param) {
                    var parts = param.split('=');
                    trafficParams[parts[0]] = parts[1];
                });
            }
            return Object.keys(trafficParams).length ? trafficParams : undefined;
        }

    }

    angular.module('spServices')
        .config(['$provide', function ($provide) {
            provider = $provide.service('spAIAnalyticService', [
                '$filter', '$controller', '$rootScope', 'Api',
                spAIAnalyticService
            ]);
        }]);
})(angular);


(function (angular) {
    /**
     * @typedef {object} Locals
     *
     * @property {boolean} isRegularPriceWithTax
     * @property {boolean} includeTaxInPrice
     * @property {number} languageId
     * @property {boolean} withDeliveryProduct
     * @property {array<number>} loyaltyClubIds
     * @property {number} [userId]
     * @property {object} [apiOptions]
     * @property {number} [deliveryItemsLimit]
     */

    /**
     * @typedef {object} Line
     * @property {object} product
     * @property {boolean} isCase
     * @property {boolean} isPseudo
     * @property {boolean} removed
     * @property {number} [totalPriceWithTax]
     * @property {number} [totalPriceWithoutTax]
     * @property {number} [totalPriceForView]
     * @property {number} [totalPriceForViewPotential]
     * @property {number} [finalPriceWithTax]
     * @property {number} [finalPriceWithTaxPotential]
     * @property {number} [finalPriceWithoutTax]
     * @property {number} [finalPriceWithoutTaxPotential]
     * @property {number} [finalPriceForView]
     * @property {number} [finalPriceForViewPotential]
     * @property {number} [totalGifts]
     * @property {number} [totalGiftsPotential]
     * @property {number} [totalSimulateClubsGifts]
     * @property {number} [totalSimulateClubsGiftsPotential]
     * @property {number} [totalClubsGifts]
     * @property {number} [totalClubsGiftsPotential]
     * @property {number} [totalGiftsWithTax]
     * @property {number} [totalGiftsWithoutTax]
     * @property {number} [totalGiftsForView]
     * @property {number} [totalGiftsWithTaxPotential]
     * @property {number} [totalGiftsWithoutTaxPotential]
     * @property {number} [totalGiftsForViewPotential]
     * @property {number} [totalClubsGiftsWithTax]
     * @property {number} [totalClubsGiftsWithoutTax]
     * @property {number} [totalClubsGiftsForView]
     * @property {number} [totalClubsGiftsWithTaxPotential]
     * @property {number} [totalClubsGiftsWithoutTaxPotential]
     * @property {number} [totalClubsGiftsForViewPotential]
     * @property {number} [totalSimulateClubsGiftsWithTax]
     * @property {number} [totalSimulateClubsGiftsWithoutTax]
     * @property {number} [totalSimulateClubsGiftsForView]
     * @property {number} [totalSimulateClubsGiftsWithTaxPotential]
     * @property {number} [totalSimulateClubsGiftsWithoutTaxPotential]
     * @property {number} [totalSimulateClubsGiftsForViewPotential]
     * @property {number} [sort]
     * @property {number} [type]
     * @property {number} [quantity]
     * @property {string} [comments]
     * @property {number} [productPropertyValueId]
     * @property {object} [productPropertySelectedValue]
     */

    /**
     * Service name
     */
    var serviceName = 'SpCartService';

    /**
     * Provider instance
     */
    var provider;

    /**
     * Max line quantity
     */
    var MAX_LINE_QUANTITY = 999999;

    /**
     * Service
     */
    function SpCartService($rootScope, $injector, $filter, $timeout, $q, Api, SpProductTags, EmbedHistoryRetailer, SP_SERVICES, PRODUCT_TAG_TYPES) {
        var self = this;

        self.init = init;
        self.clear = clearCart;
        self.addLine = addLine;
        self.addLines = addLines;
        self.removeLine = removeLine;
        self.removeLines = removeLines;
        self.quantityInterval = quantityInterval;
        self.minusQuantity = minusQuantity;
        self.plusQuantity = plusQuantity;
        self.quantityChanged = quantityChanged;
        self.commentsChanged = commentsChanged;
        self.productPropertyValueChanged = productPropertyValueChanged;
        self.getProduct = getProduct;
        self.getProducts = getProducts;
        self.getLines = getLines;
        self.isUnitsWeighable = isUnitsWeighable;
        self.save = saveCart;
        self.setTimeTravel = setTimeTravel;
        self.keyByLine = getLineKey;
        self.replaceCart = replaceCart;
        self.createCart = createCart;
        self.validateMinimumCost = validateMinimumCost;
        self.validateDeliveryItemsLimit = validateDeliveryItemsLimit;
        self.getEBTEligible = getEBTEligible;
        self.getRemainingPayment = getRemainingPayment;
        self.disableLoyalty = disableLoyalty;
        self.checkEBTEligibleCartChange = checkEBTEligibleCartChange;
        self.lines = {};

        var deletedLines = {},
            dirtyLines = {},
            _saveCartDefer,
            _trafficParams = _getTrafficParams(),
            _isLoyaltyDisabled = false,
            _roundCurrencyFilter;

        /**
         * The cart sale date
         * @type {Date}
         */
        self.saleDate = null;

        /**
         * The cart id in data base
         * @type {number}
         */
        self.serverCartId = undefined;

        /**
         * If is edit order cart
         * @type {number}
         */
        self.editOrderId = null;

        /**
         * Special Reminders data. Reset on each server response.
         * @type {Object}
         */
        self.specialReminders = {};

        /**
         * Object with the products that has been changed in the last 3 seconds.
         * Reset on each server response.
         * @type {Object<string, Promise>}
         */
        self.changedProducts = {};

        if (provider.initCart && (angular.isFunction(provider.initCart) || angular.isArray(provider.initCart))) {
            $injector.invoke(provider.initCart, self, { cart: self });
        }

        /**
         * To sort lines by added order
         * @type {number}
         */
        var sortNumber = 0;
        // set sort number from local cart lines
        angular.forEach(self.lines, function (line) {
            if (!('sort' in line)) {
                line.sort = sortNumber++;
            } else if (angular.isNumber(line.sort) && sortNumber <= line.sort) {
                sortNumber = line.sort + 1;
            }
        });

        /***
         * Validate Minimum Cost per cart
         * @param {Number} minimumOrderPrice
         * @param {Boolean} notIncludeSpecials
         * @returns {Promise}
         */
        function validateMinimumCost(minimumOrderPrice, notIncludeSpecials ){
            var minimumOrderPriceNum = Number(minimumOrderPrice),
                price = notIncludeSpecials ? self.total.priceWithoutPromotionProductsForView : self.total.finalPriceForView;

            if (_roundDecimal(price) < (!isNaN(minimumOrderPriceNum) && minimumOrderPriceNum > 0 ? _roundDecimal(minimumOrderPriceNum) : 0)) {
                return $q.reject(price);
            }

            return $q.resolve();
        }


        /**
         * Round number to two decimal digits
         * @private
         *
         * @param {number} num
         *
         * @return {number}
         */
        function _roundDecimal(num) {
            if (!_roundCurrencyFilter) {
                try {
                    _roundCurrencyFilter = $filter('roundCurrency');
                } catch (err) {
                    console.warn('sp-services: roundCurrency filter wasn\'t found, using default round');
                    _roundCurrencyFilter = function (amount) {
                        return Math.round(amount * 1000) / 1000;
                    };
                }
            }

            return _roundCurrencyFilter(num);
        }

        /**
         * Gets the locals
         * @private
         *
         * @returns {Promise<Locals>}
         */
        function _getLocals() {
            var shouldReturn = 'a promise with the following object: { loyaltyClubIds, languageId, includeTaxInPrice, withDeliveryProduct, userId, [apiOptions], [deliveryItemsLimit] }';
            if (!provider.getLocals || (!angular.isFunction(provider.getLocals) && !angular.isArray(provider.getLocals))) {
                throw new Error(serviceName + 'Provider.getLocals must be implemented (returns ' + shouldReturn + ' )');
            }

            var promise = $injector.invoke(provider.getLocals, self);
            if (!promise || !promise.then || !angular.isFunction(promise.then)) {
                throw new Error(serviceName + 'Provider.getLocals must return ' + shouldReturn);
            }

            return promise.then(function(locals) {
                if (typeof locals.includeTaxInPrice !== 'boolean') {
                    locals.includeTaxInPrice = locals.isRegularPriceWithTax;
                }

                return locals;
            });
        }

        /**
         * Get line key by is case
         * @param {Line} line
         * @returns {string}
         * @public
         */
        function getLineKey(line) {
            return (line.product.id ? line.product.id.toString() : '') + (!!line.isCase ? '1' : '0');
        }

        /**
         * Set line prices
         * @param {Line} line
         * @param {Locals} locals
         * @returns {Line}
         * @private
         */
        function _setLinePrices(line, locals) {
            var price = 0;
            if (!line.isPseudo && !(line.product.isCoupon && line.isCouponPriceZero === 'true') && line.product.branch) {
                var packInformationExist = line.product.branch.packQuantity && line.product.branch.packPrice;
                if (packInformationExist) {
                    price = (line.product.branch.packPrice / line.product.branch.packQuantity)  * line.quantity;
                } else {
                    if (!!line.soldBy && line.soldBy === $rootScope.PRODUCT_DISPLAY.WEIGHT.name) {
                        price = (line.isCase ? line.product.branch.case.price : line.product.branch.regularPrice) * (!line.isCase && isUnitsWeighable(line) ? line.product.weight : 1) * (line.quantity / (!line.isCase && isUnitsWeighable(line) ? line.product.weight : 1));
                    } else {
                        price = (line.isCase ? line.product.branch.case.price : line.product.branch.regularPrice) * (!line.isCase && isUnitsWeighable(line) ? line.product.weight : 1) * line.quantity;
                    }
                }
            }

            _setLinePriceProp(line, 'totalGifts', line.totalGifts, line.totalGiftsPotential, locals);
            _setLinePriceProp(line, 'totalClubsGifts', line.totalClubsGifts, line.totalClubsGiftsPotential, locals);
            _setLinePriceProp(line, 'totalPrice', price, price, locals);

            if (line.totalSimulateClubsGifts){
                _setLinePriceProp(line, 'totalSimulateClubsGifts', line.totalSimulateClubsGifts, line.totalSimulateClubsGiftsPotential, locals);
            }

            _setPropTypes(line, 'finalPrice', function(type) {
                return line['totalPrice' + type] - line['totalGifts' + type] - line['totalClubsGifts' + type];
            });

            return line;
        }

        /**
         * Sets the price prop for all prices types and for all types in potential
         * @private
         *
         * @param {Object} obj
         * @param {String} propName
         * @param {Function} value
         */
        function _setPropTypes(obj, propName, value) {
            angular.forEach(['WithTax', 'WithoutTax', 'ForView'], function(type) {
                obj[propName + type] = value(type);
                obj[propName + type + 'Potential'] = value(type + 'Potential');
            });
        }

        /**
         * Set line price prop fields
         * @param {Line} line
         * @param {string} key
         * @param {number} price
         * @param {number} [potentialPrice]
         * @param {Locals} locals
         * @private
         */
        function _setLinePriceProp(line, key, price, potentialPrice, locals) {
            var markupPercentage = (line.product.branch && line.product.branch.markupPercentage || 0);
            line[key + 'Markup'] = price * markupPercentage;
            line[key + 'MarkupPotential'] = potentialPrice * markupPercentage;

            var taxAmount = ((line.product.branch && line.product.branch.taxAmount || 0) + 1);
            if (locals.isRegularPriceWithTax) {
                line[key + 'WithTax'] = price + line[key + 'Markup'];
                line[key + 'WithoutTax'] = price / taxAmount;

                line[key + 'WithTaxPotential'] = potentialPrice;
                line[key + 'WithoutTaxPotential'] = potentialPrice / taxAmount;
            } else {
                line[key + 'WithTax'] = (price + line[key + 'Markup']) * taxAmount;
                line[key + 'WithoutTax'] = price;
                if (key === 'totalPrice' && line.product && line.product.branch && line.product.branch.linkedProductPrice) {
                  line[key + 'WithTax'] = ((price - (line.product.branch.linkedProductPrice * line.quantity) + line[key + 'Markup']) * taxAmount) + (line.product.branch.linkedProductPrice * line.quantity)
                }
                line[key + 'WithTaxPotential'] = potentialPrice * taxAmount;
                line[key + 'WithoutTaxPotential'] = potentialPrice;
            }

            var taxKey = locals.includeTaxInPrice ? 'WithTax' : 'WithoutTax';
            line[key + 'ForView'] = line[key + taxKey];
            line[key + 'ForViewPotential'] = line[key + taxKey + 'Potential'];
        }

        /**
         * @constant
         * @type {Object}
         */
        var TOTAL_DEFAULT = {
            lines: 0,
            actualLines: 0,
            coupons: 0,
            priceWithTax: 0,
            priceWithoutTax: 0,
            priceForView: 0,
            giftsWithTax: 0,
            giftsWithoutTax: 0,
            giftsForView: 0,
            giftsWithTaxPotential: 0,
            giftsWithoutTaxPotential: 0,
            giftsForViewPotential: 0,
            clubsGiftsWithTax: 0,
            clubsGiftsWithoutTax: 0,
            clubsGiftsForView: 0,
            clubsGiftsWithTaxPotential: 0,
            clubsGiftsWithoutTaxPotential: 0,
            clubsGiftsForViewPotential: 0,
            simulateClubsGiftsWithTax: 0,
            simulateClubsGiftsWithoutTax: 0,
            simulateClubsGiftsForView: 0,
            simulateClubsGiftsWithTaxPotential: 0,
            simulateClubsGiftsWithoutTaxPotential: 0,
            simulateClubsGiftsForViewPotential: 0,
            priceWithoutPromotionProductsForView: 0,
            finalPriceWithTax: 0,
            finalPriceWithoutTax: 0,
            finalPriceForView: 0,
            finalPriceWithTaxPotential: 0,
            finalPriceWithoutTaxPotential: 0,
            finalPriceForViewPotential: 0,
            multiPassGift: 0,
            tax: 0,
            markup: 0
        };

        /**
         * set linePrice with tax but withot deposit
         * @param {Number} finalPriceWithoutTax
         * @param {Number} finalPriceWithTax
         * @param {Number} lineDepositPrice
         * @param {Number} lineDepositPrice
         * @param {Number} lineQuantity
         * @param {Number} lineTax
         * @param {Locals} [locals]
         * @param {Boolean} round,
         * @param {Number} [caseItems]
         * @private
         */
        function _lineFinalPriceWithTaxWithoutDepositAmount(finalPriceWithoutTax, finalPriceWithTax, lineDepositPrice, lineTax, lineQuantity, locals, round, caseItems) {
            var taxAmount = ((lineTax || 0) + 1);
            var valueToReturn = finalPriceWithTax
            caseItems = caseItems || 1;
            if(!locals.includeTaxInPrice){
                valueToReturn = lineDepositPrice ? ((finalPriceWithoutTax - (lineDepositPrice * caseItems * lineQuantity)) * taxAmount) + (lineDepositPrice * caseItems * lineQuantity) : finalPriceWithTax;
            }
            return round ? _roundDecimal(valueToReturn) : valueToReturn;
        }

        /**
         * Sets the cart's totals
         * @param {Locals} [locals]
         * @private
         */
        function _setCartTotal(locals) {
            var prevCrossProductTagTypes = self.total && self.total.crossProductTagTypes;

            self.total = angular.copy(TOTAL_DEFAULT);
            self.total.deliveryCost = angular.copy(TOTAL_DEFAULT);
            self.total.serviceFee = angular.copy(TOTAL_DEFAULT);
            self.total.calculating = true;

            // TODO: should be exposed instead of self.total
            var totalByType = {},
                multiPassGiftSum = {
                    finalPriceForView: 0,
                    totalPriceForView: 0,
                    finalPriceWithTax: 0,
                    finalPriceForViewWithDeliveryFee: 0,
                };

            totalByType[SP_SERVICES.CART_LINE_TYPES.DELIVERY] = self.total.deliveryCost;
            totalByType[SP_SERVICES.CART_LINE_TYPES.SERVICE_FEE] = self.total.serviceFee;

            if (!locals) {
                self.total.crossProductTagTypes = prevCrossProductTagTypes;
                return _getLocals().then(_setCartTotal);
            }

            var productsTagTypesCount = { all: 0, byType: {} };
            angular.forEach(self.lines, function (line) {
                line.id = getLineKey(line);
                var totalObj = totalByType[line.type] || self.total;
                if (line.type !== SP_SERVICES.CART_LINE_TYPES.COUPON) {
                    totalObj.lines += line.quantity ? 1 : 0;
                    totalObj.actualLines++;
                } else {
                    totalObj.coupons++;
                }

                if(line.gifts && line.gifts.length) {
                    line.gifts.forEach(function (gift, index) {
                        if(gift.promotion && gift.promotion.localId && gift.promotion.localId.toLowerCase().includes(SP_SERVICES.PROMOTION_ID.COLLEAGUE)) {
                            line.gifts[index].promotion.loyalty = line.gifts[index].promotion.loyalty || [];
                            line.gifts[index].promotion.loyalty.push(line.gifts[index].promotion.localId);

                            _setPropTypes(totalObj, 'colleague', function(type) {
                                return _roundDecimal(totalObj['colleague' + type] || 0) + (line.gifts[index].discount || 0);
                            });
                        }
                    })
                }

                // This should be before setting the totalObj
                if (line.product && line.product.branch && line.product.branch.linkedProductPrice && $rootScope.config.retailer.isRegularPriceWithTax) {
                    const taxAmount = ((line.product && line.product.branch && line.product.branch.taxAmount) ? line.product.branch.taxAmount : 0);
                    const priceWithoutLinkedProductPrice = line.product.branch.linkedProductPrice * line.quantity;
                    line.finalPriceWithoutTax = (line.finalPriceForView - priceWithoutLinkedProductPrice) / (1 + taxAmount) + (priceWithoutLinkedProductPrice);
                }

                if (!line.type || line.type === SP_SERVICES.CART_LINE_TYPES.PRODUCT) {
                    _addLineProductTagTypes(line, productsTagTypesCount);
                }

                _setPropTypes(totalObj, 'price', function(type) {
                    return _roundDecimal(totalObj['price' + type] || 0) + (line['totalPrice' + type] || 0);
                });

                _setPropTypes(totalObj, 'gifts', function(type) {
                    return _roundDecimal(totalObj['gifts' + type] || 0) + (line['totalGifts' + type] || 0);
                });

                _setPropTypes(totalObj, 'clubsGifts', function(type) {
                    return _roundDecimal(totalObj['clubsGifts' + type] || 0) + (line['totalClubsGifts' + type] || 0);
                });

                _setPropTypes(totalObj, 'simulateClubsGifts', function(type) {
                    return _roundDecimal(totalObj['simulateClubsGifts' + type] || 0) + (line['totalSimulateClubsGifts' + type] || 0);
                });

                if (!line.isProductOutOfStock) {
                    _setPropTypes(totalObj, 'finalPrice', function(type) {
                        return _roundDecimal((totalObj['finalPrice' + type] || 0) + (line['finalPrice' + type] || 0));
                    });
                }

                if (line.gifts && line.gifts.length && line.gifts[0].promotion.loyalty && line.gifts[0].promotion.loyalty.length) {
                    var id = line.gifts[0].promotion.loyalty[0];
                    totalObj.loyaltyClubs = totalObj.loyaltyClubs || {};
                    totalObj.loyaltyClubs[id] =  totalObj.loyaltyClubs[id] || {};
                    _setPropTypes( totalObj.loyaltyClubs[id], 'clubGifts', function(type) {
                        return (totalObj.loyaltyClubs[id]['clubGifts' + type] || 0) + (line['totalClubsGifts' + type] || 0);
                    });
                }
                var finalPriceWithTaxForEbtEligibility = (line.type === SP_SERVICES.CART_LINE_TYPES.PRODUCT || line.type === SP_SERVICES.CART_LINE_TYPES.COUPON || line.type === SP_SERVICES.CART_LINE_TYPES.SERVICE_FEE || line.type === SP_SERVICES.CART_LINE_TYPES.DELIVERY) ?
                    _lineFinalPriceWithTaxWithoutDepositAmount(
                        line.finalPriceWithoutTax,
                        line.finalPriceWithTax,
                        (line.product && line.product.branch && line.product.branch.linkedProductPrice || 0),
                        (line.product && line.product.branch && line.product.branch.taxAmount || 0),
                        line.quantity,
                        locals,
                        false,
                        line.isCase ? line.product && line.product.branch && line.product.branch.case && line.product.branch.case.items : 1) : 0;
                finalPriceWithTaxForEbtEligibility = _roundDecimal(finalPriceWithTaxForEbtEligibility + _getAdditionalTaxFromTaxableCoupons(line));
                var finalPriceWithTax = line.type === SP_SERVICES.CART_LINE_TYPES.PRODUCT || line.type === SP_SERVICES.CART_LINE_TYPES.COUPON ? finalPriceWithTaxForEbtEligibility : 0;

                line.finalPriceWithoutTax = _roundDecimal(line.finalPriceWithoutTax);
                line.totalPriceMarkup = _roundDecimal(line.totalPriceMarkup);
                line.finalPriceWithTaxForEbtEligibility = finalPriceWithTaxForEbtEligibility;
                line.finalPriceWithTax = finalPriceWithTax;
                totalObj.tax += _roundDecimal(finalPriceWithTax - line.finalPriceWithoutTax - line.totalPriceMarkup || 0);
                totalObj.markup += line.totalPriceMarkup || 0;

                _calculateMultiPassTotals(multiPassGiftSum, line);

                // line with only global gifts considered as "without promotion"
                var isWithoutPromotion = !line.gifts || !line.gifts.find(function(gift) {
                    return !gift.isGlobal;
                });
                if (isWithoutPromotion) {
                    totalObj.priceWithoutPromotionProductsForView += line.totalPriceForView || 0;
                }
            });

            _applyMultiPassDiscount(multiPassGiftSum);
            _setCrossProductTagTypes(productsTagTypesCount, prevCrossProductTagTypes);

            self.total.calculating = false;

            var finalPriceWithTax = 0;
            var deliveryLine = null;
            angular.forEach(self.lines, function (line) {
                if (line.type === SP_SERVICES.CART_LINE_TYPES.DELIVERY) {
                    deliveryLine = line;
                }
                if (line.type !== SP_SERVICES.CART_LINE_TYPES.PRODUCT &&
                    line.type !== SP_SERVICES.CART_LINE_TYPES.COUPON) return;

                if(!isNaN(line.finalPriceWithoutTax) && !isNaN(line.finalPriceWithoutTax)) {
                    finalPriceWithTax += _lineFinalPriceWithTaxWithoutDepositAmount(
                        line.finalPriceWithoutTax,
                        line.finalPriceWithTax,
                        (line.product && line.product.branch && line.product.branch.linkedProductPrice || 0),
                        (line.product && line.product.branch && line.product.branch.taxAmount || 0),
                        line.quantity,
                        locals,
                        true,
                        line.isCase ? line.product && line.product.branch && line.product.branch.case && line.product.branch.case.items : 1);
                }
            });
            self.total.finalPriceWithTax = finalPriceWithTax;

            if ($rootScope.config.retailer.settings.includeDeliveryFeeInCart === 'true') {
                self.total.finalPriceForViewWithDeliveryFee = getDeliveryPrice(self.total, totalByType, deliveryLine);
            }
            $rootScope.$emit('cart.totalCalculated');
        }

        function _getAdditionalTaxFromTaxableCoupons(line){
            var additionalTax = 0;
            if(line.gifts && (line.gifts.length > 0)){
                angular.forEach(line.gifts, function(gift){
                    if(gift.isTaxable === false){
                        var taxAmount = ((line.product.branch && line.product.branch.taxAmount || 0) + 1);
                        var taxPercentage = (taxAmount === 0) ? 0 : (taxAmount - 1);
                        additionalTax += gift.discount * taxPercentage;
                    }
                })
            }
            return additionalTax;
        }

        function getDeliveryPrice(total, totalByType, deliveryLine) {
            if(isNaN(total.finalPriceWithTax)) {
                return;
            }
            var result = total.finalPriceWithTax;
            if (totalByType[SP_SERVICES.CART_LINE_TYPES.DELIVERY] && totalByType[SP_SERVICES.CART_LINE_TYPES.DELIVERY].finalPriceForView) {
                result += totalByType[SP_SERVICES.CART_LINE_TYPES.DELIVERY].finalPriceForView;
            } else if (deliveryLine && deliveryLine.product && deliveryLine.product.branch &&
                deliveryLine.product.branch.regularPrice && deliveryLine.product.branch.regularPrice > 0) {
                result += deliveryLine.product.branch.regularPrice;
            }

            return result;
        }


        //Init the totals
        _setCartTotal();

        /**
         * Add the given line to the given product tag type count map
         * @private
         *
         * @param {Object} line
         * @param {{all: number, byType: Object}} productsTagTypesCount
         */
        function _addLineProductTagTypes(line, productsTagTypesCount) {
            if (line.removed) {
                return;
            }

            productsTagTypesCount.all++;

            if (!line.product || !line.product.productTagsData) {
                return;
            }

            line.productTagTypes = {};
            angular.forEach(line.product.productTagsData, function(productTag) {
                if (SpProductTags.isProductTagRelationActive(productTag)) {
                    line.productTagTypes[productTag.typeId || PRODUCT_TAG_TYPES.SIMPLE] = true;
                }
            });

            angular.forEach(line.productTagTypes, function(bool, typeId) {
                productsTagTypesCount.byType[typeId] = (productsTagTypesCount.byType[typeId] || 0) + 1;
            });
        }

        /**
         * Set the cross cart product tag types by the given count map
         * @private
         *
         * @param {{all: number, byType: Object}} productsTagTypesCount
         * @param {Object} prevCrossProductTagTypes
         */
        function _setCrossProductTagTypes(productsTagTypesCount, prevCrossProductTagTypes) {
            var prevValue = Object.keys(prevCrossProductTagTypes || {}),
                newLength = 0;

            // when the entire cart products contains the same product tag type
            self.total.crossProductTagTypes = {};

            angular.forEach(productsTagTypesCount.byType, function(count, typeId) {
                if (count === productsTagTypesCount.all) {
                    newLength++;
                    self.total.crossProductTagTypes[typeId] = Number(typeId);
                }
            });

            if (productsTagTypesCount.all) {
                // represents a non empty cart
                self.total.crossProductTagTypes.none = true;
                newLength++;
            }

            var isChanged = newLength !== prevValue.length;
            if (!isChanged) {
                angular.forEach(prevValue, function(key) {
                    if (!self.total.crossProductTagTypes[key]) {
                        isChanged = true;
                    }
                });
            }

            if (isChanged) {
                $rootScope.$emit('cart.crossProductTagTypes.change', {
                    value: self.total.crossProductTagTypes,
                    oldValue: prevCrossProductTagTypes
                });
            }
        }

        /**
         * those products have MultiPass discount
         */
        function _calculateMultiPassTotals(multiPassGiftSum, line) {
            //== if this line is not a Product type
            if( line.type === SP_SERVICES.CART_LINE_TYPES.COUPON ) {
                return;
            }

            var departmentId = line.product && line.product.department && line.product.department.id || null;

            // //== check if this product's department is forbidden for discount
            if( departmentId && Array.isArray(self.multiPassNoDiscountDepartments) && self.multiPassNoDiscountDepartments.indexOf(departmentId) !== -1 ) {
                return;
            }

            //== if this product is not forbidden for discount - calculate totals for further discount
            multiPassGiftSum.finalPriceForView += _roundDecimal(line.finalPriceForView || 0);
            multiPassGiftSum.totalPriceForView += _roundDecimal(line.totalPriceForView || 0);
            multiPassGiftSum.finalPriceWithTax += _roundDecimal(line.finalPriceWithTax || 0);
        }

        /**
         * Apply estimated MultiPass discount
         */
        function _applyMultiPassDiscount(multiPassGiftSum) {
            if(self.multiPassDiscount && self.multiPassObligo && multiPassGiftSum) {

                //== calculate maximum discount amount this user can get
                self.total.multiPassGift = multiPassGiftSum.finalPriceForView * self.multiPassDiscount / 100;
                var actualDiscountPercent = self.multiPassDiscount;

                //== if remaining obligo is smaller than discount user should get
                if( self.multiPassObligo - self.total.multiPassGift < 0 ) {

                    //== then the maximum discount user can get is exactly the remaining obligo
                    self.total.multiPassGift = self.multiPassObligo;

                    //== and the actual discount for further total calculation changes
                    actualDiscountPercent = (self.multiPassObligo / self.total.finalPriceForView) * 100;
                }

                //== re-calculate all total PriceForView using totals from products WITH discount
                self.total.finalPriceForView = self.total.finalPriceForView - self.total.multiPassGift;
                self.total.totalPriceForView = self.total.totalPriceForView - multiPassGiftSum.totalPriceForView * (actualDiscountPercent / 100);
                self.total.finalPriceWithTax = self.total.finalPriceWithTax - multiPassGiftSum.finalPriceWithTax * (actualDiscountPercent / 100);
            }
        }

        /**
         * Add line key to deleted lines
         * @param {Line} line
         * @private
         */
        function _addToDeletedProduct(line) {
            deletedLines[getLineKey(line)] = {
                isCase: line.isCase,
                productId: line.product.id,
                isPseudo: line.isPseudo,
                type: line.type
            };
        }

        /**
         * Remove line key from deleted lines
         * @param {Line} line
         */
        function _removeFromDeletedProduct(line) {
            var lineKey = getLineKey(line);
            if (lineKey in deletedLines) {
                delete deletedLines[lineKey];
            }
        }

        /**
         * Clear the cart lines and possibly the server cart id
         * @public
         *
         * @param {boolean} [withCartId]
         *
         * @returns {Promise}
         */
        function clearCart(withCartId) {
            _removeLines(self.lines, true);
            $rootScope.$emit('cartCleared',self.serverCartId);
            if (withCartId) {
                self.serverCartId = undefined;
                _setEditOrderId(null);
                deletedLines = {};
            }
            return saveCart();
        }

        /**
         * Gets whether the line's product is weighable and does not have weight per unit
         * @param {object} line
         * @returns {boolean}
         * @private
         */
        function _isWeightQuantity(line) {
            return line.product.isWeighable && !line.product.weight;
        }

        /**
         * Gets the quantity of two numbers - to fix js bugs (such as: 0.1 +0.2)
         * @param {number} numberToFix
         * @returns {number}
         * @private
         */
        function _fixNumber(numberToFix) {
            return parseFloat(numberToFix.toFixed(5))
        }

        /**
         * Add a new line
         * @param {Line} line
         * @param {Locals} locals
         * @returns {object}
         * @private
         */
        function _addLine(line, locals, soldBy) {
            var lineKey = getLineKey(line);
            if (!self.lines[lineKey]) {
                line.quantity = line.quantity || quantityInterval(line, soldBy);

                _setPropTypes(line, 'totalGifts', function() {return 0});
                _setPropTypes(line, 'totalClubsGifts', function() {return 0});

                if (line.totalSimulateClubsGifts) {
                    _setPropTypes(line, 'totalSimulateClubsGifts', function() {return 0});
                }

                _setLinePrices(line, locals);

                line.sort = sortNumber++;
                line.type = line.type || SP_SERVICES.CART_LINE_TYPES.PRODUCT;
                _removeFromDeletedProduct(line);
                self.lines[lineKey] = line;
            } else if (!self.lines[lineKey].quantity) {
                self.lines[lineKey].quantity = line.quantity || quantityInterval(line, soldBy);
            } else {
                self.lines[lineKey].quantity += line.quantity || quantityInterval(line, soldBy);

                if(self.lines[lineKey].productPropertyValue) {
                    self.lines[lineKey].productPropertyValue = line.productPropertyValue;
                }
            }

            if (self.lines[lineKey].type == SP_SERVICES.CART_LINE_TYPES.COUPON && self.lines[lineKey].quantity > 1) {
                self.lines[lineKey].quantity = 1;
            }

            _setQuantityLimit(self.lines[lineKey]);

            self.lines[lineKey].soldBy = soldBy;
            self.lines[lineKey].removed = false;
            self.lines[lineKey].id = lineKey;
            self.lines[lineKey].oldQuantity = self.lines[lineKey].quantity;
            return self.lines[lineKey];
        }

        /**
         * Add a new line, save and call event
         * @param {Line} line
         * @public
         */
        function addLine(line, soldBy) {
            // This code is for retailers that assign to Hub for log on user browser cart process started
            EmbedHistoryRetailer.embedHubIframe();
            return _getLocals().then(function (locals) {
                var lineAffected = _addLine(line, locals, soldBy);
                dirtyLines[getLineKey(lineAffected)] = true;
                saveCart({
                    withoutServer: false,
                    affectedLines: [lineAffected],
                    locals: locals,
                    source: line.source,
                    cartLinesAddAlreadyFired: true
                });
                $rootScope.$emit('cart.lines.add', {lines: [lineAffected]});
                $rootScope.$emit('cart.lines.add.click', {lines: [lineAffected]});
                prepareLinesForAnalytics([lineAffected], line.source);
                return lineAffected;
            });
        }

        /**
         * Add a list of lines, save and call event
         * @param {Array.<Line>} lines
         * @param {boolean} skipActiveProductsValidation
         * @param {string} source
         * @public
         */
        function addLines(lines, source,  skipActiveProductsValidation, originatedId) {
            return _getLocals().then(function (locals) {
                if (!lines || !angular.isArray(lines)) {
                    return [];
                }

                var linesAffected = [];
                angular.forEach(lines, function (line) {
                    var lineAffected = _addLine(line, locals, line.soldBy);
                    linesAffected.push(lineAffected);
                    dirtyLines[getLineKey(lineAffected)] = true;
                });
                saveCart({
                    withoutServer: false,
                    affectedLines: linesAffected,
                    skipActiveProductsValidation: skipActiveProductsValidation,
                    locals: locals,
                    originatedId: originatedId,
                    source: source,
                    cartLinesAddAlreadyFired: true
                });
                $rootScope.$emit('cart.lines.add', {lines: linesAffected});
                $rootScope.$emit('cart.lines.add.click', {lines: linesAffected});
                if (source)
                {
                    prepareLinesForAnalytics(linesAffected, source);
                }

                return linesAffected;
            });
        }

        function prepareLinesForAnalytics(lines, source){
            angular.forEach(lines, function(line){
                if(line.product && line.product.branch){
                    var quantity = (line.product.isWeighable) ? (line.quantity * 1000): line.quantity;
                    var price = (line.product.isWeighable) ? (line.product.branch.regularPrice / 1000): line.product.branch.regularPrice;
                    $rootScope.$emit('cart.lines.added', {productId:line.product.id, quantity: Math.round(quantity),
                        unitPrice: price, sourceType: 'internal', sourceValue: source,cartId: self.serverCartId})
                }
            })
        }

        /**
         * Remove a bunch of lines and call event
         * @param {Array.<Line>|Line} lines
         * @param {boolean} [isDelete] - whether to skip the 'removed' state
         * @private
         */
        function _removeLines(lines, isDelete) {
            var removedLines = [],
                voidLines = [];
            angular.forEach(lines, function (line) {
                line.quantity = 0;
                line.comments = '';
                if (line.removed && isDelete !== false || isDelete) {
                    line.removed = false;
                    _addToDeletedProduct(line);
                    removedLines.push(line);
                    delete self.lines[getLineKey(line)];
                } else {
                    voidLines.push(line);
                    dirtyLines[getLineKey(line)] = true;
                    line.removed = true;
                }
            });

            if (removedLines.length) {
                $rootScope.$emit('cart.lines.remove', {lines: removedLines});
            }

            if (voidLines.length) {
                $rootScope.$emit('cart.lines.quantityChanged', {lines: voidLines});
            }

            return $q.resolve(removedLines);
        }

        /**
         * Remove a line
         * @param {Line} line
         * @param {boolean} [isDelete] - whether to skip the 'removed' state
         * @private
         */
        function _removeLine(line, isDelete) {
            var delta = line.oldQuantity - line.quantity;
            if (_isWeightQuantity(line)) {
                delta = delta * 1000;
            }
            if(delta > 0){
                $rootScope.$emit('cart.lines.removed', {
                    productId: line.product.id,
                    quantity: Math.round(delta),
                    cartId: self.serverCartId});
                line.oldQuantity = line.quantity;
            }

            return _removeLines([line], isDelete).then(function (removedLines) {
                return removedLines && removedLines[0];
            });
        }

        /**
         * Remove a line and save
         * @param {Line} line
         * @param {boolean} [isDelete] - whether to skip the 'removed' state
         * @param {boolean} [isSaveCartPromise] - if need to return saveCart promise
         */
        function removeLine(line, isDelete, isSaveCartPromise) {
            var promise = _removeLine(line, isDelete);
            var saveCartPromise = saveCart();
            return isSaveCartPromise ? saveCartPromise: promise;
        }

         /**
         * Remove lines and save
         * @param {Line} line
         * @param {boolean} [isDelete] - whether to skip the 'removed' state
         * @param {boolean} [isSaveCartPromise] - if need to return saveCart promise
         */
        function removeLines(lines, isDelete, isSaveCartPromise) {
            var promise = _removeLines(lines, isDelete);
            var saveCartPromise = saveCart();
            return isSaveCartPromise ? saveCartPromise: promise;
        }

        /**
         * Gets the quantity interval of a product
         * @param {Line} line
         * @returns {number}
         * @public
         */
        function quantityInterval(line, soldBy) {
            return !line.isCase && (_productSoldBy(line, soldBy) === $rootScope.PRODUCT_DISPLAY.WEIGHT.name || _isWeightQuantity(line)) ? (line.product.unitResolution || 0.5) : 1;
        }

        /**
         * Decrease the quantity of a line by quantity interval
         * @param {Line} line
         * @public
         */
        function minusQuantity(line) {
            var lineKey = getLineKey(line);
            if (!self.lines[lineKey]) return;

            self.lines[lineKey].quantity = _fixNumber(self.lines[lineKey].quantity - quantityInterval(line));
            quantityChanged(line);
        }

        /**
         * Increase the quantity of a line by quantity interval
         * @param {Line} line
         * @public
         */
        function plusQuantity(line) {
            if (!line.product || !('id' in line.product) || angular.isUndefined(line.product.id)) return;

            if (line.product.quantityLimit && line.quantity >= line.product.quantityLimit) {
                $rootScope.$emit('cart.lines.quantityLimit', {line: line});
                return;
            }

            var lineKey = getLineKey(line);
            if (!self.lines[lineKey]) {
                self.addLine(line);
            } else {
                self.lines[lineKey].quantity = _fixNumber(self.lines[lineKey].quantity + quantityInterval(line));
                self.quantityChanged(line);
            }
        }

        /**
         * Validates the quantity limits and corrects it if needed
         * @private
         *
         * @param {Line} line
         */
        function _setQuantityLimit(line) {
            if (line.product.quantityLimit && line.quantity > line.product.quantityLimit) {
                line.quantity = line.product.quantityLimit;
                $rootScope.$emit('cart.lines.quantityLimit', {line: line});
            }
        }

        /**
         * Remove line if no quantity or add line if removed and save
         * @param {Line} line
         * @params {Boolean} notForAnalyticProvider
         * @returns {*}
         * @public
         */
        function quantityChanged(line, notForAnalyticProvider, soldBy) {
            line.quantity < 0 && (line.quantity = 0);
            line.quantity > MAX_LINE_QUANTITY && (line.quantity = MAX_LINE_QUANTITY);

            _setQuantityLimit(line);

            //to remove . from quantity that should be int
            _productSoldBy(line, soldBy) === $rootScope.PRODUCT_DISPLAY.UNIT.name && !_isWeightQuantity(line) && (line.quantity = Number(line.quantity.toString().replace(/\./g, '')));

            if (line.quantity == 0 && !line.removed) {
                return removeLine(line);
            }
            if (line.quantity > 0 && line.removed) {
                _getLocals().then(function (locals) {
                    _addLine(line, locals);
                });
            }

            if(!notForAnalyticProvider){
                _sendQuantityChangeToAnalyticsProvider(line);
                line.oldQuantity = line.quantity;
            }

            dirtyLines[getLineKey(line)] = true;
            saveCart({
                withoutServer: false,
                affectedLines: [line]
            });
            $rootScope.$emit('cart.lines.quantityChanged', {lines: [line]});
        }

        function _sendQuantityChangeToAnalyticsProvider(line){
            if(line.product && line.product.branch && line.oldQuantity) {
                var delta = line.quantity - line.oldQuantity;
                var price = line.product.branch.regularPrice
                if (_isWeightQuantity(line)) {
                    delta = delta * 1000;
                    price = price / 1000;
                }
                if (delta < 0) {
                    $rootScope.$emit('cart.lines.removed', {
                        productId: line.product.id,
                        quantity: Math.round((-1) * delta),
                        cartId: self.serverCartId
                    })
                } else if (delta > 0) {
                    $rootScope.$emit('cart.lines.added', {
                        productId: line.product.id,
                        quantity: Math.round(delta),
                        unitPrice: price,
                        cartId: self.serverCartId,
                        sourceType: 'internal',
                        sourceValue: 'update'
                    })
                }
            }
        }

        /**
         * Add line to dirty lines and fire event
         * @param {Line} line
         * @public
         */
        function commentsChanged(line) {
            var lineKey = getLineKey(line);
            if (!self.lines[lineKey]) return;
            dirtyLines[lineKey] = true;
            saveCart({
                withoutServer: false,
                affectedLines: [line]
            });
            $rootScope.$emit('cart.lines.commentsChanged', {lines: [line]});
        }

        /**
         * Add line to dirty lines
         * @param {Line} line
         * @public
         */
        function productPropertyValueChanged(line) {
            var lineKey = getLineKey(line);
            if (!self.lines[lineKey]) return;
            dirtyLines[lineKey] = true;
            saveCart({
                withoutServer: false,
                affectedLines: [line]
            });
        }

        /**
         * Gets the product object with its lines
         * @param {object} product
         * @returns {object}
         * @public
         */
        function getProduct(product) {
            var caseLine,
                singleLine;
            
                var soldBy =  product && product.soldBy || null;
                if (!soldBy) {
                    if (product && product.productDisplayModeId && product.isWeighable && product.weight) {
                        var cartLine;
                        angular.forEach(self.lines, function (line) { 
                            if (line.product.id === product.id) {
                                cartLine = line;
                            }
                        });
                        if (!cartLine) {
                            switch (product.productDisplayModeId) {
                                case $rootScope.PRODUCT_DISPLAY.UNIT.id:
                                    soldBy = $rootScope.PRODUCT_DISPLAY.UNIT.name;
                                    break;
                                case $rootScope.PRODUCT_DISPLAY.WEIGHT.id:
                                    soldBy = $rootScope.PRODUCT_DISPLAY.WEIGHT.name;
                                    break;
                                default:
                                    soldBy = product.isWeighable ? (product.weight ? $rootScope.PRODUCT_DISPLAY.UNIT.name : $rootScope.PRODUCT_DISPLAY.WEIGHT.name) : null;
                            } 
                        } else {
                            soldBy = !!cartLine.soldBy && cartLine.soldBy;
                        }
                    }
                }
                
                if (soldBy) {
                    product.soldBy = soldBy;
                }
    

            if (product && Object.keys(product).length) {
                caseLine = self.lines[product.id.toString() + '1'];
                singleLine = self.lines[product.id.toString() + '0'];
            }

            if (caseLine) {
                product.caseLine = caseLine;
            } else if ('caseLine' in product) {
                delete product.caseLine;
            }

            if (singleLine) {
                singleLine['soldBy'] = soldBy;
                product.singleLine = singleLine;
            } else if ('singleLine' in product) {
                delete product.singleLine;
            }

            return product;
        }

        /**
         * Gets the products object with its lines
         * @param {Array} [products]
         * @returns {Array}
         * @public
         */
        function getProducts(products) {
            var arr = [];

            var lines = products || getLines();
            angular.forEach(lines, function (line) {
                arr.push(self.getProduct(line.product || line));
            });

            return arr;
        }

        /**
         * Gets the active lines
         * @returns {Array}
         * @public
         */
        function getLines() {
            var arr = [];

            angular.forEach(self.lines, function (line) {
                !line.removed && arr.push(line);
            });

            return arr;
        }

        /**
         * Gets whether the line's product is weighable and has weight per unit
         * @returns {boolean}
         * @private
         */
        function isUnitsWeighable(line) {
            return line.product.isWeighable && !!line.product.weight;
        }

        /**
        * Gets the products sold by mode based on the condition of whether the line's product is weighable and has a weight per unit
        * @returns {string || null}
        * @private
        */
        function _productSoldBy(line, soldBy) { 
            return line.product.isWeighable && !!line.product.weight && soldBy;
        }
        
        /**
         * Calc the lines's prices
         * @param {Array.<Line>|Line} lines
         * @private
         */
        function _calcLocalPrices(lines) {
            if (!lines || !lines.length) {
                return;
            }

            _getLocals().then(function (locals) {
                angular.forEach(lines, function (line) {
                    line.gifts = [];
                    line.totalGifts = line.totalClubsGifts = 0;
                    line.totalGiftsPotential = line.totalClubsGiftsPotential = 0;
                    _setLinePrices(line, locals);
                });
            });
        }

        var sendToServerTimeout;

        /**
         * Save cart
         * @public
         *
         * @param {Object} [options]
         * @param {boolean} [options.withoutServer] - do not call server
         * @param {Array} [options.affectedLines]
         * @param {Locals} [options.locals]
         * @param {number} [options.paymentMethodId]
         * @param {boolean} [options.skipActiveProductsValidation]
         * @param {boolean} [options.forceCreation]
         * @param {string} [options.source]
         * @param {boolean} [options.cartLinesAddAlreadyFired]
         *
         * @returns {Promise}
         */
        function saveCart(options) {
            options = options || {};

            if (!options.withoutServer) {
                _saveCartDefer = _saveCartDefer || $q.defer();

                sendToServerTimeout && $timeout.cancel(sendToServerTimeout);
                _calcLocalPrices(options.affectedLines || []);
                sendToServerTimeout = $timeout(function() {
                    var defer = _saveCartDefer;
                    _saveCartDefer = null;

                    _sendToServer({
                        paymentMethodId: options.paymentMethodId,
                        skipActiveProductsValidation: options.skipActiveProductsValidation,
                        forceCreation: options.forceCreation,
                        originatedId: options.originatedId,
                        source: options.source,
                        cartLinesAddAlreadyFired: options.cartLinesAddAlreadyFired
                    }).then(function(result) {
                        defer.resolve(result);
                    }).catch(function(err) {
                        defer.reject(err);
                    });
                }, 500);
            }

            _setCartTotal(options.locals);

            if (options.withoutServer) {
                return $q.resolve();
            } else {
                return _saveCartDefer.promise;
            }
        }

        /**
         * Set time travel
         * @public
         *
         * @param {object} [timeTravel]
         */
        function setTimeTravel(timeTravel) {
            if (!angular.isObject(timeTravel)) {
                delete self.timeTravel;
                return;
            }

            self.timeTravel = self.timeTravel || {};
            angular.extend(self.timeTravel, timeTravel);
        }

        /**
         * Replace the current cart with another cart
         * @public
         *
         * @param {int} withCartId
         * @param {boolean} [merge]
         *
         * @returns {Promise}
         */
        function replaceCart(withCartId, merge) {
            if (self.serverCartId === withCartId) {
                return $q.resolve();
            }

            if (preventCalls) {
                return $timeout(function () {
                    return replaceCart(withCartId, merge);
                }, 100);
            }

            var currentCartId = self.serverCartId;
            clearCart(true);
            deletedLines = {};
            self.serverCartId = withCartId;

            return _sendToServer({
                cartToDelete: currentCartId,
                mergeDeletedCart: merge
            });
        }

        /**
         * Make sure the cart exists, and if not create it
         * @public
         *
         * @return {Promise}
         */
        function createCart() {
            if (self.serverCartId) {
                return $q.resolve();
            }

            return _sendToServer({ forceCreation: true });
        }

        /**
         * Gets the line's weighableProductUnits
         * @param {object} line
         * @returns {number}
         * @private
         */
        function _getWeighableProductUnits(line) {
            line.weighableProductUnits = line.weighableProductUnits;
            if (line.weighableProductUnits && line.quantity) {
                return Math.round(line.quantity / line.product.weight);
            }
            return line.quantity;
        }

        /**
         * Sets the server's quantity and comments to local line
         * @param {string} lineKey
         * @param {object} serverLine
         * @private
         */
        function _setCommentsAndQuantityFromServer(lineKey, serverLine) {
            self.lines[lineKey].comments = serverLine.comments;
            self.lines[lineKey].metaData = serverLine.metaData;
            self.lines[lineKey].quantity = !serverLine.isCase && isUnitsWeighable(self.lines[lineKey]) ? _getWeighableProductUnits(serverLine) : serverLine.quantity;
        }

        /**
         * Add a server cart line to local cart
         * @param {object} line
         * @param {Locals} locals
         * @private
         */
        function _addServerProduct(line, locals) {
            var newLine = {isCase: line.isCase, product: {}};
            if (line.isPseudo) {
                newLine.isPseudo = true;
                newLine.product.id = line.text;
                newLine.product.name = line.text;

                if (line.categoryId) {
                    newLine.product.categories = [{
                        id: line.categoryId,
                        names: line.categoryNames
                    }];
                }
                newLine.product.names = {};
                newLine.product.names[locals.languageId || 0] = {
                    short: line.text,
                    long: line.text
                };
            } else {
                angular.extend(newLine.product, line.product);
            }
            _addLine(newLine, locals);

            _setCommentsAndQuantityFromServer(getLineKey(newLine), line);
        }

        /**
         * Gets a local cart line as a server cart line
         * @param {Line} line
         * @returns {object}
         * @private
         */
        function _getServerRequestLine(line) {
            var serverLine = {quantity: line.quantity, soldBy: line.soldBy, comments: line.comments, isCase: line.isCase, metaData: line.metaData, adConversionUrl: line.adConversionUrl, adminComments: formatBase64toString(line.adminComments)};
            if (line.isPseudo) {
                serverLine.isPseudo = true;
                serverLine.text = line.product.id;
                serverLine.categoryId = line.product.categories && line.product.categories[0] ? line.product.categories[0].id : null;
            } else if(line.type === SP_SERVICES.CART_LINE_TYPES.DELIVERY) {
                serverLine.retailerProductId = line.product.id;
                serverLine.type = line.type;
                serverLine.areaId = line.areaId;
                serverLine.deliveryTypeId = line.deliveryTypeId;
            } else {
                serverLine.retailerProductId = line.product.id;
                serverLine.type = line.type || SP_SERVICES.CART_LINE_TYPES.PRODUCT;
            }

            if (line.productPropertyValueId) {
                serverLine.productPropertyValueId = line.productPropertyValueId;
            } else if (line.productPropertyValue && line.productPropertyValue.id) {
                serverLine.productPropertyValueId = line.productPropertyValue.id;
            }

            if (line.type === SP_SERVICES.CART_LINE_TYPES.COUPON) {
                serverLine.isForAllUsers = (line.product && line.product.isForAllUsers) || false;
            }
            return serverLine;
        }

        /**
         * Gets the local cart lines as a server cart lines
         * @returns {{deletedLines: object, dirtyLines: object, lines: Array}}
         * @private
         */
        function _getServerRequestLines() {
            var serverLines = [],
                requestDirtyLines = {};
            angular.forEach(dirtyLines, function (val, lineKey) {
                delete dirtyLines[lineKey];
                if (!self.lines[lineKey]) return;
                requestDirtyLines[lineKey] = true;
                serverLines.push(_getServerRequestLine(self.lines[lineKey]));
            });

            var requestDeletedLines = {};
            angular.forEach(deletedLines, function (deletedLine, lineKey) {
                delete deletedLines[lineKey];
                requestDeletedLines[lineKey] = deletedLine;
                var toAdd = {delete: true};
                if (deletedLine.isPseudo) {
                    toAdd.text = deletedLine.productId;
                    toAdd.isPseudo = true;
                } else {
                    toAdd.retailerProductId = deletedLine.productId;
                    toAdd.isCase = !!deletedLine.isCase;
                    toAdd.type = deletedLine.type;
                }
                serverLines.push(toAdd);
            });

            return {deletedLines: requestDeletedLines, dirtyLines: requestDirtyLines, lines: serverLines};
        }

        /**
         * Emit event for other cart
         * @param {object} otherCart
         * @param {number} [otherCart.id]
         * @param {number} [otherCart.linesCount]
         */
        function _emitOtherCart(otherCart) {
            if (!otherCart) {
                return;
            }

            $rootScope.$emit('cart.otherCart', {
                otherCart: otherCart,
                next: function(deleteCartId, merge) {
                    return _sendToServer({
                        cartToDelete: deleteCartId,
                        mergeDeletedCart: merge
                    });
                }
            });
        }

        /**
         * Emit event for inactive lines
         * @param {Array|object} inactiveLines
         * @private
         */
        function _emitInactiveLines(inactiveLines) {
            if (!inactiveLines) {
                return;
            }

            var properLines = [],
                toRemoveProducts = [];
            angular.forEach(inactiveLines, function (inactiveLine) {
                var localKey = getLineKey({isCase: inactiveLine.isCase, product: {id: inactiveLine.retailerProductId}});
                localKey in self.lines && toRemoveProducts.push(self.lines[localKey]);

                if (!inactiveLine.isTipLine) {
                    properLines.push({
                        productId: inactiveLine.retailerProductId ,
                        name: inactiveLine.text ,
                        product: inactiveLine.product ,
                        isCase: inactiveLine.isCase ,
                        isPseudo: inactiveLine.isPseudo
                    });
                }
            });

            _removeLines(toRemoveProducts, true);

            if (!!properLines.length) {
                $rootScope.$emit('cart.lines.inactive', {lines: properLines});
            }
        }

        function _setEditOrderId(value) {
            if (!self.editOrderId === !value || self.editOrderId === value) {
                return;
            }

            self.editOrderId = value || null;
            $rootScope.$emit('cart.editOrderId.change', self.editOrderId);
        }

        /**
         * Sets prices and add new lines from server cart
         * @param {object} serverCart
         * @param {number} serverCart.id
         * @param {number} serverCart.editOrderId
         * @param {Array} serverCart.lines
         * @param {object} serverCart.specialReminders
         * @param {number} serverCart.multiPassDiscount
         * @param {number} serverCart.multiPassObligo
         * @param {number} serverCart.multiPassNoDiscountDepartments
         * @param {object} requestDirtyLines
         * @param {Locals} locals
         * @param {object} serverCart.cartMetaData
         * @param {boolean} serverCart.tipsWasRemoved
         * @param {boolean} [cartLinesAddAlreadyFired]
         * @private
         */
        function _setDetailsFromServerCart(serverCart, requestDirtyLines, locals, cartLinesAddAlreadyFired) {
            // when the api call did actually happen, and a server cart was not actually returned
            if (!serverCart) {
                return;
            }

            self.serverCartId = serverCart.id;
            self.specialReminders = serverCart.specialReminders || {};
            self.multiPassDiscount = serverCart.multiPassDiscount || 0;
            self.multiPassObligo = serverCart.multiPassObligo || 0;
            self.multiPassNoDiscountDepartments = serverCart.multiPassNoDiscountDepartments || 0;
            self.cartMetaData = serverCart.cartMetaData;
            self.tipsWasRemoved = serverCart.tipsWasRemoved;
            _setChangedProducts(requestDirtyLines);

            // reset the traffic params, it was successfully sent
            _trafficParams = undefined;

            _setEditOrderId(serverCart.editOrderId);

            var addedLines = [],
                toRemoveKeys = {};
            angular.forEach(self.lines, function (line, lineKey) {                
                if (requestDirtyLines[lineKey] || dirtyLines[lineKey]) return;
                toRemoveKeys[lineKey] = true;
            });

            angular.forEach(serverCart.lines, function (line) {
                var lineKey = getLineKey({
                    isCase: line.isCase,
                    product: {id: line.isPseudo ? line.text : line.retailerProductId}
                });

                if (line.isForAllUsers && line.massCouponId) {
                    var dirtyLineKey = getLineKey({ isCase: line.isCase, product: { id: line.massCouponId }});
                    if (requestDirtyLines[dirtyLineKey]) {
                        var newDirtyLineKey = getLineKey({ isCase: line.isCase, product: { id: line.retailerProductId }});
                        requestDirtyLines[newDirtyLineKey] = true;
                        if(self.lines[dirtyLineKey]){
                            var oldLine = self.lines[dirtyLineKey];
                            oldLine.id = line.retailerProductId;
                            self.lines[newDirtyLineKey] = self.lines[dirtyLineKey];
                            delete self.lines[dirtyLineKey];
                        }
                   }
                }

                if (!self.lines[lineKey] && !deletedLines[lineKey]) {
                    _addServerProduct(line, locals);
                    addedLines.push(self.lines[lineKey]);
                } else if (self.lines[lineKey]) {
                    angular.extend(self.lines[lineKey].product, line.product);

                    if (toRemoveKeys[lineKey]) {
                        _setCommentsAndQuantityFromServer(lineKey, line);
                        delete toRemoveKeys[lineKey];
                    }
                }

                if (self.lines[lineKey]) {
                    if (!self.lines[lineKey].removed && self.lines[lineKey].quantity <= 0) {
                        _removeLine(self.lines[lineKey]);
                    }

                    self.lines[lineKey].type = line.type;
                    self.lines[lineKey].sellDates = line.sellDates;
                    self.lines[lineKey].isHideFromCart = line.isHideFromCart || false;
                    self.lines[lineKey].isProductOutOfStock = line.isProductOutOfStock;
                    self.lines[lineKey].isCouponActive = line.isCouponActive;
                    self.lines[lineKey].productPropertyValue = line.productPropertyValue || null;
                    if (line.product && line.product.productDisplayModeId && isUnitsWeighable(line)) {
                        self.lines[lineKey].soldBy = !!line.weighableProductUnits && line.weighableProductUnits > 0 ? 'Unit' : 'Weight';
                    } else {
                        self.lines[lineKey].soldBy = null;
                    }
                    if (!line.isPseudo) {
                        self.lines[lineKey].gifts = line.gifts;
                        self.lines[lineKey].totalGifts = line.totalGifts;
                        self.lines[lineKey].totalClubsGifts = line.totalClubsGifts;
                        self.lines[lineKey].totalGiftsPotential = line.totalGiftsPotential;
                        self.lines[lineKey].totalClubsGiftsPotential = line.totalClubsGiftsPotential;
                        if (line.totalSimulateClubsGifts){
                            self.lines[lineKey].totalSimulateClubsGifts = line.totalSimulateClubsGifts;
                            self.lines[lineKey].totalSimulateClubsGiftsPotential = line.totalSimulateClubsGiftsPotential;
                        }
                        _setLinePrices(self.lines[lineKey], locals);
                    }
                }
            });

            var removedLines = [];
            angular.forEach(toRemoveKeys, function (val, lineKey) {
                removedLines.push(self.lines[lineKey]);
                delete self.lines[lineKey];
            });
            if (removedLines.length) {
                $rootScope.$emit('cart.lines.remove', {lines: removedLines});
            }

            if (addedLines && addedLines.length) {
                $rootScope.$emit('cart.lines.add', {lines: addedLines, isSecondTime: cartLinesAddAlreadyFired});
            }
        }

        /**
         * Prevent calls until call ends
         */
        var preventCalls,
            _resendData,
            retryTimeout;

        /**
         * On sever call return success
         * @param {object} resp
         * @param {object} resp.cart
         * @param {object} [resp.otherCart]
         * @param {Array|object} [resp.inactiveLines]
         * @param {object} requestDirtyLines
         * @param {Locals} locals
         * @param {boolean} [cartLinesAddAlreadyFired]
         * @private
         */
        function _serverCallCompleted(resp, requestDirtyLines, locals, cartLinesAddAlreadyFired) {
            _setDetailsFromServerCart(resp.cart, requestDirtyLines, locals, cartLinesAddAlreadyFired);
            _emitOtherCart(resp.otherCart);
            _emitInactiveLines(resp.inactiveLines);
            saveCart({
                withoutServer: true,
                locals: locals
            });

            if (retryTimeout) {
                $timeout.cancel(retryTimeout.timeout);
                retryTimeout = undefined;
            }

            preventCalls = false;
            if (_resendData) {
                _sendToServer(_resendData.options);
            } else {
                self.sendingSucceeded = true;
                $rootScope.$emit('cart.update.complete', requestDirtyLines);
            }
        }

        /**
         * Set failed request's data to dirty or deleted lines
         * @param {object} requestLinesData
         * @param {object} requestLinesData.dirtyLines
         * @param {object} requestLinesData.deletedLines
         * @param {object} [err]
         * @param {object} [err.response]
         * @param {Array} [err.errors]
         * @param {object} [err.errors.i]
         * @param {string} [err.errors.i.param]
         * @param {string} [err.errors.i.msg]
         * @private
         */
        function _restoreRequestDataOnError(requestLinesData, err) {
            var badRows = {},
                errors = [];
            if (err && err.response && err.response.errors && err.response.errors.length) {
                angular.forEach(err.response.errors, function (error) {
                    if (!error.param || error.param.indexOf('lines[') != 0) return;

                    var line = $rootScope.$eval(error.param.substring(0, error.param.indexOf('].') + 1), requestLinesData);
                    if (!line) return;

                    var lineKey = getLineKey({
                        isCase: line.isCase,
                        product: {id: line.isPseudo ? line.text : line.retailerProductId}
                    });
                    badRows[lineKey] = true;
                    !!self.lines[lineKey] && (error.line = self.lines[lineKey]);
                    error.requestLine = line;
                    errors.push(error);
                });
            }

            angular.forEach(requestLinesData.deletedLines, function (deletedLine, lineKey) {
                if (!!badRows[lineKey] || self.lines[lineKey]) return;
                deletedLines[lineKey] = deletedLine;
            });
            angular.forEach(requestLinesData.dirtyLines, function (val, lineKey) {
                if (!!badRows[lineKey] || deletedLines[lineKey] || !self.lines[lineKey]) return;
                dirtyLines[lineKey] = true;
            });

            if (!errors.length) return;

            self.sendingSucceeded = true;
            $rootScope.$emit('cart.lines.error', {errors: errors})
        }

        /**
         * On sever call return error
         * @private
         *
         * @param {object} err
         * @param {boolean} [err.aborted]
         * @param {number} err.statusCode
         * @param {*} err.response
         * @param {object} requestLinesData
         * @param {object} requestLinesData.dirtyLines
         * @param {object} requestLinesData.deletedLines
         * @param {object} locals
         * @param {object} options
         */
        function _serverCallError(err, requestLinesData, locals, options) {
            preventCalls = false;
            retryTimeout && $timeout.cancel(retryTimeout.timeout);
            retryTimeout = retryTimeout || { count: 0 };
            _restoreRequestDataOnError(requestLinesData, err);
            if (!err.aborted) {
                if (err.statusCode === 403 || (err.statusCode === 404 && err.response.error === 'Cart not found')) {
                    _errorRetry(locals, options);
                } else if (err.statusCode === 0 || err.statusCode === -1) {
                    _noConnectionRetry(options);
                }
            } else if (self.sendingSucceeded) {
                _sendToServer(options);
            }
        }

        function _errorRetry(locals, options) {
            retryTimeout.count++;

            retryTimeout.timeout = $timeout(function () {
                self.serverCartId = undefined;
                saveCart({
                    withoutServer: true,
                    locals: locals
                });
                _sendToServer(options);
            }, _getRetryTimeoutTime(500));
        }

        function _noConnectionRetry(options) {
            retryTimeout.count++;

            retryTimeout = $timeout(function () {
                _sendToServer(options);
            }, _getRetryTimeoutTime(2500));
        }

        function _getRetryTimeoutTime(defaultTime) {
            var time = defaultTime || 500;
            if (retryTimeout.count > 3) {
                time = 30000;
            } else if (retryTimeout.count > 4) {
                time = 60000;
            } else if (retryTimeout.count > 5) {
                time = 120000;
            }
            return time;
        }

        /**
         * Send the local cart to server
         * @private
         *
         * @param {Object} [options]
         * @param {number} [options.cartToDelete] - the id of the cart to delete
         * @param {boolean} [options.mergeDeletedCart] - whether to merge the current cart with the deleted cart
         * @param {number} [options.paymentMethodId]
         * @param {boolean} [options.skipActiveProductsValidation]
         * @param {boolean} [options.forceCreation]
         * @param {string} [options.source]
         * @param {boolean} [options.cartLinesAddAlreadyFired]
         *
         * @returns {Promise}
         */
        function _sendToServer(options) {
            options = options || {};

            var deferred = _resendData ? _resendData.defer : $q.defer();
            if (preventCalls) {
                _resendData = { defer: deferred, options: options };
                return _resendData.defer.promise;
            }

            _setCartTotal();
            _resendData = null;
            self.sendingSucceeded = false;
            preventCalls = true;

            var requestLinesData = _getServerRequestLines(),
                data = { lines: requestLinesData.lines };

            if (options.cartToDelete) {
                data.deleteCart = options.cartToDelete;
                data.merge = !!options.mergeDeletedCart;
            }

            if (options.paymentMethodId) {
                data.paymentMethodId = options.paymentMethodId;
            }

            if (self.editOrderId || !!options.skipActiveProductsValidation) {
                data.skipActiveProductsValidation = true;
                data.excludeCouponValidation = true; // Skip validation of all lines except coupons
            }

            if (options.originatedId) {
                data.originatedId = options.originatedId;
            }

            if (self.timeTravel) {
                data.timeTravel = self.timeTravel;
            }

            if (_trafficParams) {
                data.trafficParams = _trafficParams;
            }

            if (options.source) {
                data.source = options.source;
            }

            _getLocals().then(function (locals) {
                if (!!locals.withDeliveryProduct || self.editOrderId) {
                    data.deliveryProduct = true;
                } else {
                    angular.forEach(self.lines, function (line) {
                        if (!$rootScope.config.retailer.settings.includeDeliveryFeeInCart === 'true') {
                            line.type == SP_SERVICES.CART_LINE_TYPES.DELIVERY && _removeLine(line, true);
                        }
                    });
                }

                try {
                    var selectedStore = $rootScope.config.getBranchArea() || {}
                    data.deliveryProduct_Id = selectedStore.deliveryProduct_Id;
                    data.deliveryType = selectedStore.deliveryTypeId;
                } catch (e) {
                    // Do nothing
                }

                return _makeApiCall(locals, data, options.forceCreation).then(function (resp) {
                    _serverCallCompleted(resp, requestLinesData.dirtyLines, locals, options.cartLinesAddAlreadyFired);
                    deferred.resolve(resp);
                }).catch(function (err) {
                    _serverCallError(err, requestLinesData, locals, options);
                    deferred.reject(err);
                });
            });

            return deferred.promise;
        }

        /**
         * Make the actual api call to get/create/update the cart
         * @private
         *
         * @param {Locals} locals
         * @param {Object} data
         * @param {boolean} [forceCreation]
         *
         * @return {Promise<Object>}
         */
        function _makeApiCall(locals, data, forceCreation) {
            var httpOptions,
                loyaltyClubIds = !_isLoyaltyDisabled && locals.loyaltyClubIds || null;
            if (forceCreation || !_isDoNotCreateCart(data)) {
                if($rootScope.couponRedemptions) {
                    data.couponRedemptions = $rootScope.couponRedemptions;
                }

                httpOptions = {
                    method: self.serverCartId ? 'PATCH' : 'POST',
                    url: '/v2/retailers/:rid/branches/:bid/carts' + (self.serverCartId ? '/' + self.serverCartId : ''),
                    params: {
                        loyalty: loyaltyClubIds
                    },
                    data: data
                };
            } else if (locals.userId) {
                httpOptions = {
                    method: 'GET',
                    url: '/v2/retailers/:rid/branches/:bid/carts',
                    params: {
                        loyalty: loyaltyClubIds,
                        trafficParams: data.trafficParams
                    },
                    data: {
                        deliveryProduct_Id: data.deliveryProduct_Id
                    }
                };
            } else {
                return $q.resolve({});
            }

            var apiOptions = angular.copy(locals.apiOptions || {});
            apiOptions.id = 'spServicesServerCart';
            apiOptions.loadingElement = (locals.apiOptions || {}).loadingElement;
            return Api.request(httpOptions, apiOptions)
        }

        var DO_NOT_SKIP_CALL_KEYS = ['deleteCart', 'merge', 'paymentMethodId', 'skipActiveProductsValidation', 'excludeCouponValidation', 'timeTravel', 'deliveryProduct'];

        /**
         * Returns whether the cart is empty and there is no reason to actually make the api call to create it
         * @private
         *
         * @param {Object} data
         *
         * @return {boolean}
         */
        function _isDoNotCreateCart(data) {
            if (self.serverCartId || data.lines.length) {
                return false;
            }

            for (var i = 0; i < DO_NOT_SKIP_CALL_KEYS.length; i++) {
                if (data.hasOwnProperty(DO_NOT_SKIP_CALL_KEYS[i])) {
                    return false;
                }
            }

            return true;
        }

        // Init cart
        _sendToServer();

        function init(options) {
            return _sendToServer(options);
        }

        function _getTrafficParams() {
            var trafficParams = {};
            if (document.referrer && document.referrer !== document.location.href) {
                if (document.referrer.indexOf(document.location.hostname) > -1) {
                    return;
                }
                trafficParams.dr = document.referrer;
            }
            if (document.location.search) {
                var searchParams = document.location.search.substring(1).split('&');
                angular.forEach(searchParams, function (param) {
                    var parts = param.split('=');
                    trafficParams[parts[0]] = parts[1];
                });
            }
            return Object.keys(trafficParams).length ? trafficParams : undefined;
        }

        /**
         * Validate and emit event when the delivery items limit exceeded
         * @private
         *
         * @param {Array} [changedLines]
         *
         * @returns {void}
         */
        function validateDeliveryItemsLimit(changedLines) {
            _getLocals().then(function(locals) {
                if (isNaN(locals.deliveryItemsLimit) || locals.deliveryItemsLimit === null) {
                    return;
                }

                var totalItems = 0,
                    cartLines = [];
                angular.forEach(self.lines, function(line) {
                    if (!_validateIsDeliverItemsLimitLine(line)) {
                        return;
                    }

                    cartLines.push(line);
                    totalItems += 1;//line.quantity;
                });

                if (totalItems <= locals.deliveryItemsLimit) {
                    return;
                }

                // an array with the entire cart lines first and then the changed lines
                Array.prototype.push.apply(cartLines, changedLines || []);

                var linesToRemove = [],
                    keysToRemove = {},
                    toRemoveLength = totalItems - locals.deliveryItemsLimit;

                // run backwards on the array to collect the changed lines first
                for (var i = cartLines.length - 1; i >= 0 && linesToRemove.length < toRemoveLength; i--) {
                    var cartLineKey = getLineKey(cartLines[i]);
                    if (!keysToRemove[cartLineKey] && _validateIsDeliverItemsLimitLine(cartLines[i])) {
                        linesToRemove.push(cartLines[i]);
                        keysToRemove[cartLineKey] = true;
                    }
                }

                if (linesToRemove.length) {
                    _removeLines(linesToRemove, false);
                    $rootScope.$emit('cart.lines.deliveryItemsLimit', {lines: linesToRemove});
                }
            });
        }

        /**
         * Detect EBTEligibleType closure function
         * @private
         *
         * @param {object} [product]
         *
         * @returns {boolean}
         */

        function _detectEBTEligibleType(product) {
            var detectType = {
                snap: function () {
                    return !!((product.productTagsData || []).find(function(productTag) {
                        return SpProductTags.isProductTagRelationActive(productTag) && productTag.typeId === PRODUCT_TAG_TYPES.EBT_ELIGIBLE;
                    }) || product.branch && product.branch.isEbtEligible) ? "snap" : false;
                },
                cash: function() {
                    return !!((product.productTagsData || []).find(function(productTag) {
                        return SpProductTags.isProductTagRelationActive(productTag) && (productTag.typeId === PRODUCT_TAG_TYPES.EBT_CASH_ELIGIBLE || productTag.typeId === PRODUCT_TAG_TYPES.EBT_ELIGIBLE);
                    }) || product.branch && product.branch.isEbtCashEligible) ? "cash" : false;
                }
            };

            function detectByType(type) {
                return type && detectType[type] && detectType[type]();
            }

            function detectAllTypes(cb) {
                angular.forEach(detectType, function(detectFunc) {
                    var currType = detectFunc();

                    return (cb && typeof(cb) === 'function') ? cb(currType) : currType;
                });
            }
            9
            return {
                detectByType: detectByType,
                detectAllTypes: detectAllTypes
            };
        }

        /**
         * @typedef {Object} CartEBTEligibleDataItem
         *
         * @property {number} eligibleWithTax
         * @property {number} eligibleWithoutTax
         * @property {number} depositWithTax
         * @property {number} depositWithoutTax
         * @property {number} totalWithTax
         * @property {number} totalWithoutTax
         */

        /**
         * @typedef {Object} CartEBTEligibleData
         *
         * @property {CartEBTEligibleDataItem} snap
         * @property {CartEBTEligibleDataItem} entireCart
         */

        /**
         * Sums the ebt eligible amounts
         * @public
         *
         * @returns {CartEBTEligibleData}
         */
        function getEBTEligible(order) {
            var taxSuffixes = ['WithTax', 'WithoutTax'],
                cartLineTaxSuffixes = {
                    'WithTax': 'WithTaxForEbtEligibility',
                    'WithoutTax': 'WithoutTax',
                },
                groups = ['entireCart', 'snap', 'cash'],
                /**
                 * @type {CartEBTEligibleData}
                 */
                result = {snap: {}, entireCart: {}, cash: {}};
            angular.forEach(taxSuffixes, function (taxSuffix) {
                angular.forEach(groups, function (groupKey) {
                    result[groupKey]['eligible' + taxSuffix] = 0;
                    result[groupKey]['deposit' + taxSuffix] = 0;
                });
            });

            var index = 0;
            var lines = {}
            if (order) {
                order.lines.forEach(function (line) {
                    lines[line.id] = line;
                })
            }
            var totalWeighableForDeposit = angular.copy(result);

            angular.forEach(order ? lines : self.lines, function (line) {
                var addTo = ['entireCart'];
                var lineEBTEligibleDetection = _detectEBTEligibleType(line.product);
                lineEBTEligibleDetection.detectAllTypes(function(type) {
                    type && addTo.push(type);
                });
                angular.forEach(addTo, function (key) {

                    angular.forEach(taxSuffixes, function (taxSuffix) {
                        var finalPrice = _roundDecimal(line['finalPrice' + cartLineTaxSuffixes[taxSuffix]] || line['totalPrice'] || 0);
                        result[key]['eligible' + taxSuffix] += finalPrice;
                        if (line.product.isWeighable) {
                            totalWeighableForDeposit[key]['deposit' + taxSuffix] += finalPrice;
                        }
                    });
                });

                index++;
            });

            //adding ebt deposit of weighable product to the total sum
            angular.forEach(Object.keys(result), function (key) {
                angular.forEach(taxSuffixes, function (taxSuffix) {
                    result[key]['deposit' + taxSuffix] += _roundDecimal(totalWeighableForDeposit[key]['deposit' + taxSuffix] * 0.1);

                });
            });

            angular.forEach(groups, function(groupKey) {
                angular.forEach(taxSuffixes, function(taxSuffix) {
                    var fullEligibleKey = 'eligible' + taxSuffix,
                        fullDepositKey = 'deposit' + taxSuffix;
                    result[groupKey][fullEligibleKey] = _roundDecimal(result[groupKey][fullEligibleKey])
                    result[groupKey][fullDepositKey] = _roundDecimal(result[groupKey][fullDepositKey]);
                    result[groupKey]['total' + taxSuffix] = _roundDecimal(result[groupKey][fullEligibleKey] + result[groupKey][fullDepositKey]);
                    result[groupKey]['immutable' + taxSuffix] = _roundDecimal(result[groupKey][fullEligibleKey] + result[groupKey][fullDepositKey]);
                    result[groupKey]['immutable'+taxSuffix+'WithoutDeposit']= _roundDecimal(result[groupKey][fullEligibleKey]);
                    result[groupKey]['immutableDeposit'+taxSuffix]= _roundDecimal(result[groupKey][fullDepositKey]);
                });
            });

            return _roundEbtAmounts(result);
        }

        /**
         * Check EBTEligible new lines or quantity in edit cart
         * @public
         *
         * @param {object} currentCartLines
         *
         * @returns {boolean}
         */

        function checkEBTEligibleCartChange(currentCartLines) {
            var retailerIdsWhiteList = [1249];
            var newCartLines = getLines();
            var isExistCartLine = function(productId) {
                return function(line) { return line.product.id === productId }
            };
            var isQuantityIncrease = function(productId, quantity) {
                return function(line) {
                    return isExistCartLine(productId)(line) && line.quantity < quantity}
            };

            if (!newCartLines.length) return false;

            var additionNewEBTEligibleCartLine = newCartLines.find(function (newCartLine) {
                var ebtEligibleDetection = _detectEBTEligibleType(newCartLine.product);
                var isEligible = ebtEligibleDetection.detectByType('snap') || ebtEligibleDetection.detectByType('cash');

                return isEligible;
            });

            return !!additionNewEBTEligibleCartLine;
        }

        function _roundEbtAmounts(ebtEligible){
            if (ebtEligible){
                for (var objectProperty in ebtEligible) {
                    if(objectProperty){
                        for (var numericProperty in ebtEligible[objectProperty]) {
                            var numericPropertyObject = ebtEligible[objectProperty][numericProperty];
                            if(angular.isNumber(numericPropertyObject) && numericPropertyObject > 0) {
                                ebtEligible[objectProperty][numericProperty] = _roundDecimal(numericPropertyObject);
                            }
                        }
                    }
                }
            }
            return ebtEligible;
        }

        /**
         * @typedef {Object} CartEBTEligibleData
         * @param {Object} order
         * @property {CartEBTEligibleDataItem} snap
         * @property {CartEBTEligibleDataItem} cash
         * @property {CartEBTEligibleDataItem} entireCart
         */

        function getRemainingPayment(order) {
            var remainingPayment = {
                ebt: {
                    snap: 0,
                    cash: 0
                },
                total: 0
            }
            if (!order || !order.paymentData) {
                return remainingPayment
            }

            var orderPayment = order.paymentData;
            var ebtCashPayment = orderPayment.ebtCashPayment,
                ebtPayment = orderPayment.ebtPayment,
                ebtSNAPPayment = orderPayment.ebtSNAPPayment,
                mainPayment = orderPayment.mainPayment;

            remainingPayment.total = (self.total.finalPriceWithTax + self.total.serviceFee.finalPriceWithTax + self.total.deliveryCost.finalPriceWithTax - order.initialCheckoutCharge) || 0;

            var orderEbtEligible = getEBTEligible(order); // what eligible lines were in the order
            var ebtEligible = getEBTEligible(); // what is currently eligible in the cart

            if (ebtEligible && remainingPayment.total > 0) {
                if (ebtEligible.cash && ebtEligible.cash.totalWithTax > 0) {
                    var cashPayment = ebtCashPayment ? (ebtCashPayment.preAuthAmount || ebtCashPayment.initialAmount) : 0;
                    remainingPayment.ebt.cash = ebtEligible.cash.eligibleWithTax - orderEbtEligible.cash.eligibleWithTax;
                }else{
                    var cashPayment = ebtCashPayment ? (ebtCashPayment.preAuthAmount || ebtCashPayment.initialAmount) : 0;
                    remainingPayment.ebt.snap = cashPayment * -1
                }
                if (ebtEligible.snap && ebtEligible.snap.totalWithTax > 0) {
                    var snapPayment = ebtSNAPPayment ? (ebtSNAPPayment.preAuthAmount || ebtSNAPPayment.initialAmount) : 0;
                    remainingPayment.ebt.snap = ebtEligible.snap.eligibleWithTax - orderEbtEligible.snap.eligibleWithTax;
                } else {
                    var snapPayment = ebtSNAPPayment ? (ebtSNAPPayment.preAuthAmount || ebtSNAPPayment.initialAmount) : 0;
                    remainingPayment.ebt.snap = snapPayment * -1
                }
            }

            return remainingPayment;
        }

        /**
         * Set the loyalty disabled flag
         * @public
         *
         * @param {boolean} [value]
         *
         * @returns {boolean}
         */
        function disableLoyalty(value) {
            if (typeof value === 'boolean') {
                if (_isLoyaltyDisabled !== value) {
                    $rootScope.$emit('cart.disableLoyalty.change', { value: value });
                    saveCart();
                }

                _isLoyaltyDisabled = value;
            }

            return _isLoyaltyDisabled;
        }

        function _validateIsDeliverItemsLimitLine(line) {
            return !line.removed && (!line.type || line.type === SP_SERVICES.CART_LINE_TYPES.PRODUCT);
        }

        function _onLinesAdded(event, data) {
            _setLinesInProducts(event, data);
            validateDeliveryItemsLimit(data.lines);
        }

        //listen to cart add and remove line to set lines in products
        function _setLinesInProducts(event, data) {
            getProducts(data.lines);
        }

        function _setChangedProducts(requestDirtyLines) {
            angular.forEach(Object.keys(requestDirtyLines), function (lineKey) {
                var line = self.lines[lineKey];
                if (!line || !line.product || !line.id) {
                    return;
                }

                var productId = line.product.id;
                if (self.changedProducts[productId]) {
                    $timeout.cancel(self.changedProducts[productId]);
                }

                self.changedProducts[productId] = $timeout(function () {
                    delete self.changedProducts[productId];
                }, 3000);
            });
        }

        function formatBase64toString(data) {
            return data ? atob(data) : data;
        }

        $rootScope.$on('cart.lines.add', _onLinesAdded);
        $rootScope.$on('cart.lines.remove', _setLinesInProducts);
    }

    /**
     * Register service
     */
    angular.module('spServices').config(['$provide', function ($provide) {
        provider = $provide.service(serviceName, [
            '$rootScope', '$injector', '$filter', '$timeout', '$q', 'Api', 'SpProductTags', 'EmbedHistoryRetailer', 'SP_SERVICES', 'PRODUCT_TAG_TYPES',
            SpCartService
        ]);

        /**
         * To init the cart id and lines (and support backwards compatibility)
         */
        provider.initCart = null;

        /**
         * Get cart's locals
         * @type {function|Array}
         * @returns {Promise} - a promise that send the following object: {
         *                          loyaltyClubIds,
         *                          languageId,
         *                          includeTaxInPrice,
         *                          withDeliveryProduct,
         *                          [userId],
         *                          [apiOptions],
         *                          [deliveryItemsLimit]
         *                      }
         */
        provider.getLocals = null;

    }]);
})(angular);

(function (angular) {
    var CART_LINE_TYPES = {
        PRODUCT: 1,
        DELIVERY: 2,
        COUPON: 3,
        REGISTER_LOYALTY: 4,
        SERVICE_FEE: 5
    };

    var CHOOSE_AREA_MODE = {
        TYPE_GOOGLE_ADDRESS: 1,
        TYPE_AREA: 2,
        CHOOSE_RETAILER: 3
    };

    var CHOOSE_AREA_EVENT = {
        NONE: 1,
        ENTER_SITE: 2,
        CART_ACTIVITY: 3
    };

    var MULTI_DOMAINS_PICKERS = {
        ZIP_CODE: 1,
        CHOOSE_RETAILER: 2,
        CHOOSE_AREA: 3
    };

    var DELIVERY_AREA_METHODS = {
        POLYGON: 1,
        ZIP_CODE: 2,
        CITY: 3,
        GOOGLE_MAPS: 4
    };

    var DELIVERY_TYPES = {
        DELIVERY: 1,
        PICKUP: 2,
        PICK_AND_GO: 3,
        SCAN_AND_GO: 4,
        EXPRESS_DELIVERY: 5
    };

    var PROMOTION_TYPES = {
        BUY_X_GET_Y_IN_PROMOTION: 1,
        BUY_X_IN_Y_PROMOTION: 2,
        TOTAL_GET_Y_IN_PROMOTION: 3,
        BUY_X_GET_Y_DISCOUNT_PROMOTION: 4,
        BUY_X_GET_Y_FIXED_DISCOUNT_PROMOTION: 5,
        DISCOUNT_TOTAL_PROMOTION: 6,
        BUY_X_GET_FIXED_DISCOUNT_PROMOTION: 7,
        BUY_X_GET_DISCOUNT_PROMOTION: 8,
        FIXED_DISCOUNT_TOTAL_PROMOTION: 9,
        TOTAL_GET_Y_DISCOUNT_PROMOTION: 10
    };

    var DELIVERY_PROVIDERS = {
        SHIP_STATION: 1,
        BUZZR: 2,
        DELIV: 3,
        BRINGOZ: 4,
        GETT: 5,
        MANUAL: 6,
        DELIVEREE: 7
    };

    var DELIVERY_TIMES_TYPES = {
        REGULAR: 1,
        DELIVERY_WITHIN_HOURS: 2,
        DELIVERY_WITHIN_DAYS: 3
    };

    var PROMOTION_ID = {
        COLLEAGUE: 'colleague'
    };

    var SOURCES = {
        ORDER: 'order',
        ORDER_HISTORY: 'orderHistory',
        ORDERS: 'orders',
        CHECKOUT_SUMMARY: 'checkoutSummary',
        RECIPE: 'recipe',
        SHOP_LIST: 'shopList',
        SMART_LIST: 'smartList'
    };

    var HUB_DOMAIN = {
        LOCAL_PATH: '.hubRetailer.domainName',
        WEB_DATA: 'frontendData',
        MOBILE_DATA: 'mobileData',
        LOCAL_STORAGE_NAME: 'isLastVisitRetailer'
    }

    // Register service
    angular.module('spServices').constant('SP_SERVICES', {
        CART_LINE_TYPES: CART_LINE_TYPES,
        CHOOSE_AREA_MODE: CHOOSE_AREA_MODE,
        CHOOSE_AREA_EVENT: CHOOSE_AREA_EVENT,
        MULTI_DOMAINS_PICKERS: MULTI_DOMAINS_PICKERS,
        DELIVERY_AREA_METHODS: DELIVERY_AREA_METHODS,
        DELIVERY_TYPES: DELIVERY_TYPES,
        PROMOTION_TYPES: PROMOTION_TYPES,
        DELIVERY_PROVIDERS: DELIVERY_PROVIDERS,
        DELIVERY_TIMES_TYPES: DELIVERY_TIMES_TYPES,
        PROMOTION_ID: PROMOTION_ID,
        SOURCES: SOURCES,
        HUB_DOMAIN: HUB_DOMAIN
    });

    // Add to root scope
    angular.module('spServices').run(['$rootScope', 'SP_SERVICES', function($rootScope, SP_SERVICES) {
        $rootScope.SP_SERVICES = SP_SERVICES;
    }]);

})(angular);

(function (angular) {
    /**
     * Service name
     */
    var serviceName = 'SpDeliveryAreasService';

    var AUTO_COMPLETE_LIMIT = 8;

    var DEFAULT_LANGUAGE_ID = 2;

    /**
     * Service
     */
    function SpDeliveryAreasService($q, $injector, $filter, Api, SP_SERVICES) {
        var self = this,
            _filterFilter = $filter('filter'),
            _orderByFilter = $filter('orderBy'),
            _limitToFilter = $filter('limitTo'),
            spGoogleMaps,
            languageIdResolver = new DataResolver($q),
            retailerConfigurationsResolver = new DataResolver($q),
            multiRetailersResolver = new DataResolver($q),
            areasResolver = new DataResolver($q),
            pickupAreasResolver = new DataResolver($q),
            _areasByNameResolver = new DataResolver($q),
            _deliveryAreasByIdResolver = new DataResolver($q),
            preferredRetailerIdResolver = new DataResolver($q),
            organizationBranchesResolver = new DataResolver($q),
            _areasDeliveryTimeTypesCache = {};

        self.setLanguageId = languageIdResolver.setData;
        self.setRetailerConfigurations = retailerConfigurationsResolver.setData;
        self.setMultiRetailers = multiRetailersResolver.setData;
        self.setPreferredRetailerId = preferredRetailerIdResolver.setData;
        self.setOrganizationBranches = organizationBranchesResolver.setData;
        self.getAreas = areasResolver.getData;
        self.getPickupAreas = pickupAreasResolver.getData;

        self.getChooseAreaMode = getChooseAreaMode;
        self.getChooseAreaEvent = getChooseAreaEvent;
        self.isZipCodeArea = isZipCodeArea;
        self.autoCompleteDeliveryAreas = autoCompleteDeliveryAreas;
        self.autoCompleteDeliveryAreasWithFullData = autoCompleteDeliveryAreasWithFullData;
        self.filterDeliveryAreas = filterDeliveryAreas;
        self.addressComponentsToSpAddress = addressComponentsToSpAddress;
        self.setRetailerIdCookie = setRetailerIdCookie;
        self.setLanguageCookie = setLanguageCookie;
        self.addAreaNotFoundLog = addAreaNotFoundLog;
        self.getAreaAddressText = getAreaAddressText;
        self.isAreaMatchTerm = isAreaMatchTerm;
        self.initBranchAndArea = initBranchAndArea;
        self.getAreasDeliveryTimeTypes = getAreasDeliveryTimeTypes;
        self.isAreaDeliveryWithinDaysOnly = isAreaDeliveryWithinDaysOnly;

        languageIdResolver.setData(DEFAULT_LANGUAGE_ID);
        retailerConfigurationsResolver.onChange(_setAreas);
        multiRetailersResolver.onChange(_setAreas);
        preferredRetailerIdResolver.onChange(_setAreasPreferredRetailerIdSort);

        /**
         * Returns the state of the choose area dialog
         * @public
         *
         * @param {boolean} allowMultiMode
         *
         * @returns {Promise<number>}
         */
        function getChooseAreaMode(allowMultiMode) {
            return $q.all({
                configurations: retailerConfigurationsResolver.getData(),
                multiRetailers: multiRetailersResolver.getData()
            }).then(function(results) {
                var isMulti = results.multiRetailers && results.multiRetailers.length > 1;
                if (allowMultiMode && isMulti && Number(results.configurations.settings.multiDomainsPicker) === SP_SERVICES.MULTI_DOMAINS_PICKERS.CHOOSE_RETAILER) {
                    return SP_SERVICES.CHOOSE_AREA_MODE.CHOOSE_RETAILER;
                }

                if (results.configurations.deliveryAreaMethod === SP_SERVICES.DELIVERY_AREA_METHODS.GOOGLE_MAPS) {
                    return SP_SERVICES.CHOOSE_AREA_MODE.TYPE_GOOGLE_ADDRESS;
                }

                return SP_SERVICES.CHOOSE_AREA_MODE.TYPE_AREA;
            });
        }

        /**
         * Returns the event in which the choose area dialog should appear on
         * @public
         *
         * @returns {Promise<number>}
         */
        function getChooseAreaEvent() {
            return $q.all({
                configurations: retailerConfigurationsResolver.getData(),
                multiRetailers: multiRetailersResolver.getData(),
                organizationBranches: organizationBranchesResolver.getData()
            }).then(function(results) {
                // when there are multi retailers or it is marked in the backend to show the dialog on enter
                if (results.multiRetailers && results.multiRetailers.length > 1 || results.configurations.settings.chooseAreaOnEnterSite === 'true') {
                    return SP_SERVICES.CHOOSE_AREA_EVENT.ENTER_SITE;
                }

                // when it is marked in the backend to show on cart activity
                if (results.configurations.settings.chooseAreaOnEnterSite === 'false') {
                    return SP_SERVICES.CHOOSE_AREA_EVENT.CART_ACTIVITY;
                }

                var branches = _getAreasBranches(results.configurations.branches, results.organizationBranches);

                // when the retailer has multi branches
                var availableBranches = [];
                angular.forEach(branches, function(branch) {
                    if (!branch.isDisabled && branch.isOnline && (branch.areas || []).find(_isAvailableArea)) {
                        availableBranches.push(branch);
                    }
                });
                if (availableBranches.length > 1) {
                    return SP_SERVICES.CHOOSE_AREA_EVENT.ENTER_SITE;
                }

                return SP_SERVICES.CHOOSE_AREA_EVENT.NONE;
            });
        }

        /**
         * Returns the array of branches filtered by the organization branches
         * @private
         *
         * @param {Array<Object>} allBranches
         * @param {Array<number>} organizationBranches
         *
         * @return {Array<Object>}
         */
        function _getAreasBranches(allBranches, organizationBranches) {
            var organizationBranchesMap = {};
            if (organizationBranches) {
                angular.forEach(organizationBranches, function(branchId) {
                    organizationBranchesMap[branchId] = true;
                });
            }

            var filtered = [];
            angular.forEach(allBranches, function(branch) {
                if (organizationBranches && organizationBranchesMap[branch.id] ||
                    !organizationBranches && !branch.isInstitutional) {
                    filtered.push(branch);
                }
            });
            return filtered;
        }

        /**
         * Returns whether the choose area is a zip code
         * @public
         *
         * @returns {Promise<boolean>}
         */
        function isZipCodeArea() {
            return $q.all({
                configurations: retailerConfigurationsResolver.getData(),
                multiRetailers: multiRetailersResolver.getData()
            }).then(function(results) {
                var multiDomainsPicker = Number(results.configurations.settings.multiDomainsPicker);
                return !!results.multiRetailers && results.multiRetailers.length > 1 &&
                    multiDomainsPicker !== SP_SERVICES.MULTI_DOMAINS_PICKERS.CHOOSE_RETAILER &&
                    multiDomainsPicker !== SP_SERVICES.MULTI_DOMAINS_PICKERS.CHOOSE_AREA;
            });
        }

        /**
         * Returns auto complete values for delivery areas
         * @public
         *
         * @param {number} chooseAreaMode
         * @param {string} address
         *
         * @returns {Promise<Array<string>>}
         */
        function autoCompleteDeliveryAreas(chooseAreaMode, address) {
            if (chooseAreaMode !== SP_SERVICES.CHOOSE_AREA_MODE.TYPE_GOOGLE_ADDRESS) {
                return _areasByNameResolver.getData().then(function(deliveryAreasByName) {
                    var names = [],
                        filteredKeys = _filterFilter(Object.keys(deliveryAreasByName), (address || '').toLowerCase());
                    angular.forEach(filteredKeys, function(key) {
                        names.push(deliveryAreasByName[key].name);
                    });
                    return _limitToFilter(_orderByFilter(names), AUTO_COMPLETE_LIMIT);
                });
            }

            // https://developers.google.com/maps/documentation/javascript/reference/places-autocomplete-service#AutocompletionRequest
            // There is no way to limit the response length
            var spGoogleMaps = _getSpGoogleMaps();
            return spGoogleMaps.spGoogleMapsUtil.placesAutocomplete(address, spGoogleMaps.GOOGLE_AUTO_COMPLETE_TYPES.GEOCODE).then(function(response) {
                var names = [];
                angular.forEach(response, function (place) {
                    names.push(place.description);
                });
                return names;
            });
        }

        function autoCompleteDeliveryAreasWithFullData(chooseAreaMode, address, placeId, zipCode, validZipCode, languageId) {
            var areasWithFullData = {
                names: null,
                places: null
            }
            if (chooseAreaMode !== SP_SERVICES.CHOOSE_AREA_MODE.TYPE_GOOGLE_ADDRESS) {
                return _areasByNameResolver.getData().then(function(deliveryAreasByName) {
                    var names = [],
                        filteredKeys = _filterFilter(Object.keys(deliveryAreasByName), (address || '').toLowerCase());
                    angular.forEach(filteredKeys, function(key) {
                        if (deliveryAreasByName[key].areas[0].deliveryTypeId == SP_SERVICES.DELIVERY_TYPES.DELIVERY) {
                            names.push(deliveryAreasByName[key].name);
                        }
                    });
                    areasWithFullData.names =_limitToFilter(_orderByFilter(names), AUTO_COMPLETE_LIMIT);
                    return areasWithFullData;
                });
            }

            // https://developers.google.com/maps/documentation/javascript/reference/places-autocomplete-service#AutocompletionRequest
            // There is no way to limit the response length
            var spGoogleMaps = _getSpGoogleMaps();
            return spGoogleMaps.spGoogleMapsUtil.placesAutocomplete(address, spGoogleMaps.GOOGLE_AUTO_COMPLETE_TYPES.GEOCODE, placeId, zipCode, validZipCode, languageId).then(function(response) {
                var names = [];
                angular.forEach(response, function (place) {
                    names.push(place.description);
                });
                areasWithFullData.names = names;
                areasWithFullData.places = response;
                return areasWithFullData;
            });
        }

        /**
         * Returns delivery areas for the given address
         * @public
         *
         * @param {number} chooseAreaMode
         * @param {string} address
         *
         * @returns {Promise<{areas: Array<SpDeliveryArea>, [boundsAddress]: boolean, [addressComponents]: Array<Object>}>, [hasStreetNumber]: boolean}
         */
        function filterDeliveryAreas(chooseAreaMode, address, ignoreMultipleAddresses, placeId) {
            if (chooseAreaMode !== SP_SERVICES.CHOOSE_AREA_MODE.TYPE_GOOGLE_ADDRESS) {
                return _areasByNameResolver.getData().then(function (deliveryAreasByName) {

                    var areas = (deliveryAreasByName[(address || '').toLowerCase()] || {}).areas;
                    if (!areas && Object.values(deliveryAreasByName) && Object.values(deliveryAreasByName).length) {
                        var foundArea = Object.values(deliveryAreasByName).find(function (item) {
                            return item && item.areas && item.areas.length && item.areas.some(function (area) {
                                return area.deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.DELIVERY && area.name.toLowerCase() === address.toLowerCase() && area.branch && area.branch.areas;
                            })
                        })

                        if (foundArea) {
                            areas = foundArea.areas;
                        }
                    }
                    if (areas != undefined && areas.length != 0) {
                        var filteredAreas = areas.filter(function(area) {
                            return area.deliveryTypeId == SP_SERVICES.DELIVERY_TYPES.DELIVERY;
                        });
                        return {areas: filteredAreas};
                    }
                    else return {areas: []};
                });
            }

            return $q.all({
                languageId: languageIdResolver.getData(),
                multiRetailers: multiRetailersResolver.getData(),
                areasById: _deliveryAreasByIdResolver.getData()
            }).then(function (results) {
                var promises = [];
                angular.forEach(results.multiRetailers, function (retailer) {
                    // TODO rethink whether to leave it like this or develop a new route in the rest for areas of multi retailers
                    promises.push(Api.request({
                        url: '/v2/retailers/' + retailer.id + '/areas',
                        method: 'GET',
                        cache: true,
                        params: {
                            query: address,
                            deliveryTypeId: [SP_SERVICES.DELIVERY_TYPES.DELIVERY, SP_SERVICES.DELIVERY_TYPES.EXPRESS_DELIVERY],
                            languageId: results.languageId,
                            ignoreMultipleAddresses: ignoreMultipleAddresses,
                            placeId: placeId
                        }
                    }, {
                        fireAndForgot: true
                    }).then(function (response) {
                        var foundAreas = [];
                        angular.forEach(response.areas, function (area) {
                            if (results.areasById[area.id]) {
                                foundAreas.push(results.areasById[area.id]);
                            }
                        });
                        return {
                            areas: foundAreas,
                            boundsAddress: response.boundsAddress,
                            addressComponents: response.addressComponents
                        };
                    }).catch(function (err) {
                        if (err.statusCode !== 404) {
                            throw err;
                        } else {
                            return {areas: []};
                        }
                    }));
                });
                return $q.all(promises);
            }).then(function (responses) {
                var joined = {areas: []};
                angular.forEach(responses, function (response) {
                    joined.boundsAddress = joined.boundsAddress || response.boundsAddress;
                    joined.addressComponents = joined.addressComponents || response.addressComponents;
                    Array.prototype.push.apply(joined.areas, response.areas);
                });
                joined.hasStreetNumber = joined.addressComponents && joined.addressComponents.length && _isAddressComponentsContainsStreetNumberType(joined.addressComponents);

                return joined;
            });
        }

        /**
         * return whether there's a street number type in one of the addresses
         * @private
         *
         * @param {ArraysObject>} addressComponents
         *
         * @returns {boolean}
         */
        function _isAddressComponentsContainsStreetNumberType(addressComponents) {
            return !!addressComponents.find(function (addressComponent) {
                return addressComponent.types.find(function (type) {
                    return type === 'street_number'
                });
            });
        }

        function addressComponentsToSpAddress(addressComponents) {
            var data = {};
            angular.forEach(addressComponents, function (addressComponent) {
                angular.forEach(addressComponent.types, function (type) {
                    if ((!data.city && type === 'locality') || type === 'sublocality') {
                        data.city = addressComponent.long_name;
                    } else if (type === 'route') {
                        data.route = addressComponent.long_name;
                    } else if (type === 'street_number') {
                        data.number = addressComponent.long_name;
                    } else if (type === 'postal_code') {
                        data.zipCode = addressComponent.long_name;
                    } else if (type === 'premise') {
                        data.premise = addressComponent.long_name;
                    }
                });
            });

            return {
                city: data.city ,
                text1: data.number || data.route || data.premise ?
                    (data.number || '') + ((data.number && data.route) || (data.number && data.premise) ? ' ' : '') + (data.premise ? data.premise + ' ' : '') + (data.route || '') :
                    '',
                zipCode: data.zipCode
            };
        }

        /**
         * Set the given retailer id into the cookie
         * @public
         *
         * @param {number} retailerId
         *
         * @returns {Promise}
         */
        function setRetailerIdCookie(retailerId) {
            return Api.request({
                method: 'PUT',
                url: '/frontend/' + retailerId
            });
        }

        /**
         * Set the given lang into the cookie
         * tempoary put there
         * @public
         *
         * @param {string} lang : en, he ...
         *
         * @returns {Promise}
         */
        function setLanguageCookie(lang) {
            return Api.request({
                method: 'PUT',
                url: '/frontend/lang/' + lang
            });
        }

        /**
         * Add area not found log
         * @public
         *
         * @param {number} address
         * @param {string} userEmail
         *
         * @returns {Promise}
         */
        function addAreaNotFoundLog(address, userEmail) {
            return retailerConfigurationsResolver.getData().then(function(configurations) {
                return Api.request({
                    method: 'POST',
                    url: '/v2/retailers/' + configurations.id + '/areas/_addAreaLog',
                    data: {
                        area: address,
                        userEmail: userEmail
                    }
                });
            });
        }

        /**
         * Returns the appropriate address text for the given area
         * @public
         *
         * @param {SpDeliveryArea} area
         * @param {boolean} [includeBranchName=true]
         * @param {object} [branch]
         *
         * @returns {string}
         */
        function getAreaAddressText(area, includeBranchName, branch) {
            branch = branch || area.branch;

            var textBuilder = [];
            if (includeBranchName !== false) {
                textBuilder.push(branch.name);
            }

            var multiplePickupAddresses = false;
            if (area && (area.deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.PICKUP)) {
                if (branch.pickupAreasCount === undefined) {
                    branch.pickupAreasCount = 0;
                    angular.forEach(branch.areas, function(area) {
                        if (_isAvailableArea(area) && area.deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.PICKUP) {
                            branch.pickupAreasCount++;
                        }
                    });
                }

                multiplePickupAddresses = branch.pickupAreasCount > 1;
            }

            if (multiplePickupAddresses) {
                textBuilder.push(area.name);
            } else {
                var locationBuilder = [];
                if (branch.location) {
                    locationBuilder.push(branch.location);
                }
                if (branch.city && (branch.location || '').indexOf(branch.city) === -1) {
                    locationBuilder.push(branch.city);
                }
                textBuilder.push(locationBuilder.join(', '));
            }


            return textBuilder.join(' - ');
        }

        /**
         * Returns whether or not the given area matches the given string term
         * @public
         *
         * @param {SpDeliveryArea} area
         * @param {string} term
         *
         * @returns {boolean}
         */
        function isAreaMatchTerm(area, term) {
            if (!term) {
                return true;
            }

            term = term.toLowerCase();
            return area.name.toLowerCase().indexOf(term) > -1 ||
                getAreaAddressText(area).toLowerCase().indexOf(term) > -1 ||
                area.branch.name.toLowerCase().indexOf(term) > -1;
        }

        /**
         * @typedef {Object} InitBranchAndAreaOptions
         *
         * @property {number} branchId
         * @property {number} branchAreaId
         * @property {string} branchAreaText
         * @property {number} queryBranchId
         * @property {string} queryZipCode
         * @property {string} isPrerender
         */

        /**
         * Will initiate the branch and the area
         * @public
         *
         * @param {InitBranchAndAreaOptions} options
         *
         * @returns {Promise<{branch: Object, branchAreaId: number, branchAreaText: string, isQueryBranch: boolean, isQueryArea: boolean}>}
         */
        function initBranchAndArea(options) {
            return $q.all({
                retailer: retailerConfigurationsResolver.getData(),
                isZipCodeArea: isZipCodeArea(),
                chooseAreaEvent: getChooseAreaEvent(),
                chooseAreaMode: getChooseAreaMode(false),
                organizationBranches: organizationBranchesResolver.getData()
            }).then(function(results) {
                var response = {
                    isQueryBranch: !!options.queryBranchId,
                    isQueryArea: false
                };

                var branches = _getAreasBranches(results.retailer.branches, results.organizationBranches),
                    branchesWithAreas = [],
                    defaultWithAreaBranch,
                    savedBranchId = options.queryBranchId || options.branchId;
                angular.forEach(branches, function(branch) {
                    if (branch.isDisabled || !branch.isOnline) {
                        return;
                    }

                    if (savedBranchId === branch.id) {
                        return response.branch = branch;
                    }

                    var availableArea = (branch.areas || []).find(_isAvailableArea);
                    if (availableArea) {
                        if (branch.isDefault) {
                            defaultWithAreaBranch = branch;
                        }
                        branchesWithAreas.push(branch);
                    }
                });

                if (!response.branch) {
                    if (!branchesWithAreas.length) {
                        response.branch = branches[0] || results.retailer.branches[0];
                    } else if (branchesWithAreas.length === 1 || options.isPrerender ||
                        results.chooseAreaEvent === SP_SERVICES.CHOOSE_AREA_EVENT.CART_ACTIVITY) {
                        response.branch = defaultWithAreaBranch || branchesWithAreas[0];
                    }
                }

                if (!response.branch) {
                    return response;
                }

                var area;
                if (options.branchAreaText) {
                    response.branchAreaText = options.branchAreaText;
                }
                if (options.branchAreaId && !isNaN(options.branchAreaId)) {
                    response.branchAreaId = options.branchAreaId;
                }

                if (results.isZipCodeArea && options.queryZipCode && results.chooseAreaMode === SP_SERVICES.CHOOSE_AREA_MODE.TYPE_AREA) {
                    area = response.branch.areas.find(function(area) {
                        return (area.names || []).find(function(areaName) {
                            return options.queryZipCode === areaName.name;
                        });
                    });

                    if (area) {
                        response.branchAreaText = options.queryZipCode;
                        response.branchAreaId = area.id;
                    }
                }

                //make sure the saved area is in the branch areas
                if (response.branchAreaId || response.branchAreaText) {
                    area = response.branch.areas.find(function(area) {
                        return area.id === response.branchAreaId &&
                            _isAvailableInitArea(area, results.chooseAreaEvent, results.chooseAreaMode);
                    });

                    if (!area) {
                        response.branchAreaId = response.branchAreaText = null;
                    } else if (results.chooseAreaMode === SP_SERVICES.CHOOSE_AREA_MODE.TYPE_AREA && area.names && area.names.length) {
                        var areaName = area.names.find(function(name) {
                            return name.name === response.branchAreaText;
                        });

                        if (!areaName) {
                            response.branchAreaText = area.names[0].name;
                        }
                    } else {
                        response.branchAreaText = area.name;
                    }
                }

                // take the first available area when a branch id is in the query
                if ((!response.branchAreaId || !response.branchAreaText) && options.queryBranchId) {
                    response.isQueryArea = true;
                    area = response.branch.areas.find(function(area){
                        return _isAvailableInitArea(area, results.chooseAreaEvent, results.chooseAreaMode);
                    });
                    if (area) {
                        if (results.chooseAreaMode === SP_SERVICES.CHOOSE_AREA_MODE.TYPE_AREA) {
                            if (area.names && area.names.length) {
                                response.branchAreaText = area.names[0].name;
                            }
                        } else {
                            response.branchAreaText = area.name;
                        }
                        response.branchAreaId = area.id;
                    }
                }

                // when the popup will not show, choose the default areas. 
                // If dont have available default areas, choose the first area (prefer the first delivery area).
                if (!response.branchAreaId && (options.isPrerender || results.chooseAreaEvent === SP_SERVICES.CHOOSE_AREA_EVENT.NONE)) {
                    var chosenArea;
                    chosenArea = response.branch.areas.find(function(area){
                        return _isAvailableInitArea(area) && area.isDefault;
                    });

                    if(!chosenArea) {
                        for (var i = 0; i < response.branch.areas.length; i++) {
                            if (!_isAvailableArea(response.branch.areas[i])) {
                                continue;
                            }
                            if (!chosenArea && (results.retailer.settings.deliveryMethodDefault === SP_SERVICES.DELIVERY_TYPES.DELIVERY ||
                                response.branch.areas[i].deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.DELIVERY) ||
                                response.branch.areas[i].deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.DELIVERY &&
                                chosenArea.deliveryTypeId !== SP_SERVICES.DELIVERY_TYPES.DELIVERY) {
                                chosenArea = response.branch.areas[i];

                                // If the first area matches and the user did not choose delivery or pickup (Choose area on enter to site feature enabled)
                                // we set the first area as a default one.
                                if(chosenArea && (!options.branchAreaId || !options.branchAreaText)) {
                                    break;
                                }
                            }

                            if (chosenArea && chosenArea.deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.DELIVERY) {
                                break;
                            }
                        }
                    }
                    
                    if (chosenArea) {
                        response.branchAreaId = chosenArea.id;
                        response.branchAreaText = chosenArea.name;
                    }
                }

                return response;
            });
        }

        function _isAvailableInitArea(area, chooseAreaEvent, chooseAreaMode) {
            // when the area is not available it can't be chosen
            if (!_isAvailableArea(area)) {
                return false
            }

            // when the choose area dialog won't be opened, do not require names
            if (chooseAreaEvent === SP_SERVICES.CHOOSE_AREA_EVENT.NONE) {
                return true;
            }

            // when the given area is of pickup type, it will never need any names in the dialog
            if (area.deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.PICKUP) {
                return true;
            }

            // when the choose area dialog will open on type area mode, the area must contain names
            return chooseAreaMode !== SP_SERVICES.CHOOSE_AREA_MODE.TYPE_AREA || area.names && area.names.length || area.name;
        }

        /**
         * Returns the necessary spGoogleMaps services
         * @private
         *
         * @returns {{spGoogleMapsUtil: Object, GOOGLE_AUTO_COMPLETE_TYPES: Object}}
         */
        function _getSpGoogleMaps() {
            if (spGoogleMaps) {
                return spGoogleMaps;
            }

            try {
                return spGoogleMaps = {
                    spGoogleMapsUtil: $injector.get('spGoogleMapsUtil'),
                    GOOGLE_AUTO_COMPLETE_TYPES: $injector.get('GOOGLE_AUTO_COMPLETE_TYPES')
                };
            } catch(err) {
                throw new Error('In order to filter areas by google address the sp-google-maps module must be included.' +
                    '\r\nSee https://bitbucket.org/{companyName}/sp-google-maps.git');
            }
        }

        /**
         * Set the area resolvers data
         * @private
         *
         * @returns {Promise}
         */
        function _setAreas() {
            return $q.all({
                retailer: retailerConfigurationsResolver.getData(),
                retailers: multiRetailersResolver.getData(),
                preferredRetailerId: preferredRetailerIdResolver.hasData ? preferredRetailerIdResolver.getData() : undefined,
                organizationBranches: organizationBranchesResolver.getData()
            }).then(function(results) {
                var areas = [],
                    pickupAreas = [],
                    deliveryAreasByName = {},
                    deliveryAreasById = {},
                    branchesById = {};
                angular.forEach(results.retailers, function(retailer) {
                    var branches = _getAreasBranches(retailer.branches, results.organizationBranches);

                    angular.forEach(branches, function(branch) {
                        if (branch.isDisabled || !branch.isOnline) {
                            return;
                        }

                        branchesById[branch.id] = branchesById[branch.id] || angular.extend({}, branch, {
                            pickupAreasCount: 0
                        });

                        angular.forEach(branch.areas, function(area) {
                            if (!_isAvailableArea(area)) {
                                return;
                            }

                            var toPush = angular.extend({}, area, {
                                branch: branchesById[branch.id],
                                retailer: retailer
                            });

                            _setAreaPreferredRetailerIdSort(toPush, results.preferredRetailerId);

                            areas.push(toPush);
                            deliveryAreasById[toPush.id] = toPush;

                            var usedNames = {};
                            var lowerCase;
                            if (area.names && area.names.length) {
                                angular.forEach(area.names, function (areaName) {
                                    lowerCase = areaName.name.toLowerCase();
                                    if (usedNames[lowerCase]) {
                                        return;
                                    }

                                    usedNames[lowerCase] = true;
                                    if (!deliveryAreasByName[lowerCase]) {
                                        deliveryAreasByName[lowerCase] = {
                                            name: areaName.name,
                                            areas: []
                                        };
                                    }
                                    deliveryAreasByName[lowerCase].areas.push(toPush);
                                });
                            } else {
                                lowerCase = area.name.toLowerCase();
                                if (!deliveryAreasByName[lowerCase]) {
                                    deliveryAreasByName[lowerCase] = {
                                        name: area.name,
                                        areas: []
                                    };
                                }
                                deliveryAreasByName[lowerCase].areas.push(toPush);
                            }

                            if (area.deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.PICKUP) {
                                branchesById[branch.id].pickupAreasCount++;
                                pickupAreas.push(toPush);
                            }
                        });
                    });
                });
                areasResolver.setData(areas);
                pickupAreasResolver.setData(pickupAreas);
                _areasByNameResolver.setData(deliveryAreasByName);
                _deliveryAreasByIdResolver.setData(deliveryAreasById);
            });
        }

        function _isAvailableArea(area) {
            return area.deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.DELIVERY ||
                area.deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.EXPRESS_DELIVERY ||
                area.deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.PICKUP;
        }

        function _setAreasPreferredRetailerIdSort() {
            // when there aren't areas yet do not try to set the is preferred, it will be set with the areas
            if (!areasResolver.hasData) {
                return;
            }

            return $q.all({
                preferredId: preferredRetailerIdResolver.hasData ? preferredRetailerIdResolver.getData() : undefined,
                areas: areasResolver.getData()
            }).then(function(results) {
                angular.forEach(results.areas, function(area) {
                    _setAreaPreferredRetailerIdSort(area, results.preferredId);
                });
            });
        }

        function _setAreaPreferredRetailerIdSort(area, preferredRetailerId) {
            area.preferredRetailerSort = area.retailer.id === preferredRetailerId ? 1 : 0;
        }

        /**
         * Returns the delivery time types by area
         * @public
         *
         * @param {Array<Object>} areas
         * @param {boolean} [ignoreRetailerConfig]
         *
         * @return {Promise<Object>}
         */
        function getAreasDeliveryTimeTypes(areas, ignoreRetailerConfig) {
            var byRetailer = {},
                res = {};
            angular.forEach(areas, function(area) {
                if (_areasDeliveryTimeTypesCache[area.id]) {
                    res[area.id] = _areasDeliveryTimeTypesCache[area.id];
                    return;
                }

                byRetailer[area.retailer.id] = byRetailer[area.retailer.id] || [];
                byRetailer[area.retailer.id].push(area.id);
            });

            return $q.resolve().then(function() {
                if (!Object.keys(byRetailer).length) {
                    return [];
                }

                return multiRetailersResolver.getData();
            }).then(function(retailers) {
                if (!retailers.length) {
                    return [];
                }

                var promises = [];
                angular.forEach(retailers, function(retailer) {
                    if (!byRetailer[retailer.id] || !ignoreRetailerConfig && !retailer.isDeliveryWithinDaysByTag) {
                        return;
                    }

                    promises.push(Api.request({
                        method: 'GET',
                        url: '/v2/retailers/' + retailer.id + '/areas/delivery-time-types',
                        params: { areaId: byRetailer[retailer.id] }
                    }));
                });

                return $q.all(promises)
            }).then(function(retailersResults) {
                angular.forEach(retailersResults, function(retailersResult) {
                    angular.forEach(retailersResult.areas, function(areaResult) {
                        res[areaResult.id] = areaResult.deliveryTimeTypes;
                        _areasDeliveryTimeTypesCache[areaResult.id] = areaResult.deliveryTimeTypes;
                    });
                });
                return res;
            });
        }

        /**
         * Returns whether the given area id has delivery within days slots only
         * @public
         *
         * @param {number} areaId
         * @param {number} retailerId
         * @param {boolean} [ignoreRetailerConfig]
         *
         * @returns {Promise<boolean>}
         */
        function isAreaDeliveryWithinDaysOnly(areaId, retailerId, ignoreRetailerConfig) {
            return getAreasDeliveryTimeTypes([{
                id: areaId,
                retailer: {
                    id: retailerId
                }
            }], ignoreRetailerConfig).then(function(areasMap) {
                return !!(areasMap[areaId] && areasMap[areaId].length === 1 &&
                    areasMap[areaId][0] === SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_DAYS);
            });
        }
    }

    function DataResolver($q) {
        var self = this,
            dataDefer,
            _changeListeners = [];

        self.setData = setData;
        self.getData = getData;
        self.onChange = onChange;
        self.unOnChange = unOnChange;
        self.hasData = false;

        /**
         * Saves the data on the data resolver
         * @public
         *
         * @param {*} data
         *
         * @returns {Promise}
         */
        function setData(data) {
            return $q.resolve().then(function() {
                if (!self.hasData) {
                    self.hasData = data !== undefined;
                    return;
                }

                return _getDataDefer().promise;
            }).then(function(savedData) {
                // do not resolve the new value when it is the value that is already saved
                if (savedData && savedData === data) {
                    return;
                }
                // when the defer was already resolved with previous data - reset it
                if (savedData) {
                    _resetDataPromise();
                }
                // when the data is undefined the promise should stay unresolved
                if (data !== undefined) {
                    _getDataDefer().resolve(data);
                }
                _invokeOnChange(data, savedData);
            });
        }

        /**
         * Returns a promise which will be resolved when the data will be set
         * @public
         *
         * @returns {Promise<number>}
         */
        function getData() {
            return _getDataDefer().promise;
        }

        /**
         * Listen to change of value
         * @public
         *
         * @param {function} callback
         */
        function onChange(callback) {
            _changeListeners.push(callback);
        }

        /**
         * Cancel listen of a callback to the change of value
         * @public
         *
         * @param {function} callback
         */
        function unOnChange(callback) {
            for (var i = _changeListeners.length - 1; i >= 0; i--) {
                if (callback === _changeListeners[i]) {
                    _changeListeners.splice(i, 1);
                    return;
                }
            }

        }

        /**
         * Returns a deferred promise for the data
         * @private
         *
         * @returns {Deferred}
         */
        function _getDataDefer() {
            if (!dataDefer) {
                dataDefer = $q.defer();
            }

            return dataDefer;
        }

        /**
         * Resets the data promise when it was fulfilled
         * @private
         */
        function _resetDataPromise() {
            if (self.hasData) {
                self.hasData = false;
                dataDefer = undefined;
            }
        }

        /**
         * Invoke the on change listeners
         * @private
         *
         * @param {*} data
         * @param {*} prevData
         */
        function _invokeOnChange(data, prevData) {
            angular.forEach(_changeListeners, function(callback) {
                callback(data, prevData);
            });
        }
    }

    /**
     * @typedef {Object} SpDeliveryArea
     *
     * @property {number} id
     * @property {string} name
     * @property {object} branch
     * @property {number} branch.id
     * @property {string} branch.city
     * @property {object} retailer
     * @property {number} retailer.id
     * @property {string} [retailer.title]
     */

    /**
     * Register service
     */
    angular.module('spServices').service(serviceName, ['$q', '$injector', '$filter', 'Api', 'SP_SERVICES', SpDeliveryAreasService]);
})(angular);

(function(angular) {
	/**
	 * Service name
	 */
	var serviceName = 'SpDeliveryTimesService';

	/**
	 * @typedef {Object} SpDeliveryTimesServiceLocals
	 *
	 * @property {number} timeZoneOffset
	 * @property {number} languageId
	 */

	/**
	 * Provider instance
	 *
	 * @property {Array|function|Function} [getLocals] - ng inject. should return SpDeliveryTimesServiceLocals
	 */
	var provider = {};

	/**
	 * Branch delivery types
	 * @type {Object}
	 */
	var BRANCH_DELIVERY_TYPES = {
		DELIVERY: 1,
		PICKUP: 2,
		PICK_AND_GO: 3,
		SCAN_AND_GO: 4,
		EXPRESS_DELIVERY: 5
	};

	/**
	 * Service
	 */
	function SpDeliveryTimesService($controller, $q, $timeout, Api, SpCartService, SpDeliveryAreasService, SP_SERVICES, PRODUCT_TAG_TYPES) {
		var self = this;

		self.init = init;
		self.getTimes = getTimes;
		self.getNextDeliveryTimes = getNextDeliveryTimes;
		self.setNextDeliveryCurrentSlots = setNextDeliveryCurrentSlots;
		self.timesObject = timesObject;
		self.now = now;
		self.getExpressTimeProductTagLimits = getExpressTimeProductTagLimits;
		self.isDeliveryWithinDaysOnly = isDeliveryWithinDaysOnly;
		var areaData = {};
		var areaDataLoading = {};
		var areaDataResolves = {};
		/**
		 * Gets the locals
		 * @private
		 *
		 * @returns {Object}
		 */
		function _getLocals() {
			if (!provider.getLocals || (!angular.isFunction(provider.getLocals) && !angular.isArray(provider.getLocals))) {
				throw new Error(serviceName + 'Provider.getLocals must be implemented');
			}

			return $controller(provider.getLocals);
		}

		/**
		 * Init
		 * @public
		 *
		 * @param {Object} [apiOptions]
		 *
		 * @return {Promise.<Object>}
		 */
		function init(apiOptions) {
			return Api.request({
				url: '/v2/retailers/:rid/branches/:bid/areas/types'
			}, apiOptions).then(function(types) {
				self.types = types;
				return self.types;
			});
		}

		/**
		 * Get times
		 * @public
		 *
		 * @param {Number} type
		 * @param {Object} options
		 * @param {String} [options.address]
		 * @param {Number} [options.cartId]
		 * @param {Number} [options.areaId]
		 * @param {Number} [options.languageId]
		 * @param {Date} [options.startDate]
		 * @param {Number} [options.lat]
		 * @param {Number} [options.lng]
		 * @param {string} [options.externalPlaceId]
		 * @param {boolean} [options.isDeliveryWithinDaysByTag]
		 * @param {Date} [options.maxDisplayDays]
		 *
		 * @returns {Promise}
		 */
		function getTimes(type, options) {
			var params = {
				deliveryTypeId: (type === BRANCH_DELIVERY_TYPES.DELIVERY ? [BRANCH_DELIVERY_TYPES.DELIVERY, BRANCH_DELIVERY_TYPES.EXPRESS_DELIVERY] : type),
				address: (type === BRANCH_DELIVERY_TYPES.DELIVERY ? options.address : null),
				cartId: options.cartId,
				areaId: options.areaId,
				languageId: options.languageId || 2,
				startDate: options.startDate,
				lat: options.lat,
				lng: options.lng,
				externalPlaceId: options.externalPlaceId,
				isGoogleMapDropPinNotDetect: options.isGoogleMapDropPinNotDetect,
				maxDisplayDays: options.maxDisplayDays
			};

			if (options.isDeliveryWithinDaysByTag && type !== BRANCH_DELIVERY_TYPES.PICKUP) {
				if (_isOnlyDeliveryWithinTimes()) {
					params.deliveryTimeTypeId = SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_DAYS;
				} else {
					params.notDeliveryTimeTypeId = SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_DAYS;
				}
			}
			var paramsCacheKey = JSON.stringify(params);

			if (areaData[paramsCacheKey]) {
				return $q.resolve(areaData[paramsCacheKey]);
			}

			// if the same call is call. puh it to queue, and first call is finish it will return result of this call.
			if(areaDataLoading[paramsCacheKey]){
				return $q(function(resolve, reject){
					if(!areaDataResolves[paramsCacheKey]){
						areaDataResolves[paramsCacheKey] = []
					}
					areaDataResolves[paramsCacheKey].push({resolve: resolve, reject: reject})
				});

			}

			areaDataLoading[paramsCacheKey] = true;


			return Api.request({
				method: 'GET',
				url: '/v2/retailers/:rid/branches/:bid/areas',
				params: params
			}, {
				fireAndForgot: true
			}).then(function(resp) {
				var result = { times: resp.timesObject.times, area: resp };
				areaData[paramsCacheKey] = result;
				areaDataLoading[paramsCacheKey] = false;

				if(areaDataResolves[paramsCacheKey] && areaDataResolves[paramsCacheKey].length > 0){

					angular.forEach(areaDataResolves[paramsCacheKey], function(r){
						r.resolve(areaData[paramsCacheKey]);
					});
					delete areaDataResolves[paramsCacheKey];
				}

				// auto clear cache after 30s
				$timeout(function(){
					delete areaData[paramsCacheKey];
				}, 1000 * 5);// 5s

				return result;
			}).finally(function(){
				areaDataLoading[paramsCacheKey] = false;
			});
		}

		/**
		 * Get next deliveries time slots
		 * @public
		 *
		 * @param {Object} nextDelivery
		 * @param {function(): Promise<Object>} getUserDataCb
		 * @param {Object} [area]
		 * @param {number} [area.id]
		 * @param {number} [area.deliveryTypeId]
		 *
		 * @returns {Promise<{nextTime: Object, area: Object, noAvailableDeliverySlots: boolean}|void>}
		 */
		function getNextDeliveryTimes(nextDelivery, getUserDataCb, area, cartId) {
			var deliveryPromises = {};

			// deliveryPromises[BRANCH_DELIVERY_TYPES.DELIVERY] = $q.resolve().then(function() {
			// 	if (area && area.deliveryTypeId === BRANCH_DELIVERY_TYPES.DELIVERY) {
			// 		return { areaId: area.id }
			// 	}
			//
			// 	return getUserDataCb();
			// }).then(function(options) {
			// 	if (area.deliveryTypeId && area.deliveryTypeId === BRANCH_DELIVERY_TYPES.DELIVERY) {
			// 		options.cartId = cartId;
			// 	}
			//
			// 	return self.getTimes(BRANCH_DELIVERY_TYPES.DELIVERY, options);
			// }).catch(function() {
			// 	return null;
			// });

			var options = {};

			if (area && area.deliveryTypeId === BRANCH_DELIVERY_TYPES.DELIVERY) {
				options.areaId = area.id;
				options.cartId = cartId;
			}

			deliveryPromises[BRANCH_DELIVERY_TYPES.DELIVERY] = self
				.getTimes(BRANCH_DELIVERY_TYPES.DELIVERY, options)
				.catch(function() { return null });

			options = {};

			if (area && area.deliveryTypeId === BRANCH_DELIVERY_TYPES.PICKUP) {
				options.areaId = area.id;
				options.cartId = cartId;
			}

			deliveryPromises[BRANCH_DELIVERY_TYPES.PICKUP] = self
				.getTimes(BRANCH_DELIVERY_TYPES.PICKUP, options)
				.catch(function() { return null });

			nextDelivery.times = nextDelivery.times || [];
			if (nextDelivery.times.length > 1) {
				nextDelivery.times.splice(1);
			}

			return $q.all(deliveryPromises).then(function(responses) {
				var preparedResponse = _setTimes(responses, nextDelivery, area);

				if (!preparedResponse.nextTime) {
					nextDelivery.times.splice(0);
					return preparedResponse;
				}

				if (nextDelivery.times.length && nextDelivery.times[0].id === preparedResponse.nextTime.id) {
					return preparedResponse;
				}

				nextDelivery.times.unshift(preparedResponse.nextTime);
				preparedResponse.nextTime._isActiveInNav = false;
				$timeout(function() {
					preparedResponse.nextTime._isActiveInNav = true;
				}, 0);

				return preparedResponse;
			}).catch(function() {
				nextDelivery.times.splice(0);
			})
		}

		/**
		 * Set time slots for deliveries
		 * @private
		 *
		 * @param {Object} respByDeliveryType
		 * @param {Object} nextDelivery
		 * @param {Object} [area]
		 * @param {number} [area.id]
		 * @param {number} [area.deliveryTypeId]
		 *
		 * @returns {{nextTime: Object, area: Object, noAvailableDeliverySlots: boolean}}
		 */
		function _setTimes(respByDeliveryType, nextDelivery, area) {
			var nextTime,
				noAvailableDeliverySlots = false,
				respArea,
				dateNow = now();

			nextDelivery.moreSlots = {};
			nextDelivery.isAllSpecialSlots = true;
			angular.forEach(respByDeliveryType, function(resp, deliveryType) {
				if (!resp) {
					return;
				}

				if (!resp.area.areas &&
					(resp.area.deliveryTypeId === BRANCH_DELIVERY_TYPES.DELIVERY ||
					resp.area.deliveryTypeId === BRANCH_DELIVERY_TYPES.EXPRESS_DELIVERY) &&
					resp.area.haveAvailableDeliverySlots === false) {
					noAvailableDeliverySlots = true;
				}

				var areas = [resp.area];
				if (resp.area.areas && resp.area.areas.length > 1) {
					areas = resp.area.areas;
				}
				angular.forEach(areas, function(_area) {
					if (!respArea || area && _area.id === area.id) {
						respArea = _area;
					}

					var areaExistingSlots = _area.timesObject.times.localDelivery,
						areaSlots = {};

					var isChosenDeliveryType = !area || _area.deliveryTypeId === area.deliveryTypeId,
						prevNextTime = nextTime;
					if (areaExistingSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_HOURS]) {
						areaSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_HOURS] =
							areaExistingSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_HOURS];

						// try to set the next time as the delivery withing hours
						if (!nextTime && isChosenDeliveryType) {
							nextTime = areaExistingSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_HOURS];
						}
					}

					angular.forEach(areaExistingSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.REGULAR], function(slot) {
						if (!slot.times.length) {
							return;
						}

						slot.deliveryTypeId = deliveryType;
						areaSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.REGULAR] = areaSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.REGULAR] || [];
						areaSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.REGULAR].push(slot);

						angular.forEach(slot.times, function(time) {
							if (nextDelivery.isAllSpecialSlots) {
								nextDelivery.isAllSpecialSlots = !!(time.deliveryProduct && time.deliveryProduct.branch.specials && time.deliveryProduct.branch.specials.length && !time.deliveryProduct.branch.specials[0].isCoupon);
							}

							if (!isChosenDeliveryType || nextTime && time.newFrom >= nextTime.newFrom) {
								return;
							}

							nextTime = time;
							nextTime.isToday = slot.isToday;
							nextTime.date = slot.date;
							nextTime.isWithinSixDays =  new Date(nextTime.date).getTime() <= dateNow.setUTCDate(dateNow.getUTCDate() + 6);


						});
					});

					if (areaExistingSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_DAYS]) {
						areaSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_DAYS] =
							areaExistingSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_DAYS];

						if (isChosenDeliveryType && !nextTime) {
							nextTime = areaExistingSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_DAYS];
						}
					}

					// when the nextTime was set from the current area, use its delivery type
					if (nextTime && nextTime !== prevNextTime) {
						nextDelivery.currentNextDeliveryTypeId = _area.deliveryTypeId;
					}

					nextDelivery.moreSlots[deliveryType] = nextDelivery.moreSlots[deliveryType] || {};
					nextDelivery.moreSlots[deliveryType][_area.id] = { areaName: _area.name, slots: areaSlots };
				});
			});

			nextDelivery.moreSlotsTypes = Object.keys(nextDelivery.moreSlots);

			return {
				nextTime: nextTime,
				noAvailableDeliverySlots: noAvailableDeliverySlots,
				area: respArea
			};
		}

		/**
		 * Set the currentMoreSlots object on the given nextDelivery object
		 * @public
		 *
		 * @param {Object} nextDelivery
		 * @param {Object} areasSlots
		 * @param {string|number} deliveryTypeId
		 */
		function setNextDeliveryCurrentSlots(nextDelivery, areasSlots, deliveryTypeId) {
			nextDelivery.currentMoreSlots = { deliveryTypeId: Number(deliveryTypeId) };

			if(areasSlots){
				nextDelivery.currentMoreSlots.areas = Object.keys(areasSlots).reduce(function(acc, areaId) {
					if (areasSlots[areaId] && areasSlots[areaId].slots && Object.keys(areasSlots[areaId].slots).length) {
						acc[areaId] = areasSlots[areaId];
					}
					return acc;
				}, {});

				nextDelivery.currentMoreSlots.length = Object.keys(nextDelivery.currentMoreSlots.areas).length;
			}

		}

		/**
		 * Create times object
		 * @public
		 *
		 * @param {Object} [timesByDayInWeek={}]
		 * @param {Object} [timesObject={}]
		 * @param {number} [numberOfDays=7]
		 *
		 * @returns {Object}
		 */
		function timesObject(timesByDayInWeek, timesObject, numberOfDays) {
			timesByDayInWeek = timesByDayInWeek || {};
			timesObject = timesObject || {};
			timesObject[SP_SERVICES.DELIVERY_TIMES_TYPES.REGULAR] = [];

			var now = self.now(),
				end = new Date(now);
			now.setUTCHours(0, 0, 0, 0);
			end.setUTCHours(0, 0, 0, 0);
			end.setUTCDate(end.getUTCDate() + (numberOfDays || 7));

			for (var d = new Date(now); d < end; d.setUTCDate(d.getUTCDate() + 1)) {
				timesObject[SP_SERVICES.DELIVERY_TIMES_TYPES.REGULAR].push({
					date: new Date(d),
					isToday: d.getTime() === now.getTime(),
					day: d.getUTCDay(),
					times: timesByDayInWeek[d.getUTCDay()] || []
				});
			}

			return timesObject;
		}

		/**
		 * Get retailer now as utc
		 * @private
		 */
		function now() {
			var d = new Date();
			d.setUTCMinutes(d.getUTCMinutes() - _getLocals().timeZoneOffset);
			return d;
		}

		/**
		 * Filter the given delivery times object by the delivery within product tag type
		 * @public
		 *
		 * @param {Object} options
		 * @param {Object} options.deliveryTimesObj
		 * @param {number} options.areaId
		 * @param {number} options.retailerId
		 * @param {boolean} options.isDeliveryWithinDaysByTag
		 *
		 * @return {Promise<boolean>}
		 */
		function isDeliveryWithinDaysOnly(options) {
			if (!options.deliveryTimesObj[SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_DAYS]) {
				return $q.resolve(false);
			}

			if (options.isDeliveryWithinDaysByTag && _isOnlyDeliveryWithinTimes()) {
				return $q.resolve(true);
			}

			return SpDeliveryAreasService.isAreaDeliveryWithinDaysOnly(options.areaId, options.retailerId, true);
		}

		function _isOnlyDeliveryWithinTimes() {
			return !!(SpCartService.total.crossProductTagTypes && SpCartService.total.crossProductTagTypes[PRODUCT_TAG_TYPES.DELIVERY_WITHIN_DAYS]);
		}

		/**
		 * Get the limits of an express delivery time by the express delivery product tag type
		 * @public
		 *
		 * @param {boolean} isExpressDeliveryByTag
		 * @param {number|null} expressDeliveryProductsLimit
		 *
		 * @return {{ isEnabled: boolean, isUnderQuantityLimit: boolean }}
		 */
		function getExpressTimeProductTagLimits(isExpressDeliveryByTag, expressDeliveryProductsLimit) {
			var isUnderQuantityLimit = true,
				isExpressProductsOnly = true;

			if (expressDeliveryProductsLimit !== undefined && expressDeliveryProductsLimit !== null) {
				var productLinesCount = 0;
				angular.forEach(SpCartService.lines, function(cartLine) {
					if (cartLine.type === SP_SERVICES.CART_LINE_TYPES.PRODUCT && cartLine.product) {
						productLinesCount += cartLine.quantity || 0;
					}
				});

				// return whether the number of cart lines did not exceed the express delivery products limit
				isUnderQuantityLimit = expressDeliveryProductsLimit >= productLinesCount;
			}

			if (isExpressDeliveryByTag && SpCartService.total.crossProductTagTypes) {
				isExpressProductsOnly = !!SpCartService.total.crossProductTagTypes[PRODUCT_TAG_TYPES.EXPRESS_DELIVERY];
			}

			return {
				isUnderQuantityLimit: isUnderQuantityLimit,
				isEnabled: isExpressProductsOnly
			};
		}
	}

	/**
	 * Register service
	 */
	angular.module('spServices').config(['$provide', function($provide) {
		provider = $provide.service(serviceName, [
			'$controller', '$q', '$timeout', 'Api', 'SpCartService', 'SpDeliveryAreasService', 'SP_SERVICES', 'PRODUCT_TAG_TYPES',
			SpDeliveryTimesService
		]);
	}]);
})(angular);

/**
 * @typedef {Object} Time
 * @property {Number} id
 * @property {Number} type
 * @property {Number} dayInWeek
 * @property {Date} from
 * @property {Date} to
 * @property {Date} timeRange
 * @property {Boolean} disabled
 * @property {Number|Null} ordersLimit
 *
 * @typedef {Object} Times
 * @property {Date} date
 * @property {Object<Number, Array.<Time>>} times
 *
 * @typedef {Object} Area
 * @property {Number} id
 * @property {Object} branch
 * @property {Object.<Number, Times>} times
 *
 *
 */

(function (angular) {
    'use strict';

    angular.module('spServices').service('EmbedHistoryRetailer', [
        'SP_SERVICES',
        function (SP_SERVICES) {
            var self = this;

            self.embedHubIframe = embedHubIframe;

            /**
             * Embeds an iframe for hub communication
             * and manages history registration
             * @function embedHubIframe
             */
            function embedHubIframe() {
                var hubConts = SP_SERVICES.HUB_DOMAIN;
                var hubDomain = _getHubRetailerDomain();
                if (!hubDomain) return;

                // Checks if the registration flag is set in `localStorage` to prevent duplicate executions
                var isLastRetailerVisit = window.localStorage.getItem(hubConts.LOCAL_STORAGE_NAME);
                if (isLastRetailerVisit) return;

                // Creates and appends a hidden iframe to communicate with the hub
                var iframe = document.createElement('iframe');
                iframe.id = 'child-hub-retailer';
                iframe.style.display = 'none';
                iframe.src = _generateIframeSrc(_normalizeURL(hubDomain));

                // Stores the registration flag in `localStorage` and removes the iframe after 5 seconds
                iframe.onload = function () {
                    setTimeout(function () {
                        window.localStorage.setItem(hubConts.LOCAL_STORAGE_NAME, !isLastRetailerVisit);
                        iframe.remove();
                    }, 5000);
                };

                document.body.appendChild(iframe);
            }

            /**
             * Generate the source URL embed query parameter
             * @returns {string} source URL with query to embed i-frame
             */
            function _generateIframeSrc(hubDomain) {
                // Constructs a query parameter containing the current [domain] and [timestamp].
                // Parameter [blank] = 'true' will trigger to render static html
                // Add parameter [template] to avoid render from cache
                var params = new URLSearchParams({ childRetailer: window.location.origin, date: Date.now(), blank: true, template: 'hub' });
                var iframeSrc = hubDomain + '?' + params.toString();

                // Handles debugging mode by modifying the domain parameter if `?domain=` is present in the URL
                var searchQuery = new URLSearchParams(window.location.search)
                if (searchQuery.get('domain')) {
                    params.set('childRetailer', window.location.origin + '?domain=' + searchQuery.get('domain'));
                    iframeSrc = window.location.origin + '?domain=' + hubDomain + '&' + params.toString();
                }

                return iframeSrc;
            }

            /**
             * Retrieves hub-domain from window.sp
             * Check get domain from frontend | mobile browser
             * 
             * @returns {string | null}
             */
            function _getHubRetailerDomain() {
                var hubConts = SP_SERVICES.HUB_DOMAIN;
                if (!window.sp) return null

                var dataPath = hubConts.WEB_DATA
                if (!window.sp[dataPath]) {
                    dataPath = hubConts.MOBILE_DATA
                }

                return _getNestedObject(window.sp, dataPath + hubConts.LOCAL_PATH, null)
            }

            /**
             * Retrieves a nested value from an object by a dot-separated path.
             * Returning a default value if not found.
             * @param {Object} obj
             * @param {string} path
             * @param {*} defaultValue
             * @returns {*}
             */
            function _getNestedObject(obj, path, defaultValue) {
                if (!obj) return defaultValue

                var keys = path.split('.');
                for (var i = 0; i < keys.length; i++) {
                    if (!obj || typeof obj !== 'object') return defaultValue;
                    obj = obj[keys[i]];
                }
                return obj !== undefined ? obj : defaultValue;
            }

            /**
             * Normalizes a given URL
             * @param {string} url - The input URL (can be incomplete or missing parts).
             * @returns {string} - The fully normalized URL.
             */
            function _normalizeURL(url) {
                var newUrl = url.trim();
                var protocol = location.protocol + '//';
                if (location.search.indexOf('?domain=') > -1) {
                    return newUrl.replace(/^(https?:\/\/)?(www\.)?/, '');
                }

                // Assume if URL is missing and prepend 'https://'
                if (!newUrl.startsWith(protocol)) {
                    newUrl = protocol + newUrl;
                }

                // Create a URL object to easily modify components
                var urlObj = new URL(newUrl);

                // Ensure 'www.' is present at the beginning of the hostname
                if (!urlObj.hostname.startsWith('www.')) {
                    urlObj.hostname = 'www.' + urlObj.hostname;
                }

                // Return the formatted origin (protocol + hostname)
                return urlObj.origin;
            }
        },
    ]);
})(angular);

(function (angular) {
    var ANALYTICS_CURRENCY = {
        '$': 'USD',
        '₪': 'ILS',
        '€': 'EUR',
        'KWD': 'KWD',
        '£': 'GBP'
    };

    /**
     * Provider instance
     * @property {Array/Number/String} [getLocals] - ng inject. should return { currencySymbol: {String}, googleAnalyticsId: {String}, [userId]: {Number} }
     */
    var provider = {};

    function spAnalytics($filter, $controller, $rootScope, $location, SpCartService, SP_SERVICES) {
        var self = this,
            _nameFilter = $filter('name');

        self.init = init;

        function init(companyName) {
            if (!window.ga) {
                return;
            }
            console.log('Google analytics service has been successfully initialized');
            self.companyName = companyName;

            var locals = _getLocals();

            // set e-commerce plugin
            _ga('require', 'ec');

            // set Demographics and Interest Reports
            _ga('require', 'displayfeatures');

            // set currency
            _ga('set', '&cu', ANALYTICS_CURRENCY[locals.currencySymbol] || 'USD');

            // set user id if logged in
            _setUserId();

            $rootScope.$on('$locationChangeSuccess', _onLocationChange);
            $rootScope.$on('login', _setUserId);
            $rootScope.$on('checkout', _onCheckout);
            $rootScope.$on('spAnalytics.event', _onSpAnalyticsEvent);

            var listener = $rootScope.$on('cart.update.complete', function () {
                listener();
                $rootScope.$on('cart.lines.add', _onCartLinesAdded);
                $rootScope.$on('cart.lines.remove', _onCartLinesRemoved);
            });

            window.addEventListener('error', _onError);
        }

        /**
         * Gets the locals
         * @private
         *
         * @returns {Object}
         */
        function _getLocals() {
            if (!provider.getLocals || (!angular.isFunction(provider.getLocals) && !angular.isArray(provider.getLocals))) {
                throw new Error('spAnalyticsProvider.getLocals must be implemented');
            }

            return $controller(provider.getLocals);
        }

        /**
         * Send to retailer and company analytics
         * @private
         */
        function _ga() {
            var locals = _getLocals();

            // retailer analytics
            if (locals.googleAnalyticsId) {
                window.ga.apply(this, arguments);
            }

            // company analytics
            arguments[0] = (self.companyName || 'frontend') + '.' + arguments[0];
            window.ga.apply(this, arguments);
        }

        /**
         * On location change
         * @private
         *
         * @param {Event} event
         * @param {String} [newUrl]
         * @param {String} [oldUrl]
         */
        function _onLocationChange(event, newUrl, oldUrl) {
            if (!newUrl || event.defaultPrevented) {
                return;
            }

            if (!oldUrl || newUrl.split('?')[0] === oldUrl.split('?')[0]) { // if there is changes only after "?" it means that the state doesn't change
                return _pageViewEvent();
            }

            var stopListeners = [
                $rootScope.$on('$stateChangeSuccess', function () {
                    _pageViewEvent();
                    _stopListeners(stopListeners);
                }),
                $rootScope.$on('$stateChangeError', function () {
                    _stopListeners(stopListeners);
                })
            ];
        }

        function _pageViewEvent() {
            _ga('send', 'pageview', $location.url());
        }

        function _stopListeners(stopListeners) {
            angular.forEach(stopListeners, function (stopListener) {
                stopListener();
            });
        }

        /**
         * On login
         * @private
         */
        function _setUserId() {
            var uid = _getLocals().userId;
            if (uid) {
                _ga('set', '&uid', uid);
                _ga('set', 'dimension1', uid); // send uid to allow analytics by uid
            }
        }

        /**
         * On cart lines added
         * @private
         *
         * @param {Event} event
         * @param {Object} data
         */
        function _onCartLinesAdded(event, data) {
            _addLines(data.lines);
            _ga('ec:setAction', 'add');
            _ga('send', 'event', 'UX', 'Click', 'Add To Cart');
        }

        /**
         * On cart lines removed
         * @private
         *
         * @param {Event} event
         * @param {Object} data
         */
        function _onCartLinesRemoved(event, data) {
            _addLines(data.lines);
            _ga('ec:setAction', 'remove');
            _ga('send', 'event', 'UX', 'Click', 'Remove From Cart');
        }

        /**
         * Add lines
         * @private
         *
         * @param {Array} lines
         */
        function _addLines(lines) {
            angular.forEach(lines, function(line) {
                _ga('ec:addProduct', _analyticsCartLine(line, false));
            });
        }

        /**
         * On checkout
         * @private
         *
         * @param {Event} event
         * @param {Object} order
         */
        function _onCheckout(event, order) {
            var data = _getCheckoutEventData(order);
            var isPurchaseMode = $rootScope.config && $rootScope.config.retailer && $rootScope.config.retailer.settings && $rootScope.config.retailer.settings.preventUpdatingAnalyticsAfterCollection === 'true';

            var transaction = { id: order.id };
            angular.forEach(data.chunks, function(linesChunk) {
                angular.forEach(linesChunk, function(line) {
                    _ga('ec:addProduct', line);
                });

                if (isPurchaseMode) {
                    _ga('ec:setAction', 'purchase', transaction);
                    _ga('send', 'event', 'Checkout', 'Purchase', 'items batch');
                } else {
                    _ga('ec:setAction', 'checkout', transaction);
                    _ga('send', 'event', 'Checkout', 'checkout');
                }
            });

            //Send the transaction total data
            var fullTransaction = {
                id: order.id,
                // Don't send the revenue since the batches sum the revenue
                // revenue: data.totalAmount,
                tax: data.totalTax,
                shipping: data.totalDelivery
            };

            if (isPurchaseMode) {
                _ga('ec:setAction', 'purchase', fullTransaction);
                _ga('send', 'event', 'Checkout', 'Purchase', 'transaction details');
            } else {
                _ga('ec:setAction', 'checkout', fullTransaction);
                _ga('send', 'event', 'Checkout', 'checkout');
            }
        }

        /**
         * Returns the checkout event data
         * @private
         *
         * @param {Object} order
         *
         * @return {{totalTax: number, totalDelivery: number, totalAmount: number, chunks: Array<Array<Object>>}}
         */
        function _getCheckoutEventData(order) {
            var chunks = [[]],
                isOrderLines = !!order.lines,
                index = 0,
                totalTax = 0,
                totalDelivery = 0;

            angular.forEach(order.lines || SpCartService.lines, function(line) {
                var currentChunk = chunks[chunks.length - 1];

                //break the transaction of batches of 20 items
                if (currentChunk.length === 20) {
                    currentChunk = [];
                    chunks.push(currentChunk);
                }

                currentChunk.push(_analyticsCartLine(line, isOrderLines));

                if (isOrderLines) {
                    var price = line.totalPrice || line.price * line.quantity,
                        taxAmount = (line.taxAmount || 0) + 1;
                    totalTax += price - (price / taxAmount);
                    if (line.type === SP_SERVICES.CART_LINE_TYPES.DELIVERY) {
                        totalDelivery += price;
                    }
                }

                index++;
            });

            return {
                chunks: chunks,
                totalAmount: isOrderLines ? order.totalAmount : SpCartService.total.finalPriceWithTax + SpCartService.total.deliveryCost.finalPriceWithTax,
                totalTax: isOrderLines ? totalTax : SpCartService.total.tax + SpCartService.total.deliveryCost.tax,
                totalDelivery: isOrderLines ? totalDelivery : SpCartService.total.deliveryCost.finalPriceWithTax
            };
        }

        /**
         * On sp analytics event
         * @private
         *
         * @param {Event} event
         * @param {Object} data
         */
        function _onSpAnalyticsEvent(event, data) {
            if (!data.category || !data.action) {
                throw new Error('spAnalytics.event emitted but there is no category or action on the event');
            }

            _ga('send', 'event', data.category, data.action, data.label);
        }

        /**
         * On error
         * @private
         *
         * @param {Event} event
         */
        function _onError(event) {
            _ga('send', 'exception', {
                exDescription: (event.filename + ':' + event.lineno + ':' + event.colno + ' ' + event.message).substring(0, 150), // make sure it's no longer than 150
                exFatal: true
            });
        }

        /**
         * Generate analytics cart line
         * @private
         *
         * @param {Object} line
         * @param {boolean} isOrderLines - is order line or cart service lines
         *
         * @returns {Object}
         */
        function _analyticsCartLine(line, isOrderLines) {
            var quantityByWeight = SpCartService.isUnitsWeighable(line) ? line.product.weight : 1;
            var analyticsLine = {
                id: line.product.id,
                name: _nameFilter(line.product.names).long,
                category: line.product.family && line.product.family.categories && line.product.family.categories[0] ? _nameFilter(line.product.family.categories[0].names) : null,
                brand: _nameFilter(line.product.brand && line.product.brand.names)
            };

            if (isOrderLines) {
                analyticsLine.price = line.totalPrice ? line.totalPrice / line.quantity : line.price;
                analyticsLine.quantity = line.quantity;
            } else {
                analyticsLine.price = (line.finalPriceWithTax / (line.quantity * quantityByWeight)) || (line.product.branch && line.product.branch.regularPrice) || 0;
                analyticsLine.quantity = Math.ceil(line.quantity * quantityByWeight);
            }

            return analyticsLine;
        }
    }

    angular.module('spServices')
        .config(['$provide', function ($provide) {
            provider = $provide.service('spAnalytics', [
                '$filter', '$controller', '$rootScope', '$location', 'SpCartService', 'SP_SERVICES',
                spAnalytics
            ]);
        }]);
})(angular);

(function (angular) {
    'use strict';

    angular.module('spServices').service('HubService', ['Api', function (Api) {
        var self = this,
            _dataPromise;

        self.getData = getData;
        self.backToHub = backToHub;

        /**
         * Get the hub data
         * @public
         *
         * @param {number} hubId
         * @param {boolean} [withoutCache]
         *
         * @returns {Promise}
         */
        function getData(hubId, withoutCache) {
            if (!withoutCache && _dataPromise) {
                return _dataPromise;
            }

            return _dataPromise = Api.request({
                url: '/v2/retailers/:rid/retailers/' + hubId,
                method: 'GET'
            }).then(function(data) {
                return data.retailer;
            });
        }

        /**
         * Redirect back to the hub from a child frontend app
         * @public
         *
         * @param {string} hubDomain
         */
        function backToHub(hubDomain) {
            if (window.spHostMobileAppId) {
                location.href = location.origin + '/?mobileAppId=' + window.spHostMobileAppId + '&clear=1';
            } else if (location.hostname === 'localhost') {
                location.href = location.origin + '/?domain=' + hubDomain + '&clear=1';
            } else {
                location.href = location.protocol + '//' + hubDomain + '/?clear=1';
            }
        }
    }]);
})(angular);
(function (angular) {


    var provider = {};

    function spMixPanel($filter, $controller, $rootScope, SpCartService) {
        var self = this;
        var _nameFilter = $filter('name');
        var locals;
        var script = document.createElement("script");

        self.init = init;

        function init() {

            locals = _getLocals();
            if(locals.mixPanelToken){
                initMixPanel();
                initEventListeners();
            }
        }

        function initMixPanel(){
            script.type = "text/javascript";
            script.async = true;
            script.innerText = '(function (f, b) { if (!b.__SV) { var e, g, i, h; window.mixpanel = b; b._i = []; b.init = function (e, f, c) { function g(a, d) { var b = d.split("."); 2 == b.length && ((a = a[b[0]]), (d = b[1])); a[d] = function () { a.push([d].concat(Array.prototype.slice.call(arguments, 0))); }; } var a = b; "undefined" !== typeof c ? (a = b[c] = []) : (c = "mixpanel"); a.people = a.people || []; a.toString = function (a) { var d = "mixpanel"; "mixpanel" !== c && (d += "." + c); a || (d += " (stub)"); return d; }; a.people.toString = function () { return a.toString(1) + ".people (stub)"; }; i = "disable time_event track track_pageview track_links track_forms track_with_groups add_group set_group remove_group register register_once alias unregister identify name_tag set_config reset opt_in_tracking opt_out_tracking has_opted_in_tracking has_opted_out_tracking clear_opt_in_out_tracking start_batch_senders people.set people.set_once people.unset people.increment people.append people.union people.track_charge people.clear_charges people.delete_user people.remove".split( " "); for (h = 0; h < i.length; h++) g(a, i[h]); var j = "set set_once union unset remove delete".split(" "); a.get_group = function () { function b(c) { d[c] = function () { call2_args = arguments; call2 = [c].concat(Array.prototype.slice.call(call2_args, 0)); a.push([e, call2]); }; } for ( var d = {}, e = ["get_group"].concat( Array.prototype.slice.call(arguments, 0)), c = 0; c < j.length; c++) b(j[c]); return d; }; b._i.push([e, f, c]); }; b.__SV = 1.2; e = f.createElement("script"); e.type = "text/javascript"; e.async = !0; e.src = "undefined" !== typeof MIXPANEL_CUSTOM_LIB_URL ? MIXPANEL_CUSTOM_LIB_URL : "file:" === f.location.protocol && "//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\\/\\//) ? "https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js" : "//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js"; g = f.getElementsByTagName("script")[0]; g.parentNode.insertBefore(e, g); } })(document, window.mixpanel || []);'
            document.getElementsByTagName("head")[0].appendChild(script);
            window.mixpanel.init(locals.mixPanelToken, {
                track_pageview: "full-url"
            });
        }

        function _getLocals() {
            if (!provider.getLocals || (!angular.isFunction(provider.getLocals) && !angular.isArray(provider.getLocals))) {
                throw new Error('spMixPanel.getLocals must be implemented');
            }

            return $controller(provider.getLocals);
        }

        function initEventListeners(){
            $rootScope.$on('loginForAnalyticServiceProvider', _setUserData);
            $rootScope.$on('productPage.open', _eventProductPageView);
            $rootScope.$on('cart.lines.add', _eventAddToCart);
            $rootScope.$on('cart.lines.remove', _eventRemoveFromCart);
            $rootScope.$on('cart.lines.quantityChanged.click', _eventCartQuantityChanged);
            $rootScope.$on('setSoldBy', _eventUnitsChange);
        }

        function _setUserData(){
            var locals = _getLocals();
            if(locals.userId){
                window.mixpanel.track('sign in');
                window.mixpanel.register({
                    userName: locals.username,
                });
                window.mixpanel.identify(locals.userId);
            }
        }

        //======== Events

        function _eventProductPageView(event, dataObject, extraData) {
            window.mixpanel.track('Product Details Page Viewed', {
                'Product Name': _getProductName(dataObject),
                'Product ID': dataObject.id,
                'Product Position': extraData && extraData.hasOwnProperty('index') ? (extraData.index + 1) : undefined,
                'Source': 'Not Set Yet',
            });
        }

        function _eventAddToCart(event, dataObject, extraData) {
            var cartLine = _extractCartLine(dataObject);
            if(!cartLine) return;

            if(_isCouponCartLine(cartLine)) { // Add Coupon to Cart have a different event
                return _eventCouponAddToCart(event, cartLine.product);
            }

            window.mixpanel.track('Product Added to Cart', {
                'Product Name': _getProductName(cartLine.product),
                'Product ID': cartLine.product.id,
                'Product Position': extraData && extraData.hasOwnProperty('index') ? (extraData.index + 1) : undefined,
                'Quantity': cartLine.quantity,
                'Product with Special indication': !!(cartLine.product.branch &&cartLine.product.branch.specials && cartLine.product.branch.specials[0]),
                'Product Tags Ids': _getProductTagIds(cartLine.product),
                'Source': 'Not Set Yet',
            });
        }

        function _eventRemoveFromCart(event, dataObject) {
            var cartLine = _extractCartLine(dataObject);
            if(!cartLine) return;

            if(_isCouponCartLine(cartLine)) { // Remove Coupon from Cart don't have an event
                return;
            }

            window.mixpanel.track('Product Removed from Cart', {
                'Product Name': _getProductName(cartLine.product),
                'Product ID': cartLine.product.id,
                'Cart Value After Removal': _getCartTotal()
            });
        }

        function _eventCouponAddToCart(event, couponData) {
            window.mixpanel.track('Coupons Applied to Cart', {
                'Coupon Name': _getProductName(couponData),
                'Coupon ID': couponData.id,
                'Source': 'Not Set Yet',
            });
        }

        function _eventCartQuantityChanged(event, dataObject) {
            var cartLine = _extractCartLine(dataObject);
            if(!cartLine) return;

            if(_isCouponCartLine(cartLine)) { // Coupon Quantity Change don't have an event
                return;
            }

            var eventNane = dataObject.direction === 'decrease' ? 'Quantity Decreased' : 'Quantity Increased';

            window.mixpanel.track(eventNane, {
                'Product Name': _getProductName(cartLine.product),
                'Product ID': cartLine.product.id,
                'Quantity': cartLine.quantity,
                'Source': 'Not Set Yet',
            });
        }
        function _eventUnitsChange(event, object) {
            if(!(object && object.cartLine && object.cartLine.product && object.cartLine.product.id)) return;

            window.mixpanel.track('Change Units', {
                'Product ID': object.cartLine.product.id,
                'Source': 'Not Set Yet',
            });
        }


        //======= Service Functions

        // Check if this Cart Line item is a Coupon
        function _isCouponCartLine(cartLine) {
            return cartLine && cartLine.type === 3 && cartLine.product && cartLine.product.isCoupon === true;
        }

        // Check if a valid CartLines object and extract a single Cart Line
        function _extractCartLine(object) {
            if(!(object && object.lines && object.lines[0])) return;
            if(object.lines.length > 1) return; // not a single product add to cart - page load or cart copy

            if(object.lines[0].id && object.lines[0].product) {
                return object.lines[0];
            }
        }

        function _getProductTagIds(product) {
            try {
                if (product.productTags && product.productTags.length) {
                    return product.productTags.join(',')
                } else if (product.productTagsData && product.productTagsData.length) {
                    return product.productTagsData.map(function (item) {
                        return item.id;
                    }).join(',');
                }
            } catch (err) {}

            return '';
        }

        // Get Product Name - we will take a Short Name, first from En, than from He, then from any other language if previous doesn't exists
        function _getProductName(product) {
            var name = 'Undefined';

            if(product.names) {
                name = (product.names[2] && product.names[2].short) ||
                    (product.names[1] && product.names[1].short) ||
                    (product.names[3] && product.names[3].short) ||
                    (product.names[4] && product.names[4].short) ||
                    (product.names[5] && product.names[5].short) ||
                    (product.names[6] && product.names[6].short) || name;
            } else if (product.localName) {
                name = product.localName;
            }

            return name;
        }

        function _getCartTotal() {
            return SpCartService ? SpCartService.finalPriceForView || SpCartService.finalPriceWithTax : 0;
        }


    }

    angular.module('spServices')
        .config(['$provide', function ($provide) {
            provider = $provide.service('spMixPanel', [
                '$filter', '$controller', '$rootScope', 'SpCartService',
                spMixPanel
            ]);
        }]);
})(angular);

(function (angular) {
    'use strict';

    var LOCAL_STORAGE_KEY = 'permanentFilters',
        TYPES = {
            PRODUCT_TAGS: 1
        };

    var provider;

    function PermanentFiltersService($rootScope, $q, $timeout, $injector) {
        var self = this,
            _valuesMaps = {},
            _eventListeners = [],
            _saveData = {};

        self.TYPES = TYPES;
        self.values = {};

        var savedValues = _getLocalStorageItem(LOCAL_STORAGE_KEY) || {};
        angular.forEach(TYPES, function(valuesType) {
            self.values[valuesType] = savedValues[valuesType] || [];

            _valuesMaps[valuesType] = {};
            angular.forEach(self.values[valuesType], function(value) {
                _valuesMaps[valuesType][value] = true;
            });
        });

        self.hasFilter = hasFilter;
        self.addFilterValue = addFilterValue;
        self.removeFilterValue = removeFilterValue;
        self.addFilterValues = addFilterValues;
        self.removeFilterValues = removeFilterValues;
        self.clearFilterValues = clearFilterValues;
        self.subscribe = onPermanentFiltersChange;

        self.hasProductTagFilter = hasProductTagFilter;
        self.addProductTagFilter = addProductTagFilter;
        self.removeProductTagFilter = removeProductTagFilter;
        self.addProductTagsFilters = addProductTagsFilters;
        self.removeProductTagsFilters = removeProductTagsFilters;
        self.clearProductTagsFilters = clearProductTagsFilters;

        /**
         * Returns whether the given value exists in the given permanent filters type
         * @public
         *
         * @param {number} type
         * @param {number|string} value
         *
         * @return {boolean}
         */
        function hasFilter(type, value) {
            return _valuesMaps[type][value] || false;
        }

        /**
         * Add filter value on the given permanent filters type
         * @public
         *
         * @param {number} type
         * @param {number|string} value
         *
         * @return {Promise}
         */
        function addFilterValue(type, value) {
            return _saveType(type, _addFilterValue(type, value));
        }

        /**
         * Add value to the given type's filter (inner)
         * @private
         *
         * @param {number} type
         * @param {number|string} value
         *
         * @return {Array<number>}
         */
        function _addFilterValue(type, value) {
            if (!_valuesMaps[type].hasOwnProperty(value)) {
                _valuesMaps[type][value] = true;
                self.values[type].push(value);

                return [value];
            }

            return [];
        }

        /**
         * Remove filter value from the given permanent filters type
         * @public
         *
         * @param {number} type
         * @param {number|string} value
         *
         * @return {Promise}
         */
        function removeFilterValue(type, value) {
            return _saveType(type, _removeFilterValue(type, value));
        }

        /**
         * Remove value from the given type's filter (inner)
         * @private
         *
         * @param {number} type
         * @param {number|string} value
         *
         * @return {Array<number>}
         */
        function _removeFilterValue(type, value) {
            if (_valuesMaps[type].hasOwnProperty(value)) {
                delete _valuesMaps[type][value];
                self.values[type].splice(self.values[type].indexOf(value), 1);

                return [value];
            }

            return [];
        }

        /**
         * Add an array of values into the given permanent filters type
         * @public
         *
         * @param {number} type
         * @param {Array<number|string>} values
         * @param {boolean} [replace] - replace all current values
         *
         * @return {Promise}
         */
        function addFilterValues(type, values, replace) {
            var newValues = {},
                changes = [];

            angular.forEach(values, function(value) {
                Array.prototype.push.apply(changes, _addFilterValue(type, value));
                newValues[value] = true;
            });

            if (replace) {
                angular.forEach(self.values[type].slice(0), function(value) {
                    if (!newValues[value]) {
                        Array.prototype.push.apply(changes, _removeFilterValue(type, value));
                    }
                });
            }

            return _saveType(type, changes);
        }

        /**
         * Remove the given values from the given permanent filters type
         * @public
         *
         * @param {number} type
         * @param {Array<number|string>} values
         *
         * @return {Promise}
         */
        function removeFilterValues(type, values) {
            var changes = [];
            angular.forEach(values, function(value) {
                Array.prototype.push.apply(changes, _removeFilterValue(type, value));
            });
            return _saveType(type, changes);
        }

        /**
         * Clear all values of the given permanent filters type
         * @public
         *
         * @param {number} type
         *
         * @return {Promise}
         */
        function clearFilterValues(type) {
            return removeFilterValues(type, self.values[type].slice(0));
        }

        /**
         * Emit change event on the given type's changes and save to local storage
         * @private
         *
         * @param {number} type
         * @param {Array<number|string>} changes
         *
         * @returns {Promise}
         */
        function _saveType(type, changes) {
            if (!changes || !changes.length) {
                return $q.resolve();
            }

            if (_saveData.timeout) {
                $timeout.cancel(_saveData.timeout);
            }

            _saveData.collectedChanges = _saveData.collectedChanges || {};
            _saveData.collectedChanges[type] = _saveData.collectedChanges[type] || [];
            _saveData.collectedChanges[type].push(changes);

            var defer = $q.defer();

            _saveData.defers = _saveData.defers || [];
            _saveData.defers.push(defer);

            _setSaveTimeout();

            return defer.promise;
        }

        /**
         * Set timeout for the save action
         * @private
         */
        function _setSaveTimeout() {
            // timeout the actual save to allow multiple changes in one save
            _saveData.timeout = $timeout(_saveTimeoutHandler, 100);
        }

        /**
         * Handle the save
         * @private
         *
         * @return {Promise<Array<*>>}
         */
        function _saveTimeoutHandler() {
            delete _saveData.timeout;

            // if a save is currently in progress, do not do anything until it finished
            if (_saveData.saving) {
                return _setSaveTimeout();
            }

            _saveData.saving = true;

            _setLocalStorageItem(LOCAL_STORAGE_KEY, self.values);

            // join changes per type
            var changes = {};
            angular.forEach(_saveData.collectedChanges || {}, function(typeBulks, typeKey) {
                changes[typeKey] = [];

                var valuesMap = {};
                angular.forEach(typeBulks, function(bulk) {
                    angular.forEach(bulk, function(value) {
                        if (!valuesMap[value]) {
                            changes[typeKey].push(value);
                            valuesMap[value] = true;
                        }
                    });
                });
            });
            _saveData.collectedChanges = {};

            var defers = (_saveData.defers || []).splice(0); // get and empty the array

            var promises = [];
            angular.forEach(_eventListeners, function(_eventListener) {
                promises.push(_eventListener(changes));
            });
            return $q.all(promises).then(function(res) {
                angular.forEach(defers, function(defer) {
                    defer.resolve(res);
                });
            }).catch(function(err) {
                angular.forEach(defers, function(defer) {
                    defer.reject(err);
                });
            }).finally(function() {
                _saveData.saving = false;
            });
        }

        /**
         * Listen to filters change event
         * @public
         *
         * @param {function} callback
         * @param {Object} [$scope]
         *
         * @returns {function}
         */
        function onPermanentFiltersChange(callback, $scope) {
            _eventListeners.push(callback);

            function _removeListener() {
                _eventListeners.splice(_eventListeners.indexOf(callback), 1);
            }

            if ($scope) {
                $scope.$on('$destroy', _removeListener);
            }

            return _removeListener;
        }

        /**
         * Returns whether the given product tag id exists in the product tags permanent filters
         * @public
         *
         * @param {number} productTagId
         *
         * @return {boolean}
         */
        function hasProductTagFilter(productTagId) {
            return hasFilter(TYPES.PRODUCT_TAGS, productTagId);
        }

        /**
         * Add product tag filter value
         * @public
         *
         * @param {number} productTagId
         *
         * @return {Promise}
         */
        function addProductTagFilter(productTagId) {
            return addFilterValue(TYPES.PRODUCT_TAGS, productTagId);
        }

        /**
         * Remove product tag filter
         * @public
         *
         * @param {number} productTagId
         *
         * @return {Promise}
         */
        function removeProductTagFilter(productTagId) {
            return removeFilterValue(TYPES.PRODUCT_TAGS, productTagId);
        }

        /**
         * Set an array of product tags filters
         * @public
         *
         * @param {Array<number>} productTags
         * @param {boolean} [replace] - replace all current values
         *
         * @return {Promise}
         */
        function addProductTagsFilters(productTags, replace) {
            return addFilterValues(TYPES.PRODUCT_TAGS, productTags, replace);
        }

        /**
         * Remove given product tags filters
         * @public
         *
         * @param {Array<number>} productTags
         *
         * @return {Promise}
         */
        function removeProductTagsFilters(productTags) {
            return removeFilterValues(TYPES.PRODUCT_TAGS, productTags);
        }

        /**
         * Clear all product tags filters
         * @public
         *
         * @return {Promise}
         */
        function clearProductTagsFilters() {
            return clearFilterValues(TYPES.PRODUCT_TAGS);
        }

        /**
         * Returns the local storage value for the given key
         * @private
         *
         * @param {string} key
         *
         * @return {*}
         */
        function _getLocalStorageItem(key) {
            return _getLocalStorage('getItem').getItem(key);
        }

        /**
         * Sets a local storage value on the given key
         * @private
         *
         * @param {string} key
         * @param {string} value
         *
         * @return {*}
         */
        function _setLocalStorageItem(key, value) {
            return _getLocalStorage('setItem').setItem(key, value);
        }

        /**
         * Validate the provider's local storage service and the given key on it
         * @private
         *
         * @param {string} key
         */
        function _getLocalStorage(key) {
            if (!provider.localStorage) {
                throw new Error('No local storage service was set on the PermanentFiltersProvider');
            }

            var localStorage = $injector.invoke(provider.localStorage);

            if (!localStorage[key] || !angular.isFunction(localStorage[key])) {
                throw new Error('The function ' + key + ' is not implemented on the local storage service');
            }

            return localStorage;
        }
    }

    angular.module('spServices').config(['$provide', function($provide) {
        provider = $provide.service('PermanentFilters', ['$rootScope', '$q', '$timeout', '$injector', PermanentFiltersService]);

        provider.localStorage = undefined;
    }]);
})(angular);
(function (angular) {
    'use strict';

    var PRODUCT_TAGS_FILTERS = { isViewFilter: true, isViewFilterActive: true, isActive: true };

    var provider;

    function PermanentFiltersGroups($q, Api, SpProductTags) {
        var self = this,
            _initPromise,
            _initResults;

        self.init = init;
        self.get = get;

        /**
         * Inits the first groups and products tags, to figure out the total groups going forward
         * @public
         *
         * @returns {Promise<PermanentFiltersGroups>}
         */
        function init() {
            if (_initPromise) {
                return _initPromise;
            }

            return _initPromise = $q.all({
                groups: _getViewFilterGroups({from: 0, size: provider.visibleGroups * 2}),
                productTags: _getViewFilterProductTags({from: 0, size: provider.allTagsGroupsSize * provider.visibleGroups})
            }).then(function(results) {
                _initResults = results;
                return self;
            });
        }

        /**
         * Get view filters groups by the given range
         * @public
         *
         * @param {number} from
         * @param {number} size
         *
         * @return {Promise<{total: number, groups: Array<Object>}>}
         */
        function get(from, size) {
            return init().then(function() {
                return $q.all([
                    _getFilterGroupsChunk(from, size),
                    _getProductTagsGroupsChunk(from, size)
                ]);
            }).then(function(results) {
                return {
                    total: _initResults.groups.total + _getProductTagsGroupsTotal(),
                    groups: results[0].concat(results[1])
                };
            });
        }

        /**
         * Return the total groups of the 'all' product tags
         * @private
         *
         * @return {number}
         */
        function _getProductTagsGroupsTotal() {
            return (_initResults.productTags.total > 0 ? 1 : 0) +
                Math.ceil(Math.max(_initResults.productTags.total - _getProductTagsFirstChunkLength(), 0) / provider.allTagsGroupsSize)
        }

        /**
         * Returns the view filters groups by the given range
         * @private
         *
         * @param {number} from
         * @param {number} size
         *
         * @return {Promise<Array<Object>>}
         */
        function _getFilterGroupsChunk(from, size) {
            var initGroups = _initResults.groups.permanentViewFilterGroups.slice(from, from + size);
            if ((from + initGroups.length) >= _initResults.groups.total || initGroups.length === size) {
                return $q.resolve(initGroups);
            }

            return _getViewFilterGroups({
                from: from + initGroups.length,
                size: size - initGroups.length
            }).then(function(res) {
                return initGroups.concat(res.permanentViewFilterGroups);
            });
        }

        /**
         * Returns the groups of all product tags by the given range
         * @private
         *
         * @param {number} from
         * @param {number} size
         *
         * @return {Promise<Array<Object>>}
         */
        function _getProductTagsGroupsChunk(from, size) {
            var to = from + size;
            if (to < _initResults.groups.total) {
                return $q.resolve([]);
            }

            var tagsGroupsFrom = Math.max(from - _initResults.groups.total, 0),
                tagsGroupsTo = Math.max(to - _initResults.groups.total, 0),
                tagsFrom = _convertGroupIndexToProductTagsIndex(tagsGroupsFrom),
                tagsTo = _convertGroupIndexToProductTagsIndex(tagsGroupsTo);
            return $q.resolve().then(function() {
                if (tagsTo <= _initResults.productTags.productTags.length) {
                    return _initResults.productTags.productTags.slice(tagsFrom, tagsTo);
                }

                var realTagsFrom = Math.max(from, _initResults.productTags.productTags.length);
                return _getViewFilterProductTags({
                    from: realTagsFrom,
                    size: tagsTo - realTagsFrom
                }).then(function(res) {
                    return _initResults.productTags.productTags.slice(tagsFrom, realTagsFrom).concat(res.productTags);
                });
            }).then(function(productTags) {
                var groups = [];
                for (var i = 0; i < (tagsGroupsTo - tagsGroupsFrom); i++) {
                    var isFirstAll = i === 0 && tagsGroupsFrom === 0,
                        fromIndex = _convertGroupIndexToProductTagsIndex(tagsGroupsFrom + i) - tagsFrom,
                        toIndex = _convertGroupIndexToProductTagsIndex(tagsGroupsFrom + i + 1) - tagsFrom;
                    groups.push({
                        isAll: true,
                        isFirstAll: isFirstAll,
                        productTags: productTags.slice(fromIndex, toIndex).map(function(productTag) {
                            return { productTagId: productTag.id };
                        })
                    });

                    if (productTags.length <= toIndex) {
                        break;
                    }
                }
                return groups;
            });
        }

        /**
         * Returns the product tags index of the first tag in the given group index
         * @private
         *
         * @param {number} groupIndex
         *
         * @return {number}
         */
        function _convertGroupIndexToProductTagsIndex(groupIndex) {
            return Math.max(groupIndex * provider.allTagsGroupsSize - (provider.allTagsGroupsSize - _getProductTagsFirstChunkLength()), 0);
        }

        /**
         * Returns the all product tags first group tags length
         * @private
         *
         * @return {number}
         */
        function _getProductTagsFirstChunkLength() {
            return _initResults.groups.total ? provider.allTagsFirstGroupSize : provider.allTagsGroupsSize;
        }

        /**
         * Get view filter groups chunk
         * @private
         *
         * @param {Object} params
         * @param {number} params.from
         * @param {number} params.size
         *
         * @return {{total: number, permanentViewFilterGroups: Array<Object>}}
         */
        function _getViewFilterGroups(params) {
            return Api.request({
                url: '/v2/retailers/:rid/permanent-view-filters/groups',
                method: 'GET',
                params: params
            });
        }

        /**
         * Get product tags chunk
         * @private
         *
         * @param {Object} params
         * @param {number} params.from
         * @param {number} params.size
         *
         * @return {{total: number, productTags: Array<Object>}}
         */
        function _getViewFilterProductTags(params) {
            return SpProductTags.getProductTags(Object.assign(params, PRODUCT_TAGS_FILTERS));
        }
    }

    angular.module('spServices').provider('PermanentFiltersGroups', function() {
        provider = this;

        provider.visibleGroups = 4;
        provider.allTagsGroupsSize = 9;
        provider.allTagsFirstGroupSize = provider.allTagsGroupsSize - 2;

        provider.$get = ['$q', 'Api', 'SpProductTags', function($q, Api, SpProductTags) {
            return {
                create: function() {
                    return (new PermanentFiltersGroups($q, Api, SpProductTags)).init();
                }
            }
        }];
    });
})(angular);
(function (angular) {
    /**
     * Service name
     */
    var serviceName = 'SpRecipeService';

    function SpRecipeService(api, Cart, $rootScope, $location, $q, SP_SERVICES) {
        var self = this;

        self.isValidRecipeQuery = isValidRecipeQuery;
        self.parseRecipeFromQuery = parseRecipeFromQuery;
        self.addIngredientProductToCart = addIngredientProductToCart;
        self.addIngredientsToCartByRecipeId = addIngredientsToCartByRecipeId;
        self.fixIngredientQuantity = fixIngredientQuantity;
        self.initRecipeListener = initRecipeListener;


        /**
         * Check if recipeQuery is number or json
         * @public
         *
         * @param {string} recipeQuery
         *
         * @returns {boolean}
         */
        function isValidRecipeQuery(recipeQuery) {
            try {
                if (angular.isObject(JSON.parse(recipeQuery))) {
                    return true;
                }
            } catch (err) {}

            return !isNaN(recipeQuery);
        }

        /**
         * Return recipe object by recipeQuery (can be number or json)
         * @public
         *
         * @param {string|number} recipeQuery
         *
         * @returns {Promise.<object>}
         */
        function parseRecipeFromQuery(recipeQuery) {
            if (!isNaN(recipeQuery)) {
                return api.request({
                    method: 'GET',
                    url: '/v2/retailers/:rid/branches/:bid/bumpers/recipes/' + recipeQuery
                });
            }

            try {
                var parsed = JSON.parse(recipeQuery),
                    fillInto = {};
                angular.forEach(parsed.groups, function (group, groupIndex) {
                    angular.forEach(group.ingredients, function (ingredient, ingredientIndex) {
                        if (!ingredient.productsFilters) return;

                        ingredient.products = [];
                        angular.forEach(ingredient.productsFilters, function (productFilters) {
                            if (!fillInto[productFilters.barcode]) {
                                fillInto[productFilters.barcode] = [];
                            }

                            fillInto[productFilters.barcode].push([groupIndex, ingredientIndex]);
                        });
                    });
                });

                var barcodes = Object.keys(fillInto);
                return api.request({
                    method: 'GET',
                    url: '/v2/retailers/:rid/branches/:bid/products',
                    params: {
                        size: barcodes.length,
                        filters: {
                            must: {
                                term: {
                                    barcode: barcodes
                                }
                            }
                        }
                    }
                }).then(function (response) {
                    angular.forEach(response.products, function (product) {
                        angular.forEach(fillInto[product.barcode], function (into) {
                            parsed.groups[into[0]].ingredients[into[1]].products.push({product: product});
                        });
                    });

                    return parsed;
                });
            } catch (err) {}
        }

        /**
         * Prepare and set product quantities for cart
         * @public
         *
         * @param {Object} ingredient
         *
         * @returns {void}
         */
        function fixIngredientQuantity(ingredient) {
            ingredient.wholeNumber = parseInt(ingredient.quantity);
            var decimal = ingredient.quantity - Math.floor(ingredient.quantity),
                fraction = new Fraction(decimal);
            ingredient.numerator = fraction.n;
            ingredient.denominator = fraction.d;

            angular.forEach(ingredient.products, function (product) {
                product.conversionCount = ingredient.conversionCount;
                product.quantity = Cart.quantityInterval(product);
            });
        }

        /**
         * Prepare and set product quantities for cart
         * @private
         *
         * @param {Object} recipe
         *
         * @returns {void}
         */
        function _fixProductsQuantityInRecipe(recipe) {

            angular.forEach(recipe.groups, function (group) {
                angular.forEach(group.ingredients, function (ingredient) {
                    fixIngredientQuantity(ingredient);
                });
            });
        }

        /**
         * Add single ingredient tc cart or update cart quantity
         * @public
         *
         * @param {Object} ingredientProduct
         *
         * @returns {void}
         */
        function addIngredientProductToCart(ingredientProduct) {

            var line = Cart.getProduct(ingredientProduct.product).singleLine || null;
            if (line) {
                line.quantity += ingredientProduct.quantity;
                Cart.quantityChanged(line);
                return;
            }

            Cart.addLine({
                product: ingredientProduct.product,
                quantity: ingredientProduct.quantity,
                conversionCount: ingredientProduct.conversionCount,
                source: SP_SERVICES.SOURCES.RECIPE
            });
        }

        /**
         * Add all products from recipe to cart
         * @public
         *
         * @param {Object} recipe
         *
         * @returns {void}
         */
        function _addAllIngredientsToCart(recipe) {
            angular.forEach(recipe.groups, function (group) {
                angular.forEach(group.ingredients, function (ingredient) {
                    if(ingredient.products && ingredient.products[0] && ingredient.products[0].quantity){
                        addIngredientProductToCart(ingredient.products[0]);
                    }
                });
            });
        }

        /**
         * Get recipe by Id and add all it's ingredients to cart
         * @public
         *
         * @param {Number} recipeId
         *
         * @returns {Promise.<object>}
         */
        function addIngredientsToCartByRecipeId(recipeId) {
            if(!recipeId || isNaN(recipeId)){
                return $q.resolve();
            }

            return parseRecipeFromQuery(recipeId).then(function(recipe) {
                _fixProductsQuantityInRecipe(recipe);
                _addAllIngredientsToCart(recipe);
            });
        }

        /**
         * Listener for adding a recipe by query parameter
         * @private
         *
         * @returns {Promise}
         */
        function initRecipeListener() {
            var locationSearch = $location.search(),
                recipeId = locationSearch && locationSearch.addRecipeIngredients;

            if (!recipeId) {
                if (recipeId !== undefined) {
                    _clearAddRecipeParam();
                }

                return $q.resolve();
            }

            return _waitForCartInit().then(function () {
                return addIngredientsToCartByRecipeId(recipeId);
            }).then(function() {
                _clearAddRecipeParam();
            });
        }

        /**
         * Remove query parameter to prevent double adding ingredients on page refresh
         * @private
         *
         * @returns {void}
         */
        function _clearAddRecipeParam() {
            $location.search('addRecipeIngredients', null);
        }


        /**
         * Check if cart initialized
         * @private
         *
         * @returns {Promise}
         */
        function _waitForCartInit() {
            return new $q(function(resolve) {
                if (Cart.sendingSucceeded) {
                    return resolve();
                }
                var listener = $rootScope.$on('cart.update.complete', function () {
                    listener();
                    resolve();
                });
            });
        }
    }

    /**
     * Register service
     */
    angular.module('spServices')
        .service(serviceName, ['Api', 'SpCartService', '$rootScope', '$location', '$q', 'SP_SERVICES', SpRecipeService]);
})(angular);

(function (angular) {
    'use strict';

    var EVENTS = {
        VIEW_ITEM_LIST: 'productImpressions',
        VIEW_ITEM: 'productDetail',
        SELECT_ITEM: 'productClick',
        SEARCH: 'searchProduct',
        ADD_TO_CART: 'addToCart',
        REMOVE_FROM_CART: 'removeFromCart',
        CHECKOUT: 'checkout',
        PURCHASE: 'purchase',
        CLUB_MEMBER_USE: 'clubMembersUse',
        ADD_TO_WISH_LIST: 'addProductToWishlist',
        VIEW_LOGIN: 'loginView',
        LOGIN: 'loginSuccess',
        ERROR_LOGIN: 'loginFail',
        VIEW_SIGN_UP: 'registrationView',
        SIGN_UP: 'registrationSuccess',
        ERROR_SIGN_UP: 'registrationFail',
        VIEW_CONTACT_US: 'contactUsView',
        CONTACT_US: 'contactUsSuccess',
        ERROR_CONTACT_US: 'contactUsFail',
        VIEW_PROMOTION: 'promotionView',
        SELECT_PROMOTION: 'promotionClick',
        SELECT_CONTENT: 'selectContent',
        PAGE_VIEW: 'pageView'
    };

    var EVENTS_WITH_VALUES = {productDetail: true, productClick: true, addToCart: true, removeFromCart: true, checkout: true};

    // used to filter path that includes user data. i.e - in mobile, on purchase, the path includes the transaction id.
    var PATH_FILTER = {
        'app.cart.checkout.finish': '/cart/checkout/finish'
    };

    var MODES = {
        GTM: 1,
        FIREBASE: 2,
        UNDETERMINED: 0
    };

    var GA_PARAMS = {
        PLATFORM: 'ga_gp',
        MODE: 'ga_gm',
        CONSENT: 'ga_gc',
        CLIENT_ID: 'ga_gci',
        SESSION_ID: 'ga_gsi',
        ERRORS: 'ga_err',
        WARNINGS: 'ga_warn',
        DL_LEN: 'dl_len'
    };

    var PLATFORMS = {
        FRONTEND: 'front',
        MOBILE_WEB: 'mobile',
        ANDROID: 'android',
        IOS: 'ios',
        UNDETERMINED: 'undetermined'
    };

    var PII_FIELDS = ['userEmail'];

    angular.module('spServices').service('DataLayer', ['$injector', '$filter', '$window', '$rootScope', '$location', '$q', '$state', '$timeout', '$cookies', 'Config', 'User', 'LocalStorage', 'Api',
        function ($injector, $filter, $window, $rootScope, $location, $q, $state, $timeout, $cookies, config, user, localStorageService, Api) {
            var self = this,
                _platform = undefined,
                _mode = undefined,
                _isUserConsentGiven = undefined,
                _userId = undefined,
                _errors = {},
                _warnings = {},
                _listeners = [],
                _defaultEmptyValue = undefined,
                _defaultEmptyBrand = 'no_brand',
                _defaultBranchId = 0,
                _translateFilter = $filter('translate'),
                _currencyFilter = $filter('currency'),
                _nameFilter = $filter('name'),
                _regularPriceFilter = $filter('regularPrice'),
                _productNameFilter = $filter('productName'),
                _eventsBuilder = undefined,
                _ignoredEvents = {},
                _isCartUpdateComplete = undefined,
                _serverEvents = {},
                _checkoutMode = false;

            self.push = push;
            self.pushDebug = pushDebug;
            self.getGaParams = getGaParams;
            self.EVENTS = EVENTS;

            _init();

            function push(event, payload, remoteLogging) {
                var eventData;
                var preparedEvents;

                try {
                    if (_checkoutMode && !(event === EVENTS.PURCHASE || event === EVENTS.PAGE_VIEW)) {
                        return;
                    }
                    if (!_isUserConsentGiven || _ignoredEvents[event]) {
                        pushDebug('analytics info on skipping event ' + (event || 'unknown'), remoteLogging)
                        return;
                    }
                    eventData = _prepareEventData(event, payload);
                    if (!_isObject(eventData)) {
                        pushDebug('analytics warning on preparing event ' + (event || 'unknown'), remoteLogging);
                        return;
                    }
                }
                catch (error) {
                    pushDebug('analytics error on preparing event ' + (event || 'unknown'), remoteLogging);
                    _setWarning('prepare-event')
                    return;
                }

                try {
                    preparedEvents = _eventsBuilder[event](eventData);
                    if (!_isObject(preparedEvents)) {
                        pushDebug('analytics warning on building event ' + (event || 'unknown'), remoteLogging);
                        return;
                    }
                    else if (preparedEvents.skip) {
                        return;
                    }
                    else if (!Array.isArray(preparedEvents)) {
                        preparedEvents = [preparedEvents];
                    }
                }
                catch (error) {
                    pushDebug('analytics error on building event ' + (event || 'unknown'), remoteLogging);
                    _setWarning('build-event');
                    return;
                }

                _prepareEnrichmentData()
                    .then(function(enrichmentData) {
                        _eventsBuilder.enrichEvents(preparedEvents, enrichmentData);
                        if (enrichmentData.withError) {
                            return $q.reject();
                        }
                    }).catch(function(error) {
                    preparedEvents.forEach(function(event) {
                        event['error_description'] = 'enriching event';
                    });
                    pushDebug('analytics error on enriching event ' + event, remoteLogging);
                    _setWarning('enrich');
                }).then(function() {
                    angular.forEach(preparedEvents, function (event) {
                        var error;
                        if (_mode === MODES.GTM) {
                            error = _pushGTM(event);
                        }
                        else if (_mode === MODES.FIREBASE) {
                            error = _pushFirebase(event);
                        }

                        if (remoteLogging) {
                            var debugMetadata = _getDebugData(event);
                            var payload = {
                                message: 'analytics ' + (error ? 'error' : 'info') + ' on pushing event ' + event.event,
                                metadata: debugMetadata.metadata,
                                userId: debugMetadata.userId,
                                event: debugMetadata.event,
                                error: error && error.message
                            }

                            _remoteLog(payload);
                        }
                    });
                }).catch(function() {
                        pushDebug('analytics error on pushing event ' + event, remoteLogging);
                        _setWarning('push-event-' + event);
                });
            }

            function pushDebug(message, remoteLogging) {
                var debugMetaData = _getDebugData();
                if (remoteLogging) {
                    _remoteLog({message: message, metadata: debugMetaData.metadata, userId: debugMetaData.userId});
                }
                if (!_isUserConsentGiven) {
                    return;
                }

                else if (_mode === MODES.GTM) {
                    _pushGTM({event: 'sp_debug', sp_message: message, sp_metadata: debugMetaData.metadata});
                }
                else if (_mode === MODES.FIREBASE) {
                    _pushFirebase({event: 'sp_debug', sp_message: message, sp_metadata: debugMetaData.metadata});
                }
            }

            function getGaParams() {
                try {
                    if (!_isRetailerDataLayerEventsEnabled()) {
                        if (_mode !== undefined) {
                            pushDebug('analytics error on getting ga params a');
                            _clearObject(_warnings);
                            return $q.resolve({'err': 'gaparams-a'});
                        }
                        return $q.resolve();
                    }
                }
                catch (error) {
                    pushDebug('analytics error on getting ga params b');
                    _clearObject(_warnings);
                    return $q.resolve({'err': 'gaparams-b'});
                }

                return $q.all({
                    clientId: _getClientId(500),
                    sessionId: _getSessionId(500)
                }).then(function(results) {
                    var gaParams = {d:1};

                    gaParams[GA_PARAMS.PLATFORM] = _platform;
                    gaParams[GA_PARAMS.MODE] = _mode;
                    gaParams[GA_PARAMS.CONSENT] = _isUserConsentGiven;
                    gaParams[GA_PARAMS.CLIENT_ID] = results.clientId || _defaultEmptyValue;
                    gaParams[GA_PARAMS.SESSION_ID] = results.sessionId || _defaultEmptyValue;
                    gaParams[GA_PARAMS.ERRORS] =  _joinObjectKeys(_errors);
                    gaParams[GA_PARAMS.WARNINGS] = _joinObjectKeys(_warnings);
                    gaParams[GA_PARAMS.DL_LEN] = $window.dataLayer && $window.dataLayer.length;

                    return gaParams;
                }).catch(function(error) {
                    pushDebug('analytics error on getting ga params c');
                    return {'err': 'gaparams-c'};
                }).finally(function() {
                    _clearObject(_warnings);
                });
            }

            function _getDebugData(event) {
                var debugData = {
                    metadata: ('platform:' + _platform + ';mode:' + _mode + ';consent:' + _isUserConsentGiven)
                };

                if (event) {
                    try {
                        debugData.event = JSON.stringify(event);
                    }
                    catch (error) {
                        debugData.event = 'json error - ' + error.message;
                    }
                }
                if (_userId) {
                    debugData.userId = _userId;
                }
                return debugData;
            }

            function _remoteLog(payload) {
                try {
                    Api.request({
                        method: 'POST',
                        url: '/v2/retailers/:rid/analytic-services/_log',
                        data: payload
                    });
                }
                catch (error) {}
            }

            function _pushGTM(event) {
                try {
                    var flushObject = _prepareFlushObject();
                    $window.dataLayer = $window.dataLayer || [];
                    $window.dataLayer.push(flushObject);
                    $window.dataLayer.push(event);
                }
                catch (error) {
                    _setWarning('push-gtm');
                    return {message: error.message};
                }
            }

            function _pushFirebase(event) {
                try {
                    var eventName = event.event;
                    var eventEcommerceParams = event.ecommerce;
                    delete event.event;
                    delete event.ecommerce;

                    Object.assign(event, eventEcommerceParams);

                    $window.cordova.plugins.firebase.analytics.logEvent(eventName, event);
                }
                catch (error) {
                    _setWarning('push-firebase');
                    return {message: error.message};
                }
            }

            function _prepareEventData(event, payload) {
                payload = payload || {};

                var eventData = {
                    data: _prepareMetadata(payload.data),
                    purchase: _preparePurchase(payload.purchase),
                    wishListItem: _prepareWishListItem(payload.wishListItem),
                }

                eventData.data.event = event;
                eventData.products = _prepareProducts(payload.products, eventData.data);
                eventData.promotion = _preparePromotion(payload.promotion, eventData.data);
                if (EVENTS_WITH_VALUES[event]) {
                    eventData.values = _prepareValues(eventData.products);
                }

                return eventData;
            }

            function _prepareValues(products) {
                if (!Array.isArray(products)) return;

                var productsValue = 0;
                angular.forEach(products, function (product) {
                    var productValue = (product.price - (product.discount || 0)) * product.quantity;
                    productsValue += productValue;
                });

                return {
                    itemValue: _parseNumber(products[0].price - (products[0].discount || 0), 2),
                    cartItemValue: _parseNumber((products[0].price - (products[0].discount || 0)) * products[0].quantity, 2),
                    checkoutValue: _parseNumber(productsValue, 2)
                }
            }

            function _prepareMetadata(data) {
                data = data || {};

                return {
                    location: _parseString(_getPath(data.toState)),
                    searchTerm: _parseString(data.query),
                    resultsCount: data.results >= 0 ? _parseNumber(data.results) : _defaultEmptyValue,
                    quantity: data.quantity > 0 ? _parseNumber(data.quantity, 2) : _defaultEmptyValue,
                    step: data.step > 0 ? _parseNumber(data.step): _defaultEmptyValue,
                    option: _parseString(data.option),
                    clubMemberAmount: _parseNumber(data.clubMemberAmount, 2),
                    email: _parseString(data.email),
                    error: _errToString(data.error),
                    type: _parseString(data.type),
                    category: _parseString(data.category),
                    action: _parseString(data.action),
                    label: _parseString(_nameFilter(data.label)),
                    productsStartingPosition: data.productsStartingPosition >= 0 ? _parseNumber(data.productsStartingPosition) : 0,
                    cartUpdateComplete: _isCartUpdateComplete,
                    serverEvents: _serverEvents
                }
            }

            function _prepareEnrichmentData() {
                var enrichmentData = {};
                var userSession = _defaultEmptyValue;
                var isFirst = _defaultEmptyValue;
                var withError = false;
                var branchId = _defaultEmptyValue;
                var frontendBranchId = _defaultEmptyValue;
                var mobileZuzBranchId = _defaultEmptyValue;

                try {
                    userSession = localStorageService.getItem('session');
                    isFirst = localStorageService.getItem('isFirstUserLogin');
                    frontendBranchId = localStorageService.getItem('branchId');
                    mobileZuzBranchId = localStorageService.getItem('mobileZuzBranchId');
                    branchId = frontendBranchId ? frontendBranchId : (mobileZuzBranchId ? mobileZuzBranchId : _defaultBranchId);
                }
                catch (error) {
                    pushDebug('analytics error on storage in prepare enrichment data')
                    withError = true;
                }

                return user.getData()
                    .then(function(userData) {
                        enrichmentData.userEmail = _parseString((userData && userData.email) || (userSession && userSession.username));
                        enrichmentData.userId = _parseString((userData && userData.id) || (userSession && userSession.userId));
                        enrichmentData.purchaseCounter = _parseNumber((userData && userData.purchaseCounter) || (userSession && userSession.purchaseCounter) || 0);
                        if (config.retailer && config.retailer.settings && config.retailer.settings.enableSendingExternalUserId === 'true') {
                            enrichmentData.foreignId = userData ? _parseString(userData.externalForeignId) : _defaultEmptyValue;
                        }
                    }).catch(function(error) {
                        if ($rootScope.USER_ERRORS && $rootScope.USER_ERRORS.NOT_LOGGED_IN !== error) {
                            pushDebug('analytics error on user data');
                            withError = true;

                            enrichmentData.userEmail = enrichmentData.userEmail || _parseString(userSession && userSession.username);
                            enrichmentData.userId = enrichmentData.userId || _parseString(userSession && userSession.userId);
                            enrichmentData.purchaseCounter = enrichmentData.purchaseCounter || _parseNumber(userSession && userSession.purchaseCounter || 0);
                            enrichmentData.foreignId = enrichmentData.foreignId ? enrichmentData.foreignId : _defaultEmptyValue;
                        }
                    }).then(function() {
                        enrichmentData.currencyCode = _parseString(config.retailer && config.retailer.currencyCode);
                        if (!enrichmentData.currencyCode) {
                            return $q.reject();
                        }
                    }).catch(function(error) {
                        pushDebug('analytics error on currency');
                        withError = true;
                    }).then(function() {
                        enrichmentData.isFirst = typeof(isFirst) === 'boolean' ? _parseString(isFirst) : _defaultEmptyValue;
                        enrichmentData.branchId = _parseString(branchId);
                    }).then(function() {
                        _clearPIIData(enrichmentData);
                    }).catch(function(error) {
                        pushDebug('analytics error on pii data');
                        return $q.reject('critical error');
                    }).then(function() {
                        if (withError) {
                            enrichmentData.withError = true;
                        }
                        return enrichmentData;
                    });
            }

            function _preparePurchase(purchase) {
                if (!_isObject(purchase)) return;

                return {
                    id: _parseString(purchase.id),
                    revenue: _parseNumber(purchase.revenue, 2),
                    shippingTier: _parseString(purchase.affiliation),
                    tax: _parseNumber(purchase.tax, 2),
                    shipping: _parseNumber(purchase.shipping, 2),
                    coupon: _parseString(purchase.coupon),
                    paymentType: _parseString(purchase.paymentType)
                }
            }

            function _prepareWishListItem(wishListItem) {
                if (!_isObject(wishListItem)) return;

                var wishListItemData = _prepareWishListItemData(wishListItem);
                var preparedWishListItem = _eventsBuilder.prepareWishListItem(wishListItemData);
                return preparedWishListItem;
            }

            function _prepareWishListItemData(wishListItem) {
                var item = {
                    name: _defaultEmptyValue,
                    price: _parseNumber(wishListItem.price, 2)
                }

                if (wishListItem.names || wishListItem.branch) {
                    var names = wishListItem.names || (wishListItem.branch ? wishListItem.branch.names : null);
                    var name = _nameFilter(names);
                    if (name && name.short) {
                        item.name = _parseString(name.short);
                    }
                }

                return item;
            }

            function _prepareProducts(products, data) {
                if (!Array.isArray(products)) return;

                var preparedProducts = [];
                var currentProductPosition = data.productsStartingPosition;
                var isRealIndexPosition = ['addToCart', 'productImpressions', 'productClick', 'productDetail'].includes(data.event);

                angular.forEach(products, function(product) {
                    product = (product && product.item) || product;
                    if (!_isObject(product)) return;

                    var productPosition = isRealIndexPosition ? (product.indexPosition || 0) : currentProductPosition;
                    var productData = _prepareProductData(product, productPosition);
                    if (!_isObject(productData)) return;

                    var preparedProduct = _eventsBuilder.prepareProduct(data.event, productData)
                    if (!_isObject(preparedProduct)) return;

                    preparedProducts.push(preparedProduct);
                    currentProductPosition++;
                });

                if (preparedProducts.length > 0) {
                    return preparedProducts;
                }
                else {
                    return;
                }
            }

            function _prepareProductData(product, currentProductPosition) {
                var productQuantity = 1;
                if (product.singleLine && product.singleLine.quantity !== 0) {
                    productQuantity = product.singleLine.quantity;
                }
                var productPrice = !!product.isWeighable ? productQuantity * product.branch.regularPrice : _parseNumber(_regularPriceFilter(product, product.isCaseMode), 2);
                var quantity = !!product.isWeighable ? 1 : _parseNumber(productQuantity);
                var productData = {
                    'currentProductPosition': currentProductPosition,
                    'currentCategory': (product.family && product.family.categoriesPaths && product.family.categoriesPaths[0] && product.family.categoriesPaths[0][0]) || (product.categories && product.categories[0]) || {names: []},
                    'brandName':  product.brand && _parseString(_nameFilter(product.brand.names)) || _defaultEmptyBrand,
                    'productName': _productNameFilter(product, product.isCaseMode) !== 'undefined' ? _parseString(_productNameFilter(product, product.isCaseMode) || product.localName) : _defaultEmptyValue,
                    'coupon': _parseString(product.coupon),
                    'affiliation': _parseString(product.affiliation),
                    'price': productPrice,
                    'currentDiscount': _defaultEmptyValue,
                    'currentDiscountPrecent': _defaultEmptyValue,
                    'currentDiscountPerCartQuantity': _defaultEmptyValue,
                    'id': _parseString(product.id),
                    'isWeighable': !!product.isWeighable,
                    'unitResolution': _parseNumber(product.unitResolution, 2),
                    'weight': _parseNumber(product.weight, 2),
                    'quantity': quantity,
                    'cartQuantity': _defaultEmptyValue,
                    'addedFrom': _parseString(product.addedFrom),
                    'itemWeight': !!product.isWeighable ? product.unitResolution * productQuantity : _defaultEmptyValue
                }

                productData.pricePerCartQuantity = _parseNumber(_valuePerCartQuantity(productData.price, productData.isWeighable, productData.weight), 2);
                productData.category = _parseString(_nameFilter(productData.currentCategory.names));

                if (product.branch && product.branch.regularPrice && product.branch.salePrice >= 0) {
                    productData.currentDiscount = _parseNumber(product.branch.regularPrice - product.branch.salePrice, 2);
                    productData.currentDiscountPrecent = _parseNumber((product.currentDiscount * 100) / product.branch.regularPrice, 2);
                    productData.currentDiscountPerCartQuantity = _parseNumber(_valuePerCartQuantity(productData.currentDiscount, productData.isWeighable, productData.weight), 2);
                }
                if (product.singleLine && product.singleLine.quantity >= 0) {
                    productData.cartQuantity = _parseNumber(Math.abs(product.singleLine.quantity - ((product.singleLine.oldQuantity >= 0 && product.singleLine.oldQuantity) || 0)) || product.singleLine.quantity) || 1;
                }

                return productData;
            }

            function _preparePromotion(promotion, data) {
                if (!_isObject(promotion)) return;

                var promotionData = _preparePromotionData(promotion, data.type);
                if (!_isObject(promotionData)) return;

                var preparedPromotion = _eventsBuilder.preparePromotion(promotionData);
                if (!_isObject(preparedPromotion)) return;

                return preparedPromotion;
            }

            function _preparePromotionData(promotion, type) {
                var currentPromotion = promotion;

                var promotionData = {
                    creativeUrl: _parseString(currentPromotion.pictureUrl || currentPromotion.image || currentPromotion.url || ''), // Different promotion types have different object properties
                    creativeName: _parseString(currentPromotion.name || ('Untitled ' + type).trim()),
                    id: _parseString(promotion.id),
                    type: _parseString(type || '')
                }

                return promotionData;
            }

            function _prepareFlushObject() {
                var flushObject = {ecommerce: null, sp_message: null, sp_metadata: null, error_description: null};

                if (_eventsBuilder.CUSTOM_PARAMS && Array.isArray(_eventsBuilder.CUSTOM_PARAMS)) {
                    _eventsBuilder.CUSTOM_PARAMS.forEach(function(param) {
                        flushObject[param] = null;
                    });
                }

                return flushObject;
            }

            function _valuePerCartQuantity(value, isWeighable, weight) {
                if (value && isWeighable && weight) {
                    return value * weight;
                }
                else {
                    return value;
                }
            }

            function _parseNumber(number, decimalPlaces) {
                try {
                    decimalPlaces = decimalPlaces || 0;
                    var type = typeof(number);
                    if (!(type === 'number' || type === 'string')) {
                        return _defaultEmptyValue;
                    }
                    var number = Number((+number).toFixed(decimalPlaces));
                    if (Number.isNaN(number) ) {
                        return _defaultEmptyValue;
                    }
                    return number;
                }
                catch (error) {
                    pushDebug('analytics error on parsing number');
                    _setWarning('parse-number');
                    return _defaultEmptyValue;
                }
            }

            function _parseString(string, maxLength) {
                try {
                    maxLength = maxLength || 100 //max param value for ga4
                    if (string === undefined || string === null || string === '') {
                        return _defaultEmptyValue;
                    }

                    var parsedString = typeof(string) === 'string' ? string : string.toString();
                    if (parsedString.length > maxLength) {
                        return parsedString.slice(0, maxLength);
                    }
                    return parsedString;
                }
                catch (error) {
                    pushDebug('analytics error on parsing string');
                    _setWarning('parse-string');
                    return _defaultEmptyValue;
                }
            }

            function _errToString(err) {
                try {
                    return (err && err.response && err.response.error && err.response.error.toString()) || (err && err.message && err.message.toString()) || (typeof err === 'string' && err) || _defaultEmptyValue;
                }
                catch (error) {
                    pushDebug('analytics error on error to string');
                    _setWarning('err-to-string');
                    return _defaultEmptyValue;
                }
            }

            function _resetModel() {
                try {
                    if (_mode === MODES.GTM) {
                        $window.dataLayer = $window.dataLayer || [];
                        $window.dataLayer.push(function() {
                            this.reset();
                        });
                    }
                }
                catch (error) {
                    pushDebug('analytics error on resetting model');
                    _setError('reset-model');
                }
            }

            function _clearPIIData(obj) {
                if (_mode === MODES.FIREBASE) {
                    for (var paramIndex in PII_FIELDS) {
                        obj && obj[PII_FIELDS[paramIndex]] && delete obj[PII_FIELDS[paramIndex]];
                    }
                }
            }

            function _stopListeners(stopListeners) {
                try {
                    angular.forEach(stopListeners, function (stopListener) {
                        stopListener();
                    });
                }
                catch (error) {
                    pushDebug('analytics error on stopping listeners');
                    _setError('stop-listeners');
                }
            }

            function _joinObjectKeys(obj) {
                var joinedMessages = Object.keys(obj).join(';');
                if (joinedMessages.length) {
                    return joinedMessages
                }
            }

            function _clearObject(obj) {
                try {
                    Object.keys(obj).forEach(function(key) {
                        delete obj[key];
                    })
                }
                catch (error) {}
            }

            function _isObject(object) {
                return angular.isObject(object);
            }

            function _isGtagExists() {
                return typeof($window.gtag) === 'function';
            }

            function _isRetailerDataLayerEventsEnabled() {
                return (config && config.retailer && config.retailer.settings && config.retailer.settings.isDataLayerEventActive === 'true');
            }

            function _isLegacyMode() {
                return (config && config.retailer && config.retailer.settings && config.retailer.settings.dataLayerEventMode === 'ga4' ? false : true);
            }


            function _isIos() {
                return ($window.cordova && $window.cordova.platformId === PLATFORMS.IOS);
            }

            function _onCookieWallEvent() {
                _setConsent();
                _setUserId();
            }

            function _onUserLogin() {
                _setUserId();
            }

            function _onUserLogout() {
                _setUserId();
                _resetModel();
            }

            function _onLocationChange(event, toState, toParams, fromState, fromParams) {
                if (!event || event.defaultPrevented) {
                    return;
                }

                self.push(EVENTS.PAGE_VIEW, {data: {toState: (toState && toState.name)}});
            }

            function _onCheckout() {
                _checkoutMode = true;
                $timeout(function() {
                    _checkoutMode = false;
                }, 10000);
            }

            function _onCheckoutFinished() {
                _checkoutMode = false;
            }

            function _setPlatform() {
                var platform;
                try {
                    if ($window.sp && $window.sp.frontendData) {
                        platform = PLATFORMS.FRONTEND;
                    }
                    else if ($window.cordova && $window.cordova.platformId) {
                        platform = $window.cordova.platformId;
                    }
                    else if ($window.sp && $window.sp.mobileData) {
                        platform = PLATFORMS.MOBILE_WEB;
                    }
                    else {
                        throw new Error('analytics error on setting platform');
                    }
                }
                catch (error) {
                    platform = PLATFORMS.UNDETERMINED;
                    _setError('platform');
                }

                _platform = platform;

            }

            function _setMode() {
                var mode;
                try {
                    if ($window.cordova && $window.cordova.plugins && $window.cordova.plugins.firebase && $window.cordova.plugins.firebase.analytics && !_isLegacyMode()) {
                        mode = MODES.FIREBASE;
                    }
                    else if (config.retailer && config.retailer.googleTagManagerId && _isGtagExists()) {
                        mode = MODES.GTM;
                    }
                    else {
                        throw new Error('analytics error on setting mode');
                    }
                }
                catch (error) {
                    mode = MODES.UNDETERMINED;
                    _setError('mode');
                }

                _mode = mode;
            }

            function _setSpecialEvents() {
                try {
                    if (config && config.retailer && config.retailer.settings) {
                        _serverEvents.purchase = config.retailer.settings.isDataLayerServerPurchaseEventActive === 'true';
                        _ignoredEvents.selectContent = config.retailer.settings.isDataLayerSelectContentActive === 'false';
                        _ignoredEvents.pageView = config.retailer.settings.isDataLayerPageviewEventActive === 'false';
                    }
                }
                catch (error) {
                    pushDebug('analytics error on setting ignored events');
                    _setError('ignored-events');
                }
            }

            function _setConsent() {
                var isUserConsent;
                try {
                    var userApprovedCookies = localStorageService.getItem('approvedCookies');
                    isUserConsent = ((config && config.retailer && !config.retailer.isCookieWallEnabled && !_isIos()) || (userApprovedCookies && userApprovedCookies.googleAnalytics)) ? true : false;
                    if (_mode === MODES.GTM || _isGtagExists()) {
                        _setConsentGTM(isUserConsent);
                    }
                    if (_mode === MODES.FIREBASE) {
                        _setConsentFirebase(isUserConsent);
                    }
                }
                catch (error) {
                    isUserConsent = false;
                    pushDebug('analytics error on setting consent');
                    _setError('consent');
                }

                _isUserConsentGiven = isUserConsent;
            }

            function _setConsentGTM(isUserConsent) {
                if (isUserConsent) {
                    gtag('consent', 'update', {
                        'ad_storage': 'granted',
                        'analytics_storage': 'granted'
                    });
                }
                else {
                    gtag('consent', 'update', {
                        'ad_storage': 'denied',
                        'analytics_storage': 'denied'
                    });
                }
            }

            function _setConsentFirebase(isUserConsent) {
                // plugin crashes and causes app to crash if null or undefined is passed - use bool only
                $window.cordova.plugins.firebase.analytics.setEnabled(isUserConsent ? true : false);
            }

            function _setUserId() {
                user.getData().then(function (userData) {
                    _userId = (userData && _parseString(userData.id));
                    if (!_userId) {
                        return $q.reject('analytics error on getting user id');
                    }
                }).catch(function(error) {
                    _userId = null;
                    if ($rootScope.USER_ERRORS && $rootScope.USER_ERRORS.NOT_LOGGED_IN !== error) {
                        pushDebug('analytics error on getting user id');
                        _setError('userid-get');
                    }
                }).then(function() {
                    if (!_isUserConsentGiven) {
                        return;
                    }
                    else if (_mode === MODES.GTM) {
                        _setUserIdGTM();
                    }
                    else if (_mode === MODES.FIREBASE) {
                        _setUserIdFirebase();
                    }
                }).catch(function(error) {
                    pushDebug('analytics error on setting user id');
                    _setError('userid-set');
                });
            }

            function _setUserIdGTM() {
                _pushGTM({'user_id': _userId});
            }

            function _setUserIdFirebase() {
                $window.cordova.plugins.firebase.analytics.setUserId(_userId);
            }

            function _setError(errorIn) {
                try {
                    _errors[errorIn] = true;
                }
                catch (error) {}
            }

            function _setWarning(warningIn) {
                try {
                    _warnings[warningIn] = true;
                }
                catch (error) {}
            }

            function _getPath(toState) {
                return PATH_FILTER[toState] || $location.path() || $window.location.pathname || $window.location.href;
            }

            function _getClientId(timeout) {
                if (_mode === MODES.GTM && config.retailer.googleAnalyticsId) {
                    var clientId = _getClientIdStorage();
                    if (clientId) {
                        return clientId;
                    }
                    return _getClientIdGtag(timeout);
                }
                else if (_mode === MODES.FIREBASE) {
                    return _getClientIdFirebase(timeout);
                }
                else {
                    pushDebug('analytics error on getClientId');
                    _setWarning('getClientId');
                }
            }

            function _getClientIdStorage() {
                try {
                    var cookieName = '_ga';
                    var pattern = /GA\d\.\d\.(.+)/;
                    var cookie = $cookies.get(cookieName);
                    var match = cookie && cookie.match(pattern)
                    if (match && match[1]) {
                        return match[1];
                    }
                    else {
                        pushDebug('analytics error on getting client id cookie');
                        _setWarning('clientid-cookie');
                    }
                }
                catch (error) {
                    pushDebug('analytics error on getting client id getClientIdStorage');
                    _setWarning('clientid-getClientIdStorage');
                }
            }

            function _getClientIdGtag(timeout) {
                var defer = $q.defer();
                var visited = false;
                gtag('get', config.retailer.googleAnalyticsId, 'client_id', function (client_id) {
                    if (visited) return;
                    $timeout.cancel(timeout);
                    if (!client_id && config.retailer.googleAnalyticsId.startsWith('G-')) {
                        pushDebug('analytics error on getting client id gtm');
                        _setWarning('clientid-gtm');
                    }
                    defer.resolve(client_id);
                });
                var timeout = $timeout(function() {
                    pushDebug('analytics error on gtag callback client id gtm');
                    _setWarning('gtag-clientid-gtm');
                    visited = true;
                    defer.resolve();
                }, timeout || 0);

                return defer.promise;
            }

            function _getClientIdFirebase(timeout) {
                var defer = $q.defer();
                var visited = false;
                $window.cordova.plugins.firebase.analytics.getAppInstanceId().then(function(appInstanceId) {
                    if (visited) return;
                    $timeout.cancel(timeout);
                    if (!appInstanceId) {
                        pushDebug('analytics error on getting client id firebase');
                        _setWarning('clientid-firebase')
                    }
                    defer.resolve(appInstanceId);
                });
                var timeout = $timeout(function() {
                    pushDebug('analytics error on getId promise client id firebase');
                    _setWarning('getId-clientid-firebase');
                    visited = true;
                    defer.resolve();
                }, timeout || 0);

                return defer.promise;
            }

            function _getSessionId(timeout) {
                if (_mode === MODES.GTM && config.retailer.googleAnalyticsId) {
                    var sessionId = _getSessionIdStorage();
                    if (sessionId) {
                        return sessionId;
                    }
                    return _getSessionIdGtag(timeout);
                }
                else if (_mode !== MODES.FIREBASE) {
                    pushDebug('analytics error on getSessionId');
                    _setWarning('getSessionId');
                }
            }

            function _getSessionIdStorage() {
                try {
                    var cookieName = '_ga_' + config.retailer.googleAnalyticsId.slice(2);
                    var pattern = /GS\d\.\d\.(\w+)/;
                    var cookie = $cookies.get(cookieName);
                    var match = cookie && cookie.match(pattern)
                    if (match && match[1]) {
                        return match[1];
                    }
                    else {
                        pushDebug('analytics error on getting session id cookie');
                        _setWarning('sessionid-cookie');
                    }
                }
                catch (error) {
                    pushDebug('analytics error on getting session id getSessionIdStorage');
                    _setWarning('sessionid-getSessionIdStorage');
                }
            }

            function _getSessionIdGtag(timeout) {
                var defer = $q.defer();
                var visited = false;
                gtag('get', config.retailer.googleAnalyticsId, 'session_id', function (session_id) {
                    if (visited) return;
                    $timeout.cancel(timeout);
                    if (!session_id && config.retailer.googleAnalyticsId.startsWith('G-')) {
                        pushDebug('analytics error on getting session id gtm');
                        _setWarning('sessionid-gtm')
                    }
                    defer.resolve(session_id);
                });
                var timeout = $timeout(function() {
                    pushDebug('analytics error on gtag callback session id gtm');
                    _setWarning('gtag-sessionid-gtm')
                    visited = true;
                    defer.resolve();
                }, timeout || 0);

                return defer.promise;
            }

            function _init() {
                config.waitForInit().then(function() {
                    if (!_isRetailerDataLayerEventsEnabled()) {
                        return;
                    }

                    _setPlatform();
                    _setMode();
                    if (_mode === MODES.UNDETERMINED) {
                        return;
                    }

                    var isLegacyMode = _isLegacyMode();
                    _eventsBuilder = $injector.get(isLegacyMode ? 'Ga3' : 'Ga4');

                    _listeners.push(
                        $rootScope.$on('tracking.permissions.updated', _onCookieWallEvent),
                        $rootScope.$on('login', _onUserLogin),
                        $rootScope.$on('logout', _onUserLogout),
                        $rootScope.$on('$stateChangeSuccess', _onLocationChange),
                        $rootScope.$on('checkout', _onCheckout),
                        $rootScope.$on('checkoutFinished', _onCheckoutFinished)
                    );

                    _isCartUpdateComplete = false;
                    var listener = $rootScope.$on('cart.update.complete', function () {
                        listener();
                        _isCartUpdateComplete = true;
                    });

                    _setSpecialEvents();
                    _setConsent();
                    _setUserId();
                }).catch(function(error) {
                    pushDebug('analytics error on init');
                    _setError('init');
                    _stopListeners(_listeners);
                });
            }
        }]);

})(angular);

(function (angular) {
    'use strict';

    var CUSTOM_PARAMS = [
        'category',
        'action',
        'label',
        'clubMemberAmount',
        'addProductToWishlistProd',
        'searchTerm',
        'results',
        'resultsCount',
        'pageLocation',
        'isFirst',
        'purchaseCounter'
    ];

    var SELECT_CONTENT_ACTIONS = ['Click', 'Open']
    var SELECT_CONTENT_CATEGORIES = {Click: ['Button', 'Link', 'Menu'], Open: ['Dialog']};

    angular.module('spServices').service('Ga3', [
        function () {
            var self = this;

            angular.extend(self, {
                productImpressions: productImpressions,
                productDetail: productDetail,
                searchProduct: searchProduct,
                addToCart: addToCart,
                removeFromCart: removeFromCart,
                checkout: checkout,
                purchase: purchase,
                clubMembersUse: clubMembersUse,
                addProductToWishlist: addProductToWishlist,
                loginView: loginView,
                loginSuccess: loginSuccess,
                loginFail: loginFail,
                registrationView: registrationView,
                registrationSuccess: registrationSuccess,
                registrationFail: registrationFail,
                contactUsView: contactUsView,
                contactUsSuccess: contactUsSuccess,
                contactUsFail: contactUsFail,
                promotionView: promotionView,
                promotionClick: promotionClick,
                selectContent: selectContent,
                pageView: pageView,
                prepareProduct: prepareProduct,
                preparePromotion: preparePromotion,
                prepareWishListItem: prepareWishListItem,
                enrichEvents: enrichEvents,
                CUSTOM_PARAMS: CUSTOM_PARAMS
            });

            function productImpressions(eventData) {
                if (!eventData.products) {
                    return;
                }

                return {
                    'event': 'productImpressions',
                    'ecommerce': {
                        'impressions': eventData.products
                    }
                }
            }

            function productDetail(eventData) {
                if (!eventData.products) {
                    return;
                }

                return {
                    'event': 'productDetail',
                    'ecommerce': {
                        'detail': {
                            'actionField': {
                                'list': eventData.data.location,
                            },
                            'products': eventData.products
                        }
                    }
                };
            }

            function searchProduct(eventData) {
                if(!eventData.data.searchTerm) {
                    return;
                }

                return {
                    'event': 'searchProduct',
                    'searchTerm': eventData.data.searchTerm,
                    'results': eventData.data.resultsCount > 0,
                    'resultsCount': eventData.data.resultsCount
                };
            }

            function addToCart(eventData) {
                if (!(eventData.products && eventData.data.quantity > 0 && eventData.data.cartUpdateComplete)) {
                    return;
                }

                eventData.products[0].quantity = eventData.data.quantity;

                return {
                    'event': 'addToCart',
                    'ecommerce': {
                        'add': {
                            'actionField': {
                                'list': eventData.data.location,
                            },
                            'products': eventData.products
                        }
                    }
                };
            }

            function removeFromCart(eventData) {
                if (!(eventData.products && eventData.data.quantity >= 0 && eventData.data.cartUpdateComplete)) {
                    return;
                }

                eventData.products[0].quantity = eventData.data.quantity;

                return {
                    'event': 'removeFromCart',
                    'ecommerce': {
                        'remove': {
                            'products': eventData.products
                        }
                    }
                };
            }

            function checkout(eventData) {
                if (!eventData.products) {
                    return;
                }

                return {
                    'event': 'checkout',
                    'ecommerce': {
                        'checkout': {
                            'actionField': {
                                'step': eventData.data.step,
                                'option': eventData.data.option
                            },
                            'products': eventData.products
                        }
                    }
                }
            }

            function purchase(eventData) {
                if (!(eventData.products && eventData.purchase && eventData.purchase.id)) {
                    return;
                }

                return {
                    'event': 'purchase',
                    'ecommerce': {
                        'purchase': {
                            'actionField': {
                                'id': eventData.purchase.id,
                                'affiliation': eventData.purchase.shippingTier,
                                'revenue': eventData.purchase.revenue,
                                'tax': eventData.purchase.tax,
                                'shipping': eventData.purchase.shipping,
                                'coupon': eventData.purchase.coupon
                            },
                            'products': eventData.products
                        }
                    }
                };
            }

            function clubMembersUse(eventData) {
                return {
                    'event': 'clubMembersUse',
                    'clubMemberAmount': eventData.data.clubMemberAmount
                };
            }

            function addProductToWishlist(eventData) {
                if (!eventData.wishListItem) {
                    return;
                }

                return {
                    'event': 'addProductToWishlist',
                    'addProductToWishlistProd': eventData.wishListItem
                };
            }

            function loginView() {
                return {
                    'event': 'loginView'
                };
            }

            function loginSuccess() {
                return {
                    'event': 'loginSuccess'
                };
            }

            function loginFail(eventData) {
                return {
                    'event': 'loginFail',
                    'loginFailName': 'Login failure',
                    'loginFailEmail': eventData.data.email
                };
            }

            function registrationView() {
                return {
                    'event': 'registrationView'
                };
            }

            function registrationSuccess() {
                return {
                    'event': 'registrationSuccess'
                };
            }

            function registrationFail(eventData) {
                return {
                    'event': 'registrationFail',
                    'registrationFailName': eventData.data.error,
                    'registrationFailEmail': eventData.data.email
                };
            }

            function contactUsView() {
                return {
                    'event': 'contactUsView'
                };
            }

            function contactUsSuccess() {
                return {
                    'event': 'contactUsSuccess'
                };
            }

            function contactUsFail(eventData) {
                return {
                    'event': 'contactUsFail',
                    'contactUsFailName': eventData.data.error
                };
            }

            function promotionView(eventData) {
                if (!eventData.promotion) {
                    return;
                }

                return {
                    'event': 'promotionView',
                    'ecommerce': {
                        'promoView': {
                            'promotions': [eventData.promotion]
                        }
                    }
                };
            }

            function promotionClick(eventData) {
                if (!eventData.promotion) {
                    return;
                }

                return {
                    'event': 'promotionClick',
                    'ecommerce': {
                        'promoClick': {
                            'promotions': [eventData.promotion],
                        }
                    },
                    'eventCallback': _promotionCallback(eventData)
                };
            }

            function selectContent(eventData) {
                if (eventData.data.label && SELECT_CONTENT_ACTIONS.includes(eventData.data.action) && SELECT_CONTENT_CATEGORIES[eventData.data.action].includes(eventData.data.category)) {
                    if (eventData.data.category === 'Menu' ) {
                        return {
                            'event': 'menuClick',
                            'menuCategory': eventData.data.label
                        };
                    }
                    else {
                        return {
                            'event': 'buttonClick',
                            'category': eventData.data.category,
                            'action': eventData.data.action,
                            'label': eventData.data.label
                        };
                    }
                }
            }

            function pageView(eventData) {
                return {
                    'event': 'virtualPageview',
                    'pageLocation': eventData.data.location
                };
            }

            //toDo: make sure numbers are prepared as numbers in data and product - to fixed should only be at event level for preparing view
            function prepareProduct(event, productData) {
                if (!(productData.productName && productData.price)) {
                    return;
                }

                var preparedProduct = {
                    'name': productData.productName,
                    'id': productData.id,
                    'price': productData.price && productData.price,
                    'brand': productData.brandName,
                    'category': productData.category,
                    'discount': productData.currentDiscount && productData.currentDiscount,
                    'discountPrecent': productData.currentDiscountPrecent && productData.currentDiscountPrecent,
                    'position': productData.currentProductPosition
                }

                if (productData.quantity >= 0) {
                    preparedProduct.quantity = productData.quantity;
                }

                if (productData.addedFrom) {
                    preparedProduct.addedFrom = productData.addedFrom;
                }

                return preparedProduct;
            }

            function preparePromotion(promotionData) {
                if (!promotionData.id) {
                    return;
                }

                return {
                    'id': promotionData.id,
                    'name': promotionData.creativeName,
                    'creative': promotionData.creativeUrl,
                    'type': promotionData.type
                };
            }

            function prepareWishListItem(wishListItemData) {
                if (!wishListItemData.name) {
                    return;
                }

                return wishListItemData.name;
            }

            function enrichEvents(events, enrichmentData) {
                angular.forEach(events, function (event) {
                    if (event.ecommerce) {
                        event.ecommerce.currencyCode = enrichmentData.currencyCode;
                    }

                    event.userEmail = enrichmentData.userEmail;
                    event.userId = enrichmentData.userId;
                    event.isFirst = enrichmentData.isFirst;
                    event.purchaseCounter = enrichmentData.purchaseCounter;
                });
            }

            //toDo: what does it do? returns to whom? - usually used to navigate - something else here - check if changes dataLayer or perhaps fires event
            function _promotionCallback(eventData) {
                var promotion = eventData.promotion || {};

                var event = '';

                switch (promotion.type) {
                    case 'video':
                        event = 'video playback';
                        break;

                    case 'banner':
                        event = 'banner click';
                        break;

                    case 'recipe':
                        event = 'recipe popup';
                }

                return {
                    'url': promotion.url,
                    'event': event
                }
            }
        }]);

})(angular);

(function (angular) {
    'use strict';

    var CUSTOM_PARAMS = [
        'content_type',
        'content_category',
        'content_label',
        'club_member_amount',
        'search_term',
        'results',
        'results_count',
        'page_location',
        'is_first',
        'purchase_counter',
        'foreign_id',
        'branch_id'
    ];

    var SELECT_CONTENT_ACTIONS = ['Click', 'Open'];
    var SELECT_CONTENT_CATEGORIES = {Click: ['Button', 'Link', 'Menu'], Open: ['Dialog']};
    var SELECT_CONTENT_IGNORE_LIST = ['Open Product Page'];

    var SUPPORTED_KEYS = {
        'price': 0,
        'discount': 1,
        'quantity': 2
    }

    var EVENTS_TO_KEY_MAPPER = {
        'addToCart': ['pricePerCartQuantity', 'currentDiscountPerCartQuantity', 'cartQuantity'],
        'removeFromCart': ['pricePerCartQuantity', 'currentDiscountPerCartQuantity', 'cartQuantity'],
        'checkout': ['pricePerCartQuantity', 'currentDiscountPerCartQuantity', 'quantity'],
        'purchase': ['pricePerCartQuantity', 'currentDiscountPerCartQuantity', 'quantity']
    }

    angular.module('spServices').service('Ga4', [
        function () {
            var self = this

            angular.extend(self, {
                productImpressions: productImpressions,
                productDetail: productDetail,
                productClick: productClick,
                searchProduct: searchProduct,
                addToCart: addToCart,
                removeFromCart: removeFromCart,
                checkout: checkout,
                purchase: purchase,
                clubMembersUse: clubMembersUse,
                addProductToWishlist: addProductToWishlist,
                loginView: loginView,
                loginSuccess: loginSuccess,
                loginFail: loginFail,
                registrationView: registrationView,
                registrationSuccess: registrationSuccess,
                registrationFail: registrationFail,
                contactUsView: contactUsView,
                contactUsSuccess: contactUsSuccess,
                contactUsFail: contactUsFail,
                promotionView: promotionView,
                promotionClick: promotionClick,
                selectContent: selectContent,
                pageView: pageView,
                prepareProduct: prepareProduct,
                preparePromotion: preparePromotion,
                prepareWishListItem: prepareWishListItem,
                enrichEvents: enrichEvents,
                CUSTOM_PARAMS: CUSTOM_PARAMS
            });

            function productImpressions(eventData) {
                if (!eventData.products) {
                    return;
                }

                return {
                    'event': 'view_item_list',
                    'ecommerce': {
                        'item_list_name': eventData.data.location,
                        'item_list_id': eventData.data.location,
                        'items': eventData.products
                    }
                };
            }

            function productDetail(eventData) {
                if (!(eventData.products && eventData.values)) {
                    return;
                }

                return {
                    'event': 'view_item',
                    'ecommerce': {
                        'item_list_name': eventData.data.location,
                        'item_list_id': eventData.data.location,
                        'value': eventData.values.itemValue,
                        'items': eventData.products
                    }
                };
            }

            function productClick(eventData) {
                if (!eventData.products && eventData.values) {
                    return;
                }

                return {
                    'event': 'select_item',
                    'ecommerce': {
                        'item_list_name': eventData.data.location,
                        'item_list_id': eventData.data.location,
                        'value': eventData.values.itemValue,
                        'items': eventData.products
                    }
                };
            }

            function searchProduct(eventData) {
                if (!eventData.data.searchTerm) {
                    return;
                }

                return {
                    'event': 'search',
                    'search_term': eventData.data.searchTerm,
                    'results': eventData.data.resultsCount > 0,
                    'results_count': eventData.data.resultsCount
                };
            }

            function addToCart(eventData) {
                if (!(eventData.products && eventData.values && (eventData.data.quantity || eventData.products[0].quantity) && eventData.data.cartUpdateComplete)) {
                    return;
                }

                eventData.products[0].quantity = eventData.products[0].quantity || eventData.data.quantity;

                return {
                    'event': 'add_to_cart',
                    'ecommerce': {
                        'item_list_name': eventData.data.location,
                        'item_list_id': eventData.data.location,
                        'value': eventData.values.cartItemValue,
                        'items': eventData.products
                    }
                };
            }

            function removeFromCart(eventData) {
                if (!(eventData.products && eventData.values && (eventData.data.quantity || eventData.products[0].quantity) && eventData.data.cartUpdateComplete)) {
                    return;
                }

                eventData.products[0].quantity = eventData.products[0].quantity || eventData.data.quantity;

                return {
                    'event': 'remove_from_cart',
                    'ecommerce': {
                        'value': eventData.values.cartItemValue,
                        'items': eventData.products
                    }
                };
            }


            function checkout(eventData) {
                if (!(eventData.products && eventData.values && eventData.data.option)) {
                    return;
                }

                //toDo: refactor
                switch (eventData.data.option) {
                    case 'cart summary':
                        return _ga4ViewCart(eventData.products, eventData.values.checkoutValue);
                    case 'user address':
                        var events = [];
                        events.push(_ga4BeginCheckout(eventData.products, eventData.values.checkoutValue));
                        events.push(_ga4AddShippingInfo(eventData.products, eventData.values.checkoutValue));
                        return events;
                    case 'user address and time slot':
                        var events = [];
                        events.push(_ga4BeginCheckout(eventData.products, eventData.values.checkoutValue));
                        events.push(_ga4AddShippingInfo(eventData.products, eventData.values.checkoutValue));
                        return events;
                    case 'time slot':
                        return {skip: true};
                    case 'payment details':
                        return _ga4AddPaymentInfo(eventData.products, eventData.values.checkoutValue);
                    case 'payment summary':
                        return {skip: true};
                }
            }

            function purchase(eventData) {
                if (!(eventData.products && eventData.purchase && eventData.purchase.id)) {
                    return;
                }

                var event = {
                    'event': eventData.data.serverEvents.purchase ? 'sp_purchase' : 'purchase',
                    'ecommerce': {
                        'transaction_id': eventData.purchase.id,
                        'value': eventData.purchase.revenue,
                        'shipping_tier': eventData.purchase.shippingTier,
                        'payment_type': eventData.purchase.paymentType,
                        'tax': eventData.purchase.tax,
                        'shipping': eventData.purchase.shipping,
                        'coupon': eventData.purchase.coupon,
                        'items': eventData.products
                    }
                };

                try {
                    var eventStr = JSON.stringify(event);
                    var eventJson = JSON.parse(eventStr);
                    return eventJson;
                }
                catch (err) {
                    return;
                }
            }

            function clubMembersUse(eventData) {
                return {
                    'event': 'club_member_use',
                    'club_member_amount': eventData.data.clubMemberAmount
                };
            }

            function addProductToWishlist(eventData) {
                if (!eventData.wishListItem) {
                    return;
                }

                //toDo: make sure price exists
                return {
                    'event': 'add_to_wishlist',
                    'ecommerce': {
                        'item_list_name': eventData.data.location,
                        'item_list_id': eventData.data.location,
                        'value': eventData.wishListItem.price,
                        'items': [eventData.wishListItem]
                    }
                };
            }

            function loginView() {
                return {
                    'event': 'login_view'
                };
            }

            function loginSuccess() {
                return {
                    'event': 'login'
                };
            }

            function loginFail() {
                return {
                    'event': 'login_fail'
                };
            }

            function registrationView() {
                return {
                    'event': 'sign_up_view'
                };
            }

            function registrationSuccess() {
                return {
                    'event': 'sign_up'
                };
            }

            function registrationFail() {
                return {
                    'event': 'sign_up_fail'
                };
            }

            function contactUsView() {
                return {
                    'event': 'contact_us_view'
                };
            }

            function contactUsSuccess() {
                return {
                    'event': 'contact_us'
                };
            }

            function contactUsFail() {
                return {
                    'event': 'contact_us_fail'
                };
            }

            function promotionView(eventData) {
                if (!eventData.promotion) {
                    return;
                }

                return {
                    'event': 'view_promotion',
                    'ecommerce': eventData.promotion
                };
            }

            function promotionClick(eventData) {
                if (!eventData.promotion) {
                    return;
                }

                return {
                    'event': 'select_promotion',
                    'ecommerce': eventData.promotion
                };
            }

            function selectContent(eventData) {
                if (SELECT_CONTENT_IGNORE_LIST.includes(eventData.data.label)) {
                    return {skip: true}
                }
                else if (!(eventData.data.label && SELECT_CONTENT_ACTIONS.includes(eventData.data.action) && SELECT_CONTENT_CATEGORIES[eventData.data.action].includes(eventData.data.category))) {
                    return;
                }

                return {
                    'event': 'select_content',
                    'content_type': eventData.data.action.toLowerCase(),
                    'content_category': eventData.data.category.toLowerCase(),
                    'content_label': eventData.data.label
                };
            }

            function pageView(eventData) {
                return {
                    'event': 'virtual_page_view',
                    'page_location': eventData.data.location
                };
            }

            function prepareProduct(event, productData) {
                if (!(productData.productName && productData.price)) {
                    return;
                }

                var priceKey = _getKeyByEvent(event, 'price') || 'price';
                var discountKey = _getKeyByEvent(event, 'discount') || 'currentDiscount';
                var quantityKey = _getKeyByEvent(event, 'quantity') || 'quantity';

                return {
                    'item_name': productData.productName,
                    'item_id': productData.id,
                    'affiliation': productData.affiliation,
                    'coupon': productData.coupon,
                    'price': productData[priceKey],
                    'item_brand': productData.brandName,
                    'item_category': productData.category,
                    'index': productData.currentProductPosition,
                    'discount': productData[discountKey],
                    'quantity': productData[quantityKey],
                    'item_list_name': productData.addedFrom,
                    'item_list_id': productData.addedFrom,
                    'item_weight': productData.itemWeight
                };

            }

            function preparePromotion(promotionData) {
                if (!promotionData.id) {
                    return;
                }

                return {
                    'promotion_id': promotionData.id,
                    'promotion_name': promotionData.creativeName,
                    'creative_name': promotionData.creativeUrl,
                    'creative_slot': promotionData.type
                };
            }

            function prepareWishListItem(wishListItemData) {
                if (!wishListItemData.name) {
                    return;
                }

                return {
                    'item_name': wishListItemData.name,
                    'price': wishListItemData.price
                };
            }

            function enrichEvents(events, enrichmentData) {
                angular.forEach(events, function (event) {
                    if (event.ecommerce) {
                        event.ecommerce.currency = enrichmentData.currencyCode;
                    }

                    event.user_email = enrichmentData.userEmail;
                    event.user_id = enrichmentData.userId;
                    event.is_first = enrichmentData.isFirst;
                    event.purchase_counter = enrichmentData.purchaseCounter;
                    event.foreign_id = enrichmentData.foreignId;
                    event.branch_id = enrichmentData.branchId;
                });
            }

            function _ga4ViewCart(products, productsValue) {
                return {
                    'event': 'view_cart',
                    'ecommerce': {
                        'value': productsValue,
                        'items': products
                    }
                };
            }

            function _ga4BeginCheckout(products, productsValue) {
                return {
                    'event': 'begin_checkout',
                    'ecommerce': {
                        'value': productsValue,
                        'items': products
                    }
                };
            }

            function _ga4AddShippingInfo(products, productsValue) {
                return {
                    'event': 'add_shipping_info',
                    'ecommerce': {
                        'value': productsValue,
                        'items': products
                    }
                };
            }

            function _ga4AddPaymentInfo(products, productsValue) {
                return {
                    'event': 'add_payment_info',
                    'ecommerce': {
                        'value': productsValue,
                        'items': products
                    }
                };
            }

            function _getKeyByEvent(event, key) {
                if (!EVENTS_TO_KEY_MAPPER[event] || SUPPORTED_KEYS[key] === undefined) {
                    return;
                }

                return EVENTS_TO_KEY_MAPPER[event][SUPPORTED_KEYS[key]];
            }
        }]);

})(angular);
