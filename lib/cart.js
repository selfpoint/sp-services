
(function (angular) {
    /**
     * @typedef {object} Locals
     *
     * @property {boolean} isRegularPriceWithTax
     * @property {boolean} includeTaxInPrice
     * @property {number} languageId
     * @property {boolean} withDeliveryProduct
     * @property {array<number>} loyaltyClubIds
     * @property {number} [userId]
     * @property {object} [apiOptions]
     * @property {number} [deliveryItemsLimit]
     */

    /**
     * @typedef {object} Line
     * @property {object} product
     * @property {boolean} isCase
     * @property {boolean} isPseudo
     * @property {boolean} removed
     * @property {number} [totalPriceWithTax]
     * @property {number} [totalPriceWithoutTax]
     * @property {number} [totalPriceForView]
     * @property {number} [totalPriceForViewPotential]
     * @property {number} [finalPriceWithTax]
     * @property {number} [finalPriceWithTaxPotential]
     * @property {number} [finalPriceWithoutTax]
     * @property {number} [finalPriceWithoutTaxPotential]
     * @property {number} [finalPriceForView]
     * @property {number} [finalPriceForViewPotential]
     * @property {number} [totalGifts]
     * @property {number} [totalGiftsPotential]
     * @property {number} [totalSimulateClubsGifts]
     * @property {number} [totalSimulateClubsGiftsPotential]
     * @property {number} [totalClubsGifts]
     * @property {number} [totalClubsGiftsPotential]
     * @property {number} [totalGiftsWithTax]
     * @property {number} [totalGiftsWithoutTax]
     * @property {number} [totalGiftsForView]
     * @property {number} [totalGiftsWithTaxPotential]
     * @property {number} [totalGiftsWithoutTaxPotential]
     * @property {number} [totalGiftsForViewPotential]
     * @property {number} [totalClubsGiftsWithTax]
     * @property {number} [totalClubsGiftsWithoutTax]
     * @property {number} [totalClubsGiftsForView]
     * @property {number} [totalClubsGiftsWithTaxPotential]
     * @property {number} [totalClubsGiftsWithoutTaxPotential]
     * @property {number} [totalClubsGiftsForViewPotential]
     * @property {number} [totalSimulateClubsGiftsWithTax]
     * @property {number} [totalSimulateClubsGiftsWithoutTax]
     * @property {number} [totalSimulateClubsGiftsForView]
     * @property {number} [totalSimulateClubsGiftsWithTaxPotential]
     * @property {number} [totalSimulateClubsGiftsWithoutTaxPotential]
     * @property {number} [totalSimulateClubsGiftsForViewPotential]
     * @property {number} [sort]
     * @property {number} [type]
     * @property {number} [quantity]
     * @property {string} [comments]
     * @property {number} [productPropertyValueId]
     * @property {object} [productPropertySelectedValue]
     */

    /**
     * Service name
     */
    var serviceName = 'SpCartService';

    /**
     * Provider instance
     */
    var provider;

    /**
     * Max line quantity
     */
    var MAX_LINE_QUANTITY = 999999;

    /**
     * Service
     */
    function SpCartService($rootScope, $injector, $filter, $timeout, $q, Api, SpProductTags, EmbedHistoryRetailer, SP_SERVICES, PRODUCT_TAG_TYPES) {
        var self = this;

        self.init = init;
        self.clear = clearCart;
        self.addLine = addLine;
        self.addLines = addLines;
        self.removeLine = removeLine;
        self.removeLines = removeLines;
        self.quantityInterval = quantityInterval;
        self.minusQuantity = minusQuantity;
        self.plusQuantity = plusQuantity;
        self.quantityChanged = quantityChanged;
        self.commentsChanged = commentsChanged;
        self.productPropertyValueChanged = productPropertyValueChanged;
        self.getProduct = getProduct;
        self.getProducts = getProducts;
        self.getLines = getLines;
        self.isUnitsWeighable = isUnitsWeighable;
        self.save = saveCart;
        self.setTimeTravel = setTimeTravel;
        self.keyByLine = getLineKey;
        self.replaceCart = replaceCart;
        self.createCart = createCart;
        self.validateMinimumCost = validateMinimumCost;
        self.validateDeliveryItemsLimit = validateDeliveryItemsLimit;
        self.getEBTEligible = getEBTEligible;
        self.getRemainingPayment = getRemainingPayment;
        self.disableLoyalty = disableLoyalty;
        self.checkEBTEligibleCartChange = checkEBTEligibleCartChange;
        self.lines = {};

        var deletedLines = {},
            dirtyLines = {},
            _saveCartDefer,
            _trafficParams = _getTrafficParams(),
            _isLoyaltyDisabled = false,
            _roundCurrencyFilter;

        /**
         * The cart sale date
         * @type {Date}
         */
        self.saleDate = null;

        /**
         * The cart id in data base
         * @type {number}
         */
        self.serverCartId = undefined;

        /**
         * If is edit order cart
         * @type {number}
         */
        self.editOrderId = null;

        /**
         * Special Reminders data. Reset on each server response.
         * @type {Object}
         */
        self.specialReminders = {};

        /**
         * Object with the products that has been changed in the last 3 seconds.
         * Reset on each server response.
         * @type {Object<string, Promise>}
         */
        self.changedProducts = {};

        if (provider.initCart && (angular.isFunction(provider.initCart) || angular.isArray(provider.initCart))) {
            $injector.invoke(provider.initCart, self, { cart: self });
        }

        /**
         * To sort lines by added order
         * @type {number}
         */
        var sortNumber = 0;
        // set sort number from local cart lines
        angular.forEach(self.lines, function (line) {
            if (!('sort' in line)) {
                line.sort = sortNumber++;
            } else if (angular.isNumber(line.sort) && sortNumber <= line.sort) {
                sortNumber = line.sort + 1;
            }
        });

        /***
         * Validate Minimum Cost per cart
         * @param {Number} minimumOrderPrice
         * @param {Boolean} notIncludeSpecials
         * @returns {Promise}
         */
        function validateMinimumCost(minimumOrderPrice, notIncludeSpecials ){
            var minimumOrderPriceNum = Number(minimumOrderPrice),
                price = notIncludeSpecials ? self.total.priceWithoutPromotionProductsForView : self.total.finalPriceForView;

            if (_roundDecimal(price) < (!isNaN(minimumOrderPriceNum) && minimumOrderPriceNum > 0 ? _roundDecimal(minimumOrderPriceNum) : 0)) {
                return $q.reject(price);
            }

            return $q.resolve();
        }


        /**
         * Round number to two decimal digits
         * @private
         *
         * @param {number} num
         *
         * @return {number}
         */
        function _roundDecimal(num) {
            if (!_roundCurrencyFilter) {
                try {
                    _roundCurrencyFilter = $filter('roundCurrency');
                } catch (err) {
                    console.warn('sp-services: roundCurrency filter wasn\'t found, using default round');
                    _roundCurrencyFilter = function (amount) {
                        return Math.round(amount * 1000) / 1000;
                    };
                }
            }

            return _roundCurrencyFilter(num);
        }

        /**
         * Gets the locals
         * @private
         *
         * @returns {Promise<Locals>}
         */
        function _getLocals() {
            var shouldReturn = 'a promise with the following object: { loyaltyClubIds, languageId, includeTaxInPrice, withDeliveryProduct, userId, [apiOptions], [deliveryItemsLimit] }';
            if (!provider.getLocals || (!angular.isFunction(provider.getLocals) && !angular.isArray(provider.getLocals))) {
                throw new Error(serviceName + 'Provider.getLocals must be implemented (returns ' + shouldReturn + ' )');
            }

            var promise = $injector.invoke(provider.getLocals, self);
            if (!promise || !promise.then || !angular.isFunction(promise.then)) {
                throw new Error(serviceName + 'Provider.getLocals must return ' + shouldReturn);
            }

            return promise.then(function(locals) {
                if (typeof locals.includeTaxInPrice !== 'boolean') {
                    locals.includeTaxInPrice = locals.isRegularPriceWithTax;
                }

                return locals;
            });
        }

        /**
         * Get line key by is case
         * @param {Line} line
         * @returns {string}
         * @public
         */
        function getLineKey(line) {
            return (line.product.id ? line.product.id.toString() : '') + (!!line.isCase ? '1' : '0');
        }

        /**
         * Set line prices
         * @param {Line} line
         * @param {Locals} locals
         * @returns {Line}
         * @private
         */
        function _setLinePrices(line, locals) {
            var price = 0;
            if (!line.isPseudo && !(line.product.isCoupon && line.isCouponPriceZero === 'true') && line.product.branch) {
                var packInformationExist = line.product.branch.packQuantity && line.product.branch.packPrice;
                if (packInformationExist) {
                    price = (line.product.branch.packPrice / line.product.branch.packQuantity)  * line.quantity;
                } else {
                    if (!!line.soldBy && line.soldBy === $rootScope.PRODUCT_DISPLAY.WEIGHT.name) {
                        price = (line.isCase ? line.product.branch.case.price : line.product.branch.regularPrice) * (!line.isCase && isUnitsWeighable(line) ? line.product.weight : 1) * (line.quantity / (!line.isCase && isUnitsWeighable(line) ? line.product.weight : 1));
                    } else {
                        price = (line.isCase ? line.product.branch.case.price : line.product.branch.regularPrice) * (!line.isCase && isUnitsWeighable(line) ? line.product.weight : 1) * line.quantity;
                    }
                }
            }

            _setLinePriceProp(line, 'totalGifts', line.totalGifts, line.totalGiftsPotential, locals);
            _setLinePriceProp(line, 'totalClubsGifts', line.totalClubsGifts, line.totalClubsGiftsPotential, locals);
            _setLinePriceProp(line, 'totalPrice', price, price, locals);

            if (line.totalSimulateClubsGifts){
                _setLinePriceProp(line, 'totalSimulateClubsGifts', line.totalSimulateClubsGifts, line.totalSimulateClubsGiftsPotential, locals);
            }

            _setPropTypes(line, 'finalPrice', function(type) {
                return line['totalPrice' + type] - line['totalGifts' + type] - line['totalClubsGifts' + type];
            });

            return line;
        }

        /**
         * Sets the price prop for all prices types and for all types in potential
         * @private
         *
         * @param {Object} obj
         * @param {String} propName
         * @param {Function} value
         */
        function _setPropTypes(obj, propName, value) {
            angular.forEach(['WithTax', 'WithoutTax', 'ForView'], function(type) {
                obj[propName + type] = value(type);
                obj[propName + type + 'Potential'] = value(type + 'Potential');
            });
        }

        /**
         * Set line price prop fields
         * @param {Line} line
         * @param {string} key
         * @param {number} price
         * @param {number} [potentialPrice]
         * @param {Locals} locals
         * @private
         */
        function _setLinePriceProp(line, key, price, potentialPrice, locals) {
            var markupPercentage = (line.product.branch && line.product.branch.markupPercentage || 0);
            line[key + 'Markup'] = price * markupPercentage;
            line[key + 'MarkupPotential'] = potentialPrice * markupPercentage;

            var taxAmount = ((line.product.branch && line.product.branch.taxAmount || 0) + 1);
            if (locals.isRegularPriceWithTax) {
                line[key + 'WithTax'] = price + line[key + 'Markup'];
                line[key + 'WithoutTax'] = price / taxAmount;

                line[key + 'WithTaxPotential'] = potentialPrice;
                line[key + 'WithoutTaxPotential'] = potentialPrice / taxAmount;
            } else {
                line[key + 'WithTax'] = (price + line[key + 'Markup']) * taxAmount;
                line[key + 'WithoutTax'] = price;
                if (key === 'totalPrice' && line.product && line.product.branch && line.product.branch.linkedProductPrice) {
                  line[key + 'WithTax'] = ((price - (line.product.branch.linkedProductPrice * line.quantity) + line[key + 'Markup']) * taxAmount) + (line.product.branch.linkedProductPrice * line.quantity)
                }
                line[key + 'WithTaxPotential'] = potentialPrice * taxAmount;
                line[key + 'WithoutTaxPotential'] = potentialPrice;
            }

            var taxKey = locals.includeTaxInPrice ? 'WithTax' : 'WithoutTax';
            line[key + 'ForView'] = line[key + taxKey];
            line[key + 'ForViewPotential'] = line[key + taxKey + 'Potential'];
        }

        /**
         * @constant
         * @type {Object}
         */
        var TOTAL_DEFAULT = {
            lines: 0,
            actualLines: 0,
            coupons: 0,
            priceWithTax: 0,
            priceWithoutTax: 0,
            priceForView: 0,
            giftsWithTax: 0,
            giftsWithoutTax: 0,
            giftsForView: 0,
            giftsWithTaxPotential: 0,
            giftsWithoutTaxPotential: 0,
            giftsForViewPotential: 0,
            clubsGiftsWithTax: 0,
            clubsGiftsWithoutTax: 0,
            clubsGiftsForView: 0,
            clubsGiftsWithTaxPotential: 0,
            clubsGiftsWithoutTaxPotential: 0,
            clubsGiftsForViewPotential: 0,
            simulateClubsGiftsWithTax: 0,
            simulateClubsGiftsWithoutTax: 0,
            simulateClubsGiftsForView: 0,
            simulateClubsGiftsWithTaxPotential: 0,
            simulateClubsGiftsWithoutTaxPotential: 0,
            simulateClubsGiftsForViewPotential: 0,
            priceWithoutPromotionProductsForView: 0,
            finalPriceWithTax: 0,
            finalPriceWithoutTax: 0,
            finalPriceForView: 0,
            finalPriceWithTaxPotential: 0,
            finalPriceWithoutTaxPotential: 0,
            finalPriceForViewPotential: 0,
            multiPassGift: 0,
            tax: 0,
            markup: 0
        };

        /**
         * set linePrice with tax but withot deposit
         * @param {Number} finalPriceWithoutTax
         * @param {Number} finalPriceWithTax
         * @param {Number} lineDepositPrice
         * @param {Number} lineDepositPrice
         * @param {Number} lineQuantity
         * @param {Number} lineTax
         * @param {Locals} [locals]
         * @param {Boolean} round,
         * @param {Number} [caseItems]
         * @private
         */
        function _lineFinalPriceWithTaxWithoutDepositAmount(finalPriceWithoutTax, finalPriceWithTax, lineDepositPrice, lineTax, lineQuantity, locals, round, caseItems) {
            var taxAmount = ((lineTax || 0) + 1);
            var valueToReturn = finalPriceWithTax
            caseItems = caseItems || 1;
            if(!locals.includeTaxInPrice){
                valueToReturn = lineDepositPrice ? ((finalPriceWithoutTax - (lineDepositPrice * caseItems * lineQuantity)) * taxAmount) + (lineDepositPrice * caseItems * lineQuantity) : finalPriceWithTax;
            }
            return round ? _roundDecimal(valueToReturn) : valueToReturn;
        }

        /**
         * Sets the cart's totals
         * @param {Locals} [locals]
         * @private
         */
        function _setCartTotal(locals) {
            var prevCrossProductTagTypes = self.total && self.total.crossProductTagTypes;

            self.total = angular.copy(TOTAL_DEFAULT);
            self.total.deliveryCost = angular.copy(TOTAL_DEFAULT);
            self.total.serviceFee = angular.copy(TOTAL_DEFAULT);
            self.total.calculating = true;

            // TODO: should be exposed instead of self.total
            var totalByType = {},
                multiPassGiftSum = {
                    finalPriceForView: 0,
                    totalPriceForView: 0,
                    finalPriceWithTax: 0,
                    finalPriceForViewWithDeliveryFee: 0,
                };

            totalByType[SP_SERVICES.CART_LINE_TYPES.DELIVERY] = self.total.deliveryCost;
            totalByType[SP_SERVICES.CART_LINE_TYPES.SERVICE_FEE] = self.total.serviceFee;

            if (!locals) {
                self.total.crossProductTagTypes = prevCrossProductTagTypes;
                return _getLocals().then(_setCartTotal);
            }

            var productsTagTypesCount = { all: 0, byType: {} };
            angular.forEach(self.lines, function (line) {
                line.id = getLineKey(line);
                var totalObj = totalByType[line.type] || self.total;
                if (line.type !== SP_SERVICES.CART_LINE_TYPES.COUPON) {
                    totalObj.lines += line.quantity ? 1 : 0;
                    totalObj.actualLines++;
                } else {
                    totalObj.coupons++;
                }

                if(line.gifts && line.gifts.length) {
                    line.gifts.forEach(function (gift, index) {
                        if(gift.promotion && gift.promotion.localId && gift.promotion.localId.toLowerCase().includes(SP_SERVICES.PROMOTION_ID.COLLEAGUE)) {
                            line.gifts[index].promotion.loyalty = line.gifts[index].promotion.loyalty || [];
                            line.gifts[index].promotion.loyalty.push(line.gifts[index].promotion.localId);

                            _setPropTypes(totalObj, 'colleague', function(type) {
                                return _roundDecimal(totalObj['colleague' + type] || 0) + (line.gifts[index].discount || 0);
                            });
                        }
                    })
                }

                // This should be before setting the totalObj
                if (line.product && line.product.branch && line.product.branch.linkedProductPrice && $rootScope.config.retailer.isRegularPriceWithTax) {
                    const taxAmount = ((line.product && line.product.branch && line.product.branch.taxAmount) ? line.product.branch.taxAmount : 0);
                    const priceWithoutLinkedProductPrice = line.product.branch.linkedProductPrice * line.quantity;
                    line.finalPriceWithoutTax = (line.finalPriceForView - priceWithoutLinkedProductPrice) / (1 + taxAmount) + (priceWithoutLinkedProductPrice);
                }

                if (!line.type || line.type === SP_SERVICES.CART_LINE_TYPES.PRODUCT) {
                    _addLineProductTagTypes(line, productsTagTypesCount);
                }

                _setPropTypes(totalObj, 'price', function(type) {
                    return _roundDecimal(totalObj['price' + type] || 0) + (line['totalPrice' + type] || 0);
                });

                _setPropTypes(totalObj, 'gifts', function(type) {
                    return _roundDecimal(totalObj['gifts' + type] || 0) + (line['totalGifts' + type] || 0);
                });

                _setPropTypes(totalObj, 'clubsGifts', function(type) {
                    return _roundDecimal(totalObj['clubsGifts' + type] || 0) + (line['totalClubsGifts' + type] || 0);
                });

                _setPropTypes(totalObj, 'simulateClubsGifts', function(type) {
                    return _roundDecimal(totalObj['simulateClubsGifts' + type] || 0) + (line['totalSimulateClubsGifts' + type] || 0);
                });

                if (!line.isProductOutOfStock) {
                    _setPropTypes(totalObj, 'finalPrice', function(type) {
                        return _roundDecimal((totalObj['finalPrice' + type] || 0) + (line['finalPrice' + type] || 0));
                    });
                }

                if (line.gifts && line.gifts.length && line.gifts[0].promotion.loyalty && line.gifts[0].promotion.loyalty.length) {
                    var id = line.gifts[0].promotion.loyalty[0];
                    totalObj.loyaltyClubs = totalObj.loyaltyClubs || {};
                    totalObj.loyaltyClubs[id] =  totalObj.loyaltyClubs[id] || {};
                    _setPropTypes( totalObj.loyaltyClubs[id], 'clubGifts', function(type) {
                        return (totalObj.loyaltyClubs[id]['clubGifts' + type] || 0) + (line['totalClubsGifts' + type] || 0);
                    });
                }
                var finalPriceWithTaxForEbtEligibility = (line.type === SP_SERVICES.CART_LINE_TYPES.PRODUCT || line.type === SP_SERVICES.CART_LINE_TYPES.COUPON || line.type === SP_SERVICES.CART_LINE_TYPES.SERVICE_FEE || line.type === SP_SERVICES.CART_LINE_TYPES.DELIVERY) ?
                    _lineFinalPriceWithTaxWithoutDepositAmount(
                        line.finalPriceWithoutTax,
                        line.finalPriceWithTax,
                        (line.product && line.product.branch && line.product.branch.linkedProductPrice || 0),
                        (line.product && line.product.branch && line.product.branch.taxAmount || 0),
                        line.quantity,
                        locals,
                        false,
                        line.isCase ? line.product && line.product.branch && line.product.branch.case && line.product.branch.case.items : 1) : 0;
                finalPriceWithTaxForEbtEligibility = _roundDecimal(finalPriceWithTaxForEbtEligibility + _getAdditionalTaxFromTaxableCoupons(line));
                var finalPriceWithTax = line.type === SP_SERVICES.CART_LINE_TYPES.PRODUCT || line.type === SP_SERVICES.CART_LINE_TYPES.COUPON ? finalPriceWithTaxForEbtEligibility : 0;

                line.finalPriceWithoutTax = _roundDecimal(line.finalPriceWithoutTax);
                line.totalPriceMarkup = _roundDecimal(line.totalPriceMarkup);
                line.finalPriceWithTaxForEbtEligibility = finalPriceWithTaxForEbtEligibility;
                line.finalPriceWithTax = finalPriceWithTax;
                totalObj.tax += _roundDecimal(finalPriceWithTax - line.finalPriceWithoutTax - line.totalPriceMarkup || 0);
                totalObj.markup += line.totalPriceMarkup || 0;

                _calculateMultiPassTotals(multiPassGiftSum, line);

                // line with only global gifts considered as "without promotion"
                var isWithoutPromotion = !line.gifts || !line.gifts.find(function(gift) {
                    return !gift.isGlobal;
                });
                if (isWithoutPromotion) {
                    totalObj.priceWithoutPromotionProductsForView += line.totalPriceForView || 0;
                }
            });

            _applyMultiPassDiscount(multiPassGiftSum);
            _setCrossProductTagTypes(productsTagTypesCount, prevCrossProductTagTypes);

            self.total.calculating = false;

            var finalPriceWithTax = 0;
            var deliveryLine = null;
            angular.forEach(self.lines, function (line) {
                if (line.type === SP_SERVICES.CART_LINE_TYPES.DELIVERY) {
                    deliveryLine = line;
                }
                if (line.type !== SP_SERVICES.CART_LINE_TYPES.PRODUCT &&
                    line.type !== SP_SERVICES.CART_LINE_TYPES.COUPON) return;

                if(!isNaN(line.finalPriceWithoutTax) && !isNaN(line.finalPriceWithoutTax)) {
                    finalPriceWithTax += _lineFinalPriceWithTaxWithoutDepositAmount(
                        line.finalPriceWithoutTax,
                        line.finalPriceWithTax,
                        (line.product && line.product.branch && line.product.branch.linkedProductPrice || 0),
                        (line.product && line.product.branch && line.product.branch.taxAmount || 0),
                        line.quantity,
                        locals,
                        true,
                        line.isCase ? line.product && line.product.branch && line.product.branch.case && line.product.branch.case.items : 1);
                }
            });
            self.total.finalPriceWithTax = finalPriceWithTax;

            if ($rootScope.config.retailer.settings.includeDeliveryFeeInCart === 'true') {
                self.total.finalPriceForViewWithDeliveryFee = getDeliveryPrice(self.total, totalByType, deliveryLine);
            }
            $rootScope.$emit('cart.totalCalculated');
        }

        function _getAdditionalTaxFromTaxableCoupons(line){
            var additionalTax = 0;
            if(line.gifts && (line.gifts.length > 0)){
                angular.forEach(line.gifts, function(gift){
                    if(gift.isTaxable === false){
                        var taxAmount = ((line.product.branch && line.product.branch.taxAmount || 0) + 1);
                        var taxPercentage = (taxAmount === 0) ? 0 : (taxAmount - 1);
                        additionalTax += gift.discount * taxPercentage;
                    }
                })
            }
            return additionalTax;
        }

        function getDeliveryPrice(total, totalByType, deliveryLine) {
            if(isNaN(total.finalPriceWithTax)) {
                return;
            }
            var result = total.finalPriceWithTax;
            if (totalByType[SP_SERVICES.CART_LINE_TYPES.DELIVERY] && totalByType[SP_SERVICES.CART_LINE_TYPES.DELIVERY].finalPriceForView) {
                result += totalByType[SP_SERVICES.CART_LINE_TYPES.DELIVERY].finalPriceForView;
            } else if (deliveryLine && deliveryLine.product && deliveryLine.product.branch &&
                deliveryLine.product.branch.regularPrice && deliveryLine.product.branch.regularPrice > 0) {
                result += deliveryLine.product.branch.regularPrice;
            }

            return result;
        }


        //Init the totals
        _setCartTotal();

        /**
         * Add the given line to the given product tag type count map
         * @private
         *
         * @param {Object} line
         * @param {{all: number, byType: Object}} productsTagTypesCount
         */
        function _addLineProductTagTypes(line, productsTagTypesCount) {
            if (line.removed) {
                return;
            }

            productsTagTypesCount.all++;

            if (!line.product || !line.product.productTagsData) {
                return;
            }

            line.productTagTypes = {};
            angular.forEach(line.product.productTagsData, function(productTag) {
                if (SpProductTags.isProductTagRelationActive(productTag)) {
                    line.productTagTypes[productTag.typeId || PRODUCT_TAG_TYPES.SIMPLE] = true;
                }
            });

            angular.forEach(line.productTagTypes, function(bool, typeId) {
                productsTagTypesCount.byType[typeId] = (productsTagTypesCount.byType[typeId] || 0) + 1;
            });
        }

        /**
         * Set the cross cart product tag types by the given count map
         * @private
         *
         * @param {{all: number, byType: Object}} productsTagTypesCount
         * @param {Object} prevCrossProductTagTypes
         */
        function _setCrossProductTagTypes(productsTagTypesCount, prevCrossProductTagTypes) {
            var prevValue = Object.keys(prevCrossProductTagTypes || {}),
                newLength = 0;

            // when the entire cart products contains the same product tag type
            self.total.crossProductTagTypes = {};

            angular.forEach(productsTagTypesCount.byType, function(count, typeId) {
                if (count === productsTagTypesCount.all) {
                    newLength++;
                    self.total.crossProductTagTypes[typeId] = Number(typeId);
                }
            });

            if (productsTagTypesCount.all) {
                // represents a non empty cart
                self.total.crossProductTagTypes.none = true;
                newLength++;
            }

            var isChanged = newLength !== prevValue.length;
            if (!isChanged) {
                angular.forEach(prevValue, function(key) {
                    if (!self.total.crossProductTagTypes[key]) {
                        isChanged = true;
                    }
                });
            }

            if (isChanged) {
                $rootScope.$emit('cart.crossProductTagTypes.change', {
                    value: self.total.crossProductTagTypes,
                    oldValue: prevCrossProductTagTypes
                });
            }
        }

        /**
         * those products have MultiPass discount
         */
        function _calculateMultiPassTotals(multiPassGiftSum, line) {
            //== if this line is not a Product type
            if( line.type === SP_SERVICES.CART_LINE_TYPES.COUPON ) {
                return;
            }

            var departmentId = line.product && line.product.department && line.product.department.id || null;

            // //== check if this product's department is forbidden for discount
            if( departmentId && Array.isArray(self.multiPassNoDiscountDepartments) && self.multiPassNoDiscountDepartments.indexOf(departmentId) !== -1 ) {
                return;
            }

            //== if this product is not forbidden for discount - calculate totals for further discount
            multiPassGiftSum.finalPriceForView += _roundDecimal(line.finalPriceForView || 0);
            multiPassGiftSum.totalPriceForView += _roundDecimal(line.totalPriceForView || 0);
            multiPassGiftSum.finalPriceWithTax += _roundDecimal(line.finalPriceWithTax || 0);
        }

        /**
         * Apply estimated MultiPass discount
         */
        function _applyMultiPassDiscount(multiPassGiftSum) {
            if(self.multiPassDiscount && self.multiPassObligo && multiPassGiftSum) {

                //== calculate maximum discount amount this user can get
                self.total.multiPassGift = multiPassGiftSum.finalPriceForView * self.multiPassDiscount / 100;
                var actualDiscountPercent = self.multiPassDiscount;

                //== if remaining obligo is smaller than discount user should get
                if( self.multiPassObligo - self.total.multiPassGift < 0 ) {

                    //== then the maximum discount user can get is exactly the remaining obligo
                    self.total.multiPassGift = self.multiPassObligo;

                    //== and the actual discount for further total calculation changes
                    actualDiscountPercent = (self.multiPassObligo / self.total.finalPriceForView) * 100;
                }

                //== re-calculate all total PriceForView using totals from products WITH discount
                self.total.finalPriceForView = self.total.finalPriceForView - self.total.multiPassGift;
                self.total.totalPriceForView = self.total.totalPriceForView - multiPassGiftSum.totalPriceForView * (actualDiscountPercent / 100);
                self.total.finalPriceWithTax = self.total.finalPriceWithTax - multiPassGiftSum.finalPriceWithTax * (actualDiscountPercent / 100);
            }
        }

        /**
         * Add line key to deleted lines
         * @param {Line} line
         * @private
         */
        function _addToDeletedProduct(line) {
            deletedLines[getLineKey(line)] = {
                isCase: line.isCase,
                productId: line.product.id,
                isPseudo: line.isPseudo,
                type: line.type
            };
        }

        /**
         * Remove line key from deleted lines
         * @param {Line} line
         */
        function _removeFromDeletedProduct(line) {
            var lineKey = getLineKey(line);
            if (lineKey in deletedLines) {
                delete deletedLines[lineKey];
            }
        }

        /**
         * Clear the cart lines and possibly the server cart id
         * @public
         *
         * @param {boolean} [withCartId]
         *
         * @returns {Promise}
         */
        function clearCart(withCartId) {
            _removeLines(self.lines, true);
            $rootScope.$emit('cartCleared',self.serverCartId);
            if (withCartId) {
                self.serverCartId = undefined;
                _setEditOrderId(null);
                deletedLines = {};
            }
            return saveCart();
        }

        /**
         * Gets whether the line's product is weighable and does not have weight per unit
         * @param {object} line
         * @returns {boolean}
         * @private
         */
        function _isWeightQuantity(line) {
            return line.product.isWeighable && !line.product.weight;
        }

        /**
         * Gets the quantity of two numbers - to fix js bugs (such as: 0.1 +0.2)
         * @param {number} numberToFix
         * @returns {number}
         * @private
         */
        function _fixNumber(numberToFix) {
            return parseFloat(numberToFix.toFixed(5))
        }

        /**
         * Add a new line
         * @param {Line} line
         * @param {Locals} locals
         * @returns {object}
         * @private
         */
        function _addLine(line, locals, soldBy) {
            var lineKey = getLineKey(line);
            if (!self.lines[lineKey]) {
                line.quantity = line.quantity || quantityInterval(line, soldBy);

                _setPropTypes(line, 'totalGifts', function() {return 0});
                _setPropTypes(line, 'totalClubsGifts', function() {return 0});

                if (line.totalSimulateClubsGifts) {
                    _setPropTypes(line, 'totalSimulateClubsGifts', function() {return 0});
                }

                _setLinePrices(line, locals);

                line.sort = sortNumber++;
                line.type = line.type || SP_SERVICES.CART_LINE_TYPES.PRODUCT;
                _removeFromDeletedProduct(line);
                self.lines[lineKey] = line;
            } else if (!self.lines[lineKey].quantity) {
                self.lines[lineKey].quantity = line.quantity || quantityInterval(line, soldBy);
            } else {
                self.lines[lineKey].quantity += line.quantity || quantityInterval(line, soldBy);

                if(self.lines[lineKey].productPropertyValue) {
                    self.lines[lineKey].productPropertyValue = line.productPropertyValue;
                }
            }

            if (self.lines[lineKey].type == SP_SERVICES.CART_LINE_TYPES.COUPON && self.lines[lineKey].quantity > 1) {
                self.lines[lineKey].quantity = 1;
            }

            _setQuantityLimit(self.lines[lineKey]);

            self.lines[lineKey].soldBy = soldBy;
            self.lines[lineKey].removed = false;
            self.lines[lineKey].id = lineKey;
            self.lines[lineKey].oldQuantity = self.lines[lineKey].quantity;
            return self.lines[lineKey];
        }

        /**
         * Add a new line, save and call event
         * @param {Line} line
         * @public
         */
        function addLine(line, soldBy) {
            // This code is for retailers that assign to Hub for log on user browser cart process started
            EmbedHistoryRetailer.embedHubIframe();
            return _getLocals().then(function (locals) {
                var lineAffected = _addLine(line, locals, soldBy);
                dirtyLines[getLineKey(lineAffected)] = true;
                saveCart({
                    withoutServer: false,
                    affectedLines: [lineAffected],
                    locals: locals,
                    source: line.source,
                    cartLinesAddAlreadyFired: true
                });
                $rootScope.$emit('cart.lines.add', {lines: [lineAffected]});
                $rootScope.$emit('cart.lines.add.click', {lines: [lineAffected]});
                prepareLinesForAnalytics([lineAffected], line.source);
                return lineAffected;
            });
        }

        /**
         * Add a list of lines, save and call event
         * @param {Array.<Line>} lines
         * @param {boolean} skipActiveProductsValidation
         * @param {string} source
         * @public
         */
        function addLines(lines, source,  skipActiveProductsValidation, originatedId) {
            return _getLocals().then(function (locals) {
                if (!lines || !angular.isArray(lines)) {
                    return [];
                }

                var linesAffected = [];
                angular.forEach(lines, function (line) {
                    var lineAffected = _addLine(line, locals, line.soldBy);
                    linesAffected.push(lineAffected);
                    dirtyLines[getLineKey(lineAffected)] = true;
                });
                saveCart({
                    withoutServer: false,
                    affectedLines: linesAffected,
                    skipActiveProductsValidation: skipActiveProductsValidation,
                    locals: locals,
                    originatedId: originatedId,
                    source: source,
                    cartLinesAddAlreadyFired: true
                });
                $rootScope.$emit('cart.lines.add', {lines: linesAffected});
                $rootScope.$emit('cart.lines.add.click', {lines: linesAffected});
                if (source)
                {
                    prepareLinesForAnalytics(linesAffected, source);
                }

                return linesAffected;
            });
        }

        function prepareLinesForAnalytics(lines, source){
            angular.forEach(lines, function(line){
                if(line.product && line.product.branch){
                    var quantity = (line.product.isWeighable) ? (line.quantity * 1000): line.quantity;
                    var price = (line.product.isWeighable) ? (line.product.branch.regularPrice / 1000): line.product.branch.regularPrice;
                    $rootScope.$emit('cart.lines.added', {productId:line.product.id, quantity: Math.round(quantity),
                        unitPrice: price, sourceType: 'internal', sourceValue: source,cartId: self.serverCartId})
                }
            })
        }

        /**
         * Remove a bunch of lines and call event
         * @param {Array.<Line>|Line} lines
         * @param {boolean} [isDelete] - whether to skip the 'removed' state
         * @private
         */
        function _removeLines(lines, isDelete) {
            var removedLines = [],
                voidLines = [];
            angular.forEach(lines, function (line) {
                line.quantity = 0;
                line.comments = '';
                if (line.removed && isDelete !== false || isDelete) {
                    line.removed = false;
                    _addToDeletedProduct(line);
                    removedLines.push(line);
                    delete self.lines[getLineKey(line)];
                } else {
                    voidLines.push(line);
                    dirtyLines[getLineKey(line)] = true;
                    line.removed = true;
                }
            });

            if (removedLines.length) {
                $rootScope.$emit('cart.lines.remove', {lines: removedLines});
            }

            if (voidLines.length) {
                $rootScope.$emit('cart.lines.quantityChanged', {lines: voidLines});
            }

            return $q.resolve(removedLines);
        }

        /**
         * Remove a line
         * @param {Line} line
         * @param {boolean} [isDelete] - whether to skip the 'removed' state
         * @private
         */
        function _removeLine(line, isDelete) {
            var delta = line.oldQuantity - line.quantity;
            if (_isWeightQuantity(line)) {
                delta = delta * 1000;
            }
            if(delta > 0){
                $rootScope.$emit('cart.lines.removed', {
                    productId: line.product.id,
                    quantity: Math.round(delta),
                    cartId: self.serverCartId});
                line.oldQuantity = line.quantity;
            }

            return _removeLines([line], isDelete).then(function (removedLines) {
                return removedLines && removedLines[0];
            });
        }

        /**
         * Remove a line and save
         * @param {Line} line
         * @param {boolean} [isDelete] - whether to skip the 'removed' state
         * @param {boolean} [isSaveCartPromise] - if need to return saveCart promise
         */
        function removeLine(line, isDelete, isSaveCartPromise) {
            var promise = _removeLine(line, isDelete);
            var saveCartPromise = saveCart();
            return isSaveCartPromise ? saveCartPromise: promise;
        }

         /**
         * Remove lines and save
         * @param {Line} line
         * @param {boolean} [isDelete] - whether to skip the 'removed' state
         * @param {boolean} [isSaveCartPromise] - if need to return saveCart promise
         */
        function removeLines(lines, isDelete, isSaveCartPromise) {
            var promise = _removeLines(lines, isDelete);
            var saveCartPromise = saveCart();
            return isSaveCartPromise ? saveCartPromise: promise;
        }

        /**
         * Gets the quantity interval of a product
         * @param {Line} line
         * @returns {number}
         * @public
         */
        function quantityInterval(line, soldBy) {
            return !line.isCase && (_productSoldBy(line, soldBy) === $rootScope.PRODUCT_DISPLAY.WEIGHT.name || _isWeightQuantity(line)) ? (line.product.unitResolution || 0.5) : 1;
        }

        /**
         * Decrease the quantity of a line by quantity interval
         * @param {Line} line
         * @public
         */
        function minusQuantity(line) {
            var lineKey = getLineKey(line);
            if (!self.lines[lineKey]) return;

            self.lines[lineKey].quantity = _fixNumber(self.lines[lineKey].quantity - quantityInterval(line));
            quantityChanged(line);
        }

        /**
         * Increase the quantity of a line by quantity interval
         * @param {Line} line
         * @public
         */
        function plusQuantity(line) {
            if (!line.product || !('id' in line.product) || angular.isUndefined(line.product.id)) return;

            if (line.product.quantityLimit && line.quantity >= line.product.quantityLimit) {
                $rootScope.$emit('cart.lines.quantityLimit', {line: line});
                return;
            }

            var lineKey = getLineKey(line);
            if (!self.lines[lineKey]) {
                self.addLine(line);
            } else {
                self.lines[lineKey].quantity = _fixNumber(self.lines[lineKey].quantity + quantityInterval(line));
                self.quantityChanged(line);
            }
        }

        /**
         * Validates the quantity limits and corrects it if needed
         * @private
         *
         * @param {Line} line
         */
        function _setQuantityLimit(line) {
            if (line.product.quantityLimit && line.quantity > line.product.quantityLimit) {
                line.quantity = line.product.quantityLimit;
                $rootScope.$emit('cart.lines.quantityLimit', {line: line});
            }
        }

        /**
         * Remove line if no quantity or add line if removed and save
         * @param {Line} line
         * @params {Boolean} notForAnalyticProvider
         * @returns {*}
         * @public
         */
        function quantityChanged(line, notForAnalyticProvider, soldBy) {
            line.quantity < 0 && (line.quantity = 0);
            line.quantity > MAX_LINE_QUANTITY && (line.quantity = MAX_LINE_QUANTITY);

            _setQuantityLimit(line);

            //to remove . from quantity that should be int
            _productSoldBy(line, soldBy) === $rootScope.PRODUCT_DISPLAY.UNIT.name && !_isWeightQuantity(line) && (line.quantity = Number(line.quantity.toString().replace(/\./g, '')));

            if (line.quantity == 0 && !line.removed) {
                return removeLine(line);
            }
            if (line.quantity > 0 && line.removed) {
                _getLocals().then(function (locals) {
                    _addLine(line, locals);
                });
            }

            if(!notForAnalyticProvider){
                _sendQuantityChangeToAnalyticsProvider(line);
                line.oldQuantity = line.quantity;
            }

            dirtyLines[getLineKey(line)] = true;
            saveCart({
                withoutServer: false,
                affectedLines: [line]
            });
            $rootScope.$emit('cart.lines.quantityChanged', {lines: [line]});
        }

        function _sendQuantityChangeToAnalyticsProvider(line){
            if(line.product && line.product.branch && line.oldQuantity) {
                var delta = line.quantity - line.oldQuantity;
                var price = line.product.branch.regularPrice
                if (_isWeightQuantity(line)) {
                    delta = delta * 1000;
                    price = price / 1000;
                }
                if (delta < 0) {
                    $rootScope.$emit('cart.lines.removed', {
                        productId: line.product.id,
                        quantity: Math.round((-1) * delta),
                        cartId: self.serverCartId
                    })
                } else if (delta > 0) {
                    $rootScope.$emit('cart.lines.added', {
                        productId: line.product.id,
                        quantity: Math.round(delta),
                        unitPrice: price,
                        cartId: self.serverCartId,
                        sourceType: 'internal',
                        sourceValue: 'update'
                    })
                }
            }
        }

        /**
         * Add line to dirty lines and fire event
         * @param {Line} line
         * @public
         */
        function commentsChanged(line) {
            var lineKey = getLineKey(line);
            if (!self.lines[lineKey]) return;
            dirtyLines[lineKey] = true;
            saveCart({
                withoutServer: false,
                affectedLines: [line]
            });
            $rootScope.$emit('cart.lines.commentsChanged', {lines: [line]});
        }

        /**
         * Add line to dirty lines
         * @param {Line} line
         * @public
         */
        function productPropertyValueChanged(line) {
            var lineKey = getLineKey(line);
            if (!self.lines[lineKey]) return;
            dirtyLines[lineKey] = true;
            saveCart({
                withoutServer: false,
                affectedLines: [line]
            });
        }

        /**
         * Gets the product object with its lines
         * @param {object} product
         * @returns {object}
         * @public
         */
        function getProduct(product) {
            var caseLine,
                singleLine;
            
                var soldBy =  product && product.soldBy || null;
                if (!soldBy) {
                    if (product && product.productDisplayModeId && product.isWeighable && product.weight) {
                        var cartLine;
                        angular.forEach(self.lines, function (line) { 
                            if (line.product.id === product.id) {
                                cartLine = line;
                            }
                        });
                        if (!cartLine) {
                            switch (product.productDisplayModeId) {
                                case $rootScope.PRODUCT_DISPLAY.UNIT.id:
                                    soldBy = $rootScope.PRODUCT_DISPLAY.UNIT.name;
                                    break;
                                case $rootScope.PRODUCT_DISPLAY.WEIGHT.id:
                                    soldBy = $rootScope.PRODUCT_DISPLAY.WEIGHT.name;
                                    break;
                                default:
                                    soldBy = product.isWeighable ? (product.weight ? $rootScope.PRODUCT_DISPLAY.UNIT.name : $rootScope.PRODUCT_DISPLAY.WEIGHT.name) : null;
                            } 
                        } else {
                            soldBy = !!cartLine.soldBy && cartLine.soldBy;
                        }
                    }
                }
                
                if (soldBy) {
                    product.soldBy = soldBy;
                }
    

            if (product && Object.keys(product).length) {
                caseLine = self.lines[product.id.toString() + '1'];
                singleLine = self.lines[product.id.toString() + '0'];
            }

            if (caseLine) {
                product.caseLine = caseLine;
            } else if ('caseLine' in product) {
                delete product.caseLine;
            }

            if (singleLine) {
                singleLine['soldBy'] = soldBy;
                product.singleLine = singleLine;
            } else if ('singleLine' in product) {
                delete product.singleLine;
            }

            return product;
        }

        /**
         * Gets the products object with its lines
         * @param {Array} [products]
         * @returns {Array}
         * @public
         */
        function getProducts(products) {
            var arr = [];

            var lines = products || getLines();
            angular.forEach(lines, function (line) {
                arr.push(self.getProduct(line.product || line));
            });

            return arr;
        }

        /**
         * Gets the active lines
         * @returns {Array}
         * @public
         */
        function getLines() {
            var arr = [];

            angular.forEach(self.lines, function (line) {
                !line.removed && arr.push(line);
            });

            return arr;
        }

        /**
         * Gets whether the line's product is weighable and has weight per unit
         * @returns {boolean}
         * @private
         */
        function isUnitsWeighable(line) {
            return line.product.isWeighable && !!line.product.weight;
        }

        /**
        * Gets the products sold by mode based on the condition of whether the line's product is weighable and has a weight per unit
        * @returns {string || null}
        * @private
        */
        function _productSoldBy(line, soldBy) { 
            return line.product.isWeighable && !!line.product.weight && soldBy;
        }
        
        /**
         * Calc the lines's prices
         * @param {Array.<Line>|Line} lines
         * @private
         */
        function _calcLocalPrices(lines) {
            if (!lines || !lines.length) {
                return;
            }

            _getLocals().then(function (locals) {
                angular.forEach(lines, function (line) {
                    line.gifts = [];
                    line.totalGifts = line.totalClubsGifts = 0;
                    line.totalGiftsPotential = line.totalClubsGiftsPotential = 0;
                    _setLinePrices(line, locals);
                });
            });
        }

        var sendToServerTimeout;

        /**
         * Save cart
         * @public
         *
         * @param {Object} [options]
         * @param {boolean} [options.withoutServer] - do not call server
         * @param {Array} [options.affectedLines]
         * @param {Locals} [options.locals]
         * @param {number} [options.paymentMethodId]
         * @param {boolean} [options.skipActiveProductsValidation]
         * @param {boolean} [options.forceCreation]
         * @param {string} [options.source]
         * @param {boolean} [options.cartLinesAddAlreadyFired]
         *
         * @returns {Promise}
         */
        function saveCart(options) {
            options = options || {};

            if (!options.withoutServer) {
                _saveCartDefer = _saveCartDefer || $q.defer();

                sendToServerTimeout && $timeout.cancel(sendToServerTimeout);
                _calcLocalPrices(options.affectedLines || []);
                sendToServerTimeout = $timeout(function() {
                    var defer = _saveCartDefer;
                    _saveCartDefer = null;

                    _sendToServer({
                        paymentMethodId: options.paymentMethodId,
                        skipActiveProductsValidation: options.skipActiveProductsValidation,
                        forceCreation: options.forceCreation,
                        originatedId: options.originatedId,
                        source: options.source,
                        cartLinesAddAlreadyFired: options.cartLinesAddAlreadyFired
                    }).then(function(result) {
                        defer.resolve(result);
                    }).catch(function(err) {
                        defer.reject(err);
                    });
                }, 500);
            }

            _setCartTotal(options.locals);

            if (options.withoutServer) {
                return $q.resolve();
            } else {
                return _saveCartDefer.promise;
            }
        }

        /**
         * Set time travel
         * @public
         *
         * @param {object} [timeTravel]
         */
        function setTimeTravel(timeTravel) {
            if (!angular.isObject(timeTravel)) {
                delete self.timeTravel;
                return;
            }

            self.timeTravel = self.timeTravel || {};
            angular.extend(self.timeTravel, timeTravel);
        }

        /**
         * Replace the current cart with another cart
         * @public
         *
         * @param {int} withCartId
         * @param {boolean} [merge]
         *
         * @returns {Promise}
         */
        function replaceCart(withCartId, merge) {
            if (self.serverCartId === withCartId) {
                return $q.resolve();
            }

            if (preventCalls) {
                return $timeout(function () {
                    return replaceCart(withCartId, merge);
                }, 100);
            }

            var currentCartId = self.serverCartId;
            clearCart(true);
            deletedLines = {};
            self.serverCartId = withCartId;

            return _sendToServer({
                cartToDelete: currentCartId,
                mergeDeletedCart: merge
            });
        }

        /**
         * Make sure the cart exists, and if not create it
         * @public
         *
         * @return {Promise}
         */
        function createCart() {
            if (self.serverCartId) {
                return $q.resolve();
            }

            return _sendToServer({ forceCreation: true });
        }

        /**
         * Gets the line's weighableProductUnits
         * @param {object} line
         * @returns {number}
         * @private
         */
        function _getWeighableProductUnits(line) {
            line.weighableProductUnits = line.weighableProductUnits;
            if (line.weighableProductUnits && line.quantity) {
                return Math.round(line.quantity / line.product.weight);
            }
            return line.quantity;
        }

        /**
         * Sets the server's quantity and comments to local line
         * @param {string} lineKey
         * @param {object} serverLine
         * @private
         */
        function _setCommentsAndQuantityFromServer(lineKey, serverLine) {
            self.lines[lineKey].comments = serverLine.comments;
            self.lines[lineKey].metaData = serverLine.metaData;
            self.lines[lineKey].quantity = !serverLine.isCase && isUnitsWeighable(self.lines[lineKey]) ? _getWeighableProductUnits(serverLine) : serverLine.quantity;
        }

        /**
         * Add a server cart line to local cart
         * @param {object} line
         * @param {Locals} locals
         * @private
         */
        function _addServerProduct(line, locals) {
            var newLine = {isCase: line.isCase, product: {}};
            if (line.isPseudo) {
                newLine.isPseudo = true;
                newLine.product.id = line.text;
                newLine.product.name = line.text;

                if (line.categoryId) {
                    newLine.product.categories = [{
                        id: line.categoryId,
                        names: line.categoryNames
                    }];
                }
                newLine.product.names = {};
                newLine.product.names[locals.languageId || 0] = {
                    short: line.text,
                    long: line.text
                };
            } else {
                angular.extend(newLine.product, line.product);
            }
            _addLine(newLine, locals);

            _setCommentsAndQuantityFromServer(getLineKey(newLine), line);
        }

        /**
         * Gets a local cart line as a server cart line
         * @param {Line} line
         * @returns {object}
         * @private
         */
        function _getServerRequestLine(line) {
            var serverLine = {quantity: line.quantity, soldBy: line.soldBy, comments: line.comments, isCase: line.isCase, metaData: line.metaData, adConversionUrl: line.adConversionUrl, adminComments: formatBase64toString(line.adminComments)};
            if (line.isPseudo) {
                serverLine.isPseudo = true;
                serverLine.text = line.product.id;
                serverLine.categoryId = line.product.categories && line.product.categories[0] ? line.product.categories[0].id : null;
            } else if(line.type === SP_SERVICES.CART_LINE_TYPES.DELIVERY) {
                serverLine.retailerProductId = line.product.id;
                serverLine.type = line.type;
                serverLine.areaId = line.areaId;
                serverLine.deliveryTypeId = line.deliveryTypeId;
            } else {
                serverLine.retailerProductId = line.product.id;
                serverLine.type = line.type || SP_SERVICES.CART_LINE_TYPES.PRODUCT;
            }

            if (line.productPropertyValueId) {
                serverLine.productPropertyValueId = line.productPropertyValueId;
            } else if (line.productPropertyValue && line.productPropertyValue.id) {
                serverLine.productPropertyValueId = line.productPropertyValue.id;
            }

            if (line.type === SP_SERVICES.CART_LINE_TYPES.COUPON) {
                serverLine.isForAllUsers = (line.product && line.product.isForAllUsers) || false;
            }
            return serverLine;
        }

        /**
         * Gets the local cart lines as a server cart lines
         * @returns {{deletedLines: object, dirtyLines: object, lines: Array}}
         * @private
         */
        function _getServerRequestLines() {
            var serverLines = [],
                requestDirtyLines = {};
            angular.forEach(dirtyLines, function (val, lineKey) {
                delete dirtyLines[lineKey];
                if (!self.lines[lineKey]) return;
                requestDirtyLines[lineKey] = true;
                serverLines.push(_getServerRequestLine(self.lines[lineKey]));
            });

            var requestDeletedLines = {};
            angular.forEach(deletedLines, function (deletedLine, lineKey) {
                delete deletedLines[lineKey];
                requestDeletedLines[lineKey] = deletedLine;
                var toAdd = {delete: true};
                if (deletedLine.isPseudo) {
                    toAdd.text = deletedLine.productId;
                    toAdd.isPseudo = true;
                } else {
                    toAdd.retailerProductId = deletedLine.productId;
                    toAdd.isCase = !!deletedLine.isCase;
                    toAdd.type = deletedLine.type;
                }
                serverLines.push(toAdd);
            });

            return {deletedLines: requestDeletedLines, dirtyLines: requestDirtyLines, lines: serverLines};
        }

        /**
         * Emit event for other cart
         * @param {object} otherCart
         * @param {number} [otherCart.id]
         * @param {number} [otherCart.linesCount]
         */
        function _emitOtherCart(otherCart) {
            if (!otherCart) {
                return;
            }

            $rootScope.$emit('cart.otherCart', {
                otherCart: otherCart,
                next: function(deleteCartId, merge) {
                    return _sendToServer({
                        cartToDelete: deleteCartId,
                        mergeDeletedCart: merge
                    });
                }
            });
        }

        /**
         * Emit event for inactive lines
         * @param {Array|object} inactiveLines
         * @private
         */
        function _emitInactiveLines(inactiveLines) {
            if (!inactiveLines) {
                return;
            }

            var properLines = [],
                toRemoveProducts = [];
            angular.forEach(inactiveLines, function (inactiveLine) {
                var localKey = getLineKey({isCase: inactiveLine.isCase, product: {id: inactiveLine.retailerProductId}});
                localKey in self.lines && toRemoveProducts.push(self.lines[localKey]);

                if (!inactiveLine.isTipLine) {
                    properLines.push({
                        productId: inactiveLine.retailerProductId ,
                        name: inactiveLine.text ,
                        product: inactiveLine.product ,
                        isCase: inactiveLine.isCase ,
                        isPseudo: inactiveLine.isPseudo
                    });
                }
            });

            _removeLines(toRemoveProducts, true);

            if (!!properLines.length) {
                $rootScope.$emit('cart.lines.inactive', {lines: properLines});
            }
        }

        function _setEditOrderId(value) {
            if (!self.editOrderId === !value || self.editOrderId === value) {
                return;
            }

            self.editOrderId = value || null;
            $rootScope.$emit('cart.editOrderId.change', self.editOrderId);
        }

        /**
         * Sets prices and add new lines from server cart
         * @param {object} serverCart
         * @param {number} serverCart.id
         * @param {number} serverCart.editOrderId
         * @param {Array} serverCart.lines
         * @param {object} serverCart.specialReminders
         * @param {number} serverCart.multiPassDiscount
         * @param {number} serverCart.multiPassObligo
         * @param {number} serverCart.multiPassNoDiscountDepartments
         * @param {object} requestDirtyLines
         * @param {Locals} locals
         * @param {object} serverCart.cartMetaData
         * @param {boolean} serverCart.tipsWasRemoved
         * @param {boolean} [cartLinesAddAlreadyFired]
         * @private
         */
        function _setDetailsFromServerCart(serverCart, requestDirtyLines, locals, cartLinesAddAlreadyFired) {
            // when the api call did actually happen, and a server cart was not actually returned
            if (!serverCart) {
                return;
            }

            self.serverCartId = serverCart.id;
            self.specialReminders = serverCart.specialReminders || {};
            self.multiPassDiscount = serverCart.multiPassDiscount || 0;
            self.multiPassObligo = serverCart.multiPassObligo || 0;
            self.multiPassNoDiscountDepartments = serverCart.multiPassNoDiscountDepartments || 0;
            self.cartMetaData = serverCart.cartMetaData;
            self.tipsWasRemoved = serverCart.tipsWasRemoved;
            _setChangedProducts(requestDirtyLines);

            // reset the traffic params, it was successfully sent
            _trafficParams = undefined;

            _setEditOrderId(serverCart.editOrderId);

            var addedLines = [],
                toRemoveKeys = {};
            angular.forEach(self.lines, function (line, lineKey) {                
                if (requestDirtyLines[lineKey] || dirtyLines[lineKey]) return;
                toRemoveKeys[lineKey] = true;
            });

            angular.forEach(serverCart.lines, function (line) {
                var lineKey = getLineKey({
                    isCase: line.isCase,
                    product: {id: line.isPseudo ? line.text : line.retailerProductId}
                });

                if (line.isForAllUsers && line.massCouponId) {
                    var dirtyLineKey = getLineKey({ isCase: line.isCase, product: { id: line.massCouponId }});
                    if (requestDirtyLines[dirtyLineKey]) {
                        var newDirtyLineKey = getLineKey({ isCase: line.isCase, product: { id: line.retailerProductId }});
                        requestDirtyLines[newDirtyLineKey] = true;
                        if(self.lines[dirtyLineKey]){
                            var oldLine = self.lines[dirtyLineKey];
                            oldLine.id = line.retailerProductId;
                            self.lines[newDirtyLineKey] = self.lines[dirtyLineKey];
                            delete self.lines[dirtyLineKey];
                        }
                   }
                }

                if (!self.lines[lineKey] && !deletedLines[lineKey]) {
                    _addServerProduct(line, locals);
                    addedLines.push(self.lines[lineKey]);
                } else if (self.lines[lineKey]) {
                    angular.extend(self.lines[lineKey].product, line.product);

                    if (toRemoveKeys[lineKey]) {
                        _setCommentsAndQuantityFromServer(lineKey, line);
                        delete toRemoveKeys[lineKey];
                    }
                }

                if (self.lines[lineKey]) {
                    if (!self.lines[lineKey].removed && self.lines[lineKey].quantity <= 0) {
                        _removeLine(self.lines[lineKey]);
                    }

                    self.lines[lineKey].type = line.type;
                    self.lines[lineKey].sellDates = line.sellDates;
                    self.lines[lineKey].isHideFromCart = line.isHideFromCart || false;
                    self.lines[lineKey].isProductOutOfStock = line.isProductOutOfStock;
                    self.lines[lineKey].isCouponActive = line.isCouponActive;
                    self.lines[lineKey].productPropertyValue = line.productPropertyValue || null;
                    if (line.product && line.product.productDisplayModeId && isUnitsWeighable(line)) {
                        self.lines[lineKey].soldBy = !!line.weighableProductUnits && line.weighableProductUnits > 0 ? 'Unit' : 'Weight';
                    } else {
                        self.lines[lineKey].soldBy = null;
                    }
                    if (!line.isPseudo) {
                        self.lines[lineKey].gifts = line.gifts;
                        self.lines[lineKey].totalGifts = line.totalGifts;
                        self.lines[lineKey].totalClubsGifts = line.totalClubsGifts;
                        self.lines[lineKey].totalGiftsPotential = line.totalGiftsPotential;
                        self.lines[lineKey].totalClubsGiftsPotential = line.totalClubsGiftsPotential;
                        if (line.totalSimulateClubsGifts){
                            self.lines[lineKey].totalSimulateClubsGifts = line.totalSimulateClubsGifts;
                            self.lines[lineKey].totalSimulateClubsGiftsPotential = line.totalSimulateClubsGiftsPotential;
                        }
                        _setLinePrices(self.lines[lineKey], locals);
                    }
                }
            });

            var removedLines = [];
            angular.forEach(toRemoveKeys, function (val, lineKey) {
                removedLines.push(self.lines[lineKey]);
                delete self.lines[lineKey];
            });
            if (removedLines.length) {
                $rootScope.$emit('cart.lines.remove', {lines: removedLines});
            }

            if (addedLines && addedLines.length) {
                $rootScope.$emit('cart.lines.add', {lines: addedLines, isSecondTime: cartLinesAddAlreadyFired});
            }
        }

        /**
         * Prevent calls until call ends
         */
        var preventCalls,
            _resendData,
            retryTimeout;

        /**
         * On sever call return success
         * @param {object} resp
         * @param {object} resp.cart
         * @param {object} [resp.otherCart]
         * @param {Array|object} [resp.inactiveLines]
         * @param {object} requestDirtyLines
         * @param {Locals} locals
         * @param {boolean} [cartLinesAddAlreadyFired]
         * @private
         */
        function _serverCallCompleted(resp, requestDirtyLines, locals, cartLinesAddAlreadyFired) {
            _setDetailsFromServerCart(resp.cart, requestDirtyLines, locals, cartLinesAddAlreadyFired);
            _emitOtherCart(resp.otherCart);
            _emitInactiveLines(resp.inactiveLines);
            saveCart({
                withoutServer: true,
                locals: locals
            });

            if (retryTimeout) {
                $timeout.cancel(retryTimeout.timeout);
                retryTimeout = undefined;
            }

            preventCalls = false;
            if (_resendData) {
                _sendToServer(_resendData.options);
            } else {
                self.sendingSucceeded = true;
                $rootScope.$emit('cart.update.complete', requestDirtyLines);
            }
        }

        /**
         * Set failed request's data to dirty or deleted lines
         * @param {object} requestLinesData
         * @param {object} requestLinesData.dirtyLines
         * @param {object} requestLinesData.deletedLines
         * @param {object} [err]
         * @param {object} [err.response]
         * @param {Array} [err.errors]
         * @param {object} [err.errors.i]
         * @param {string} [err.errors.i.param]
         * @param {string} [err.errors.i.msg]
         * @private
         */
        function _restoreRequestDataOnError(requestLinesData, err) {
            var badRows = {},
                errors = [];
            if (err && err.response && err.response.errors && err.response.errors.length) {
                angular.forEach(err.response.errors, function (error) {
                    if (!error.param || error.param.indexOf('lines[') != 0) return;

                    var line = $rootScope.$eval(error.param.substring(0, error.param.indexOf('].') + 1), requestLinesData);
                    if (!line) return;

                    var lineKey = getLineKey({
                        isCase: line.isCase,
                        product: {id: line.isPseudo ? line.text : line.retailerProductId}
                    });
                    badRows[lineKey] = true;
                    !!self.lines[lineKey] && (error.line = self.lines[lineKey]);
                    error.requestLine = line;
                    errors.push(error);
                });
            }

            angular.forEach(requestLinesData.deletedLines, function (deletedLine, lineKey) {
                if (!!badRows[lineKey] || self.lines[lineKey]) return;
                deletedLines[lineKey] = deletedLine;
            });
            angular.forEach(requestLinesData.dirtyLines, function (val, lineKey) {
                if (!!badRows[lineKey] || deletedLines[lineKey] || !self.lines[lineKey]) return;
                dirtyLines[lineKey] = true;
            });

            if (!errors.length) return;

            self.sendingSucceeded = true;
            $rootScope.$emit('cart.lines.error', {errors: errors})
        }

        /**
         * On sever call return error
         * @private
         *
         * @param {object} err
         * @param {boolean} [err.aborted]
         * @param {number} err.statusCode
         * @param {*} err.response
         * @param {object} requestLinesData
         * @param {object} requestLinesData.dirtyLines
         * @param {object} requestLinesData.deletedLines
         * @param {object} locals
         * @param {object} options
         */
        function _serverCallError(err, requestLinesData, locals, options) {
            preventCalls = false;
            retryTimeout && $timeout.cancel(retryTimeout.timeout);
            retryTimeout = retryTimeout || { count: 0 };
            _restoreRequestDataOnError(requestLinesData, err);
            if (!err.aborted) {
                if (err.statusCode === 403 || (err.statusCode === 404 && err.response.error === 'Cart not found')) {
                    _errorRetry(locals, options);
                } else if (err.statusCode === 0 || err.statusCode === -1) {
                    _noConnectionRetry(options);
                }
            } else if (self.sendingSucceeded) {
                _sendToServer(options);
            }
        }

        function _errorRetry(locals, options) {
            retryTimeout.count++;

            retryTimeout.timeout = $timeout(function () {
                self.serverCartId = undefined;
                saveCart({
                    withoutServer: true,
                    locals: locals
                });
                _sendToServer(options);
            }, _getRetryTimeoutTime(500));
        }

        function _noConnectionRetry(options) {
            retryTimeout.count++;

            retryTimeout = $timeout(function () {
                _sendToServer(options);
            }, _getRetryTimeoutTime(2500));
        }

        function _getRetryTimeoutTime(defaultTime) {
            var time = defaultTime || 500;
            if (retryTimeout.count > 3) {
                time = 30000;
            } else if (retryTimeout.count > 4) {
                time = 60000;
            } else if (retryTimeout.count > 5) {
                time = 120000;
            }
            return time;
        }

        /**
         * Send the local cart to server
         * @private
         *
         * @param {Object} [options]
         * @param {number} [options.cartToDelete] - the id of the cart to delete
         * @param {boolean} [options.mergeDeletedCart] - whether to merge the current cart with the deleted cart
         * @param {number} [options.paymentMethodId]
         * @param {boolean} [options.skipActiveProductsValidation]
         * @param {boolean} [options.forceCreation]
         * @param {string} [options.source]
         * @param {boolean} [options.cartLinesAddAlreadyFired]
         *
         * @returns {Promise}
         */
        function _sendToServer(options) {
            options = options || {};

            var deferred = _resendData ? _resendData.defer : $q.defer();
            if (preventCalls) {
                _resendData = { defer: deferred, options: options };
                return _resendData.defer.promise;
            }

            _setCartTotal();
            _resendData = null;
            self.sendingSucceeded = false;
            preventCalls = true;

            var requestLinesData = _getServerRequestLines(),
                data = { lines: requestLinesData.lines };

            if (options.cartToDelete) {
                data.deleteCart = options.cartToDelete;
                data.merge = !!options.mergeDeletedCart;
            }

            if (options.paymentMethodId) {
                data.paymentMethodId = options.paymentMethodId;
            }

            if (self.editOrderId || !!options.skipActiveProductsValidation) {
                data.skipActiveProductsValidation = true;
                data.excludeCouponValidation = true; // Skip validation of all lines except coupons
            }

            if (options.originatedId) {
                data.originatedId = options.originatedId;
            }

            if (self.timeTravel) {
                data.timeTravel = self.timeTravel;
            }

            if (_trafficParams) {
                data.trafficParams = _trafficParams;
            }

            if (options.source) {
                data.source = options.source;
            }

            _getLocals().then(function (locals) {
                if (!!locals.withDeliveryProduct || self.editOrderId) {
                    data.deliveryProduct = true;
                } else {
                    angular.forEach(self.lines, function (line) {
                        if (!$rootScope.config.retailer.settings.includeDeliveryFeeInCart === 'true') {
                            line.type == SP_SERVICES.CART_LINE_TYPES.DELIVERY && _removeLine(line, true);
                        }
                    });
                }

                try {
                    var selectedStore = $rootScope.config.getBranchArea() || {}
                    data.deliveryProduct_Id = selectedStore.deliveryProduct_Id;
                    data.deliveryType = selectedStore.deliveryTypeId;
                } catch (e) {
                    // Do nothing
                }

                return _makeApiCall(locals, data, options.forceCreation).then(function (resp) {
                    _serverCallCompleted(resp, requestLinesData.dirtyLines, locals, options.cartLinesAddAlreadyFired);
                    deferred.resolve(resp);
                }).catch(function (err) {
                    _serverCallError(err, requestLinesData, locals, options);
                    deferred.reject(err);
                });
            });

            return deferred.promise;
        }

        /**
         * Make the actual api call to get/create/update the cart
         * @private
         *
         * @param {Locals} locals
         * @param {Object} data
         * @param {boolean} [forceCreation]
         *
         * @return {Promise<Object>}
         */
        function _makeApiCall(locals, data, forceCreation) {
            var httpOptions,
                loyaltyClubIds = !_isLoyaltyDisabled && locals.loyaltyClubIds || null;
            if (forceCreation || !_isDoNotCreateCart(data)) {
                if($rootScope.couponRedemptions) {
                    data.couponRedemptions = $rootScope.couponRedemptions;
                }

                httpOptions = {
                    method: self.serverCartId ? 'PATCH' : 'POST',
                    url: '/v2/retailers/:rid/branches/:bid/carts' + (self.serverCartId ? '/' + self.serverCartId : ''),
                    params: {
                        loyalty: loyaltyClubIds
                    },
                    data: data
                };
            } else if (locals.userId) {
                httpOptions = {
                    method: 'GET',
                    url: '/v2/retailers/:rid/branches/:bid/carts',
                    params: {
                        loyalty: loyaltyClubIds,
                        trafficParams: data.trafficParams
                    },
                    data: {
                        deliveryProduct_Id: data.deliveryProduct_Id
                    }
                };
            } else {
                return $q.resolve({});
            }

            var apiOptions = angular.copy(locals.apiOptions || {});
            apiOptions.id = 'spServicesServerCart';
            apiOptions.loadingElement = (locals.apiOptions || {}).loadingElement;
            return Api.request(httpOptions, apiOptions)
        }

        var DO_NOT_SKIP_CALL_KEYS = ['deleteCart', 'merge', 'paymentMethodId', 'skipActiveProductsValidation', 'excludeCouponValidation', 'timeTravel', 'deliveryProduct'];

        /**
         * Returns whether the cart is empty and there is no reason to actually make the api call to create it
         * @private
         *
         * @param {Object} data
         *
         * @return {boolean}
         */
        function _isDoNotCreateCart(data) {
            if (self.serverCartId || data.lines.length) {
                return false;
            }

            for (var i = 0; i < DO_NOT_SKIP_CALL_KEYS.length; i++) {
                if (data.hasOwnProperty(DO_NOT_SKIP_CALL_KEYS[i])) {
                    return false;
                }
            }

            return true;
        }

        // Init cart
        _sendToServer();

        function init(options) {
            return _sendToServer(options);
        }

        function _getTrafficParams() {
            var trafficParams = {};
            if (document.referrer && document.referrer !== document.location.href) {
                if (document.referrer.indexOf(document.location.hostname) > -1) {
                    return;
                }
                trafficParams.dr = document.referrer;
            }
            if (document.location.search) {
                var searchParams = document.location.search.substring(1).split('&');
                angular.forEach(searchParams, function (param) {
                    var parts = param.split('=');
                    trafficParams[parts[0]] = parts[1];
                });
            }
            return Object.keys(trafficParams).length ? trafficParams : undefined;
        }

        /**
         * Validate and emit event when the delivery items limit exceeded
         * @private
         *
         * @param {Array} [changedLines]
         *
         * @returns {void}
         */
        function validateDeliveryItemsLimit(changedLines) {
            _getLocals().then(function(locals) {
                if (isNaN(locals.deliveryItemsLimit) || locals.deliveryItemsLimit === null) {
                    return;
                }

                var totalItems = 0,
                    cartLines = [];
                angular.forEach(self.lines, function(line) {
                    if (!_validateIsDeliverItemsLimitLine(line)) {
                        return;
                    }

                    cartLines.push(line);
                    totalItems += 1;//line.quantity;
                });

                if (totalItems <= locals.deliveryItemsLimit) {
                    return;
                }

                // an array with the entire cart lines first and then the changed lines
                Array.prototype.push.apply(cartLines, changedLines || []);

                var linesToRemove = [],
                    keysToRemove = {},
                    toRemoveLength = totalItems - locals.deliveryItemsLimit;

                // run backwards on the array to collect the changed lines first
                for (var i = cartLines.length - 1; i >= 0 && linesToRemove.length < toRemoveLength; i--) {
                    var cartLineKey = getLineKey(cartLines[i]);
                    if (!keysToRemove[cartLineKey] && _validateIsDeliverItemsLimitLine(cartLines[i])) {
                        linesToRemove.push(cartLines[i]);
                        keysToRemove[cartLineKey] = true;
                    }
                }

                if (linesToRemove.length) {
                    _removeLines(linesToRemove, false);
                    $rootScope.$emit('cart.lines.deliveryItemsLimit', {lines: linesToRemove});
                }
            });
        }

        /**
         * Detect EBTEligibleType closure function
         * @private
         *
         * @param {object} [product]
         *
         * @returns {boolean}
         */

        function _detectEBTEligibleType(product) {
            var detectType = {
                snap: function () {
                    return !!((product.productTagsData || []).find(function(productTag) {
                        return SpProductTags.isProductTagRelationActive(productTag) && productTag.typeId === PRODUCT_TAG_TYPES.EBT_ELIGIBLE;
                    }) || product.branch && product.branch.isEbtEligible) ? "snap" : false;
                },
                cash: function() {
                    return !!((product.productTagsData || []).find(function(productTag) {
                        return SpProductTags.isProductTagRelationActive(productTag) && (productTag.typeId === PRODUCT_TAG_TYPES.EBT_CASH_ELIGIBLE || productTag.typeId === PRODUCT_TAG_TYPES.EBT_ELIGIBLE);
                    }) || product.branch && product.branch.isEbtCashEligible) ? "cash" : false;
                }
            };

            function detectByType(type) {
                return type && detectType[type] && detectType[type]();
            }

            function detectAllTypes(cb) {
                angular.forEach(detectType, function(detectFunc) {
                    var currType = detectFunc();

                    return (cb && typeof(cb) === 'function') ? cb(currType) : currType;
                });
            }
            9
            return {
                detectByType: detectByType,
                detectAllTypes: detectAllTypes
            };
        }

        /**
         * @typedef {Object} CartEBTEligibleDataItem
         *
         * @property {number} eligibleWithTax
         * @property {number} eligibleWithoutTax
         * @property {number} depositWithTax
         * @property {number} depositWithoutTax
         * @property {number} totalWithTax
         * @property {number} totalWithoutTax
         */

        /**
         * @typedef {Object} CartEBTEligibleData
         *
         * @property {CartEBTEligibleDataItem} snap
         * @property {CartEBTEligibleDataItem} entireCart
         */

        /**
         * Sums the ebt eligible amounts
         * @public
         *
         * @returns {CartEBTEligibleData}
         */
        function getEBTEligible(order) {
            var taxSuffixes = ['WithTax', 'WithoutTax'],
                cartLineTaxSuffixes = {
                    'WithTax': 'WithTaxForEbtEligibility',
                    'WithoutTax': 'WithoutTax',
                },
                groups = ['entireCart', 'snap', 'cash'],
                /**
                 * @type {CartEBTEligibleData}
                 */
                result = {snap: {}, entireCart: {}, cash: {}};
            angular.forEach(taxSuffixes, function (taxSuffix) {
                angular.forEach(groups, function (groupKey) {
                    result[groupKey]['eligible' + taxSuffix] = 0;
                    result[groupKey]['deposit' + taxSuffix] = 0;
                });
            });

            var index = 0;
            var lines = {}
            if (order) {
                order.lines.forEach(function (line) {
                    lines[line.id] = line;
                })
            }
            var totalWeighableForDeposit = angular.copy(result);

            angular.forEach(order ? lines : self.lines, function (line) {
                var addTo = ['entireCart'];
                var lineEBTEligibleDetection = _detectEBTEligibleType(line.product);
                lineEBTEligibleDetection.detectAllTypes(function(type) {
                    type && addTo.push(type);
                });
                angular.forEach(addTo, function (key) {

                    angular.forEach(taxSuffixes, function (taxSuffix) {
                        var finalPrice = _roundDecimal(line['finalPrice' + cartLineTaxSuffixes[taxSuffix]] || line['totalPrice'] || 0);
                        result[key]['eligible' + taxSuffix] += finalPrice;
                        if (line.product.isWeighable) {
                            totalWeighableForDeposit[key]['deposit' + taxSuffix] += finalPrice;
                        }
                    });
                });

                index++;
            });

            //adding ebt deposit of weighable product to the total sum
            angular.forEach(Object.keys(result), function (key) {
                angular.forEach(taxSuffixes, function (taxSuffix) {
                    result[key]['deposit' + taxSuffix] += _roundDecimal(totalWeighableForDeposit[key]['deposit' + taxSuffix] * 0.1);

                });
            });

            angular.forEach(groups, function(groupKey) {
                angular.forEach(taxSuffixes, function(taxSuffix) {
                    var fullEligibleKey = 'eligible' + taxSuffix,
                        fullDepositKey = 'deposit' + taxSuffix;
                    result[groupKey][fullEligibleKey] = _roundDecimal(result[groupKey][fullEligibleKey])
                    result[groupKey][fullDepositKey] = _roundDecimal(result[groupKey][fullDepositKey]);
                    result[groupKey]['total' + taxSuffix] = _roundDecimal(result[groupKey][fullEligibleKey] + result[groupKey][fullDepositKey]);
                    result[groupKey]['immutable' + taxSuffix] = _roundDecimal(result[groupKey][fullEligibleKey] + result[groupKey][fullDepositKey]);
                    result[groupKey]['immutable'+taxSuffix+'WithoutDeposit']= _roundDecimal(result[groupKey][fullEligibleKey]);
                    result[groupKey]['immutableDeposit'+taxSuffix]= _roundDecimal(result[groupKey][fullDepositKey]);
                });
            });

            return _roundEbtAmounts(result);
        }

        /**
         * Check EBTEligible new lines or quantity in edit cart
         * @public
         *
         * @param {object} currentCartLines
         *
         * @returns {boolean}
         */

        function checkEBTEligibleCartChange(currentCartLines) {
            var retailerIdsWhiteList = [1249];
            var newCartLines = getLines();
            var isExistCartLine = function(productId) {
                return function(line) { return line.product.id === productId }
            };
            var isQuantityIncrease = function(productId, quantity) {
                return function(line) {
                    return isExistCartLine(productId)(line) && line.quantity < quantity}
            };

            if (!newCartLines.length) return false;

            var additionNewEBTEligibleCartLine = newCartLines.find(function (newCartLine) {
                var ebtEligibleDetection = _detectEBTEligibleType(newCartLine.product);
                var isEligible = ebtEligibleDetection.detectByType('snap') || ebtEligibleDetection.detectByType('cash');

                return isEligible;
            });

            return !!additionNewEBTEligibleCartLine;
        }

        function _roundEbtAmounts(ebtEligible){
            if (ebtEligible){
                for (var objectProperty in ebtEligible) {
                    if(objectProperty){
                        for (var numericProperty in ebtEligible[objectProperty]) {
                            var numericPropertyObject = ebtEligible[objectProperty][numericProperty];
                            if(angular.isNumber(numericPropertyObject) && numericPropertyObject > 0) {
                                ebtEligible[objectProperty][numericProperty] = _roundDecimal(numericPropertyObject);
                            }
                        }
                    }
                }
            }
            return ebtEligible;
        }

        /**
         * @typedef {Object} CartEBTEligibleData
         * @param {Object} order
         * @property {CartEBTEligibleDataItem} snap
         * @property {CartEBTEligibleDataItem} cash
         * @property {CartEBTEligibleDataItem} entireCart
         */

        function getRemainingPayment(order) {
            var remainingPayment = {
                ebt: {
                    snap: 0,
                    cash: 0
                },
                total: 0
            }
            if (!order || !order.paymentData) {
                return remainingPayment
            }

            var orderPayment = order.paymentData;
            var ebtCashPayment = orderPayment.ebtCashPayment,
                ebtPayment = orderPayment.ebtPayment,
                ebtSNAPPayment = orderPayment.ebtSNAPPayment,
                mainPayment = orderPayment.mainPayment;

            remainingPayment.total = (self.total.finalPriceWithTax + self.total.serviceFee.finalPriceWithTax + self.total.deliveryCost.finalPriceWithTax - order.initialCheckoutCharge) || 0;

            var orderEbtEligible = getEBTEligible(order); // what eligible lines were in the order
            var ebtEligible = getEBTEligible(); // what is currently eligible in the cart

            if (ebtEligible && remainingPayment.total > 0) {
                if (ebtEligible.cash && ebtEligible.cash.totalWithTax > 0) {
                    var cashPayment = ebtCashPayment ? (ebtCashPayment.preAuthAmount || ebtCashPayment.initialAmount) : 0;
                    remainingPayment.ebt.cash = ebtEligible.cash.eligibleWithTax - orderEbtEligible.cash.eligibleWithTax;
                }else{
                    var cashPayment = ebtCashPayment ? (ebtCashPayment.preAuthAmount || ebtCashPayment.initialAmount) : 0;
                    remainingPayment.ebt.snap = cashPayment * -1
                }
                if (ebtEligible.snap && ebtEligible.snap.totalWithTax > 0) {
                    var snapPayment = ebtSNAPPayment ? (ebtSNAPPayment.preAuthAmount || ebtSNAPPayment.initialAmount) : 0;
                    remainingPayment.ebt.snap = ebtEligible.snap.eligibleWithTax - orderEbtEligible.snap.eligibleWithTax;
                } else {
                    var snapPayment = ebtSNAPPayment ? (ebtSNAPPayment.preAuthAmount || ebtSNAPPayment.initialAmount) : 0;
                    remainingPayment.ebt.snap = snapPayment * -1
                }
            }

            return remainingPayment;
        }

        /**
         * Set the loyalty disabled flag
         * @public
         *
         * @param {boolean} [value]
         *
         * @returns {boolean}
         */
        function disableLoyalty(value) {
            if (typeof value === 'boolean') {
                if (_isLoyaltyDisabled !== value) {
                    $rootScope.$emit('cart.disableLoyalty.change', { value: value });
                    saveCart();
                }

                _isLoyaltyDisabled = value;
            }

            return _isLoyaltyDisabled;
        }

        function _validateIsDeliverItemsLimitLine(line) {
            return !line.removed && (!line.type || line.type === SP_SERVICES.CART_LINE_TYPES.PRODUCT);
        }

        function _onLinesAdded(event, data) {
            _setLinesInProducts(event, data);
            validateDeliveryItemsLimit(data.lines);
        }

        //listen to cart add and remove line to set lines in products
        function _setLinesInProducts(event, data) {
            getProducts(data.lines);
        }

        function _setChangedProducts(requestDirtyLines) {
            angular.forEach(Object.keys(requestDirtyLines), function (lineKey) {
                var line = self.lines[lineKey];
                if (!line || !line.product || !line.id) {
                    return;
                }

                var productId = line.product.id;
                if (self.changedProducts[productId]) {
                    $timeout.cancel(self.changedProducts[productId]);
                }

                self.changedProducts[productId] = $timeout(function () {
                    delete self.changedProducts[productId];
                }, 3000);
            });
        }

        function formatBase64toString(data) {
            return data ? atob(data) : data;
        }

        $rootScope.$on('cart.lines.add', _onLinesAdded);
        $rootScope.$on('cart.lines.remove', _setLinesInProducts);
    }

    /**
     * Register service
     */
    angular.module('spServices').config(['$provide', function ($provide) {
        provider = $provide.service(serviceName, [
            '$rootScope', '$injector', '$filter', '$timeout', '$q', 'Api', 'SpProductTags', 'EmbedHistoryRetailer', 'SP_SERVICES', 'PRODUCT_TAG_TYPES',
            SpCartService
        ]);

        /**
         * To init the cart id and lines (and support backwards compatibility)
         */
        provider.initCart = null;

        /**
         * Get cart's locals
         * @type {function|Array}
         * @returns {Promise} - a promise that send the following object: {
         *                          loyaltyClubIds,
         *                          languageId,
         *                          includeTaxInPrice,
         *                          withDeliveryProduct,
         *                          [userId],
         *                          [apiOptions],
         *                          [deliveryItemsLimit]
         *                      }
         */
        provider.getLocals = null;

    }]);
})(angular);
