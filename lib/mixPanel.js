(function (angular) {


    var provider = {};

    function spMixPanel($filter, $controller, $rootScope, SpCartService) {
        var self = this;
        var _nameFilter = $filter('name');
        var locals;
        var script = document.createElement("script");

        self.init = init;

        function init() {

            locals = _getLocals();
            if(locals.mixPanelToken){
                initMixPanel();
                initEventListeners();
            }
        }

        function initMixPanel(){
            script.type = "text/javascript";
            script.async = true;
            script.innerText = '(function (f, b) { if (!b.__SV) { var e, g, i, h; window.mixpanel = b; b._i = []; b.init = function (e, f, c) { function g(a, d) { var b = d.split("."); 2 == b.length && ((a = a[b[0]]), (d = b[1])); a[d] = function () { a.push([d].concat(Array.prototype.slice.call(arguments, 0))); }; } var a = b; "undefined" !== typeof c ? (a = b[c] = []) : (c = "mixpanel"); a.people = a.people || []; a.toString = function (a) { var d = "mixpanel"; "mixpanel" !== c && (d += "." + c); a || (d += " (stub)"); return d; }; a.people.toString = function () { return a.toString(1) + ".people (stub)"; }; i = "disable time_event track track_pageview track_links track_forms track_with_groups add_group set_group remove_group register register_once alias unregister identify name_tag set_config reset opt_in_tracking opt_out_tracking has_opted_in_tracking has_opted_out_tracking clear_opt_in_out_tracking start_batch_senders people.set people.set_once people.unset people.increment people.append people.union people.track_charge people.clear_charges people.delete_user people.remove".split( " "); for (h = 0; h < i.length; h++) g(a, i[h]); var j = "set set_once union unset remove delete".split(" "); a.get_group = function () { function b(c) { d[c] = function () { call2_args = arguments; call2 = [c].concat(Array.prototype.slice.call(call2_args, 0)); a.push([e, call2]); }; } for ( var d = {}, e = ["get_group"].concat( Array.prototype.slice.call(arguments, 0)), c = 0; c < j.length; c++) b(j[c]); return d; }; b._i.push([e, f, c]); }; b.__SV = 1.2; e = f.createElement("script"); e.type = "text/javascript"; e.async = !0; e.src = "undefined" !== typeof MIXPANEL_CUSTOM_LIB_URL ? MIXPANEL_CUSTOM_LIB_URL : "file:" === f.location.protocol && "//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\\/\\//) ? "https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js" : "//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js"; g = f.getElementsByTagName("script")[0]; g.parentNode.insertBefore(e, g); } })(document, window.mixpanel || []);'
            document.getElementsByTagName("head")[0].appendChild(script);
            window.mixpanel.init(locals.mixPanelToken, {
                track_pageview: "full-url"
            });
        }

        function _getLocals() {
            if (!provider.getLocals || (!angular.isFunction(provider.getLocals) && !angular.isArray(provider.getLocals))) {
                throw new Error('spMixPanel.getLocals must be implemented');
            }

            return $controller(provider.getLocals);
        }

        function initEventListeners(){
            $rootScope.$on('loginForAnalyticServiceProvider', _setUserData);
            $rootScope.$on('productPage.open', _eventProductPageView);
            $rootScope.$on('cart.lines.add', _eventAddToCart);
            $rootScope.$on('cart.lines.remove', _eventRemoveFromCart);
            $rootScope.$on('cart.lines.quantityChanged.click', _eventCartQuantityChanged);
            $rootScope.$on('setSoldBy', _eventUnitsChange);
        }

        function _setUserData(){
            var locals = _getLocals();
            if(locals.userId){
                window.mixpanel.track('sign in');
                window.mixpanel.register({
                    userName: locals.username,
                });
                window.mixpanel.identify(locals.userId);
            }
        }

        //======== Events

        function _eventProductPageView(event, dataObject, extraData) {
            window.mixpanel.track('Product Details Page Viewed', {
                'Product Name': _getProductName(dataObject),
                'Product ID': dataObject.id,
                'Product Position': extraData && extraData.hasOwnProperty('index') ? (extraData.index + 1) : undefined,
                'Source': 'Not Set Yet',
            });
        }

        function _eventAddToCart(event, dataObject, extraData) {
            var cartLine = _extractCartLine(dataObject);
            if(!cartLine) return;

            if(_isCouponCartLine(cartLine)) { // Add Coupon to Cart have a different event
                return _eventCouponAddToCart(event, cartLine.product);
            }

            window.mixpanel.track('Product Added to Cart', {
                'Product Name': _getProductName(cartLine.product),
                'Product ID': cartLine.product.id,
                'Product Position': extraData && extraData.hasOwnProperty('index') ? (extraData.index + 1) : undefined,
                'Quantity': cartLine.quantity,
                'Product with Special indication': !!(cartLine.product.branch &&cartLine.product.branch.specials && cartLine.product.branch.specials[0]),
                'Product Tags Ids': _getProductTagIds(cartLine.product),
                'Source': 'Not Set Yet',
            });
        }

        function _eventRemoveFromCart(event, dataObject) {
            var cartLine = _extractCartLine(dataObject);
            if(!cartLine) return;

            if(_isCouponCartLine(cartLine)) { // Remove Coupon from Cart don't have an event
                return;
            }

            window.mixpanel.track('Product Removed from Cart', {
                'Product Name': _getProductName(cartLine.product),
                'Product ID': cartLine.product.id,
                'Cart Value After Removal': _getCartTotal()
            });
        }

        function _eventCouponAddToCart(event, couponData) {
            window.mixpanel.track('Coupons Applied to Cart', {
                'Coupon Name': _getProductName(couponData),
                'Coupon ID': couponData.id,
                'Source': 'Not Set Yet',
            });
        }

        function _eventCartQuantityChanged(event, dataObject) {
            var cartLine = _extractCartLine(dataObject);
            if(!cartLine) return;

            if(_isCouponCartLine(cartLine)) { // Coupon Quantity Change don't have an event
                return;
            }

            var eventNane = dataObject.direction === 'decrease' ? 'Quantity Decreased' : 'Quantity Increased';

            window.mixpanel.track(eventNane, {
                'Product Name': _getProductName(cartLine.product),
                'Product ID': cartLine.product.id,
                'Quantity': cartLine.quantity,
                'Source': 'Not Set Yet',
            });
        }
        function _eventUnitsChange(event, object) {
            if(!(object && object.cartLine && object.cartLine.product && object.cartLine.product.id)) return;

            window.mixpanel.track('Change Units', {
                'Product ID': object.cartLine.product.id,
                'Source': 'Not Set Yet',
            });
        }


        //======= Service Functions

        // Check if this Cart Line item is a Coupon
        function _isCouponCartLine(cartLine) {
            return cartLine && cartLine.type === 3 && cartLine.product && cartLine.product.isCoupon === true;
        }

        // Check if a valid CartLines object and extract a single Cart Line
        function _extractCartLine(object) {
            if(!(object && object.lines && object.lines[0])) return;
            if(object.lines.length > 1) return; // not a single product add to cart - page load or cart copy

            if(object.lines[0].id && object.lines[0].product) {
                return object.lines[0];
            }
        }

        function _getProductTagIds(product) {
            try {
                if (product.productTags && product.productTags.length) {
                    return product.productTags.join(',')
                } else if (product.productTagsData && product.productTagsData.length) {
                    return product.productTagsData.map(function (item) {
                        return item.id;
                    }).join(',');
                }
            } catch (err) {}

            return '';
        }

        // Get Product Name - we will take a Short Name, first from En, than from He, then from any other language if previous doesn't exists
        function _getProductName(product) {
            var name = 'Undefined';

            if(product.names) {
                name = (product.names[2] && product.names[2].short) ||
                    (product.names[1] && product.names[1].short) ||
                    (product.names[3] && product.names[3].short) ||
                    (product.names[4] && product.names[4].short) ||
                    (product.names[5] && product.names[5].short) ||
                    (product.names[6] && product.names[6].short) || name;
            } else if (product.localName) {
                name = product.localName;
            }

            return name;
        }

        function _getCartTotal() {
            return SpCartService ? SpCartService.finalPriceForView || SpCartService.finalPriceWithTax : 0;
        }


    }

    angular.module('spServices')
        .config(['$provide', function ($provide) {
            provider = $provide.service('spMixPanel', [
                '$filter', '$controller', '$rootScope', 'SpCartService',
                spMixPanel
            ]);
        }]);
})(angular);
