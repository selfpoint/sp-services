(function (angular) {
    /**
     * Service name
     */
    var serviceName = 'SpRecipeService';

    function SpRecipeService(api, Cart, $rootScope, $location, $q, SP_SERVICES) {
        var self = this;

        self.isValidRecipeQuery = isValidRecipeQuery;
        self.parseRecipeFromQuery = parseRecipeFromQuery;
        self.addIngredientProductToCart = addIngredientProductToCart;
        self.addIngredientsToCartByRecipeId = addIngredientsToCartByRecipeId;
        self.fixIngredientQuantity = fixIngredientQuantity;
        self.initRecipeListener = initRecipeListener;


        /**
         * Check if recipeQuery is number or json
         * @public
         *
         * @param {string} recipeQuery
         *
         * @returns {boolean}
         */
        function isValidRecipeQuery(recipeQuery) {
            try {
                if (angular.isObject(JSON.parse(recipeQuery))) {
                    return true;
                }
            } catch (err) {}

            return !isNaN(recipeQuery);
        }

        /**
         * Return recipe object by recipeQuery (can be number or json)
         * @public
         *
         * @param {string|number} recipeQuery
         *
         * @returns {Promise.<object>}
         */
        function parseRecipeFromQuery(recipeQuery) {
            if (!isNaN(recipeQuery)) {
                return api.request({
                    method: 'GET',
                    url: '/v2/retailers/:rid/branches/:bid/bumpers/recipes/' + recipeQuery
                });
            }

            try {
                var parsed = JSON.parse(recipeQuery),
                    fillInto = {};
                angular.forEach(parsed.groups, function (group, groupIndex) {
                    angular.forEach(group.ingredients, function (ingredient, ingredientIndex) {
                        if (!ingredient.productsFilters) return;

                        ingredient.products = [];
                        angular.forEach(ingredient.productsFilters, function (productFilters) {
                            if (!fillInto[productFilters.barcode]) {
                                fillInto[productFilters.barcode] = [];
                            }

                            fillInto[productFilters.barcode].push([groupIndex, ingredientIndex]);
                        });
                    });
                });

                var barcodes = Object.keys(fillInto);
                return api.request({
                    method: 'GET',
                    url: '/v2/retailers/:rid/branches/:bid/products',
                    params: {
                        size: barcodes.length,
                        filters: {
                            must: {
                                term: {
                                    barcode: barcodes
                                }
                            }
                        }
                    }
                }).then(function (response) {
                    angular.forEach(response.products, function (product) {
                        angular.forEach(fillInto[product.barcode], function (into) {
                            parsed.groups[into[0]].ingredients[into[1]].products.push({product: product});
                        });
                    });

                    return parsed;
                });
            } catch (err) {}
        }

        /**
         * Prepare and set product quantities for cart
         * @public
         *
         * @param {Object} ingredient
         *
         * @returns {void}
         */
        function fixIngredientQuantity(ingredient) {
            ingredient.wholeNumber = parseInt(ingredient.quantity);
            var decimal = ingredient.quantity - Math.floor(ingredient.quantity),
                fraction = new Fraction(decimal);
            ingredient.numerator = fraction.n;
            ingredient.denominator = fraction.d;

            angular.forEach(ingredient.products, function (product) {
                product.conversionCount = ingredient.conversionCount;
                product.quantity = Cart.quantityInterval(product);
            });
        }

        /**
         * Prepare and set product quantities for cart
         * @private
         *
         * @param {Object} recipe
         *
         * @returns {void}
         */
        function _fixProductsQuantityInRecipe(recipe) {

            angular.forEach(recipe.groups, function (group) {
                angular.forEach(group.ingredients, function (ingredient) {
                    fixIngredientQuantity(ingredient);
                });
            });
        }

        /**
         * Add single ingredient tc cart or update cart quantity
         * @public
         *
         * @param {Object} ingredientProduct
         *
         * @returns {void}
         */
        function addIngredientProductToCart(ingredientProduct) {

            var line = Cart.getProduct(ingredientProduct.product).singleLine || null;
            if (line) {
                line.quantity += ingredientProduct.quantity;
                Cart.quantityChanged(line);
                return;
            }

            Cart.addLine({
                product: ingredientProduct.product,
                quantity: ingredientProduct.quantity,
                conversionCount: ingredientProduct.conversionCount,
                source: SP_SERVICES.SOURCES.RECIPE
            });
        }

        /**
         * Add all products from recipe to cart
         * @public
         *
         * @param {Object} recipe
         *
         * @returns {void}
         */
        function _addAllIngredientsToCart(recipe) {
            angular.forEach(recipe.groups, function (group) {
                angular.forEach(group.ingredients, function (ingredient) {
                    if(ingredient.products && ingredient.products[0] && ingredient.products[0].quantity){
                        addIngredientProductToCart(ingredient.products[0]);
                    }
                });
            });
        }

        /**
         * Get recipe by Id and add all it's ingredients to cart
         * @public
         *
         * @param {Number} recipeId
         *
         * @returns {Promise.<object>}
         */
        function addIngredientsToCartByRecipeId(recipeId) {
            if(!recipeId || isNaN(recipeId)){
                return $q.resolve();
            }

            return parseRecipeFromQuery(recipeId).then(function(recipe) {
                _fixProductsQuantityInRecipe(recipe);
                _addAllIngredientsToCart(recipe);
            });
        }

        /**
         * Listener for adding a recipe by query parameter
         * @private
         *
         * @returns {Promise}
         */
        function initRecipeListener() {
            var locationSearch = $location.search(),
                recipeId = locationSearch && locationSearch.addRecipeIngredients;

            if (!recipeId) {
                if (recipeId !== undefined) {
                    _clearAddRecipeParam();
                }

                return $q.resolve();
            }

            return _waitForCartInit().then(function () {
                return addIngredientsToCartByRecipeId(recipeId);
            }).then(function() {
                _clearAddRecipeParam();
            });
        }

        /**
         * Remove query parameter to prevent double adding ingredients on page refresh
         * @private
         *
         * @returns {void}
         */
        function _clearAddRecipeParam() {
            $location.search('addRecipeIngredients', null);
        }


        /**
         * Check if cart initialized
         * @private
         *
         * @returns {Promise}
         */
        function _waitForCartInit() {
            return new $q(function(resolve) {
                if (Cart.sendingSucceeded) {
                    return resolve();
                }
                var listener = $rootScope.$on('cart.update.complete', function () {
                    listener();
                    resolve();
                });
            });
        }
    }

    /**
     * Register service
     */
    angular.module('spServices')
        .service(serviceName, ['Api', 'SpCartService', '$rootScope', '$location', '$q', 'SP_SERVICES', SpRecipeService]);
})(angular);
