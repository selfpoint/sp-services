(function (angular) {
    'use strict';

    var LOCAL_STORAGE_KEY = 'permanentFilters',
        TYPES = {
            PRODUCT_TAGS: 1
        };

    var provider;

    function PermanentFiltersService($rootScope, $q, $timeout, $injector) {
        var self = this,
            _valuesMaps = {},
            _eventListeners = [],
            _saveData = {};

        self.TYPES = TYPES;
        self.values = {};

        var savedValues = _getLocalStorageItem(LOCAL_STORAGE_KEY) || {};
        angular.forEach(TYPES, function(valuesType) {
            self.values[valuesType] = savedValues[valuesType] || [];

            _valuesMaps[valuesType] = {};
            angular.forEach(self.values[valuesType], function(value) {
                _valuesMaps[valuesType][value] = true;
            });
        });

        self.hasFilter = hasFilter;
        self.addFilterValue = addFilterValue;
        self.removeFilterValue = removeFilterValue;
        self.addFilterValues = addFilterValues;
        self.removeFilterValues = removeFilterValues;
        self.clearFilterValues = clearFilterValues;
        self.subscribe = onPermanentFiltersChange;

        self.hasProductTagFilter = hasProductTagFilter;
        self.addProductTagFilter = addProductTagFilter;
        self.removeProductTagFilter = removeProductTagFilter;
        self.addProductTagsFilters = addProductTagsFilters;
        self.removeProductTagsFilters = removeProductTagsFilters;
        self.clearProductTagsFilters = clearProductTagsFilters;

        /**
         * Returns whether the given value exists in the given permanent filters type
         * @public
         *
         * @param {number} type
         * @param {number|string} value
         *
         * @return {boolean}
         */
        function hasFilter(type, value) {
            return _valuesMaps[type][value] || false;
        }

        /**
         * Add filter value on the given permanent filters type
         * @public
         *
         * @param {number} type
         * @param {number|string} value
         *
         * @return {Promise}
         */
        function addFilterValue(type, value) {
            return _saveType(type, _addFilterValue(type, value));
        }

        /**
         * Add value to the given type's filter (inner)
         * @private
         *
         * @param {number} type
         * @param {number|string} value
         *
         * @return {Array<number>}
         */
        function _addFilterValue(type, value) {
            if (!_valuesMaps[type].hasOwnProperty(value)) {
                _valuesMaps[type][value] = true;
                self.values[type].push(value);

                return [value];
            }

            return [];
        }

        /**
         * Remove filter value from the given permanent filters type
         * @public
         *
         * @param {number} type
         * @param {number|string} value
         *
         * @return {Promise}
         */
        function removeFilterValue(type, value) {
            return _saveType(type, _removeFilterValue(type, value));
        }

        /**
         * Remove value from the given type's filter (inner)
         * @private
         *
         * @param {number} type
         * @param {number|string} value
         *
         * @return {Array<number>}
         */
        function _removeFilterValue(type, value) {
            if (_valuesMaps[type].hasOwnProperty(value)) {
                delete _valuesMaps[type][value];
                self.values[type].splice(self.values[type].indexOf(value), 1);

                return [value];
            }

            return [];
        }

        /**
         * Add an array of values into the given permanent filters type
         * @public
         *
         * @param {number} type
         * @param {Array<number|string>} values
         * @param {boolean} [replace] - replace all current values
         *
         * @return {Promise}
         */
        function addFilterValues(type, values, replace) {
            var newValues = {},
                changes = [];

            angular.forEach(values, function(value) {
                Array.prototype.push.apply(changes, _addFilterValue(type, value));
                newValues[value] = true;
            });

            if (replace) {
                angular.forEach(self.values[type].slice(0), function(value) {
                    if (!newValues[value]) {
                        Array.prototype.push.apply(changes, _removeFilterValue(type, value));
                    }
                });
            }

            return _saveType(type, changes);
        }

        /**
         * Remove the given values from the given permanent filters type
         * @public
         *
         * @param {number} type
         * @param {Array<number|string>} values
         *
         * @return {Promise}
         */
        function removeFilterValues(type, values) {
            var changes = [];
            angular.forEach(values, function(value) {
                Array.prototype.push.apply(changes, _removeFilterValue(type, value));
            });
            return _saveType(type, changes);
        }

        /**
         * Clear all values of the given permanent filters type
         * @public
         *
         * @param {number} type
         *
         * @return {Promise}
         */
        function clearFilterValues(type) {
            return removeFilterValues(type, self.values[type].slice(0));
        }

        /**
         * Emit change event on the given type's changes and save to local storage
         * @private
         *
         * @param {number} type
         * @param {Array<number|string>} changes
         *
         * @returns {Promise}
         */
        function _saveType(type, changes) {
            if (!changes || !changes.length) {
                return $q.resolve();
            }

            if (_saveData.timeout) {
                $timeout.cancel(_saveData.timeout);
            }

            _saveData.collectedChanges = _saveData.collectedChanges || {};
            _saveData.collectedChanges[type] = _saveData.collectedChanges[type] || [];
            _saveData.collectedChanges[type].push(changes);

            var defer = $q.defer();

            _saveData.defers = _saveData.defers || [];
            _saveData.defers.push(defer);

            _setSaveTimeout();

            return defer.promise;
        }

        /**
         * Set timeout for the save action
         * @private
         */
        function _setSaveTimeout() {
            // timeout the actual save to allow multiple changes in one save
            _saveData.timeout = $timeout(_saveTimeoutHandler, 100);
        }

        /**
         * Handle the save
         * @private
         *
         * @return {Promise<Array<*>>}
         */
        function _saveTimeoutHandler() {
            delete _saveData.timeout;

            // if a save is currently in progress, do not do anything until it finished
            if (_saveData.saving) {
                return _setSaveTimeout();
            }

            _saveData.saving = true;

            _setLocalStorageItem(LOCAL_STORAGE_KEY, self.values);

            // join changes per type
            var changes = {};
            angular.forEach(_saveData.collectedChanges || {}, function(typeBulks, typeKey) {
                changes[typeKey] = [];

                var valuesMap = {};
                angular.forEach(typeBulks, function(bulk) {
                    angular.forEach(bulk, function(value) {
                        if (!valuesMap[value]) {
                            changes[typeKey].push(value);
                            valuesMap[value] = true;
                        }
                    });
                });
            });
            _saveData.collectedChanges = {};

            var defers = (_saveData.defers || []).splice(0); // get and empty the array

            var promises = [];
            angular.forEach(_eventListeners, function(_eventListener) {
                promises.push(_eventListener(changes));
            });
            return $q.all(promises).then(function(res) {
                angular.forEach(defers, function(defer) {
                    defer.resolve(res);
                });
            }).catch(function(err) {
                angular.forEach(defers, function(defer) {
                    defer.reject(err);
                });
            }).finally(function() {
                _saveData.saving = false;
            });
        }

        /**
         * Listen to filters change event
         * @public
         *
         * @param {function} callback
         * @param {Object} [$scope]
         *
         * @returns {function}
         */
        function onPermanentFiltersChange(callback, $scope) {
            _eventListeners.push(callback);

            function _removeListener() {
                _eventListeners.splice(_eventListeners.indexOf(callback), 1);
            }

            if ($scope) {
                $scope.$on('$destroy', _removeListener);
            }

            return _removeListener;
        }

        /**
         * Returns whether the given product tag id exists in the product tags permanent filters
         * @public
         *
         * @param {number} productTagId
         *
         * @return {boolean}
         */
        function hasProductTagFilter(productTagId) {
            return hasFilter(TYPES.PRODUCT_TAGS, productTagId);
        }

        /**
         * Add product tag filter value
         * @public
         *
         * @param {number} productTagId
         *
         * @return {Promise}
         */
        function addProductTagFilter(productTagId) {
            return addFilterValue(TYPES.PRODUCT_TAGS, productTagId);
        }

        /**
         * Remove product tag filter
         * @public
         *
         * @param {number} productTagId
         *
         * @return {Promise}
         */
        function removeProductTagFilter(productTagId) {
            return removeFilterValue(TYPES.PRODUCT_TAGS, productTagId);
        }

        /**
         * Set an array of product tags filters
         * @public
         *
         * @param {Array<number>} productTags
         * @param {boolean} [replace] - replace all current values
         *
         * @return {Promise}
         */
        function addProductTagsFilters(productTags, replace) {
            return addFilterValues(TYPES.PRODUCT_TAGS, productTags, replace);
        }

        /**
         * Remove given product tags filters
         * @public
         *
         * @param {Array<number>} productTags
         *
         * @return {Promise}
         */
        function removeProductTagsFilters(productTags) {
            return removeFilterValues(TYPES.PRODUCT_TAGS, productTags);
        }

        /**
         * Clear all product tags filters
         * @public
         *
         * @return {Promise}
         */
        function clearProductTagsFilters() {
            return clearFilterValues(TYPES.PRODUCT_TAGS);
        }

        /**
         * Returns the local storage value for the given key
         * @private
         *
         * @param {string} key
         *
         * @return {*}
         */
        function _getLocalStorageItem(key) {
            return _getLocalStorage('getItem').getItem(key);
        }

        /**
         * Sets a local storage value on the given key
         * @private
         *
         * @param {string} key
         * @param {string} value
         *
         * @return {*}
         */
        function _setLocalStorageItem(key, value) {
            return _getLocalStorage('setItem').setItem(key, value);
        }

        /**
         * Validate the provider's local storage service and the given key on it
         * @private
         *
         * @param {string} key
         */
        function _getLocalStorage(key) {
            if (!provider.localStorage) {
                throw new Error('No local storage service was set on the PermanentFiltersProvider');
            }

            var localStorage = $injector.invoke(provider.localStorage);

            if (!localStorage[key] || !angular.isFunction(localStorage[key])) {
                throw new Error('The function ' + key + ' is not implemented on the local storage service');
            }

            return localStorage;
        }
    }

    angular.module('spServices').config(['$provide', function($provide) {
        provider = $provide.service('PermanentFilters', ['$rootScope', '$q', '$timeout', '$injector', PermanentFiltersService]);

        provider.localStorage = undefined;
    }]);
})(angular);