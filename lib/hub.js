(function (angular) {
    'use strict';

    angular.module('spServices').service('HubService', ['Api', function (Api) {
        var self = this,
            _dataPromise;

        self.getData = getData;
        self.backToHub = backToHub;

        /**
         * Get the hub data
         * @public
         *
         * @param {number} hubId
         * @param {boolean} [withoutCache]
         *
         * @returns {Promise}
         */
        function getData(hubId, withoutCache) {
            if (!withoutCache && _dataPromise) {
                return _dataPromise;
            }

            return _dataPromise = Api.request({
                url: '/v2/retailers/:rid/retailers/' + hubId,
                method: 'GET'
            }).then(function(data) {
                return data.retailer;
            });
        }

        /**
         * Redirect back to the hub from a child frontend app
         * @public
         *
         * @param {string} hubDomain
         */
        function backToHub(hubDomain) {
            if (window.spHostMobileAppId) {
                location.href = location.origin + '/?mobileAppId=' + window.spHostMobileAppId + '&clear=1';
            } else if (location.hostname === 'localhost') {
                location.href = location.origin + '/?domain=' + hubDomain + '&clear=1';
            } else {
                location.href = location.protocol + '//' + hubDomain + '/?clear=1';
            }
        }
    }]);
})(angular);