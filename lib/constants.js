(function (angular) {
    var CART_LINE_TYPES = {
        PRODUCT: 1,
        DELIVERY: 2,
        COUPON: 3,
        REGISTER_LOYALTY: 4,
        SERVICE_FEE: 5
    };

    var CHOOSE_AREA_MODE = {
        TYPE_GOOGLE_ADDRESS: 1,
        TYPE_AREA: 2,
        CHOOSE_RETAILER: 3
    };

    var CHOOSE_AREA_EVENT = {
        NONE: 1,
        ENTER_SITE: 2,
        CART_ACTIVITY: 3
    };

    var MULTI_DOMAINS_PICKERS = {
        ZIP_CODE: 1,
        CHOOSE_RETAILER: 2,
        CHOOSE_AREA: 3
    };

    var DELIVERY_AREA_METHODS = {
        POLYGON: 1,
        ZIP_CODE: 2,
        CITY: 3,
        GOOGLE_MAPS: 4
    };

    var DELIVERY_TYPES = {
        DELIVERY: 1,
        PICKUP: 2,
        PICK_AND_GO: 3,
        SCAN_AND_GO: 4,
        EXPRESS_DELIVERY: 5
    };

    var PROMOTION_TYPES = {
        BUY_X_GET_Y_IN_PROMOTION: 1,
        BUY_X_IN_Y_PROMOTION: 2,
        TOTAL_GET_Y_IN_PROMOTION: 3,
        BUY_X_GET_Y_DISCOUNT_PROMOTION: 4,
        BUY_X_GET_Y_FIXED_DISCOUNT_PROMOTION: 5,
        DISCOUNT_TOTAL_PROMOTION: 6,
        BUY_X_GET_FIXED_DISCOUNT_PROMOTION: 7,
        BUY_X_GET_DISCOUNT_PROMOTION: 8,
        FIXED_DISCOUNT_TOTAL_PROMOTION: 9,
        TOTAL_GET_Y_DISCOUNT_PROMOTION: 10
    };

    var DELIVERY_PROVIDERS = {
        SHIP_STATION: 1,
        BUZZR: 2,
        DELIV: 3,
        BRINGOZ: 4,
        GETT: 5,
        MANUAL: 6,
        DELIVEREE: 7
    };

    var DELIVERY_TIMES_TYPES = {
        REGULAR: 1,
        DELIVERY_WITHIN_HOURS: 2,
        DELIVERY_WITHIN_DAYS: 3
    };

    var PROMOTION_ID = {
        COLLEAGUE: 'colleague'
    };

    var SOURCES = {
        ORDER: 'order',
        ORDER_HISTORY: 'orderHistory',
        ORDERS: 'orders',
        CHECKOUT_SUMMARY: 'checkoutSummary',
        RECIPE: 'recipe',
        SHOP_LIST: 'shopList',
        SMART_LIST: 'smartList'
    };

    var HUB_DOMAIN = {
        LOCAL_PATH: '.hubRetailer.domainName',
        WEB_DATA: 'frontendData',
        MOBILE_DATA: 'mobileData',
        LOCAL_STORAGE_NAME: 'isLastVisitRetailer'
    }

    // Register service
    angular.module('spServices').constant('SP_SERVICES', {
        CART_LINE_TYPES: CART_LINE_TYPES,
        CHOOSE_AREA_MODE: CHOOSE_AREA_MODE,
        CHOOSE_AREA_EVENT: CHOOSE_AREA_EVENT,
        MULTI_DOMAINS_PICKERS: MULTI_DOMAINS_PICKERS,
        DELIVERY_AREA_METHODS: DELIVERY_AREA_METHODS,
        DELIVERY_TYPES: DELIVERY_TYPES,
        PROMOTION_TYPES: PROMOTION_TYPES,
        DELIVERY_PROVIDERS: DELIVERY_PROVIDERS,
        DELIVERY_TIMES_TYPES: DELIVERY_TIMES_TYPES,
        PROMOTION_ID: PROMOTION_ID,
        SOURCES: SOURCES,
        HUB_DOMAIN: HUB_DOMAIN
    });

    // Add to root scope
    angular.module('spServices').run(['$rootScope', 'SP_SERVICES', function($rootScope, SP_SERVICES) {
        $rootScope.SP_SERVICES = SP_SERVICES;
    }]);

})(angular);
