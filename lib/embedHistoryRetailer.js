(function (angular) {
    'use strict';

    angular.module('spServices').service('EmbedHistoryRetailer', [
        'SP_SERVICES',
        function (SP_SERVICES) {
            var self = this;

            self.embedHubIframe = embedHubIframe;

            /**
             * Embeds an iframe for hub communication
             * and manages history registration
             * @function embedHubIframe
             */
            function embedHubIframe() {
                var hubConts = SP_SERVICES.HUB_DOMAIN;
                var hubDomain = _getHubRetailerDomain();
                if (!hubDomain) return;

                // Checks if the registration flag is set in `localStorage` to prevent duplicate executions
                var isLastRetailerVisit = window.localStorage.getItem(hubConts.LOCAL_STORAGE_NAME);
                if (isLastRetailerVisit) return;

                // Creates and appends a hidden iframe to communicate with the hub
                var iframe = document.createElement('iframe');
                iframe.id = 'child-hub-retailer';
                iframe.style.display = 'none';
                iframe.src = _generateIframeSrc(_normalizeURL(hubDomain));

                // Stores the registration flag in `localStorage` and removes the iframe after 5 seconds
                iframe.onload = function () {
                    setTimeout(function () {
                        window.localStorage.setItem(hubConts.LOCAL_STORAGE_NAME, !isLastRetailerVisit);
                        iframe.remove();
                    }, 5000);
                };

                document.body.appendChild(iframe);
            }

            /**
             * Generate the source URL embed query parameter
             * @returns {string} source URL with query to embed i-frame
             */
            function _generateIframeSrc(hubDomain) {
                // Constructs a query parameter containing the current [domain] and [timestamp].
                // Parameter [blank] = 'true' will trigger to render static html
                // Add parameter [template] to avoid render from cache
                var params = new URLSearchParams({ childRetailer: window.location.origin, date: Date.now(), blank: true, template: 'hub' });
                var iframeSrc = hubDomain + '?' + params.toString();

                // Handles debugging mode by modifying the domain parameter if `?domain=` is present in the URL
                var searchQuery = new URLSearchParams(window.location.search)
                if (searchQuery.get('domain')) {
                    params.set('childRetailer', window.location.origin + '?domain=' + searchQuery.get('domain'));
                    iframeSrc = window.location.origin + '?domain=' + hubDomain + '&' + params.toString();
                }

                return iframeSrc;
            }

            /**
             * Retrieves hub-domain from window.sp
             * Check get domain from frontend | mobile browser
             * 
             * @returns {string | null}
             */
            function _getHubRetailerDomain() {
                var hubConts = SP_SERVICES.HUB_DOMAIN;
                if (!window.sp) return null

                var dataPath = hubConts.WEB_DATA
                if (!window.sp[dataPath]) {
                    dataPath = hubConts.MOBILE_DATA
                }

                return _getNestedObject(window.sp, dataPath + hubConts.LOCAL_PATH, null)
            }

            /**
             * Retrieves a nested value from an object by a dot-separated path.
             * Returning a default value if not found.
             * @param {Object} obj
             * @param {string} path
             * @param {*} defaultValue
             * @returns {*}
             */
            function _getNestedObject(obj, path, defaultValue) {
                if (!obj) return defaultValue

                var keys = path.split('.');
                for (var i = 0; i < keys.length; i++) {
                    if (!obj || typeof obj !== 'object') return defaultValue;
                    obj = obj[keys[i]];
                }
                return obj !== undefined ? obj : defaultValue;
            }

            /**
             * Normalizes a given URL
             * @param {string} url - The input URL (can be incomplete or missing parts).
             * @returns {string} - The fully normalized URL.
             */
            function _normalizeURL(url) {
                var newUrl = url.trim();
                var protocol = location.protocol + '//';
                if (location.search.indexOf('?domain=') > -1) {
                    return newUrl.replace(/^(https?:\/\/)?(www\.)?/, '');
                }

                // Assume if URL is missing and prepend 'https://'
                if (!newUrl.startsWith(protocol)) {
                    newUrl = protocol + newUrl;
                }

                // Create a URL object to easily modify components
                var urlObj = new URL(newUrl);

                // Ensure 'www.' is present at the beginning of the hostname
                if (!urlObj.hostname.startsWith('www.')) {
                    urlObj.hostname = 'www.' + urlObj.hostname;
                }

                // Return the formatted origin (protocol + hostname)
                return urlObj.origin;
            }
        },
    ]);
})(angular);
