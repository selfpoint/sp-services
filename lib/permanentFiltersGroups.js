(function (angular) {
    'use strict';

    var PRODUCT_TAGS_FILTERS = { isViewFilter: true, isViewFilterActive: true, isActive: true };

    var provider;

    function PermanentFiltersGroups($q, Api, SpProductTags) {
        var self = this,
            _initPromise,
            _initResults;

        self.init = init;
        self.get = get;

        /**
         * Inits the first groups and products tags, to figure out the total groups going forward
         * @public
         *
         * @returns {Promise<PermanentFiltersGroups>}
         */
        function init() {
            if (_initPromise) {
                return _initPromise;
            }

            return _initPromise = $q.all({
                groups: _getViewFilterGroups({from: 0, size: provider.visibleGroups * 2}),
                productTags: _getViewFilterProductTags({from: 0, size: provider.allTagsGroupsSize * provider.visibleGroups})
            }).then(function(results) {
                _initResults = results;
                return self;
            });
        }

        /**
         * Get view filters groups by the given range
         * @public
         *
         * @param {number} from
         * @param {number} size
         *
         * @return {Promise<{total: number, groups: Array<Object>}>}
         */
        function get(from, size) {
            return init().then(function() {
                return $q.all([
                    _getFilterGroupsChunk(from, size),
                    _getProductTagsGroupsChunk(from, size)
                ]);
            }).then(function(results) {
                return {
                    total: _initResults.groups.total + _getProductTagsGroupsTotal(),
                    groups: results[0].concat(results[1])
                };
            });
        }

        /**
         * Return the total groups of the 'all' product tags
         * @private
         *
         * @return {number}
         */
        function _getProductTagsGroupsTotal() {
            return (_initResults.productTags.total > 0 ? 1 : 0) +
                Math.ceil(Math.max(_initResults.productTags.total - _getProductTagsFirstChunkLength(), 0) / provider.allTagsGroupsSize)
        }

        /**
         * Returns the view filters groups by the given range
         * @private
         *
         * @param {number} from
         * @param {number} size
         *
         * @return {Promise<Array<Object>>}
         */
        function _getFilterGroupsChunk(from, size) {
            var initGroups = _initResults.groups.permanentViewFilterGroups.slice(from, from + size);
            if ((from + initGroups.length) >= _initResults.groups.total || initGroups.length === size) {
                return $q.resolve(initGroups);
            }

            return _getViewFilterGroups({
                from: from + initGroups.length,
                size: size - initGroups.length
            }).then(function(res) {
                return initGroups.concat(res.permanentViewFilterGroups);
            });
        }

        /**
         * Returns the groups of all product tags by the given range
         * @private
         *
         * @param {number} from
         * @param {number} size
         *
         * @return {Promise<Array<Object>>}
         */
        function _getProductTagsGroupsChunk(from, size) {
            var to = from + size;
            if (to < _initResults.groups.total) {
                return $q.resolve([]);
            }

            var tagsGroupsFrom = Math.max(from - _initResults.groups.total, 0),
                tagsGroupsTo = Math.max(to - _initResults.groups.total, 0),
                tagsFrom = _convertGroupIndexToProductTagsIndex(tagsGroupsFrom),
                tagsTo = _convertGroupIndexToProductTagsIndex(tagsGroupsTo);
            return $q.resolve().then(function() {
                if (tagsTo <= _initResults.productTags.productTags.length) {
                    return _initResults.productTags.productTags.slice(tagsFrom, tagsTo);
                }

                var realTagsFrom = Math.max(from, _initResults.productTags.productTags.length);
                return _getViewFilterProductTags({
                    from: realTagsFrom,
                    size: tagsTo - realTagsFrom
                }).then(function(res) {
                    return _initResults.productTags.productTags.slice(tagsFrom, realTagsFrom).concat(res.productTags);
                });
            }).then(function(productTags) {
                var groups = [];
                for (var i = 0; i < (tagsGroupsTo - tagsGroupsFrom); i++) {
                    var isFirstAll = i === 0 && tagsGroupsFrom === 0,
                        fromIndex = _convertGroupIndexToProductTagsIndex(tagsGroupsFrom + i) - tagsFrom,
                        toIndex = _convertGroupIndexToProductTagsIndex(tagsGroupsFrom + i + 1) - tagsFrom;
                    groups.push({
                        isAll: true,
                        isFirstAll: isFirstAll,
                        productTags: productTags.slice(fromIndex, toIndex).map(function(productTag) {
                            return { productTagId: productTag.id };
                        })
                    });

                    if (productTags.length <= toIndex) {
                        break;
                    }
                }
                return groups;
            });
        }

        /**
         * Returns the product tags index of the first tag in the given group index
         * @private
         *
         * @param {number} groupIndex
         *
         * @return {number}
         */
        function _convertGroupIndexToProductTagsIndex(groupIndex) {
            return Math.max(groupIndex * provider.allTagsGroupsSize - (provider.allTagsGroupsSize - _getProductTagsFirstChunkLength()), 0);
        }

        /**
         * Returns the all product tags first group tags length
         * @private
         *
         * @return {number}
         */
        function _getProductTagsFirstChunkLength() {
            return _initResults.groups.total ? provider.allTagsFirstGroupSize : provider.allTagsGroupsSize;
        }

        /**
         * Get view filter groups chunk
         * @private
         *
         * @param {Object} params
         * @param {number} params.from
         * @param {number} params.size
         *
         * @return {{total: number, permanentViewFilterGroups: Array<Object>}}
         */
        function _getViewFilterGroups(params) {
            return Api.request({
                url: '/v2/retailers/:rid/permanent-view-filters/groups',
                method: 'GET',
                params: params
            });
        }

        /**
         * Get product tags chunk
         * @private
         *
         * @param {Object} params
         * @param {number} params.from
         * @param {number} params.size
         *
         * @return {{total: number, productTags: Array<Object>}}
         */
        function _getViewFilterProductTags(params) {
            return SpProductTags.getProductTags(Object.assign(params, PRODUCT_TAGS_FILTERS));
        }
    }

    angular.module('spServices').provider('PermanentFiltersGroups', function() {
        provider = this;

        provider.visibleGroups = 4;
        provider.allTagsGroupsSize = 9;
        provider.allTagsFirstGroupSize = provider.allTagsGroupsSize - 2;

        provider.$get = ['$q', 'Api', 'SpProductTags', function($q, Api, SpProductTags) {
            return {
                create: function() {
                    return (new PermanentFiltersGroups($q, Api, SpProductTags)).init();
                }
            }
        }];
    });
})(angular);