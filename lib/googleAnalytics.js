(function (angular) {
    var ANALYTICS_CURRENCY = {
        '$': 'USD',
        '₪': 'ILS',
        '€': 'EUR',
        'KWD': 'KWD',
        '£': 'GBP'
    };

    /**
     * Provider instance
     * @property {Array/Number/String} [getLocals] - ng inject. should return { currencySymbol: {String}, googleAnalyticsId: {String}, [userId]: {Number} }
     */
    var provider = {};

    function spAnalytics($filter, $controller, $rootScope, $location, SpCartService, SP_SERVICES) {
        var self = this,
            _nameFilter = $filter('name');

        self.init = init;

        function init(companyName) {
            if (!window.ga) {
                return;
            }
            console.log('Google analytics service has been successfully initialized');
            self.companyName = companyName;

            var locals = _getLocals();

            // set e-commerce plugin
            _ga('require', 'ec');

            // set Demographics and Interest Reports
            _ga('require', 'displayfeatures');

            // set currency
            _ga('set', '&cu', ANALYTICS_CURRENCY[locals.currencySymbol] || 'USD');

            // set user id if logged in
            _setUserId();

            $rootScope.$on('$locationChangeSuccess', _onLocationChange);
            $rootScope.$on('login', _setUserId);
            $rootScope.$on('checkout', _onCheckout);
            $rootScope.$on('spAnalytics.event', _onSpAnalyticsEvent);

            var listener = $rootScope.$on('cart.update.complete', function () {
                listener();
                $rootScope.$on('cart.lines.add', _onCartLinesAdded);
                $rootScope.$on('cart.lines.remove', _onCartLinesRemoved);
            });

            window.addEventListener('error', _onError);
        }

        /**
         * Gets the locals
         * @private
         *
         * @returns {Object}
         */
        function _getLocals() {
            if (!provider.getLocals || (!angular.isFunction(provider.getLocals) && !angular.isArray(provider.getLocals))) {
                throw new Error('spAnalyticsProvider.getLocals must be implemented');
            }

            return $controller(provider.getLocals);
        }

        /**
         * Send to retailer and company analytics
         * @private
         */
        function _ga() {
            var locals = _getLocals();

            // retailer analytics
            if (locals.googleAnalyticsId) {
                window.ga.apply(this, arguments);
            }

            // company analytics
            arguments[0] = (self.companyName || 'frontend') + '.' + arguments[0];
            window.ga.apply(this, arguments);
        }

        /**
         * On location change
         * @private
         *
         * @param {Event} event
         * @param {String} [newUrl]
         * @param {String} [oldUrl]
         */
        function _onLocationChange(event, newUrl, oldUrl) {
            if (!newUrl || event.defaultPrevented) {
                return;
            }

            if (!oldUrl || newUrl.split('?')[0] === oldUrl.split('?')[0]) { // if there is changes only after "?" it means that the state doesn't change
                return _pageViewEvent();
            }

            var stopListeners = [
                $rootScope.$on('$stateChangeSuccess', function () {
                    _pageViewEvent();
                    _stopListeners(stopListeners);
                }),
                $rootScope.$on('$stateChangeError', function () {
                    _stopListeners(stopListeners);
                })
            ];
        }

        function _pageViewEvent() {
            _ga('send', 'pageview', $location.url());
        }

        function _stopListeners(stopListeners) {
            angular.forEach(stopListeners, function (stopListener) {
                stopListener();
            });
        }

        /**
         * On login
         * @private
         */
        function _setUserId() {
            var uid = _getLocals().userId;
            if (uid) {
                _ga('set', '&uid', uid);
                _ga('set', 'dimension1', uid); // send uid to allow analytics by uid
            }
        }

        /**
         * On cart lines added
         * @private
         *
         * @param {Event} event
         * @param {Object} data
         */
        function _onCartLinesAdded(event, data) {
            _addLines(data.lines);
            _ga('ec:setAction', 'add');
            _ga('send', 'event', 'UX', 'Click', 'Add To Cart');
        }

        /**
         * On cart lines removed
         * @private
         *
         * @param {Event} event
         * @param {Object} data
         */
        function _onCartLinesRemoved(event, data) {
            _addLines(data.lines);
            _ga('ec:setAction', 'remove');
            _ga('send', 'event', 'UX', 'Click', 'Remove From Cart');
        }

        /**
         * Add lines
         * @private
         *
         * @param {Array} lines
         */
        function _addLines(lines) {
            angular.forEach(lines, function(line) {
                _ga('ec:addProduct', _analyticsCartLine(line, false));
            });
        }

        /**
         * On checkout
         * @private
         *
         * @param {Event} event
         * @param {Object} order
         */
        function _onCheckout(event, order) {
            var data = _getCheckoutEventData(order);
            var isPurchaseMode = $rootScope.config && $rootScope.config.retailer && $rootScope.config.retailer.settings && $rootScope.config.retailer.settings.preventUpdatingAnalyticsAfterCollection === 'true';

            var transaction = { id: order.id };
            angular.forEach(data.chunks, function(linesChunk) {
                angular.forEach(linesChunk, function(line) {
                    _ga('ec:addProduct', line);
                });

                if (isPurchaseMode) {
                    _ga('ec:setAction', 'purchase', transaction);
                    _ga('send', 'event', 'Checkout', 'Purchase', 'items batch');
                } else {
                    _ga('ec:setAction', 'checkout', transaction);
                    _ga('send', 'event', 'Checkout', 'checkout');
                }
            });

            //Send the transaction total data
            var fullTransaction = {
                id: order.id,
                // Don't send the revenue since the batches sum the revenue
                // revenue: data.totalAmount,
                tax: data.totalTax,
                shipping: data.totalDelivery
            };

            if (isPurchaseMode) {
                _ga('ec:setAction', 'purchase', fullTransaction);
                _ga('send', 'event', 'Checkout', 'Purchase', 'transaction details');
            } else {
                _ga('ec:setAction', 'checkout', fullTransaction);
                _ga('send', 'event', 'Checkout', 'checkout');
            }
        }

        /**
         * Returns the checkout event data
         * @private
         *
         * @param {Object} order
         *
         * @return {{totalTax: number, totalDelivery: number, totalAmount: number, chunks: Array<Array<Object>>}}
         */
        function _getCheckoutEventData(order) {
            var chunks = [[]],
                isOrderLines = !!order.lines,
                index = 0,
                totalTax = 0,
                totalDelivery = 0;

            angular.forEach(order.lines || SpCartService.lines, function(line) {
                var currentChunk = chunks[chunks.length - 1];

                //break the transaction of batches of 20 items
                if (currentChunk.length === 20) {
                    currentChunk = [];
                    chunks.push(currentChunk);
                }

                currentChunk.push(_analyticsCartLine(line, isOrderLines));

                if (isOrderLines) {
                    var price = line.totalPrice || line.price * line.quantity,
                        taxAmount = (line.taxAmount || 0) + 1;
                    totalTax += price - (price / taxAmount);
                    if (line.type === SP_SERVICES.CART_LINE_TYPES.DELIVERY) {
                        totalDelivery += price;
                    }
                }

                index++;
            });

            return {
                chunks: chunks,
                totalAmount: isOrderLines ? order.totalAmount : SpCartService.total.finalPriceWithTax + SpCartService.total.deliveryCost.finalPriceWithTax,
                totalTax: isOrderLines ? totalTax : SpCartService.total.tax + SpCartService.total.deliveryCost.tax,
                totalDelivery: isOrderLines ? totalDelivery : SpCartService.total.deliveryCost.finalPriceWithTax
            };
        }

        /**
         * On sp analytics event
         * @private
         *
         * @param {Event} event
         * @param {Object} data
         */
        function _onSpAnalyticsEvent(event, data) {
            if (!data.category || !data.action) {
                throw new Error('spAnalytics.event emitted but there is no category or action on the event');
            }

            _ga('send', 'event', data.category, data.action, data.label);
        }

        /**
         * On error
         * @private
         *
         * @param {Event} event
         */
        function _onError(event) {
            _ga('send', 'exception', {
                exDescription: (event.filename + ':' + event.lineno + ':' + event.colno + ' ' + event.message).substring(0, 150), // make sure it's no longer than 150
                exFatal: true
            });
        }

        /**
         * Generate analytics cart line
         * @private
         *
         * @param {Object} line
         * @param {boolean} isOrderLines - is order line or cart service lines
         *
         * @returns {Object}
         */
        function _analyticsCartLine(line, isOrderLines) {
            var quantityByWeight = SpCartService.isUnitsWeighable(line) ? line.product.weight : 1;
            var analyticsLine = {
                id: line.product.id,
                name: _nameFilter(line.product.names).long,
                category: line.product.family && line.product.family.categories && line.product.family.categories[0] ? _nameFilter(line.product.family.categories[0].names) : null,
                brand: _nameFilter(line.product.brand && line.product.brand.names)
            };

            if (isOrderLines) {
                analyticsLine.price = line.totalPrice ? line.totalPrice / line.quantity : line.price;
                analyticsLine.quantity = line.quantity;
            } else {
                analyticsLine.price = (line.finalPriceWithTax / (line.quantity * quantityByWeight)) || (line.product.branch && line.product.branch.regularPrice) || 0;
                analyticsLine.quantity = Math.ceil(line.quantity * quantityByWeight);
            }

            return analyticsLine;
        }
    }

    angular.module('spServices')
        .config(['$provide', function ($provide) {
            provider = $provide.service('spAnalytics', [
                '$filter', '$controller', '$rootScope', '$location', 'SpCartService', 'SP_SERVICES',
                spAnalytics
            ]);
        }]);
})(angular);
