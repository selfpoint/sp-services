(function (angular) {
    'use strict';

    var EVENTS = {
        VIEW_ITEM_LIST: 'productImpressions',
        VIEW_ITEM: 'productDetail',
        SELECT_ITEM: 'productClick',
        SEARCH: 'searchProduct',
        ADD_TO_CART: 'addToCart',
        REMOVE_FROM_CART: 'removeFromCart',
        CHECKOUT: 'checkout',
        PURCHASE: 'purchase',
        CLUB_MEMBER_USE: 'clubMembersUse',
        ADD_TO_WISH_LIST: 'addProductToWishlist',
        VIEW_LOGIN: 'loginView',
        LOGIN: 'loginSuccess',
        ERROR_LOGIN: 'loginFail',
        VIEW_SIGN_UP: 'registrationView',
        SIGN_UP: 'registrationSuccess',
        ERROR_SIGN_UP: 'registrationFail',
        VIEW_CONTACT_US: 'contactUsView',
        CONTACT_US: 'contactUsSuccess',
        ERROR_CONTACT_US: 'contactUsFail',
        VIEW_PROMOTION: 'promotionView',
        SELECT_PROMOTION: 'promotionClick',
        SELECT_CONTENT: 'selectContent',
        PAGE_VIEW: 'pageView'
    };

    var EVENTS_WITH_VALUES = {productDetail: true, productClick: true, addToCart: true, removeFromCart: true, checkout: true};

    // used to filter path that includes user data. i.e - in mobile, on purchase, the path includes the transaction id.
    var PATH_FILTER = {
        'app.cart.checkout.finish': '/cart/checkout/finish'
    };

    var MODES = {
        GTM: 1,
        FIREBASE: 2,
        UNDETERMINED: 0
    };

    var GA_PARAMS = {
        PLATFORM: 'ga_gp',
        MODE: 'ga_gm',
        CONSENT: 'ga_gc',
        CLIENT_ID: 'ga_gci',
        SESSION_ID: 'ga_gsi',
        ERRORS: 'ga_err',
        WARNINGS: 'ga_warn',
        DL_LEN: 'dl_len'
    };

    var PLATFORMS = {
        FRONTEND: 'front',
        MOBILE_WEB: 'mobile',
        ANDROID: 'android',
        IOS: 'ios',
        UNDETERMINED: 'undetermined'
    };

    var PII_FIELDS = ['userEmail'];

    angular.module('spServices').service('DataLayer', ['$injector', '$filter', '$window', '$rootScope', '$location', '$q', '$state', '$timeout', '$cookies', 'Config', 'User', 'LocalStorage', 'Api',
        function ($injector, $filter, $window, $rootScope, $location, $q, $state, $timeout, $cookies, config, user, localStorageService, Api) {
            var self = this,
                _platform = undefined,
                _mode = undefined,
                _isUserConsentGiven = undefined,
                _userId = undefined,
                _errors = {},
                _warnings = {},
                _listeners = [],
                _defaultEmptyValue = undefined,
                _defaultEmptyBrand = 'no_brand',
                _defaultBranchId = 0,
                _translateFilter = $filter('translate'),
                _currencyFilter = $filter('currency'),
                _nameFilter = $filter('name'),
                _regularPriceFilter = $filter('regularPrice'),
                _productNameFilter = $filter('productName'),
                _eventsBuilder = undefined,
                _ignoredEvents = {},
                _isCartUpdateComplete = undefined,
                _serverEvents = {},
                _checkoutMode = false;

            self.push = push;
            self.pushDebug = pushDebug;
            self.getGaParams = getGaParams;
            self.EVENTS = EVENTS;

            _init();

            function push(event, payload, remoteLogging) {
                var eventData;
                var preparedEvents;

                try {
                    if (_checkoutMode && !(event === EVENTS.PURCHASE || event === EVENTS.PAGE_VIEW)) {
                        return;
                    }
                    if (!_isUserConsentGiven || _ignoredEvents[event]) {
                        pushDebug('analytics info on skipping event ' + (event || 'unknown'), remoteLogging)
                        return;
                    }
                    eventData = _prepareEventData(event, payload);
                    if (!_isObject(eventData)) {
                        pushDebug('analytics warning on preparing event ' + (event || 'unknown'), remoteLogging);
                        return;
                    }
                }
                catch (error) {
                    pushDebug('analytics error on preparing event ' + (event || 'unknown'), remoteLogging);
                    _setWarning('prepare-event')
                    return;
                }

                try {
                    preparedEvents = _eventsBuilder[event](eventData);
                    if (!_isObject(preparedEvents)) {
                        pushDebug('analytics warning on building event ' + (event || 'unknown'), remoteLogging);
                        return;
                    }
                    else if (preparedEvents.skip) {
                        return;
                    }
                    else if (!Array.isArray(preparedEvents)) {
                        preparedEvents = [preparedEvents];
                    }
                }
                catch (error) {
                    pushDebug('analytics error on building event ' + (event || 'unknown'), remoteLogging);
                    _setWarning('build-event');
                    return;
                }

                _prepareEnrichmentData()
                    .then(function(enrichmentData) {
                        _eventsBuilder.enrichEvents(preparedEvents, enrichmentData);
                        if (enrichmentData.withError) {
                            return $q.reject();
                        }
                    }).catch(function(error) {
                    preparedEvents.forEach(function(event) {
                        event['error_description'] = 'enriching event';
                    });
                    pushDebug('analytics error on enriching event ' + event, remoteLogging);
                    _setWarning('enrich');
                }).then(function() {
                    angular.forEach(preparedEvents, function (event) {
                        var error;
                        if (_mode === MODES.GTM) {
                            error = _pushGTM(event);
                        }
                        else if (_mode === MODES.FIREBASE) {
                            error = _pushFirebase(event);
                        }

                        if (remoteLogging) {
                            var debugMetadata = _getDebugData(event);
                            var payload = {
                                message: 'analytics ' + (error ? 'error' : 'info') + ' on pushing event ' + event.event,
                                metadata: debugMetadata.metadata,
                                userId: debugMetadata.userId,
                                event: debugMetadata.event,
                                error: error && error.message
                            }

                            _remoteLog(payload);
                        }
                    });
                }).catch(function() {
                        pushDebug('analytics error on pushing event ' + event, remoteLogging);
                        _setWarning('push-event-' + event);
                });
            }

            function pushDebug(message, remoteLogging) {
                var debugMetaData = _getDebugData();
                if (remoteLogging) {
                    _remoteLog({message: message, metadata: debugMetaData.metadata, userId: debugMetaData.userId});
                }
                if (!_isUserConsentGiven) {
                    return;
                }

                else if (_mode === MODES.GTM) {
                    _pushGTM({event: 'sp_debug', sp_message: message, sp_metadata: debugMetaData.metadata});
                }
                else if (_mode === MODES.FIREBASE) {
                    _pushFirebase({event: 'sp_debug', sp_message: message, sp_metadata: debugMetaData.metadata});
                }
            }

            function getGaParams() {
                try {
                    if (!_isRetailerDataLayerEventsEnabled()) {
                        if (_mode !== undefined) {
                            pushDebug('analytics error on getting ga params a');
                            _clearObject(_warnings);
                            return $q.resolve({'err': 'gaparams-a'});
                        }
                        return $q.resolve();
                    }
                }
                catch (error) {
                    pushDebug('analytics error on getting ga params b');
                    _clearObject(_warnings);
                    return $q.resolve({'err': 'gaparams-b'});
                }

                return $q.all({
                    clientId: _getClientId(500),
                    sessionId: _getSessionId(500)
                }).then(function(results) {
                    var gaParams = {d:1};

                    gaParams[GA_PARAMS.PLATFORM] = _platform;
                    gaParams[GA_PARAMS.MODE] = _mode;
                    gaParams[GA_PARAMS.CONSENT] = _isUserConsentGiven;
                    gaParams[GA_PARAMS.CLIENT_ID] = results.clientId || _defaultEmptyValue;
                    gaParams[GA_PARAMS.SESSION_ID] = results.sessionId || _defaultEmptyValue;
                    gaParams[GA_PARAMS.ERRORS] =  _joinObjectKeys(_errors);
                    gaParams[GA_PARAMS.WARNINGS] = _joinObjectKeys(_warnings);
                    gaParams[GA_PARAMS.DL_LEN] = $window.dataLayer && $window.dataLayer.length;

                    return gaParams;
                }).catch(function(error) {
                    pushDebug('analytics error on getting ga params c');
                    return {'err': 'gaparams-c'};
                }).finally(function() {
                    _clearObject(_warnings);
                });
            }

            function _getDebugData(event) {
                var debugData = {
                    metadata: ('platform:' + _platform + ';mode:' + _mode + ';consent:' + _isUserConsentGiven)
                };

                if (event) {
                    try {
                        debugData.event = JSON.stringify(event);
                    }
                    catch (error) {
                        debugData.event = 'json error - ' + error.message;
                    }
                }
                if (_userId) {
                    debugData.userId = _userId;
                }
                return debugData;
            }

            function _remoteLog(payload) {
                try {
                    Api.request({
                        method: 'POST',
                        url: '/v2/retailers/:rid/analytic-services/_log',
                        data: payload
                    });
                }
                catch (error) {}
            }

            function _pushGTM(event) {
                try {
                    var flushObject = _prepareFlushObject();
                    $window.dataLayer = $window.dataLayer || [];
                    $window.dataLayer.push(flushObject);
                    $window.dataLayer.push(event);
                }
                catch (error) {
                    _setWarning('push-gtm');
                    return {message: error.message};
                }
            }

            function _pushFirebase(event) {
                try {
                    var eventName = event.event;
                    var eventEcommerceParams = event.ecommerce;
                    delete event.event;
                    delete event.ecommerce;

                    Object.assign(event, eventEcommerceParams);

                    $window.cordova.plugins.firebase.analytics.logEvent(eventName, event);
                }
                catch (error) {
                    _setWarning('push-firebase');
                    return {message: error.message};
                }
            }

            function _prepareEventData(event, payload) {
                payload = payload || {};

                var eventData = {
                    data: _prepareMetadata(payload.data),
                    purchase: _preparePurchase(payload.purchase),
                    wishListItem: _prepareWishListItem(payload.wishListItem),
                }

                eventData.data.event = event;
                eventData.products = _prepareProducts(payload.products, eventData.data);
                eventData.promotion = _preparePromotion(payload.promotion, eventData.data);
                if (EVENTS_WITH_VALUES[event]) {
                    eventData.values = _prepareValues(eventData.products);
                }

                return eventData;
            }

            function _prepareValues(products) {
                if (!Array.isArray(products)) return;

                var productsValue = 0;
                angular.forEach(products, function (product) {
                    var productValue = (product.price - (product.discount || 0)) * product.quantity;
                    productsValue += productValue;
                });

                return {
                    itemValue: _parseNumber(products[0].price - (products[0].discount || 0), 2),
                    cartItemValue: _parseNumber((products[0].price - (products[0].discount || 0)) * products[0].quantity, 2),
                    checkoutValue: _parseNumber(productsValue, 2)
                }
            }

            function _prepareMetadata(data) {
                data = data || {};

                return {
                    location: _parseString(_getPath(data.toState)),
                    searchTerm: _parseString(data.query),
                    resultsCount: data.results >= 0 ? _parseNumber(data.results) : _defaultEmptyValue,
                    quantity: data.quantity > 0 ? _parseNumber(data.quantity, 2) : _defaultEmptyValue,
                    step: data.step > 0 ? _parseNumber(data.step): _defaultEmptyValue,
                    option: _parseString(data.option),
                    clubMemberAmount: _parseNumber(data.clubMemberAmount, 2),
                    email: _parseString(data.email),
                    error: _errToString(data.error),
                    type: _parseString(data.type),
                    category: _parseString(data.category),
                    action: _parseString(data.action),
                    label: _parseString(_nameFilter(data.label)),
                    productsStartingPosition: data.productsStartingPosition >= 0 ? _parseNumber(data.productsStartingPosition) : 0,
                    cartUpdateComplete: _isCartUpdateComplete,
                    serverEvents: _serverEvents
                }
            }

            function _prepareEnrichmentData() {
                var enrichmentData = {};
                var userSession = _defaultEmptyValue;
                var isFirst = _defaultEmptyValue;
                var withError = false;
                var branchId = _defaultEmptyValue;
                var frontendBranchId = _defaultEmptyValue;
                var mobileZuzBranchId = _defaultEmptyValue;

                try {
                    userSession = localStorageService.getItem('session');
                    isFirst = localStorageService.getItem('isFirstUserLogin');
                    frontendBranchId = localStorageService.getItem('branchId');
                    mobileZuzBranchId = localStorageService.getItem('mobileZuzBranchId');
                    branchId = frontendBranchId ? frontendBranchId : (mobileZuzBranchId ? mobileZuzBranchId : _defaultBranchId);
                }
                catch (error) {
                    pushDebug('analytics error on storage in prepare enrichment data')
                    withError = true;
                }

                return user.getData()
                    .then(function(userData) {
                        enrichmentData.userEmail = _parseString((userData && userData.email) || (userSession && userSession.username));
                        enrichmentData.userId = _parseString((userData && userData.id) || (userSession && userSession.userId));
                        enrichmentData.purchaseCounter = _parseNumber((userData && userData.purchaseCounter) || (userSession && userSession.purchaseCounter) || 0);
                        if (config.retailer && config.retailer.settings && config.retailer.settings.enableSendingExternalUserId === 'true') {
                            enrichmentData.foreignId = userData ? _parseString(userData.externalForeignId) : _defaultEmptyValue;
                        }
                    }).catch(function(error) {
                        if ($rootScope.USER_ERRORS && $rootScope.USER_ERRORS.NOT_LOGGED_IN !== error) {
                            pushDebug('analytics error on user data');
                            withError = true;

                            enrichmentData.userEmail = enrichmentData.userEmail || _parseString(userSession && userSession.username);
                            enrichmentData.userId = enrichmentData.userId || _parseString(userSession && userSession.userId);
                            enrichmentData.purchaseCounter = enrichmentData.purchaseCounter || _parseNumber(userSession && userSession.purchaseCounter || 0);
                            enrichmentData.foreignId = enrichmentData.foreignId ? enrichmentData.foreignId : _defaultEmptyValue;
                        }
                    }).then(function() {
                        enrichmentData.currencyCode = _parseString(config.retailer && config.retailer.currencyCode);
                        if (!enrichmentData.currencyCode) {
                            return $q.reject();
                        }
                    }).catch(function(error) {
                        pushDebug('analytics error on currency');
                        withError = true;
                    }).then(function() {
                        enrichmentData.isFirst = typeof(isFirst) === 'boolean' ? _parseString(isFirst) : _defaultEmptyValue;
                        enrichmentData.branchId = _parseString(branchId);
                    }).then(function() {
                        _clearPIIData(enrichmentData);
                    }).catch(function(error) {
                        pushDebug('analytics error on pii data');
                        return $q.reject('critical error');
                    }).then(function() {
                        if (withError) {
                            enrichmentData.withError = true;
                        }
                        return enrichmentData;
                    });
            }

            function _preparePurchase(purchase) {
                if (!_isObject(purchase)) return;

                return {
                    id: _parseString(purchase.id),
                    revenue: _parseNumber(purchase.revenue, 2),
                    shippingTier: _parseString(purchase.affiliation),
                    tax: _parseNumber(purchase.tax, 2),
                    shipping: _parseNumber(purchase.shipping, 2),
                    coupon: _parseString(purchase.coupon),
                    paymentType: _parseString(purchase.paymentType)
                }
            }

            function _prepareWishListItem(wishListItem) {
                if (!_isObject(wishListItem)) return;

                var wishListItemData = _prepareWishListItemData(wishListItem);
                var preparedWishListItem = _eventsBuilder.prepareWishListItem(wishListItemData);
                return preparedWishListItem;
            }

            function _prepareWishListItemData(wishListItem) {
                var item = {
                    name: _defaultEmptyValue,
                    price: _parseNumber(wishListItem.price, 2)
                }

                if (wishListItem.names || wishListItem.branch) {
                    var names = wishListItem.names || (wishListItem.branch ? wishListItem.branch.names : null);
                    var name = _nameFilter(names);
                    if (name && name.short) {
                        item.name = _parseString(name.short);
                    }
                }

                return item;
            }

            function _prepareProducts(products, data) {
                if (!Array.isArray(products)) return;

                var preparedProducts = [];
                var currentProductPosition = data.productsStartingPosition;
                var isRealIndexPosition = ['addToCart', 'productImpressions', 'productClick', 'productDetail'].includes(data.event);

                angular.forEach(products, function(product) {
                    product = (product && product.item) || product;
                    if (!_isObject(product)) return;

                    var productPosition = isRealIndexPosition ? (product.indexPosition || 0) : currentProductPosition;
                    var productData = _prepareProductData(product, productPosition);
                    if (!_isObject(productData)) return;

                    var preparedProduct = _eventsBuilder.prepareProduct(data.event, productData)
                    if (!_isObject(preparedProduct)) return;

                    preparedProducts.push(preparedProduct);
                    currentProductPosition++;
                });

                if (preparedProducts.length > 0) {
                    return preparedProducts;
                }
                else {
                    return;
                }
            }

            function _prepareProductData(product, currentProductPosition) {
                var productQuantity = 1;
                if (product.singleLine && product.singleLine.quantity !== 0) {
                    productQuantity = product.singleLine.quantity;
                }
                var productPrice = !!product.isWeighable ? productQuantity * product.branch.regularPrice : _parseNumber(_regularPriceFilter(product, product.isCaseMode), 2);
                var quantity = !!product.isWeighable ? 1 : _parseNumber(productQuantity);
                var productData = {
                    'currentProductPosition': currentProductPosition,
                    'currentCategory': (product.family && product.family.categoriesPaths && product.family.categoriesPaths[0] && product.family.categoriesPaths[0][0]) || (product.categories && product.categories[0]) || {names: []},
                    'brandName':  product.brand && _parseString(_nameFilter(product.brand.names)) || _defaultEmptyBrand,
                    'productName': _productNameFilter(product, product.isCaseMode) !== 'undefined' ? _parseString(_productNameFilter(product, product.isCaseMode) || product.localName) : _defaultEmptyValue,
                    'coupon': _parseString(product.coupon),
                    'affiliation': _parseString(product.affiliation),
                    'price': productPrice,
                    'currentDiscount': _defaultEmptyValue,
                    'currentDiscountPrecent': _defaultEmptyValue,
                    'currentDiscountPerCartQuantity': _defaultEmptyValue,
                    'id': _parseString(product.id),
                    'isWeighable': !!product.isWeighable,
                    'unitResolution': _parseNumber(product.unitResolution, 2),
                    'weight': _parseNumber(product.weight, 2),
                    'quantity': quantity,
                    'cartQuantity': _defaultEmptyValue,
                    'addedFrom': _parseString(product.addedFrom),
                    'itemWeight': !!product.isWeighable ? product.unitResolution * productQuantity : _defaultEmptyValue
                }

                productData.pricePerCartQuantity = _parseNumber(_valuePerCartQuantity(productData.price, productData.isWeighable, productData.weight), 2);
                productData.category = _parseString(_nameFilter(productData.currentCategory.names));

                if (product.branch && product.branch.regularPrice && product.branch.salePrice >= 0) {
                    productData.currentDiscount = _parseNumber(product.branch.regularPrice - product.branch.salePrice, 2);
                    productData.currentDiscountPrecent = _parseNumber((product.currentDiscount * 100) / product.branch.regularPrice, 2);
                    productData.currentDiscountPerCartQuantity = _parseNumber(_valuePerCartQuantity(productData.currentDiscount, productData.isWeighable, productData.weight), 2);
                }
                if (product.singleLine && product.singleLine.quantity >= 0) {
                    productData.cartQuantity = _parseNumber(Math.abs(product.singleLine.quantity - ((product.singleLine.oldQuantity >= 0 && product.singleLine.oldQuantity) || 0)) || product.singleLine.quantity) || 1;
                }

                return productData;
            }

            function _preparePromotion(promotion, data) {
                if (!_isObject(promotion)) return;

                var promotionData = _preparePromotionData(promotion, data.type);
                if (!_isObject(promotionData)) return;

                var preparedPromotion = _eventsBuilder.preparePromotion(promotionData);
                if (!_isObject(preparedPromotion)) return;

                return preparedPromotion;
            }

            function _preparePromotionData(promotion, type) {
                var currentPromotion = promotion;

                var promotionData = {
                    creativeUrl: _parseString(currentPromotion.pictureUrl || currentPromotion.image || currentPromotion.url || ''), // Different promotion types have different object properties
                    creativeName: _parseString(currentPromotion.name || ('Untitled ' + type).trim()),
                    id: _parseString(promotion.id),
                    type: _parseString(type || '')
                }

                return promotionData;
            }

            function _prepareFlushObject() {
                var flushObject = {ecommerce: null, sp_message: null, sp_metadata: null, error_description: null};

                if (_eventsBuilder.CUSTOM_PARAMS && Array.isArray(_eventsBuilder.CUSTOM_PARAMS)) {
                    _eventsBuilder.CUSTOM_PARAMS.forEach(function(param) {
                        flushObject[param] = null;
                    });
                }

                return flushObject;
            }

            function _valuePerCartQuantity(value, isWeighable, weight) {
                if (value && isWeighable && weight) {
                    return value * weight;
                }
                else {
                    return value;
                }
            }

            function _parseNumber(number, decimalPlaces) {
                try {
                    decimalPlaces = decimalPlaces || 0;
                    var type = typeof(number);
                    if (!(type === 'number' || type === 'string')) {
                        return _defaultEmptyValue;
                    }
                    var number = Number((+number).toFixed(decimalPlaces));
                    if (Number.isNaN(number) ) {
                        return _defaultEmptyValue;
                    }
                    return number;
                }
                catch (error) {
                    pushDebug('analytics error on parsing number');
                    _setWarning('parse-number');
                    return _defaultEmptyValue;
                }
            }

            function _parseString(string, maxLength) {
                try {
                    maxLength = maxLength || 100 //max param value for ga4
                    if (string === undefined || string === null || string === '') {
                        return _defaultEmptyValue;
                    }

                    var parsedString = typeof(string) === 'string' ? string : string.toString();
                    if (parsedString.length > maxLength) {
                        return parsedString.slice(0, maxLength);
                    }
                    return parsedString;
                }
                catch (error) {
                    pushDebug('analytics error on parsing string');
                    _setWarning('parse-string');
                    return _defaultEmptyValue;
                }
            }

            function _errToString(err) {
                try {
                    return (err && err.response && err.response.error && err.response.error.toString()) || (err && err.message && err.message.toString()) || (typeof err === 'string' && err) || _defaultEmptyValue;
                }
                catch (error) {
                    pushDebug('analytics error on error to string');
                    _setWarning('err-to-string');
                    return _defaultEmptyValue;
                }
            }

            function _resetModel() {
                try {
                    if (_mode === MODES.GTM) {
                        $window.dataLayer = $window.dataLayer || [];
                        $window.dataLayer.push(function() {
                            this.reset();
                        });
                    }
                }
                catch (error) {
                    pushDebug('analytics error on resetting model');
                    _setError('reset-model');
                }
            }

            function _clearPIIData(obj) {
                if (_mode === MODES.FIREBASE) {
                    for (var paramIndex in PII_FIELDS) {
                        obj && obj[PII_FIELDS[paramIndex]] && delete obj[PII_FIELDS[paramIndex]];
                    }
                }
            }

            function _stopListeners(stopListeners) {
                try {
                    angular.forEach(stopListeners, function (stopListener) {
                        stopListener();
                    });
                }
                catch (error) {
                    pushDebug('analytics error on stopping listeners');
                    _setError('stop-listeners');
                }
            }

            function _joinObjectKeys(obj) {
                var joinedMessages = Object.keys(obj).join(';');
                if (joinedMessages.length) {
                    return joinedMessages
                }
            }

            function _clearObject(obj) {
                try {
                    Object.keys(obj).forEach(function(key) {
                        delete obj[key];
                    })
                }
                catch (error) {}
            }

            function _isObject(object) {
                return angular.isObject(object);
            }

            function _isGtagExists() {
                return typeof($window.gtag) === 'function';
            }

            function _isRetailerDataLayerEventsEnabled() {
                return (config && config.retailer && config.retailer.settings && config.retailer.settings.isDataLayerEventActive === 'true');
            }

            function _isLegacyMode() {
                return (config && config.retailer && config.retailer.settings && config.retailer.settings.dataLayerEventMode === 'ga4' ? false : true);
            }


            function _isIos() {
                return ($window.cordova && $window.cordova.platformId === PLATFORMS.IOS);
            }

            function _onCookieWallEvent() {
                _setConsent();
                _setUserId();
            }

            function _onUserLogin() {
                _setUserId();
            }

            function _onUserLogout() {
                _setUserId();
                _resetModel();
            }

            function _onLocationChange(event, toState, toParams, fromState, fromParams) {
                if (!event || event.defaultPrevented) {
                    return;
                }

                self.push(EVENTS.PAGE_VIEW, {data: {toState: (toState && toState.name)}});
            }

            function _onCheckout() {
                _checkoutMode = true;
                $timeout(function() {
                    _checkoutMode = false;
                }, 10000);
            }

            function _onCheckoutFinished() {
                _checkoutMode = false;
            }

            function _setPlatform() {
                var platform;
                try {
                    if ($window.sp && $window.sp.frontendData) {
                        platform = PLATFORMS.FRONTEND;
                    }
                    else if ($window.cordova && $window.cordova.platformId) {
                        platform = $window.cordova.platformId;
                    }
                    else if ($window.sp && $window.sp.mobileData) {
                        platform = PLATFORMS.MOBILE_WEB;
                    }
                    else {
                        throw new Error('analytics error on setting platform');
                    }
                }
                catch (error) {
                    platform = PLATFORMS.UNDETERMINED;
                    _setError('platform');
                }

                _platform = platform;

            }

            function _setMode() {
                var mode;
                try {
                    if ($window.cordova && $window.cordova.plugins && $window.cordova.plugins.firebase && $window.cordova.plugins.firebase.analytics && !_isLegacyMode()) {
                        mode = MODES.FIREBASE;
                    }
                    else if (config.retailer && config.retailer.googleTagManagerId && _isGtagExists()) {
                        mode = MODES.GTM;
                    }
                    else {
                        throw new Error('analytics error on setting mode');
                    }
                }
                catch (error) {
                    mode = MODES.UNDETERMINED;
                    _setError('mode');
                }

                _mode = mode;
            }

            function _setSpecialEvents() {
                try {
                    if (config && config.retailer && config.retailer.settings) {
                        _serverEvents.purchase = config.retailer.settings.isDataLayerServerPurchaseEventActive === 'true';
                        _ignoredEvents.selectContent = config.retailer.settings.isDataLayerSelectContentActive === 'false';
                        _ignoredEvents.pageView = config.retailer.settings.isDataLayerPageviewEventActive === 'false';
                    }
                }
                catch (error) {
                    pushDebug('analytics error on setting ignored events');
                    _setError('ignored-events');
                }
            }

            function _setConsent() {
                var isUserConsent;
                try {
                    var userApprovedCookies = localStorageService.getItem('approvedCookies');
                    isUserConsent = ((config && config.retailer && !config.retailer.isCookieWallEnabled && !_isIos()) || (userApprovedCookies && userApprovedCookies.googleAnalytics)) ? true : false;
                    if (_mode === MODES.GTM || _isGtagExists()) {
                        _setConsentGTM(isUserConsent);
                    }
                    if (_mode === MODES.FIREBASE) {
                        _setConsentFirebase(isUserConsent);
                    }
                }
                catch (error) {
                    isUserConsent = false;
                    pushDebug('analytics error on setting consent');
                    _setError('consent');
                }

                _isUserConsentGiven = isUserConsent;
            }

            function _setConsentGTM(isUserConsent) {
                if (isUserConsent) {
                    gtag('consent', 'update', {
                        'ad_storage': 'granted',
                        'analytics_storage': 'granted'
                    });
                }
                else {
                    gtag('consent', 'update', {
                        'ad_storage': 'denied',
                        'analytics_storage': 'denied'
                    });
                }
            }

            function _setConsentFirebase(isUserConsent) {
                // plugin crashes and causes app to crash if null or undefined is passed - use bool only
                $window.cordova.plugins.firebase.analytics.setEnabled(isUserConsent ? true : false);
            }

            function _setUserId() {
                user.getData().then(function (userData) {
                    _userId = (userData && _parseString(userData.id));
                    if (!_userId) {
                        return $q.reject('analytics error on getting user id');
                    }
                }).catch(function(error) {
                    _userId = null;
                    if ($rootScope.USER_ERRORS && $rootScope.USER_ERRORS.NOT_LOGGED_IN !== error) {
                        pushDebug('analytics error on getting user id');
                        _setError('userid-get');
                    }
                }).then(function() {
                    if (!_isUserConsentGiven) {
                        return;
                    }
                    else if (_mode === MODES.GTM) {
                        _setUserIdGTM();
                    }
                    else if (_mode === MODES.FIREBASE) {
                        _setUserIdFirebase();
                    }
                }).catch(function(error) {
                    pushDebug('analytics error on setting user id');
                    _setError('userid-set');
                });
            }

            function _setUserIdGTM() {
                _pushGTM({'user_id': _userId});
            }

            function _setUserIdFirebase() {
                $window.cordova.plugins.firebase.analytics.setUserId(_userId);
            }

            function _setError(errorIn) {
                try {
                    _errors[errorIn] = true;
                }
                catch (error) {}
            }

            function _setWarning(warningIn) {
                try {
                    _warnings[warningIn] = true;
                }
                catch (error) {}
            }

            function _getPath(toState) {
                return PATH_FILTER[toState] || $location.path() || $window.location.pathname || $window.location.href;
            }

            function _getClientId(timeout) {
                if (_mode === MODES.GTM && config.retailer.googleAnalyticsId) {
                    var clientId = _getClientIdStorage();
                    if (clientId) {
                        return clientId;
                    }
                    return _getClientIdGtag(timeout);
                }
                else if (_mode === MODES.FIREBASE) {
                    return _getClientIdFirebase(timeout);
                }
                else {
                    pushDebug('analytics error on getClientId');
                    _setWarning('getClientId');
                }
            }

            function _getClientIdStorage() {
                try {
                    var cookieName = '_ga';
                    var pattern = /GA\d\.\d\.(.+)/;
                    var cookie = $cookies.get(cookieName);
                    var match = cookie && cookie.match(pattern)
                    if (match && match[1]) {
                        return match[1];
                    }
                    else {
                        pushDebug('analytics error on getting client id cookie');
                        _setWarning('clientid-cookie');
                    }
                }
                catch (error) {
                    pushDebug('analytics error on getting client id getClientIdStorage');
                    _setWarning('clientid-getClientIdStorage');
                }
            }

            function _getClientIdGtag(timeout) {
                var defer = $q.defer();
                var visited = false;
                gtag('get', config.retailer.googleAnalyticsId, 'client_id', function (client_id) {
                    if (visited) return;
                    $timeout.cancel(timeout);
                    if (!client_id && config.retailer.googleAnalyticsId.startsWith('G-')) {
                        pushDebug('analytics error on getting client id gtm');
                        _setWarning('clientid-gtm');
                    }
                    defer.resolve(client_id);
                });
                var timeout = $timeout(function() {
                    pushDebug('analytics error on gtag callback client id gtm');
                    _setWarning('gtag-clientid-gtm');
                    visited = true;
                    defer.resolve();
                }, timeout || 0);

                return defer.promise;
            }

            function _getClientIdFirebase(timeout) {
                var defer = $q.defer();
                var visited = false;
                $window.cordova.plugins.firebase.analytics.getAppInstanceId().then(function(appInstanceId) {
                    if (visited) return;
                    $timeout.cancel(timeout);
                    if (!appInstanceId) {
                        pushDebug('analytics error on getting client id firebase');
                        _setWarning('clientid-firebase')
                    }
                    defer.resolve(appInstanceId);
                });
                var timeout = $timeout(function() {
                    pushDebug('analytics error on getId promise client id firebase');
                    _setWarning('getId-clientid-firebase');
                    visited = true;
                    defer.resolve();
                }, timeout || 0);

                return defer.promise;
            }

            function _getSessionId(timeout) {
                if (_mode === MODES.GTM && config.retailer.googleAnalyticsId) {
                    var sessionId = _getSessionIdStorage();
                    if (sessionId) {
                        return sessionId;
                    }
                    return _getSessionIdGtag(timeout);
                }
                else if (_mode !== MODES.FIREBASE) {
                    pushDebug('analytics error on getSessionId');
                    _setWarning('getSessionId');
                }
            }

            function _getSessionIdStorage() {
                try {
                    var cookieName = '_ga_' + config.retailer.googleAnalyticsId.slice(2);
                    var pattern = /GS\d\.\d\.(\w+)/;
                    var cookie = $cookies.get(cookieName);
                    var match = cookie && cookie.match(pattern)
                    if (match && match[1]) {
                        return match[1];
                    }
                    else {
                        pushDebug('analytics error on getting session id cookie');
                        _setWarning('sessionid-cookie');
                    }
                }
                catch (error) {
                    pushDebug('analytics error on getting session id getSessionIdStorage');
                    _setWarning('sessionid-getSessionIdStorage');
                }
            }

            function _getSessionIdGtag(timeout) {
                var defer = $q.defer();
                var visited = false;
                gtag('get', config.retailer.googleAnalyticsId, 'session_id', function (session_id) {
                    if (visited) return;
                    $timeout.cancel(timeout);
                    if (!session_id && config.retailer.googleAnalyticsId.startsWith('G-')) {
                        pushDebug('analytics error on getting session id gtm');
                        _setWarning('sessionid-gtm')
                    }
                    defer.resolve(session_id);
                });
                var timeout = $timeout(function() {
                    pushDebug('analytics error on gtag callback session id gtm');
                    _setWarning('gtag-sessionid-gtm')
                    visited = true;
                    defer.resolve();
                }, timeout || 0);

                return defer.promise;
            }

            function _init() {
                config.waitForInit().then(function() {
                    if (!_isRetailerDataLayerEventsEnabled()) {
                        return;
                    }

                    _setPlatform();
                    _setMode();
                    if (_mode === MODES.UNDETERMINED) {
                        return;
                    }

                    var isLegacyMode = _isLegacyMode();
                    _eventsBuilder = $injector.get(isLegacyMode ? 'Ga3' : 'Ga4');

                    _listeners.push(
                        $rootScope.$on('tracking.permissions.updated', _onCookieWallEvent),
                        $rootScope.$on('login', _onUserLogin),
                        $rootScope.$on('logout', _onUserLogout),
                        $rootScope.$on('$stateChangeSuccess', _onLocationChange),
                        $rootScope.$on('checkout', _onCheckout),
                        $rootScope.$on('checkoutFinished', _onCheckoutFinished)
                    );

                    _isCartUpdateComplete = false;
                    var listener = $rootScope.$on('cart.update.complete', function () {
                        listener();
                        _isCartUpdateComplete = true;
                    });

                    _setSpecialEvents();
                    _setConsent();
                    _setUserId();
                }).catch(function(error) {
                    pushDebug('analytics error on init');
                    _setError('init');
                    _stopListeners(_listeners);
                });
            }
        }]);

})(angular);
