(function (angular) {
    'use strict';

    var CUSTOM_PARAMS = [
        'content_type',
        'content_category',
        'content_label',
        'club_member_amount',
        'search_term',
        'results',
        'results_count',
        'page_location',
        'is_first',
        'purchase_counter',
        'foreign_id',
        'branch_id'
    ];

    var SELECT_CONTENT_ACTIONS = ['Click', 'Open'];
    var SELECT_CONTENT_CATEGORIES = {Click: ['Button', 'Link', 'Menu'], Open: ['Dialog']};
    var SELECT_CONTENT_IGNORE_LIST = ['Open Product Page'];

    var SUPPORTED_KEYS = {
        'price': 0,
        'discount': 1,
        'quantity': 2
    }

    var EVENTS_TO_KEY_MAPPER = {
        'addToCart': ['pricePerCartQuantity', 'currentDiscountPerCartQuantity', 'cartQuantity'],
        'removeFromCart': ['pricePerCartQuantity', 'currentDiscountPerCartQuantity', 'cartQuantity'],
        'checkout': ['pricePerCartQuantity', 'currentDiscountPerCartQuantity', 'quantity'],
        'purchase': ['pricePerCartQuantity', 'currentDiscountPerCartQuantity', 'quantity']
    }

    angular.module('spServices').service('Ga4', [
        function () {
            var self = this

            angular.extend(self, {
                productImpressions: productImpressions,
                productDetail: productDetail,
                productClick: productClick,
                searchProduct: searchProduct,
                addToCart: addToCart,
                removeFromCart: removeFromCart,
                checkout: checkout,
                purchase: purchase,
                clubMembersUse: clubMembersUse,
                addProductToWishlist: addProductToWishlist,
                loginView: loginView,
                loginSuccess: loginSuccess,
                loginFail: loginFail,
                registrationView: registrationView,
                registrationSuccess: registrationSuccess,
                registrationFail: registrationFail,
                contactUsView: contactUsView,
                contactUsSuccess: contactUsSuccess,
                contactUsFail: contactUsFail,
                promotionView: promotionView,
                promotionClick: promotionClick,
                selectContent: selectContent,
                pageView: pageView,
                prepareProduct: prepareProduct,
                preparePromotion: preparePromotion,
                prepareWishListItem: prepareWishListItem,
                enrichEvents: enrichEvents,
                CUSTOM_PARAMS: CUSTOM_PARAMS
            });

            function productImpressions(eventData) {
                if (!eventData.products) {
                    return;
                }

                return {
                    'event': 'view_item_list',
                    'ecommerce': {
                        'item_list_name': eventData.data.location,
                        'item_list_id': eventData.data.location,
                        'items': eventData.products
                    }
                };
            }

            function productDetail(eventData) {
                if (!(eventData.products && eventData.values)) {
                    return;
                }

                return {
                    'event': 'view_item',
                    'ecommerce': {
                        'item_list_name': eventData.data.location,
                        'item_list_id': eventData.data.location,
                        'value': eventData.values.itemValue,
                        'items': eventData.products
                    }
                };
            }

            function productClick(eventData) {
                if (!eventData.products && eventData.values) {
                    return;
                }

                return {
                    'event': 'select_item',
                    'ecommerce': {
                        'item_list_name': eventData.data.location,
                        'item_list_id': eventData.data.location,
                        'value': eventData.values.itemValue,
                        'items': eventData.products
                    }
                };
            }

            function searchProduct(eventData) {
                if (!eventData.data.searchTerm) {
                    return;
                }

                return {
                    'event': 'search',
                    'search_term': eventData.data.searchTerm,
                    'results': eventData.data.resultsCount > 0,
                    'results_count': eventData.data.resultsCount
                };
            }

            function addToCart(eventData) {
                if (!(eventData.products && eventData.values && (eventData.data.quantity || eventData.products[0].quantity) && eventData.data.cartUpdateComplete)) {
                    return;
                }

                eventData.products[0].quantity = eventData.products[0].quantity || eventData.data.quantity;

                return {
                    'event': 'add_to_cart',
                    'ecommerce': {
                        'item_list_name': eventData.data.location,
                        'item_list_id': eventData.data.location,
                        'value': eventData.values.cartItemValue,
                        'items': eventData.products
                    }
                };
            }

            function removeFromCart(eventData) {
                if (!(eventData.products && eventData.values && (eventData.data.quantity || eventData.products[0].quantity) && eventData.data.cartUpdateComplete)) {
                    return;
                }

                eventData.products[0].quantity = eventData.products[0].quantity || eventData.data.quantity;

                return {
                    'event': 'remove_from_cart',
                    'ecommerce': {
                        'value': eventData.values.cartItemValue,
                        'items': eventData.products
                    }
                };
            }


            function checkout(eventData) {
                if (!(eventData.products && eventData.values && eventData.data.option)) {
                    return;
                }

                //toDo: refactor
                switch (eventData.data.option) {
                    case 'cart summary':
                        return _ga4ViewCart(eventData.products, eventData.values.checkoutValue);
                    case 'user address':
                        var events = [];
                        events.push(_ga4BeginCheckout(eventData.products, eventData.values.checkoutValue));
                        events.push(_ga4AddShippingInfo(eventData.products, eventData.values.checkoutValue));
                        return events;
                    case 'user address and time slot':
                        var events = [];
                        events.push(_ga4BeginCheckout(eventData.products, eventData.values.checkoutValue));
                        events.push(_ga4AddShippingInfo(eventData.products, eventData.values.checkoutValue));
                        return events;
                    case 'time slot':
                        return {skip: true};
                    case 'payment details':
                        return _ga4AddPaymentInfo(eventData.products, eventData.values.checkoutValue);
                    case 'payment summary':
                        return {skip: true};
                }
            }

            function purchase(eventData) {
                if (!(eventData.products && eventData.purchase && eventData.purchase.id)) {
                    return;
                }

                var event = {
                    'event': eventData.data.serverEvents.purchase ? 'sp_purchase' : 'purchase',
                    'ecommerce': {
                        'transaction_id': eventData.purchase.id,
                        'value': eventData.purchase.revenue,
                        'shipping_tier': eventData.purchase.shippingTier,
                        'payment_type': eventData.purchase.paymentType,
                        'tax': eventData.purchase.tax,
                        'shipping': eventData.purchase.shipping,
                        'coupon': eventData.purchase.coupon,
                        'items': eventData.products
                    }
                };

                try {
                    var eventStr = JSON.stringify(event);
                    var eventJson = JSON.parse(eventStr);
                    return eventJson;
                }
                catch (err) {
                    return;
                }
            }

            function clubMembersUse(eventData) {
                return {
                    'event': 'club_member_use',
                    'club_member_amount': eventData.data.clubMemberAmount
                };
            }

            function addProductToWishlist(eventData) {
                if (!eventData.wishListItem) {
                    return;
                }

                //toDo: make sure price exists
                return {
                    'event': 'add_to_wishlist',
                    'ecommerce': {
                        'item_list_name': eventData.data.location,
                        'item_list_id': eventData.data.location,
                        'value': eventData.wishListItem.price,
                        'items': [eventData.wishListItem]
                    }
                };
            }

            function loginView() {
                return {
                    'event': 'login_view'
                };
            }

            function loginSuccess() {
                return {
                    'event': 'login'
                };
            }

            function loginFail() {
                return {
                    'event': 'login_fail'
                };
            }

            function registrationView() {
                return {
                    'event': 'sign_up_view'
                };
            }

            function registrationSuccess() {
                return {
                    'event': 'sign_up'
                };
            }

            function registrationFail() {
                return {
                    'event': 'sign_up_fail'
                };
            }

            function contactUsView() {
                return {
                    'event': 'contact_us_view'
                };
            }

            function contactUsSuccess() {
                return {
                    'event': 'contact_us'
                };
            }

            function contactUsFail() {
                return {
                    'event': 'contact_us_fail'
                };
            }

            function promotionView(eventData) {
                if (!eventData.promotion) {
                    return;
                }

                return {
                    'event': 'view_promotion',
                    'ecommerce': eventData.promotion
                };
            }

            function promotionClick(eventData) {
                if (!eventData.promotion) {
                    return;
                }

                return {
                    'event': 'select_promotion',
                    'ecommerce': eventData.promotion
                };
            }

            function selectContent(eventData) {
                if (SELECT_CONTENT_IGNORE_LIST.includes(eventData.data.label)) {
                    return {skip: true}
                }
                else if (!(eventData.data.label && SELECT_CONTENT_ACTIONS.includes(eventData.data.action) && SELECT_CONTENT_CATEGORIES[eventData.data.action].includes(eventData.data.category))) {
                    return;
                }

                return {
                    'event': 'select_content',
                    'content_type': eventData.data.action.toLowerCase(),
                    'content_category': eventData.data.category.toLowerCase(),
                    'content_label': eventData.data.label
                };
            }

            function pageView(eventData) {
                return {
                    'event': 'virtual_page_view',
                    'page_location': eventData.data.location
                };
            }

            function prepareProduct(event, productData) {
                if (!(productData.productName && productData.price)) {
                    return;
                }

                var priceKey = _getKeyByEvent(event, 'price') || 'price';
                var discountKey = _getKeyByEvent(event, 'discount') || 'currentDiscount';
                var quantityKey = _getKeyByEvent(event, 'quantity') || 'quantity';

                return {
                    'item_name': productData.productName,
                    'item_id': productData.id,
                    'affiliation': productData.affiliation,
                    'coupon': productData.coupon,
                    'price': productData[priceKey],
                    'item_brand': productData.brandName,
                    'item_category': productData.category,
                    'index': productData.currentProductPosition,
                    'discount': productData[discountKey],
                    'quantity': productData[quantityKey],
                    'item_list_name': productData.addedFrom,
                    'item_list_id': productData.addedFrom,
                    'item_weight': productData.itemWeight
                };

            }

            function preparePromotion(promotionData) {
                if (!promotionData.id) {
                    return;
                }

                return {
                    'promotion_id': promotionData.id,
                    'promotion_name': promotionData.creativeName,
                    'creative_name': promotionData.creativeUrl,
                    'creative_slot': promotionData.type
                };
            }

            function prepareWishListItem(wishListItemData) {
                if (!wishListItemData.name) {
                    return;
                }

                return {
                    'item_name': wishListItemData.name,
                    'price': wishListItemData.price
                };
            }

            function enrichEvents(events, enrichmentData) {
                angular.forEach(events, function (event) {
                    if (event.ecommerce) {
                        event.ecommerce.currency = enrichmentData.currencyCode;
                    }

                    event.user_email = enrichmentData.userEmail;
                    event.user_id = enrichmentData.userId;
                    event.is_first = enrichmentData.isFirst;
                    event.purchase_counter = enrichmentData.purchaseCounter;
                    event.foreign_id = enrichmentData.foreignId;
                    event.branch_id = enrichmentData.branchId;
                });
            }

            function _ga4ViewCart(products, productsValue) {
                return {
                    'event': 'view_cart',
                    'ecommerce': {
                        'value': productsValue,
                        'items': products
                    }
                };
            }

            function _ga4BeginCheckout(products, productsValue) {
                return {
                    'event': 'begin_checkout',
                    'ecommerce': {
                        'value': productsValue,
                        'items': products
                    }
                };
            }

            function _ga4AddShippingInfo(products, productsValue) {
                return {
                    'event': 'add_shipping_info',
                    'ecommerce': {
                        'value': productsValue,
                        'items': products
                    }
                };
            }

            function _ga4AddPaymentInfo(products, productsValue) {
                return {
                    'event': 'add_payment_info',
                    'ecommerce': {
                        'value': productsValue,
                        'items': products
                    }
                };
            }

            function _getKeyByEvent(event, key) {
                if (!EVENTS_TO_KEY_MAPPER[event] || SUPPORTED_KEYS[key] === undefined) {
                    return;
                }

                return EVENTS_TO_KEY_MAPPER[event][SUPPORTED_KEYS[key]];
            }
        }]);

})(angular);
