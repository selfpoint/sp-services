(function (angular) {
    'use strict';

    var CUSTOM_PARAMS = [
        'category',
        'action',
        'label',
        'clubMemberAmount',
        'addProductToWishlistProd',
        'searchTerm',
        'results',
        'resultsCount',
        'pageLocation',
        'isFirst',
        'purchaseCounter'
    ];

    var SELECT_CONTENT_ACTIONS = ['Click', 'Open']
    var SELECT_CONTENT_CATEGORIES = {Click: ['Button', 'Link', 'Menu'], Open: ['Dialog']};

    angular.module('spServices').service('Ga3', [
        function () {
            var self = this;

            angular.extend(self, {
                productImpressions: productImpressions,
                productDetail: productDetail,
                searchProduct: searchProduct,
                addToCart: addToCart,
                removeFromCart: removeFromCart,
                checkout: checkout,
                purchase: purchase,
                clubMembersUse: clubMembersUse,
                addProductToWishlist: addProductToWishlist,
                loginView: loginView,
                loginSuccess: loginSuccess,
                loginFail: loginFail,
                registrationView: registrationView,
                registrationSuccess: registrationSuccess,
                registrationFail: registrationFail,
                contactUsView: contactUsView,
                contactUsSuccess: contactUsSuccess,
                contactUsFail: contactUsFail,
                promotionView: promotionView,
                promotionClick: promotionClick,
                selectContent: selectContent,
                pageView: pageView,
                prepareProduct: prepareProduct,
                preparePromotion: preparePromotion,
                prepareWishListItem: prepareWishListItem,
                enrichEvents: enrichEvents,
                CUSTOM_PARAMS: CUSTOM_PARAMS
            });

            function productImpressions(eventData) {
                if (!eventData.products) {
                    return;
                }

                return {
                    'event': 'productImpressions',
                    'ecommerce': {
                        'impressions': eventData.products
                    }
                }
            }

            function productDetail(eventData) {
                if (!eventData.products) {
                    return;
                }

                return {
                    'event': 'productDetail',
                    'ecommerce': {
                        'detail': {
                            'actionField': {
                                'list': eventData.data.location,
                            },
                            'products': eventData.products
                        }
                    }
                };
            }

            function searchProduct(eventData) {
                if(!eventData.data.searchTerm) {
                    return;
                }

                return {
                    'event': 'searchProduct',
                    'searchTerm': eventData.data.searchTerm,
                    'results': eventData.data.resultsCount > 0,
                    'resultsCount': eventData.data.resultsCount
                };
            }

            function addToCart(eventData) {
                if (!(eventData.products && eventData.data.quantity > 0 && eventData.data.cartUpdateComplete)) {
                    return;
                }

                eventData.products[0].quantity = eventData.data.quantity;

                return {
                    'event': 'addToCart',
                    'ecommerce': {
                        'add': {
                            'actionField': {
                                'list': eventData.data.location,
                            },
                            'products': eventData.products
                        }
                    }
                };
            }

            function removeFromCart(eventData) {
                if (!(eventData.products && eventData.data.quantity >= 0 && eventData.data.cartUpdateComplete)) {
                    return;
                }

                eventData.products[0].quantity = eventData.data.quantity;

                return {
                    'event': 'removeFromCart',
                    'ecommerce': {
                        'remove': {
                            'products': eventData.products
                        }
                    }
                };
            }

            function checkout(eventData) {
                if (!eventData.products) {
                    return;
                }

                return {
                    'event': 'checkout',
                    'ecommerce': {
                        'checkout': {
                            'actionField': {
                                'step': eventData.data.step,
                                'option': eventData.data.option
                            },
                            'products': eventData.products
                        }
                    }
                }
            }

            function purchase(eventData) {
                if (!(eventData.products && eventData.purchase && eventData.purchase.id)) {
                    return;
                }

                return {
                    'event': 'purchase',
                    'ecommerce': {
                        'purchase': {
                            'actionField': {
                                'id': eventData.purchase.id,
                                'affiliation': eventData.purchase.shippingTier,
                                'revenue': eventData.purchase.revenue,
                                'tax': eventData.purchase.tax,
                                'shipping': eventData.purchase.shipping,
                                'coupon': eventData.purchase.coupon
                            },
                            'products': eventData.products
                        }
                    }
                };
            }

            function clubMembersUse(eventData) {
                return {
                    'event': 'clubMembersUse',
                    'clubMemberAmount': eventData.data.clubMemberAmount
                };
            }

            function addProductToWishlist(eventData) {
                if (!eventData.wishListItem) {
                    return;
                }

                return {
                    'event': 'addProductToWishlist',
                    'addProductToWishlistProd': eventData.wishListItem
                };
            }

            function loginView() {
                return {
                    'event': 'loginView'
                };
            }

            function loginSuccess() {
                return {
                    'event': 'loginSuccess'
                };
            }

            function loginFail(eventData) {
                return {
                    'event': 'loginFail',
                    'loginFailName': 'Login failure',
                    'loginFailEmail': eventData.data.email
                };
            }

            function registrationView() {
                return {
                    'event': 'registrationView'
                };
            }

            function registrationSuccess() {
                return {
                    'event': 'registrationSuccess'
                };
            }

            function registrationFail(eventData) {
                return {
                    'event': 'registrationFail',
                    'registrationFailName': eventData.data.error,
                    'registrationFailEmail': eventData.data.email
                };
            }

            function contactUsView() {
                return {
                    'event': 'contactUsView'
                };
            }

            function contactUsSuccess() {
                return {
                    'event': 'contactUsSuccess'
                };
            }

            function contactUsFail(eventData) {
                return {
                    'event': 'contactUsFail',
                    'contactUsFailName': eventData.data.error
                };
            }

            function promotionView(eventData) {
                if (!eventData.promotion) {
                    return;
                }

                return {
                    'event': 'promotionView',
                    'ecommerce': {
                        'promoView': {
                            'promotions': [eventData.promotion]
                        }
                    }
                };
            }

            function promotionClick(eventData) {
                if (!eventData.promotion) {
                    return;
                }

                return {
                    'event': 'promotionClick',
                    'ecommerce': {
                        'promoClick': {
                            'promotions': [eventData.promotion],
                        }
                    },
                    'eventCallback': _promotionCallback(eventData)
                };
            }

            function selectContent(eventData) {
                if (eventData.data.label && SELECT_CONTENT_ACTIONS.includes(eventData.data.action) && SELECT_CONTENT_CATEGORIES[eventData.data.action].includes(eventData.data.category)) {
                    if (eventData.data.category === 'Menu' ) {
                        return {
                            'event': 'menuClick',
                            'menuCategory': eventData.data.label
                        };
                    }
                    else {
                        return {
                            'event': 'buttonClick',
                            'category': eventData.data.category,
                            'action': eventData.data.action,
                            'label': eventData.data.label
                        };
                    }
                }
            }

            function pageView(eventData) {
                return {
                    'event': 'virtualPageview',
                    'pageLocation': eventData.data.location
                };
            }

            //toDo: make sure numbers are prepared as numbers in data and product - to fixed should only be at event level for preparing view
            function prepareProduct(event, productData) {
                if (!(productData.productName && productData.price)) {
                    return;
                }

                var preparedProduct = {
                    'name': productData.productName,
                    'id': productData.id,
                    'price': productData.price && productData.price,
                    'brand': productData.brandName,
                    'category': productData.category,
                    'discount': productData.currentDiscount && productData.currentDiscount,
                    'discountPrecent': productData.currentDiscountPrecent && productData.currentDiscountPrecent,
                    'position': productData.currentProductPosition
                }

                if (productData.quantity >= 0) {
                    preparedProduct.quantity = productData.quantity;
                }

                if (productData.addedFrom) {
                    preparedProduct.addedFrom = productData.addedFrom;
                }

                return preparedProduct;
            }

            function preparePromotion(promotionData) {
                if (!promotionData.id) {
                    return;
                }

                return {
                    'id': promotionData.id,
                    'name': promotionData.creativeName,
                    'creative': promotionData.creativeUrl,
                    'type': promotionData.type
                };
            }

            function prepareWishListItem(wishListItemData) {
                if (!wishListItemData.name) {
                    return;
                }

                return wishListItemData.name;
            }

            function enrichEvents(events, enrichmentData) {
                angular.forEach(events, function (event) {
                    if (event.ecommerce) {
                        event.ecommerce.currencyCode = enrichmentData.currencyCode;
                    }

                    event.userEmail = enrichmentData.userEmail;
                    event.userId = enrichmentData.userId;
                    event.isFirst = enrichmentData.isFirst;
                    event.purchaseCounter = enrichmentData.purchaseCounter;
                });
            }

            //toDo: what does it do? returns to whom? - usually used to navigate - something else here - check if changes dataLayer or perhaps fires event
            function _promotionCallback(eventData) {
                var promotion = eventData.promotion || {};

                var event = '';

                switch (promotion.type) {
                    case 'video':
                        event = 'video playback';
                        break;

                    case 'banner':
                        event = 'banner click';
                        break;

                    case 'recipe':
                        event = 'recipe popup';
                }

                return {
                    'url': promotion.url,
                    'event': event
                }
            }
        }]);

})(angular);
