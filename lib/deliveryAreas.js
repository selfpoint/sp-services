(function (angular) {
    /**
     * Service name
     */
    var serviceName = 'SpDeliveryAreasService';

    var AUTO_COMPLETE_LIMIT = 8;

    var DEFAULT_LANGUAGE_ID = 2;

    /**
     * Service
     */
    function SpDeliveryAreasService($q, $injector, $filter, Api, SP_SERVICES) {
        var self = this,
            _filterFilter = $filter('filter'),
            _orderByFilter = $filter('orderBy'),
            _limitToFilter = $filter('limitTo'),
            spGoogleMaps,
            languageIdResolver = new DataResolver($q),
            retailerConfigurationsResolver = new DataResolver($q),
            multiRetailersResolver = new DataResolver($q),
            areasResolver = new DataResolver($q),
            pickupAreasResolver = new DataResolver($q),
            _areasByNameResolver = new DataResolver($q),
            _deliveryAreasByIdResolver = new DataResolver($q),
            preferredRetailerIdResolver = new DataResolver($q),
            organizationBranchesResolver = new DataResolver($q),
            _areasDeliveryTimeTypesCache = {};

        self.setLanguageId = languageIdResolver.setData;
        self.setRetailerConfigurations = retailerConfigurationsResolver.setData;
        self.setMultiRetailers = multiRetailersResolver.setData;
        self.setPreferredRetailerId = preferredRetailerIdResolver.setData;
        self.setOrganizationBranches = organizationBranchesResolver.setData;
        self.getAreas = areasResolver.getData;
        self.getPickupAreas = pickupAreasResolver.getData;

        self.getChooseAreaMode = getChooseAreaMode;
        self.getChooseAreaEvent = getChooseAreaEvent;
        self.isZipCodeArea = isZipCodeArea;
        self.autoCompleteDeliveryAreas = autoCompleteDeliveryAreas;
        self.autoCompleteDeliveryAreasWithFullData = autoCompleteDeliveryAreasWithFullData;
        self.filterDeliveryAreas = filterDeliveryAreas;
        self.addressComponentsToSpAddress = addressComponentsToSpAddress;
        self.setRetailerIdCookie = setRetailerIdCookie;
        self.setLanguageCookie = setLanguageCookie;
        self.addAreaNotFoundLog = addAreaNotFoundLog;
        self.getAreaAddressText = getAreaAddressText;
        self.isAreaMatchTerm = isAreaMatchTerm;
        self.initBranchAndArea = initBranchAndArea;
        self.getAreasDeliveryTimeTypes = getAreasDeliveryTimeTypes;
        self.isAreaDeliveryWithinDaysOnly = isAreaDeliveryWithinDaysOnly;

        languageIdResolver.setData(DEFAULT_LANGUAGE_ID);
        retailerConfigurationsResolver.onChange(_setAreas);
        multiRetailersResolver.onChange(_setAreas);
        preferredRetailerIdResolver.onChange(_setAreasPreferredRetailerIdSort);

        /**
         * Returns the state of the choose area dialog
         * @public
         *
         * @param {boolean} allowMultiMode
         *
         * @returns {Promise<number>}
         */
        function getChooseAreaMode(allowMultiMode) {
            return $q.all({
                configurations: retailerConfigurationsResolver.getData(),
                multiRetailers: multiRetailersResolver.getData()
            }).then(function(results) {
                var isMulti = results.multiRetailers && results.multiRetailers.length > 1;
                if (allowMultiMode && isMulti && Number(results.configurations.settings.multiDomainsPicker) === SP_SERVICES.MULTI_DOMAINS_PICKERS.CHOOSE_RETAILER) {
                    return SP_SERVICES.CHOOSE_AREA_MODE.CHOOSE_RETAILER;
                }

                if (results.configurations.deliveryAreaMethod === SP_SERVICES.DELIVERY_AREA_METHODS.GOOGLE_MAPS) {
                    return SP_SERVICES.CHOOSE_AREA_MODE.TYPE_GOOGLE_ADDRESS;
                }

                return SP_SERVICES.CHOOSE_AREA_MODE.TYPE_AREA;
            });
        }

        /**
         * Returns the event in which the choose area dialog should appear on
         * @public
         *
         * @returns {Promise<number>}
         */
        function getChooseAreaEvent() {
            return $q.all({
                configurations: retailerConfigurationsResolver.getData(),
                multiRetailers: multiRetailersResolver.getData(),
                organizationBranches: organizationBranchesResolver.getData()
            }).then(function(results) {
                // when there are multi retailers or it is marked in the backend to show the dialog on enter
                if (results.multiRetailers && results.multiRetailers.length > 1 || results.configurations.settings.chooseAreaOnEnterSite === 'true') {
                    return SP_SERVICES.CHOOSE_AREA_EVENT.ENTER_SITE;
                }

                // when it is marked in the backend to show on cart activity
                if (results.configurations.settings.chooseAreaOnEnterSite === 'false') {
                    return SP_SERVICES.CHOOSE_AREA_EVENT.CART_ACTIVITY;
                }

                var branches = _getAreasBranches(results.configurations.branches, results.organizationBranches);

                // when the retailer has multi branches
                var availableBranches = [];
                angular.forEach(branches, function(branch) {
                    if (!branch.isDisabled && branch.isOnline && (branch.areas || []).find(_isAvailableArea)) {
                        availableBranches.push(branch);
                    }
                });
                if (availableBranches.length > 1) {
                    return SP_SERVICES.CHOOSE_AREA_EVENT.ENTER_SITE;
                }

                return SP_SERVICES.CHOOSE_AREA_EVENT.NONE;
            });
        }

        /**
         * Returns the array of branches filtered by the organization branches
         * @private
         *
         * @param {Array<Object>} allBranches
         * @param {Array<number>} organizationBranches
         *
         * @return {Array<Object>}
         */
        function _getAreasBranches(allBranches, organizationBranches) {
            var organizationBranchesMap = {};
            if (organizationBranches) {
                angular.forEach(organizationBranches, function(branchId) {
                    organizationBranchesMap[branchId] = true;
                });
            }

            var filtered = [];
            angular.forEach(allBranches, function(branch) {
                if (organizationBranches && organizationBranchesMap[branch.id] ||
                    !organizationBranches && !branch.isInstitutional) {
                    filtered.push(branch);
                }
            });
            return filtered;
        }

        /**
         * Returns whether the choose area is a zip code
         * @public
         *
         * @returns {Promise<boolean>}
         */
        function isZipCodeArea() {
            return $q.all({
                configurations: retailerConfigurationsResolver.getData(),
                multiRetailers: multiRetailersResolver.getData()
            }).then(function(results) {
                var multiDomainsPicker = Number(results.configurations.settings.multiDomainsPicker);
                return !!results.multiRetailers && results.multiRetailers.length > 1 &&
                    multiDomainsPicker !== SP_SERVICES.MULTI_DOMAINS_PICKERS.CHOOSE_RETAILER &&
                    multiDomainsPicker !== SP_SERVICES.MULTI_DOMAINS_PICKERS.CHOOSE_AREA;
            });
        }

        /**
         * Returns auto complete values for delivery areas
         * @public
         *
         * @param {number} chooseAreaMode
         * @param {string} address
         *
         * @returns {Promise<Array<string>>}
         */
        function autoCompleteDeliveryAreas(chooseAreaMode, address) {
            if (chooseAreaMode !== SP_SERVICES.CHOOSE_AREA_MODE.TYPE_GOOGLE_ADDRESS) {
                return _areasByNameResolver.getData().then(function(deliveryAreasByName) {
                    var names = [],
                        filteredKeys = _filterFilter(Object.keys(deliveryAreasByName), (address || '').toLowerCase());
                    angular.forEach(filteredKeys, function(key) {
                        names.push(deliveryAreasByName[key].name);
                    });
                    return _limitToFilter(_orderByFilter(names), AUTO_COMPLETE_LIMIT);
                });
            }

            // https://developers.google.com/maps/documentation/javascript/reference/places-autocomplete-service#AutocompletionRequest
            // There is no way to limit the response length
            var spGoogleMaps = _getSpGoogleMaps();
            return spGoogleMaps.spGoogleMapsUtil.placesAutocomplete(address, spGoogleMaps.GOOGLE_AUTO_COMPLETE_TYPES.GEOCODE).then(function(response) {
                var names = [];
                angular.forEach(response, function (place) {
                    names.push(place.description);
                });
                return names;
            });
        }

        function autoCompleteDeliveryAreasWithFullData(chooseAreaMode, address, placeId, zipCode, validZipCode, languageId) {
            var areasWithFullData = {
                names: null,
                places: null
            }
            if (chooseAreaMode !== SP_SERVICES.CHOOSE_AREA_MODE.TYPE_GOOGLE_ADDRESS) {
                return _areasByNameResolver.getData().then(function(deliveryAreasByName) {
                    var names = [],
                        filteredKeys = _filterFilter(Object.keys(deliveryAreasByName), (address || '').toLowerCase());
                    angular.forEach(filteredKeys, function(key) {
                        if (deliveryAreasByName[key].areas[0].deliveryTypeId == SP_SERVICES.DELIVERY_TYPES.DELIVERY) {
                            names.push(deliveryAreasByName[key].name);
                        }
                    });
                    areasWithFullData.names =_limitToFilter(_orderByFilter(names), AUTO_COMPLETE_LIMIT);
                    return areasWithFullData;
                });
            }

            // https://developers.google.com/maps/documentation/javascript/reference/places-autocomplete-service#AutocompletionRequest
            // There is no way to limit the response length
            var spGoogleMaps = _getSpGoogleMaps();
            return spGoogleMaps.spGoogleMapsUtil.placesAutocomplete(address, spGoogleMaps.GOOGLE_AUTO_COMPLETE_TYPES.GEOCODE, placeId, zipCode, validZipCode, languageId).then(function(response) {
                var names = [];
                angular.forEach(response, function (place) {
                    names.push(place.description);
                });
                areasWithFullData.names = names;
                areasWithFullData.places = response;
                return areasWithFullData;
            });
        }

        /**
         * Returns delivery areas for the given address
         * @public
         *
         * @param {number} chooseAreaMode
         * @param {string} address
         *
         * @returns {Promise<{areas: Array<SpDeliveryArea>, [boundsAddress]: boolean, [addressComponents]: Array<Object>}>, [hasStreetNumber]: boolean}
         */
        function filterDeliveryAreas(chooseAreaMode, address, ignoreMultipleAddresses, placeId) {
            if (chooseAreaMode !== SP_SERVICES.CHOOSE_AREA_MODE.TYPE_GOOGLE_ADDRESS) {
                return _areasByNameResolver.getData().then(function (deliveryAreasByName) {

                    var areas = (deliveryAreasByName[(address || '').toLowerCase()] || {}).areas;
                    if (!areas && Object.values(deliveryAreasByName) && Object.values(deliveryAreasByName).length) {
                        var foundArea = Object.values(deliveryAreasByName).find(function (item) {
                            return item && item.areas && item.areas.length && item.areas.some(function (area) {
                                return area.deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.DELIVERY && area.name.toLowerCase() === address.toLowerCase() && area.branch && area.branch.areas;
                            })
                        })

                        if (foundArea) {
                            areas = foundArea.areas;
                        }
                    }
                    if (areas != undefined && areas.length != 0) {
                        var filteredAreas = areas.filter(function(area) {
                            return area.deliveryTypeId == SP_SERVICES.DELIVERY_TYPES.DELIVERY;
                        });
                        return {areas: filteredAreas};
                    }
                    else return {areas: []};
                });
            }

            return $q.all({
                languageId: languageIdResolver.getData(),
                multiRetailers: multiRetailersResolver.getData(),
                areasById: _deliveryAreasByIdResolver.getData()
            }).then(function (results) {
                var promises = [];
                angular.forEach(results.multiRetailers, function (retailer) {
                    // TODO rethink whether to leave it like this or develop a new route in the rest for areas of multi retailers
                    promises.push(Api.request({
                        url: '/v2/retailers/' + retailer.id + '/areas',
                        method: 'GET',
                        cache: true,
                        params: {
                            query: address,
                            deliveryTypeId: [SP_SERVICES.DELIVERY_TYPES.DELIVERY, SP_SERVICES.DELIVERY_TYPES.EXPRESS_DELIVERY],
                            languageId: results.languageId,
                            ignoreMultipleAddresses: ignoreMultipleAddresses,
                            placeId: placeId
                        }
                    }, {
                        fireAndForgot: true
                    }).then(function (response) {
                        var foundAreas = [];
                        angular.forEach(response.areas, function (area) {
                            if (results.areasById[area.id]) {
                                foundAreas.push(results.areasById[area.id]);
                            }
                        });
                        return {
                            areas: foundAreas,
                            boundsAddress: response.boundsAddress,
                            addressComponents: response.addressComponents
                        };
                    }).catch(function (err) {
                        if (err.statusCode !== 404) {
                            throw err;
                        } else {
                            return {areas: []};
                        }
                    }));
                });
                return $q.all(promises);
            }).then(function (responses) {
                var joined = {areas: []};
                angular.forEach(responses, function (response) {
                    joined.boundsAddress = joined.boundsAddress || response.boundsAddress;
                    joined.addressComponents = joined.addressComponents || response.addressComponents;
                    Array.prototype.push.apply(joined.areas, response.areas);
                });
                joined.hasStreetNumber = joined.addressComponents && joined.addressComponents.length && _isAddressComponentsContainsStreetNumberType(joined.addressComponents);

                return joined;
            });
        }

        /**
         * return whether there's a street number type in one of the addresses
         * @private
         *
         * @param {ArraysObject>} addressComponents
         *
         * @returns {boolean}
         */
        function _isAddressComponentsContainsStreetNumberType(addressComponents) {
            return !!addressComponents.find(function (addressComponent) {
                return addressComponent.types.find(function (type) {
                    return type === 'street_number'
                });
            });
        }

        function addressComponentsToSpAddress(addressComponents) {
            var data = {};
            angular.forEach(addressComponents, function (addressComponent) {
                angular.forEach(addressComponent.types, function (type) {
                    if ((!data.city && type === 'locality') || type === 'sublocality') {
                        data.city = addressComponent.long_name;
                    } else if (type === 'route') {
                        data.route = addressComponent.long_name;
                    } else if (type === 'street_number') {
                        data.number = addressComponent.long_name;
                    } else if (type === 'postal_code') {
                        data.zipCode = addressComponent.long_name;
                    } else if (type === 'premise') {
                        data.premise = addressComponent.long_name;
                    }
                });
            });

            return {
                city: data.city ,
                text1: data.number || data.route || data.premise ?
                    (data.number || '') + ((data.number && data.route) || (data.number && data.premise) ? ' ' : '') + (data.premise ? data.premise + ' ' : '') + (data.route || '') :
                    '',
                zipCode: data.zipCode
            };
        }

        /**
         * Set the given retailer id into the cookie
         * @public
         *
         * @param {number} retailerId
         *
         * @returns {Promise}
         */
        function setRetailerIdCookie(retailerId) {
            return Api.request({
                method: 'PUT',
                url: '/frontend/' + retailerId
            });
        }

        /**
         * Set the given lang into the cookie
         * tempoary put there
         * @public
         *
         * @param {string} lang : en, he ...
         *
         * @returns {Promise}
         */
        function setLanguageCookie(lang) {
            return Api.request({
                method: 'PUT',
                url: '/frontend/lang/' + lang
            });
        }

        /**
         * Add area not found log
         * @public
         *
         * @param {number} address
         * @param {string} userEmail
         *
         * @returns {Promise}
         */
        function addAreaNotFoundLog(address, userEmail) {
            return retailerConfigurationsResolver.getData().then(function(configurations) {
                return Api.request({
                    method: 'POST',
                    url: '/v2/retailers/' + configurations.id + '/areas/_addAreaLog',
                    data: {
                        area: address,
                        userEmail: userEmail
                    }
                });
            });
        }

        /**
         * Returns the appropriate address text for the given area
         * @public
         *
         * @param {SpDeliveryArea} area
         * @param {boolean} [includeBranchName=true]
         * @param {object} [branch]
         *
         * @returns {string}
         */
        function getAreaAddressText(area, includeBranchName, branch) {
            branch = branch || area.branch;

            var textBuilder = [];
            if (includeBranchName !== false) {
                textBuilder.push(branch.name);
            }

            var multiplePickupAddresses = false;
            if (area && (area.deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.PICKUP)) {
                if (branch.pickupAreasCount === undefined) {
                    branch.pickupAreasCount = 0;
                    angular.forEach(branch.areas, function(area) {
                        if (_isAvailableArea(area) && area.deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.PICKUP) {
                            branch.pickupAreasCount++;
                        }
                    });
                }

                multiplePickupAddresses = branch.pickupAreasCount > 1;
            }

            if (multiplePickupAddresses) {
                textBuilder.push(area.name);
            } else {
                var locationBuilder = [];
                if (branch.location) {
                    locationBuilder.push(branch.location);
                }
                if (branch.city && (branch.location || '').indexOf(branch.city) === -1) {
                    locationBuilder.push(branch.city);
                }
                textBuilder.push(locationBuilder.join(', '));
            }


            return textBuilder.join(' - ');
        }

        /**
         * Returns whether or not the given area matches the given string term
         * @public
         *
         * @param {SpDeliveryArea} area
         * @param {string} term
         *
         * @returns {boolean}
         */
        function isAreaMatchTerm(area, term) {
            if (!term) {
                return true;
            }

            term = term.toLowerCase();
            return area.name.toLowerCase().indexOf(term) > -1 ||
                getAreaAddressText(area).toLowerCase().indexOf(term) > -1 ||
                area.branch.name.toLowerCase().indexOf(term) > -1;
        }

        /**
         * @typedef {Object} InitBranchAndAreaOptions
         *
         * @property {number} branchId
         * @property {number} branchAreaId
         * @property {string} branchAreaText
         * @property {number} queryBranchId
         * @property {string} queryZipCode
         * @property {string} isPrerender
         */

        /**
         * Will initiate the branch and the area
         * @public
         *
         * @param {InitBranchAndAreaOptions} options
         *
         * @returns {Promise<{branch: Object, branchAreaId: number, branchAreaText: string, isQueryBranch: boolean, isQueryArea: boolean}>}
         */
        function initBranchAndArea(options) {
            return $q.all({
                retailer: retailerConfigurationsResolver.getData(),
                isZipCodeArea: isZipCodeArea(),
                chooseAreaEvent: getChooseAreaEvent(),
                chooseAreaMode: getChooseAreaMode(false),
                organizationBranches: organizationBranchesResolver.getData()
            }).then(function(results) {
                var response = {
                    isQueryBranch: !!options.queryBranchId,
                    isQueryArea: false
                };

                var branches = _getAreasBranches(results.retailer.branches, results.organizationBranches),
                    branchesWithAreas = [],
                    defaultWithAreaBranch,
                    savedBranchId = options.queryBranchId || options.branchId;
                angular.forEach(branches, function(branch) {
                    if (branch.isDisabled || !branch.isOnline) {
                        return;
                    }

                    if (savedBranchId === branch.id) {
                        return response.branch = branch;
                    }

                    var availableArea = (branch.areas || []).find(_isAvailableArea);
                    if (availableArea) {
                        if (branch.isDefault) {
                            defaultWithAreaBranch = branch;
                        }
                        branchesWithAreas.push(branch);
                    }
                });

                if (!response.branch) {
                    if (!branchesWithAreas.length) {
                        response.branch = branches[0] || results.retailer.branches[0];
                    } else if (branchesWithAreas.length === 1 || options.isPrerender ||
                        results.chooseAreaEvent === SP_SERVICES.CHOOSE_AREA_EVENT.CART_ACTIVITY) {
                        response.branch = defaultWithAreaBranch || branchesWithAreas[0];
                    }
                }

                if (!response.branch) {
                    return response;
                }

                var area;
                if (options.branchAreaText) {
                    response.branchAreaText = options.branchAreaText;
                }
                if (options.branchAreaId && !isNaN(options.branchAreaId)) {
                    response.branchAreaId = options.branchAreaId;
                }

                if (results.isZipCodeArea && options.queryZipCode && results.chooseAreaMode === SP_SERVICES.CHOOSE_AREA_MODE.TYPE_AREA) {
                    area = response.branch.areas.find(function(area) {
                        return (area.names || []).find(function(areaName) {
                            return options.queryZipCode === areaName.name;
                        });
                    });

                    if (area) {
                        response.branchAreaText = options.queryZipCode;
                        response.branchAreaId = area.id;
                    }
                }

                //make sure the saved area is in the branch areas
                if (response.branchAreaId || response.branchAreaText) {
                    area = response.branch.areas.find(function(area) {
                        return area.id === response.branchAreaId &&
                            _isAvailableInitArea(area, results.chooseAreaEvent, results.chooseAreaMode);
                    });

                    if (!area) {
                        response.branchAreaId = response.branchAreaText = null;
                    } else if (results.chooseAreaMode === SP_SERVICES.CHOOSE_AREA_MODE.TYPE_AREA && area.names && area.names.length) {
                        var areaName = area.names.find(function(name) {
                            return name.name === response.branchAreaText;
                        });

                        if (!areaName) {
                            response.branchAreaText = area.names[0].name;
                        }
                    } else {
                        response.branchAreaText = area.name;
                    }
                }

                // take the first available area when a branch id is in the query
                if ((!response.branchAreaId || !response.branchAreaText) && options.queryBranchId) {
                    response.isQueryArea = true;
                    area = response.branch.areas.find(function(area){
                        return _isAvailableInitArea(area, results.chooseAreaEvent, results.chooseAreaMode);
                    });
                    if (area) {
                        if (results.chooseAreaMode === SP_SERVICES.CHOOSE_AREA_MODE.TYPE_AREA) {
                            if (area.names && area.names.length) {
                                response.branchAreaText = area.names[0].name;
                            }
                        } else {
                            response.branchAreaText = area.name;
                        }
                        response.branchAreaId = area.id;
                    }
                }

                // when the popup will not show, choose the default areas. 
                // If dont have available default areas, choose the first area (prefer the first delivery area).
                if (!response.branchAreaId && (options.isPrerender || results.chooseAreaEvent === SP_SERVICES.CHOOSE_AREA_EVENT.NONE)) {
                    var chosenArea;
                    chosenArea = response.branch.areas.find(function(area){
                        return _isAvailableInitArea(area) && area.isDefault;
                    });

                    if(!chosenArea) {
                        for (var i = 0; i < response.branch.areas.length; i++) {
                            if (!_isAvailableArea(response.branch.areas[i])) {
                                continue;
                            }
                            if (!chosenArea && (results.retailer.settings.deliveryMethodDefault === SP_SERVICES.DELIVERY_TYPES.DELIVERY ||
                                response.branch.areas[i].deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.DELIVERY) ||
                                response.branch.areas[i].deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.DELIVERY &&
                                chosenArea.deliveryTypeId !== SP_SERVICES.DELIVERY_TYPES.DELIVERY) {
                                chosenArea = response.branch.areas[i];

                                // If the first area matches and the user did not choose delivery or pickup (Choose area on enter to site feature enabled)
                                // we set the first area as a default one.
                                if(chosenArea && (!options.branchAreaId || !options.branchAreaText)) {
                                    break;
                                }
                            }

                            if (chosenArea && chosenArea.deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.DELIVERY) {
                                break;
                            }
                        }
                    }
                    
                    if (chosenArea) {
                        response.branchAreaId = chosenArea.id;
                        response.branchAreaText = chosenArea.name;
                    }
                }

                return response;
            });
        }

        function _isAvailableInitArea(area, chooseAreaEvent, chooseAreaMode) {
            // when the area is not available it can't be chosen
            if (!_isAvailableArea(area)) {
                return false
            }

            // when the choose area dialog won't be opened, do not require names
            if (chooseAreaEvent === SP_SERVICES.CHOOSE_AREA_EVENT.NONE) {
                return true;
            }

            // when the given area is of pickup type, it will never need any names in the dialog
            if (area.deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.PICKUP) {
                return true;
            }

            // when the choose area dialog will open on type area mode, the area must contain names
            return chooseAreaMode !== SP_SERVICES.CHOOSE_AREA_MODE.TYPE_AREA || area.names && area.names.length || area.name;
        }

        /**
         * Returns the necessary spGoogleMaps services
         * @private
         *
         * @returns {{spGoogleMapsUtil: Object, GOOGLE_AUTO_COMPLETE_TYPES: Object}}
         */
        function _getSpGoogleMaps() {
            if (spGoogleMaps) {
                return spGoogleMaps;
            }

            try {
                return spGoogleMaps = {
                    spGoogleMapsUtil: $injector.get('spGoogleMapsUtil'),
                    GOOGLE_AUTO_COMPLETE_TYPES: $injector.get('GOOGLE_AUTO_COMPLETE_TYPES')
                };
            } catch(err) {
                throw new Error('In order to filter areas by google address the sp-google-maps module must be included.' +
                    '\r\nSee https://bitbucket.org/{companyName}/sp-google-maps.git');
            }
        }

        /**
         * Set the area resolvers data
         * @private
         *
         * @returns {Promise}
         */
        function _setAreas() {
            return $q.all({
                retailer: retailerConfigurationsResolver.getData(),
                retailers: multiRetailersResolver.getData(),
                preferredRetailerId: preferredRetailerIdResolver.hasData ? preferredRetailerIdResolver.getData() : undefined,
                organizationBranches: organizationBranchesResolver.getData()
            }).then(function(results) {
                var areas = [],
                    pickupAreas = [],
                    deliveryAreasByName = {},
                    deliveryAreasById = {},
                    branchesById = {};
                angular.forEach(results.retailers, function(retailer) {
                    var branches = _getAreasBranches(retailer.branches, results.organizationBranches);

                    angular.forEach(branches, function(branch) {
                        if (branch.isDisabled || !branch.isOnline) {
                            return;
                        }

                        branchesById[branch.id] = branchesById[branch.id] || angular.extend({}, branch, {
                            pickupAreasCount: 0
                        });

                        angular.forEach(branch.areas, function(area) {
                            if (!_isAvailableArea(area)) {
                                return;
                            }

                            var toPush = angular.extend({}, area, {
                                branch: branchesById[branch.id],
                                retailer: retailer
                            });

                            _setAreaPreferredRetailerIdSort(toPush, results.preferredRetailerId);

                            areas.push(toPush);
                            deliveryAreasById[toPush.id] = toPush;

                            var usedNames = {};
                            var lowerCase;
                            if (area.names && area.names.length) {
                                angular.forEach(area.names, function (areaName) {
                                    lowerCase = areaName.name.toLowerCase();
                                    if (usedNames[lowerCase]) {
                                        return;
                                    }

                                    usedNames[lowerCase] = true;
                                    if (!deliveryAreasByName[lowerCase]) {
                                        deliveryAreasByName[lowerCase] = {
                                            name: areaName.name,
                                            areas: []
                                        };
                                    }
                                    deliveryAreasByName[lowerCase].areas.push(toPush);
                                });
                            } else {
                                lowerCase = area.name.toLowerCase();
                                if (!deliveryAreasByName[lowerCase]) {
                                    deliveryAreasByName[lowerCase] = {
                                        name: area.name,
                                        areas: []
                                    };
                                }
                                deliveryAreasByName[lowerCase].areas.push(toPush);
                            }

                            if (area.deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.PICKUP) {
                                branchesById[branch.id].pickupAreasCount++;
                                pickupAreas.push(toPush);
                            }
                        });
                    });
                });
                areasResolver.setData(areas);
                pickupAreasResolver.setData(pickupAreas);
                _areasByNameResolver.setData(deliveryAreasByName);
                _deliveryAreasByIdResolver.setData(deliveryAreasById);
            });
        }

        function _isAvailableArea(area) {
            return area.deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.DELIVERY ||
                area.deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.EXPRESS_DELIVERY ||
                area.deliveryTypeId === SP_SERVICES.DELIVERY_TYPES.PICKUP;
        }

        function _setAreasPreferredRetailerIdSort() {
            // when there aren't areas yet do not try to set the is preferred, it will be set with the areas
            if (!areasResolver.hasData) {
                return;
            }

            return $q.all({
                preferredId: preferredRetailerIdResolver.hasData ? preferredRetailerIdResolver.getData() : undefined,
                areas: areasResolver.getData()
            }).then(function(results) {
                angular.forEach(results.areas, function(area) {
                    _setAreaPreferredRetailerIdSort(area, results.preferredId);
                });
            });
        }

        function _setAreaPreferredRetailerIdSort(area, preferredRetailerId) {
            area.preferredRetailerSort = area.retailer.id === preferredRetailerId ? 1 : 0;
        }

        /**
         * Returns the delivery time types by area
         * @public
         *
         * @param {Array<Object>} areas
         * @param {boolean} [ignoreRetailerConfig]
         *
         * @return {Promise<Object>}
         */
        function getAreasDeliveryTimeTypes(areas, ignoreRetailerConfig) {
            var byRetailer = {},
                res = {};
            angular.forEach(areas, function(area) {
                if (_areasDeliveryTimeTypesCache[area.id]) {
                    res[area.id] = _areasDeliveryTimeTypesCache[area.id];
                    return;
                }

                byRetailer[area.retailer.id] = byRetailer[area.retailer.id] || [];
                byRetailer[area.retailer.id].push(area.id);
            });

            return $q.resolve().then(function() {
                if (!Object.keys(byRetailer).length) {
                    return [];
                }

                return multiRetailersResolver.getData();
            }).then(function(retailers) {
                if (!retailers.length) {
                    return [];
                }

                var promises = [];
                angular.forEach(retailers, function(retailer) {
                    if (!byRetailer[retailer.id] || !ignoreRetailerConfig && !retailer.isDeliveryWithinDaysByTag) {
                        return;
                    }

                    promises.push(Api.request({
                        method: 'GET',
                        url: '/v2/retailers/' + retailer.id + '/areas/delivery-time-types',
                        params: { areaId: byRetailer[retailer.id] }
                    }));
                });

                return $q.all(promises)
            }).then(function(retailersResults) {
                angular.forEach(retailersResults, function(retailersResult) {
                    angular.forEach(retailersResult.areas, function(areaResult) {
                        res[areaResult.id] = areaResult.deliveryTimeTypes;
                        _areasDeliveryTimeTypesCache[areaResult.id] = areaResult.deliveryTimeTypes;
                    });
                });
                return res;
            });
        }

        /**
         * Returns whether the given area id has delivery within days slots only
         * @public
         *
         * @param {number} areaId
         * @param {number} retailerId
         * @param {boolean} [ignoreRetailerConfig]
         *
         * @returns {Promise<boolean>}
         */
        function isAreaDeliveryWithinDaysOnly(areaId, retailerId, ignoreRetailerConfig) {
            return getAreasDeliveryTimeTypes([{
                id: areaId,
                retailer: {
                    id: retailerId
                }
            }], ignoreRetailerConfig).then(function(areasMap) {
                return !!(areasMap[areaId] && areasMap[areaId].length === 1 &&
                    areasMap[areaId][0] === SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_DAYS);
            });
        }
    }

    function DataResolver($q) {
        var self = this,
            dataDefer,
            _changeListeners = [];

        self.setData = setData;
        self.getData = getData;
        self.onChange = onChange;
        self.unOnChange = unOnChange;
        self.hasData = false;

        /**
         * Saves the data on the data resolver
         * @public
         *
         * @param {*} data
         *
         * @returns {Promise}
         */
        function setData(data) {
            return $q.resolve().then(function() {
                if (!self.hasData) {
                    self.hasData = data !== undefined;
                    return;
                }

                return _getDataDefer().promise;
            }).then(function(savedData) {
                // do not resolve the new value when it is the value that is already saved
                if (savedData && savedData === data) {
                    return;
                }
                // when the defer was already resolved with previous data - reset it
                if (savedData) {
                    _resetDataPromise();
                }
                // when the data is undefined the promise should stay unresolved
                if (data !== undefined) {
                    _getDataDefer().resolve(data);
                }
                _invokeOnChange(data, savedData);
            });
        }

        /**
         * Returns a promise which will be resolved when the data will be set
         * @public
         *
         * @returns {Promise<number>}
         */
        function getData() {
            return _getDataDefer().promise;
        }

        /**
         * Listen to change of value
         * @public
         *
         * @param {function} callback
         */
        function onChange(callback) {
            _changeListeners.push(callback);
        }

        /**
         * Cancel listen of a callback to the change of value
         * @public
         *
         * @param {function} callback
         */
        function unOnChange(callback) {
            for (var i = _changeListeners.length - 1; i >= 0; i--) {
                if (callback === _changeListeners[i]) {
                    _changeListeners.splice(i, 1);
                    return;
                }
            }

        }

        /**
         * Returns a deferred promise for the data
         * @private
         *
         * @returns {Deferred}
         */
        function _getDataDefer() {
            if (!dataDefer) {
                dataDefer = $q.defer();
            }

            return dataDefer;
        }

        /**
         * Resets the data promise when it was fulfilled
         * @private
         */
        function _resetDataPromise() {
            if (self.hasData) {
                self.hasData = false;
                dataDefer = undefined;
            }
        }

        /**
         * Invoke the on change listeners
         * @private
         *
         * @param {*} data
         * @param {*} prevData
         */
        function _invokeOnChange(data, prevData) {
            angular.forEach(_changeListeners, function(callback) {
                callback(data, prevData);
            });
        }
    }

    /**
     * @typedef {Object} SpDeliveryArea
     *
     * @property {number} id
     * @property {string} name
     * @property {object} branch
     * @property {number} branch.id
     * @property {string} branch.city
     * @property {object} retailer
     * @property {number} retailer.id
     * @property {string} [retailer.title]
     */

    /**
     * Register service
     */
    angular.module('spServices').service(serviceName, ['$q', '$injector', '$filter', 'Api', 'SP_SERVICES', SpDeliveryAreasService]);
})(angular);
