(function(angular) {
	/**
	 * Service name
	 */
	var serviceName = 'SpDeliveryTimesService';

	/**
	 * @typedef {Object} SpDeliveryTimesServiceLocals
	 *
	 * @property {number} timeZoneOffset
	 * @property {number} languageId
	 */

	/**
	 * Provider instance
	 *
	 * @property {Array|function|Function} [getLocals] - ng inject. should return SpDeliveryTimesServiceLocals
	 */
	var provider = {};

	/**
	 * Branch delivery types
	 * @type {Object}
	 */
	var BRANCH_DELIVERY_TYPES = {
		DELIVERY: 1,
		PICKUP: 2,
		PICK_AND_GO: 3,
		SCAN_AND_GO: 4,
		EXPRESS_DELIVERY: 5
	};

	/**
	 * Service
	 */
	function SpDeliveryTimesService($controller, $q, $timeout, Api, SpCartService, SpDeliveryAreasService, SP_SERVICES, PRODUCT_TAG_TYPES) {
		var self = this;

		self.init = init;
		self.getTimes = getTimes;
		self.getNextDeliveryTimes = getNextDeliveryTimes;
		self.setNextDeliveryCurrentSlots = setNextDeliveryCurrentSlots;
		self.timesObject = timesObject;
		self.now = now;
		self.getExpressTimeProductTagLimits = getExpressTimeProductTagLimits;
		self.isDeliveryWithinDaysOnly = isDeliveryWithinDaysOnly;
		var areaData = {};
		var areaDataLoading = {};
		var areaDataResolves = {};
		/**
		 * Gets the locals
		 * @private
		 *
		 * @returns {Object}
		 */
		function _getLocals() {
			if (!provider.getLocals || (!angular.isFunction(provider.getLocals) && !angular.isArray(provider.getLocals))) {
				throw new Error(serviceName + 'Provider.getLocals must be implemented');
			}

			return $controller(provider.getLocals);
		}

		/**
		 * Init
		 * @public
		 *
		 * @param {Object} [apiOptions]
		 *
		 * @return {Promise.<Object>}
		 */
		function init(apiOptions) {
			return Api.request({
				url: '/v2/retailers/:rid/branches/:bid/areas/types'
			}, apiOptions).then(function(types) {
				self.types = types;
				return self.types;
			});
		}

		/**
		 * Get times
		 * @public
		 *
		 * @param {Number} type
		 * @param {Object} options
		 * @param {String} [options.address]
		 * @param {Number} [options.cartId]
		 * @param {Number} [options.areaId]
		 * @param {Number} [options.languageId]
		 * @param {Date} [options.startDate]
		 * @param {Number} [options.lat]
		 * @param {Number} [options.lng]
		 * @param {string} [options.externalPlaceId]
		 * @param {boolean} [options.isDeliveryWithinDaysByTag]
		 * @param {Date} [options.maxDisplayDays]
		 *
		 * @returns {Promise}
		 */
		function getTimes(type, options) {
			var params = {
				deliveryTypeId: (type === BRANCH_DELIVERY_TYPES.DELIVERY ? [BRANCH_DELIVERY_TYPES.DELIVERY, BRANCH_DELIVERY_TYPES.EXPRESS_DELIVERY] : type),
				address: (type === BRANCH_DELIVERY_TYPES.DELIVERY ? options.address : null),
				cartId: options.cartId,
				areaId: options.areaId,
				languageId: options.languageId || 2,
				startDate: options.startDate,
				lat: options.lat,
				lng: options.lng,
				externalPlaceId: options.externalPlaceId,
				isGoogleMapDropPinNotDetect: options.isGoogleMapDropPinNotDetect,
				maxDisplayDays: options.maxDisplayDays
			};

			if (options.isDeliveryWithinDaysByTag && type !== BRANCH_DELIVERY_TYPES.PICKUP) {
				if (_isOnlyDeliveryWithinTimes()) {
					params.deliveryTimeTypeId = SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_DAYS;
				} else {
					params.notDeliveryTimeTypeId = SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_DAYS;
				}
			}
			var paramsCacheKey = JSON.stringify(params);

			if (areaData[paramsCacheKey]) {
				return $q.resolve(areaData[paramsCacheKey]);
			}

			// if the same call is call. puh it to queue, and first call is finish it will return result of this call.
			if(areaDataLoading[paramsCacheKey]){
				return $q(function(resolve, reject){
					if(!areaDataResolves[paramsCacheKey]){
						areaDataResolves[paramsCacheKey] = []
					}
					areaDataResolves[paramsCacheKey].push({resolve: resolve, reject: reject})
				});

			}

			areaDataLoading[paramsCacheKey] = true;


			return Api.request({
				method: 'GET',
				url: '/v2/retailers/:rid/branches/:bid/areas',
				params: params
			}, {
				fireAndForgot: true
			}).then(function(resp) {
				var result = { times: resp.timesObject.times, area: resp };
				areaData[paramsCacheKey] = result;
				areaDataLoading[paramsCacheKey] = false;

				if(areaDataResolves[paramsCacheKey] && areaDataResolves[paramsCacheKey].length > 0){

					angular.forEach(areaDataResolves[paramsCacheKey], function(r){
						r.resolve(areaData[paramsCacheKey]);
					});
					delete areaDataResolves[paramsCacheKey];
				}

				// auto clear cache after 30s
				$timeout(function(){
					delete areaData[paramsCacheKey];
				}, 1000 * 5);// 5s

				return result;
			}).finally(function(){
				areaDataLoading[paramsCacheKey] = false;
			});
		}

		/**
		 * Get next deliveries time slots
		 * @public
		 *
		 * @param {Object} nextDelivery
		 * @param {function(): Promise<Object>} getUserDataCb
		 * @param {Object} [area]
		 * @param {number} [area.id]
		 * @param {number} [area.deliveryTypeId]
		 *
		 * @returns {Promise<{nextTime: Object, area: Object, noAvailableDeliverySlots: boolean}|void>}
		 */
		function getNextDeliveryTimes(nextDelivery, getUserDataCb, area, cartId) {
			var deliveryPromises = {};

			// deliveryPromises[BRANCH_DELIVERY_TYPES.DELIVERY] = $q.resolve().then(function() {
			// 	if (area && area.deliveryTypeId === BRANCH_DELIVERY_TYPES.DELIVERY) {
			// 		return { areaId: area.id }
			// 	}
			//
			// 	return getUserDataCb();
			// }).then(function(options) {
			// 	if (area.deliveryTypeId && area.deliveryTypeId === BRANCH_DELIVERY_TYPES.DELIVERY) {
			// 		options.cartId = cartId;
			// 	}
			//
			// 	return self.getTimes(BRANCH_DELIVERY_TYPES.DELIVERY, options);
			// }).catch(function() {
			// 	return null;
			// });

			var options = {};

			if (area && area.deliveryTypeId === BRANCH_DELIVERY_TYPES.DELIVERY) {
				options.areaId = area.id;
				options.cartId = cartId;
			}

			deliveryPromises[BRANCH_DELIVERY_TYPES.DELIVERY] = self
				.getTimes(BRANCH_DELIVERY_TYPES.DELIVERY, options)
				.catch(function() { return null });

			options = {};

			if (area && area.deliveryTypeId === BRANCH_DELIVERY_TYPES.PICKUP) {
				options.areaId = area.id;
				options.cartId = cartId;
			}

			deliveryPromises[BRANCH_DELIVERY_TYPES.PICKUP] = self
				.getTimes(BRANCH_DELIVERY_TYPES.PICKUP, options)
				.catch(function() { return null });

			nextDelivery.times = nextDelivery.times || [];
			if (nextDelivery.times.length > 1) {
				nextDelivery.times.splice(1);
			}

			return $q.all(deliveryPromises).then(function(responses) {
				var preparedResponse = _setTimes(responses, nextDelivery, area);

				if (!preparedResponse.nextTime) {
					nextDelivery.times.splice(0);
					return preparedResponse;
				}

				if (nextDelivery.times.length && nextDelivery.times[0].id === preparedResponse.nextTime.id) {
					return preparedResponse;
				}

				nextDelivery.times.unshift(preparedResponse.nextTime);
				preparedResponse.nextTime._isActiveInNav = false;
				$timeout(function() {
					preparedResponse.nextTime._isActiveInNav = true;
				}, 0);

				return preparedResponse;
			}).catch(function() {
				nextDelivery.times.splice(0);
			})
		}

		/**
		 * Set time slots for deliveries
		 * @private
		 *
		 * @param {Object} respByDeliveryType
		 * @param {Object} nextDelivery
		 * @param {Object} [area]
		 * @param {number} [area.id]
		 * @param {number} [area.deliveryTypeId]
		 *
		 * @returns {{nextTime: Object, area: Object, noAvailableDeliverySlots: boolean}}
		 */
		function _setTimes(respByDeliveryType, nextDelivery, area) {
			var nextTime,
				noAvailableDeliverySlots = false,
				respArea,
				dateNow = now();

			nextDelivery.moreSlots = {};
			nextDelivery.isAllSpecialSlots = true;
			angular.forEach(respByDeliveryType, function(resp, deliveryType) {
				if (!resp) {
					return;
				}

				if (!resp.area.areas &&
					(resp.area.deliveryTypeId === BRANCH_DELIVERY_TYPES.DELIVERY ||
					resp.area.deliveryTypeId === BRANCH_DELIVERY_TYPES.EXPRESS_DELIVERY) &&
					resp.area.haveAvailableDeliverySlots === false) {
					noAvailableDeliverySlots = true;
				}

				var areas = [resp.area];
				if (resp.area.areas && resp.area.areas.length > 1) {
					areas = resp.area.areas;
				}
				angular.forEach(areas, function(_area) {
					if (!respArea || area && _area.id === area.id) {
						respArea = _area;
					}

					var areaExistingSlots = _area.timesObject.times.localDelivery,
						areaSlots = {};

					var isChosenDeliveryType = !area || _area.deliveryTypeId === area.deliveryTypeId,
						prevNextTime = nextTime;
					if (areaExistingSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_HOURS]) {
						areaSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_HOURS] =
							areaExistingSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_HOURS];

						// try to set the next time as the delivery withing hours
						if (!nextTime && isChosenDeliveryType) {
							nextTime = areaExistingSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_HOURS];
						}
					}

					angular.forEach(areaExistingSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.REGULAR], function(slot) {
						if (!slot.times.length) {
							return;
						}

						slot.deliveryTypeId = deliveryType;
						areaSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.REGULAR] = areaSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.REGULAR] || [];
						areaSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.REGULAR].push(slot);

						angular.forEach(slot.times, function(time) {
							if (nextDelivery.isAllSpecialSlots) {
								nextDelivery.isAllSpecialSlots = !!(time.deliveryProduct && time.deliveryProduct.branch.specials && time.deliveryProduct.branch.specials.length && !time.deliveryProduct.branch.specials[0].isCoupon);
							}

							if (!isChosenDeliveryType || nextTime && time.newFrom >= nextTime.newFrom) {
								return;
							}

							nextTime = time;
							nextTime.isToday = slot.isToday;
							nextTime.date = slot.date;
							nextTime.isWithinSixDays =  new Date(nextTime.date).getTime() <= dateNow.setUTCDate(dateNow.getUTCDate() + 6);


						});
					});

					if (areaExistingSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_DAYS]) {
						areaSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_DAYS] =
							areaExistingSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_DAYS];

						if (isChosenDeliveryType && !nextTime) {
							nextTime = areaExistingSlots[SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_DAYS];
						}
					}

					// when the nextTime was set from the current area, use its delivery type
					if (nextTime && nextTime !== prevNextTime) {
						nextDelivery.currentNextDeliveryTypeId = _area.deliveryTypeId;
					}

					nextDelivery.moreSlots[deliveryType] = nextDelivery.moreSlots[deliveryType] || {};
					nextDelivery.moreSlots[deliveryType][_area.id] = { areaName: _area.name, slots: areaSlots };
				});
			});

			nextDelivery.moreSlotsTypes = Object.keys(nextDelivery.moreSlots);

			return {
				nextTime: nextTime,
				noAvailableDeliverySlots: noAvailableDeliverySlots,
				area: respArea
			};
		}

		/**
		 * Set the currentMoreSlots object on the given nextDelivery object
		 * @public
		 *
		 * @param {Object} nextDelivery
		 * @param {Object} areasSlots
		 * @param {string|number} deliveryTypeId
		 */
		function setNextDeliveryCurrentSlots(nextDelivery, areasSlots, deliveryTypeId) {
			nextDelivery.currentMoreSlots = { deliveryTypeId: Number(deliveryTypeId) };

			if(areasSlots){
				nextDelivery.currentMoreSlots.areas = Object.keys(areasSlots).reduce(function(acc, areaId) {
					if (areasSlots[areaId] && areasSlots[areaId].slots && Object.keys(areasSlots[areaId].slots).length) {
						acc[areaId] = areasSlots[areaId];
					}
					return acc;
				}, {});

				nextDelivery.currentMoreSlots.length = Object.keys(nextDelivery.currentMoreSlots.areas).length;
			}

		}

		/**
		 * Create times object
		 * @public
		 *
		 * @param {Object} [timesByDayInWeek={}]
		 * @param {Object} [timesObject={}]
		 * @param {number} [numberOfDays=7]
		 *
		 * @returns {Object}
		 */
		function timesObject(timesByDayInWeek, timesObject, numberOfDays) {
			timesByDayInWeek = timesByDayInWeek || {};
			timesObject = timesObject || {};
			timesObject[SP_SERVICES.DELIVERY_TIMES_TYPES.REGULAR] = [];

			var now = self.now(),
				end = new Date(now);
			now.setUTCHours(0, 0, 0, 0);
			end.setUTCHours(0, 0, 0, 0);
			end.setUTCDate(end.getUTCDate() + (numberOfDays || 7));

			for (var d = new Date(now); d < end; d.setUTCDate(d.getUTCDate() + 1)) {
				timesObject[SP_SERVICES.DELIVERY_TIMES_TYPES.REGULAR].push({
					date: new Date(d),
					isToday: d.getTime() === now.getTime(),
					day: d.getUTCDay(),
					times: timesByDayInWeek[d.getUTCDay()] || []
				});
			}

			return timesObject;
		}

		/**
		 * Get retailer now as utc
		 * @private
		 */
		function now() {
			var d = new Date();
			d.setUTCMinutes(d.getUTCMinutes() - _getLocals().timeZoneOffset);
			return d;
		}

		/**
		 * Filter the given delivery times object by the delivery within product tag type
		 * @public
		 *
		 * @param {Object} options
		 * @param {Object} options.deliveryTimesObj
		 * @param {number} options.areaId
		 * @param {number} options.retailerId
		 * @param {boolean} options.isDeliveryWithinDaysByTag
		 *
		 * @return {Promise<boolean>}
		 */
		function isDeliveryWithinDaysOnly(options) {
			if (!options.deliveryTimesObj[SP_SERVICES.DELIVERY_TIMES_TYPES.DELIVERY_WITHIN_DAYS]) {
				return $q.resolve(false);
			}

			if (options.isDeliveryWithinDaysByTag && _isOnlyDeliveryWithinTimes()) {
				return $q.resolve(true);
			}

			return SpDeliveryAreasService.isAreaDeliveryWithinDaysOnly(options.areaId, options.retailerId, true);
		}

		function _isOnlyDeliveryWithinTimes() {
			return !!(SpCartService.total.crossProductTagTypes && SpCartService.total.crossProductTagTypes[PRODUCT_TAG_TYPES.DELIVERY_WITHIN_DAYS]);
		}

		/**
		 * Get the limits of an express delivery time by the express delivery product tag type
		 * @public
		 *
		 * @param {boolean} isExpressDeliveryByTag
		 * @param {number|null} expressDeliveryProductsLimit
		 *
		 * @return {{ isEnabled: boolean, isUnderQuantityLimit: boolean }}
		 */
		function getExpressTimeProductTagLimits(isExpressDeliveryByTag, expressDeliveryProductsLimit) {
			var isUnderQuantityLimit = true,
				isExpressProductsOnly = true;

			if (expressDeliveryProductsLimit !== undefined && expressDeliveryProductsLimit !== null) {
				var productLinesCount = 0;
				angular.forEach(SpCartService.lines, function(cartLine) {
					if (cartLine.type === SP_SERVICES.CART_LINE_TYPES.PRODUCT && cartLine.product) {
						productLinesCount += cartLine.quantity || 0;
					}
				});

				// return whether the number of cart lines did not exceed the express delivery products limit
				isUnderQuantityLimit = expressDeliveryProductsLimit >= productLinesCount;
			}

			if (isExpressDeliveryByTag && SpCartService.total.crossProductTagTypes) {
				isExpressProductsOnly = !!SpCartService.total.crossProductTagTypes[PRODUCT_TAG_TYPES.EXPRESS_DELIVERY];
			}

			return {
				isUnderQuantityLimit: isUnderQuantityLimit,
				isEnabled: isExpressProductsOnly
			};
		}
	}

	/**
	 * Register service
	 */
	angular.module('spServices').config(['$provide', function($provide) {
		provider = $provide.service(serviceName, [
			'$controller', '$q', '$timeout', 'Api', 'SpCartService', 'SpDeliveryAreasService', 'SP_SERVICES', 'PRODUCT_TAG_TYPES',
			SpDeliveryTimesService
		]);
	}]);
})(angular);

/**
 * @typedef {Object} Time
 * @property {Number} id
 * @property {Number} type
 * @property {Number} dayInWeek
 * @property {Date} from
 * @property {Date} to
 * @property {Date} timeRange
 * @property {Boolean} disabled
 * @property {Number|Null} ordersLimit
 *
 * @typedef {Object} Times
 * @property {Date} date
 * @property {Object<Number, Array.<Time>>} times
 *
 * @typedef {Object} Area
 * @property {Number} id
 * @property {Object} branch
 * @property {Object.<Number, Times>} times
 *
 *
 */
