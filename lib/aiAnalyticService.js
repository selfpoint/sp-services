(function (angular) {

    /**
     * Provider instance
     * @property {String, String} [getDataForAnalyticServiceProvider] - ng inject. should return { storeId: {String}, userId: {String} }
     */
    var provider = {};

    function spAIAnalyticService($filter, $controller, $rootScope, Api) {
        var self = this,
            _nameFilter = $filter('name');
        var serviceData = {};

        var locals;
        var _trafficParams;

        var script = document.createElement("script");

        self.init = init;

        function init() {

            locals = _getLocals();
            getAnalyticServiceProviderData().then(function(response){
                serviceData = response;
                if (serviceData){
                    if (serviceData.selectedProviderId == 2){
                        initConnectionToProvider();
                        initEventListeners();
                        _trafficParams = _getTrafficParams();
                    }
                }
            } )
        }

        function initConnectionToProvider(){
            var companyId = serviceData.fields.storeId.value;
            script.type = "text/javascript";
            script.async = true;
            script.src = "https://sf.exposebox.com/widget/predictl.min.js?c=" + companyId; //http(s): protocol depends on your website
            document.getElementsByTagName("head")[0].appendChild(script);
            window.predictlApi = window.predictlApi || function (cb) { var pa = window.predictlApi; pa.or = pa.or || []; pa.or.push(cb); }
        }

        function initEventListeners(){
            $rootScope.$on('loginForAnalyticServiceProvider', _setUserId);
            $rootScope.$on('firstCheckout', _onFirstCheckout);
            $rootScope.$on('finalCheckout', _onFinalCheckout);
            $rootScope.$on('productPage', _onProductPage);
            $rootScope.$on('categoryPage', _onCategoryPage);
            $rootScope.$on('productSearch', _onProductSearch);
            $rootScope.$on('orderCanceled', _onOrderCanceled);
            $rootScope.$on('cartCleared', _onCartCleared);
            $rootScope.$on('contactUs', _onContactUs);
            $rootScope.$on('loyaltyClubRegistration', _onLoyaltyClubRegistration);
            $rootScope.$on('customerRegistration', _onCustomerRegistration);
            $rootScope.$on('cart.lines.added', _onCartLinesAdded);
            $rootScope.$on('cart.lines.removed', _onCartLinesRemoved);
            $rootScope.$on('cart.lines.added.to.mylist', _onLinesAddedToMyList);
        }

        /**
         * Gets the analytic service provider data
         * @private
         *
         * @returns {Object}
         */
        function getAnalyticServiceProviderData(){
            var apiOptions = angular.copy(locals.apiOptions || {});
            apiOptions.id = 'spAnalyticServiceProvider';
            apiOptions.loadingElement = (locals.apiOptions || {}).loadingElement;
            return Api.request({
                url: '/v2/retailers/:rid/analytic-services/getSelectedAnalyticServiceProviderData'
            }, apiOptions).then(function(response) {
                return response;
            }).catch(function(e){
                console.log(e);
            });

        }


        /**
         * On browse product page
         * @private
         * @param {Event} event
         * @param {String} productId
         */
        function _onProductPage(event, productId) {
            window.predictlApi(function () {
                window.predictlApi.setProducts([productId]);
            });
        }

        /**
         * On browse category
         * @private
         * @param {Event} event
         * @param {Array} categories
         */
        function _onCategoryPage(event, categories){
            var categoryNames = [];
            angular.forEach(categories, function(category){
                categoryNames.push(category.names[locals.languageId])
            })
            window.predictlApi(function () {
                window.predictlApi.setCategories(categoryNames);
            });
        }

        function _stopListeners(stopListeners) {
            angular.forEach(stopListeners, function (stopListener) {
                stopListener();
            });
        }

        /**
         * On adding products to MyList
         * @private
         * @param {Event} event
         * @param {Array} products
         */
        function _onLinesAddedToMyList(event, products){
            if(products){
                angular.forEach(products, function(product) {
                    window.predictlApi(function () {
                        window.predictlApi.events.addToWishlist(product);
                     });
                });
            }
        }

        /**
         * On login
         * @private
         */
        function _setUserId() {
            var locals = _getLocals();
            if (locals && locals.userId) {
                window.predictlApi(function () {
                    window.predictlApi.setCustomerData({
                        customerId: locals.userId.toString(),
                        email: locals.username
                    })
                });
            }
        }


        function _getLocals() {
            if (!provider.getLocals || (!angular.isFunction(provider.getLocals) && !angular.isArray(provider.getLocals))) {
                throw new Error('spAnalyticsProvider.getLocals must be implemented');
            }

            return $controller(provider.getLocals);
        }

        /**
         * On cart lines added
         * @private
         *
         * @param {Event} event
         * @param {Object} data
         */
        function _onCartLinesAdded(event, data) {
            window.predictlApi(function () {
                if(data){
                    var unitPrice =  data.unitPrice.toString().split('.');

                    if(unitPrice.length > 1 && unitPrice[1].length > 5) {
                        data.unitPrice = data.unitPrice.toFixed(5);
                    }
                    window.predictlApi.events.sendEvent("addToCart",{productId: data.productId,
                        quantity: data.quantity,unitPrice: data.unitPrice, sourceType: data.sourceType, sourceValue: data.sourceValue, cartId: data.cartId});
                }
            });
        }

        /**
         * On cart lines removed
         * @private
         *
         * @param {Event} event
         * @param {Object} data
         */
        function _onCartLinesRemoved(event, data) {
            window.predictlApi(function () {
                if(data){
                    window.predictlApi.events.sendEvent("removeFromCart",{productId: data.productId,quantity: data.quantity,cartId: data.cartId});
                }
            });
        }

        /**
         * On first checkout
         * @private
         */
        function _onFirstCheckout() {
            window.predictlApi(function () {
                window.predictlApi.events.sendEvent('checkout',{type:'cartPage'});
            });
            _setUserId();
        }

        /**
         * On final checkout
         * @private
         */
        function _onFinalCheckout(event, orderId, totalPrice, products, sourceType, sourceValue) {
            var totalPriceSplitted =  totalPrice.toString().split('.');

            if(totalPriceSplitted && totalPriceSplitted[1] && totalPriceSplitted[1].length > 5) {
                totalPrice = totalPrice.toFixed(5);
            }
            window.predictlApi(function () {
                predictlApi.events.sendEvent("conversion", {
                    cartProducts: products,
                    orderId: orderId,
                    totalPrice: totalPrice,
                    sourceType: sourceType,
                    sourceValue: sourceValue
                });
            });
        }

        /**
         * On order canceled
         * @private
         * @param {Event} event
         * @param {String} orderId
         */
        function _onOrderCanceled(event, orderId) {
            window.predictlApi(function () {
                predictlApi.events.conversion({}, orderId, 0);
            });
        }

        /**
         * On order canceled
         * @private
         * @param {Event} event
         * @param {String} cartId
         */
        function _onCartCleared(event, cartId) {
            var locals = _getLocals();
            if(locals && locals.userId && cartId) {
                window.predictlApi(function () {
                        window.predictlApi.events.sendEvent('clearCart', {customerId: locals.userId, cartId: cartId});
                    }
                )
            }
        }

        /**
         * On applying contact us
         * @private
         */
        function _onContactUs(event, userEmail, userName) {
            window.predictlApi(function () {
                    window.predictlApi.events.sendEvent('contactCS',{flag:"true", userEmail: userEmail, userName: userName });
                }
            )
        }

        /**
         * On Loyalty club registration
         * @private
         * @param {Event} event
         * @param {String} customerId
         */
        function _onLoyaltyClubRegistration(event, customerId) {
            window.predictlApi(function () {
                    window.predictlApi.events.sendEvent('clubMembershipReg', {customerId: customerId});
                }
            )
        }

        /**
         * On customer registration
         * @private
         * @param {Event} event
         * @param {Object} newCustomer
         */
        function _onCustomerRegistration(event, newCustomer) {
            var sourceType = "internal";
            var sourceValue = "Registration";
            if (_trafficParams && _trafficParams.utm_source){
                sourceType = _trafficParams.utm_source;
                sourceValue = _trafficParams.utm_campaign_id;
            }
            window.predictlApi(function () {
                window.predictlApi.events.sendEvent('newUserRegistration',
                    {
                        using: "onsite",
                        customerId: newCustomer.id,
                        email: newCustomer.email,
                        sourceType: sourceType,
                        sourceValue: sourceValue
                    });
                window.predictlApi.setCustomerData({
                    customerId: newCustomer.id.toString(),
                    email: newCustomer.email
                })
            });
        }

        /**
         * On product search
         * @private
         * @param {Event} event
         * @param {String} query
         * @param {Array} products
         * @param {Object} filters
         */
        function _onProductSearch(event, query, products, filters) {
            window.predictlApi(function () {
                window.predictlApi.events.sendEvent("searchAndFilter",{
                    query: query, //The search query
                    products: products, //Top 6 product results
                    categories: filters.categoryFilters,
                    productTags: filters.productTags,
                    brandFilters: filters.brandFilters
                });
            });
        }

        function _getTrafficParams() {
            var trafficParams = {};
            if (document.referrer && document.referrer !== document.location.href) {
                if (document.referrer.indexOf(document.location.hostname) > -1) {
                    return;
                }
                trafficParams.dr = document.referrer;
            }
            if (document.location.search) {
                var searchParams = document.location.search.substring(1).split('&');
                angular.forEach(searchParams, function (param) {
                    var parts = param.split('=');
                    trafficParams[parts[0]] = parts[1];
                });
            }
            return Object.keys(trafficParams).length ? trafficParams : undefined;
        }

    }

    angular.module('spServices')
        .config(['$provide', function ($provide) {
            provider = $provide.service('spAIAnalyticService', [
                '$filter', '$controller', '$rootScope', 'Api',
                spAIAnalyticService
            ]);
        }]);
})(angular);
