(function(angular) {
    'use strict';

    angular.module('spServices', [
        'spApi',
        'spProductTags'
    ]);
})(angular);